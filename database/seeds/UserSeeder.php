<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User;
        $user->name = "Perdana";
        $user->email = "admin@perdana.id";
        $user->password = Hash::make("asdqwe123");
        $user->api_token = str_random(100);
        $user->save();
    }
}
