<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\User;
use App\Role;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;

Route::get('/login', function () {
    return view('login');
});

Route::get('/', function () {
	$roles = explode(",", Auth::user()->roles);
	$role = Role::find($roles[0]);
	return redirect($role->url);
})->middleware('auth');

Route::get('/home', function () { return view('admission/registration'); })->middleware('auth');

Route::prefix('profile')->group(function () {
	Route::get('/', 'ProfileController@index')->middleware('auth')->name('profile');
	Route::get('edit', 'ProfileController@edit')->middleware('auth')->name('profile_edit');
	Route::post('update', 'ProfileController@update')->middleware('auth')->name('profile_update');
});

Route::prefix('admission')->group(function () {	
	Route::get('/', function () { return redirect('admission/registration'); });
	
    Route::resource('registration', 'RegistrationController', ['middleware' => 'auth']);

	Route::post('doctor_show_all', 'AdmissionController@doctorshowall');
	Route::post('doctor_list', 'AdmissionController@doctorlist');

    Route::post('reservation_ajax', 'AdmissionController@reservationajax')->middleware('auth');
    
	Route::resource('master_patient', 'PatientController', ['middleware' => 'auth']);
	Route::post('master_patient/ajaxdt', 'PatientController@ajaxdt')->middleware('auth')->name('master_patient_ajaxdt');
	Route::post('master_patient/search_by_name_dt', 'PatientController@searchbynamedt')->middleware('auth')->name('search_by_name_dt');
	
	Route::get('daily_patient', function () { return view('admission/daily-patient'); })->middleware('auth')->name('daily_patient');
	
	Route::get('doctor_schedule', 'DoctorRequestSessionController@index')->middleware('auth')->name('doctor_schedule');
	Route::post('doctor_schedule/request_session_leave', 'DoctorRequestSessionController@storeRequestSessionLeave')->middleware('auth')->name('request_session_leave');

	Route::get('doctor_late', 'DoctorLateController@index')->middleware('auth')->name('doctor_late');
	Route::post('doctor_late/request_late', 'DoctorLateController@storeRequestLate')->middleware('auth')->name('request_late');

	Route::get('doctor_leave', 'DoctorLeaveController@index')->middleware('auth')->name('doctor_leave');
	Route::post('doctor_leave/request_leave', 'DoctorLeaveController@storeRequestLeave')->middleware('auth')->name('request_leave');

	Route::get('program', function () { return view('admission/program'); })->middleware('auth')->name('program');
	
	Route::get('information', function () { return view('admission/information/index'); })->middleware('auth')->name('information');
	
	Route::post('doctor_search_by_code', 'AdmissionController@doctorsearchbycode')->middleware('auth')->name('doctor_search_by_code');
});

Route::prefix('self_checkin')->group(function () {
	Route::get('/', function() { return view('admission/self_checkin/booking'); } )->middleware('auth');
	Route::get('antrian_pharmacy', function() { return view('admission/self_checkin/antrian_pharmacy'); } )->middleware('auth');
	Route::get('antrian_laboratory', function() { return view('admission/self_checkin/antrian_laboratory'); } )->middleware('auth');
	Route::get('destination', function() { return view('admission/self_checkin/destination'); } )->middleware('auth');
	Route::get('menu_doktor', function() { return view('admission/self_checkin/menu_doktor'); } )->middleware('auth');
});

Route::prefix('nurse')->group(function () {
	Route::get('/', function () { return view('nurse/nurse_patient'); });
	
	Route::get('patient', function () { return view('nurse/nurse_patient'); })->middleware('auth');
	Route::get('anamnesa_umum', function () { return view('nurse/anamnesa_patient'); })->middleware('auth');
	
    Route::get('detail_patient', function () { return view('nurse/nurse_detail_patient'); })->middleware('auth')->name('nurse_detail_patient');
    
	
});

Route::prefix('doctor')->group(function () {
	Route::get('/', function () { return redirect('doctor/patient'); });
	
	Route::get('patient', function () { return view('doctor/patient'); })->middleware('auth');
	
	Route::get('calendar', function () { return view('doctor/calendar'); })->middleware('auth');
    
	Route::get('calendar', function () { return view('doctor/calendar'); })->middleware('auth');
	
    Route::get('form_request', function () { return view('doctor/form_request'); })->middleware('auth');

});

Route::prefix('cashier')->group(function () {
    Route::get('/', function() { return view('cashier/cashier'); })->middleware('auth')->name('cashier');
    Route::get('/cashier', function () { return view('cashier/cashier');  })->middleware('auth');
    Route::get('/selling_transaction', function () { return view('cashier/selling_transaction');  })->middleware('auth');
    Route::get('/exchange_labour_price', function () { return view('cashier/exchange_labour_price');  })->middleware('auth');
    Route::get('/table', function () { return view('cashier/table');  })->middleware('auth');
    Route::get('/cashier_transaction', function () { return view('cashier/cashier_transaction');  })->middleware('auth');
});

Route::prefix('admin')->group(function () {
	Route::get('/', function () { return redirect('admin/user'); });
    Route::resource('user', 'UserController', ['middleware' => 'auth']);
	Route::post('user/ajaxdt', 'UserController@ajaxdt')->middleware('auth');
	Route::post('user/editajax', 'UserController@editajax')->middleware('auth');
	
	Route::resource('role', 'RoleController', ['middleware' => 'auth']);
	Route::post('role/ajaxdt', 'RoleController@ajaxdt')->middleware('auth');
	Route::post('role/editajax', 'RoleController@editajax')->middleware('auth');
	
	Route::resource('doctor', 'DoctorController', ['middleware' => 'auth']);
	Route::post('doctor/ajaxdt', 'DoctorController@ajaxdt')->middleware('auth');
	Route::post('doctor/editajax', 'DoctorController@editajax')->middleware('auth');
	
	Route::get('place', 'ProvinceController@index', ['middleware' => 'auth'])->name('place');

	Route::resource('menu', 'MenuController', ['middleware' => 'auth']);
	Route::post('menu/ajaxdt','MenuController@ajaxdt')->middleware('auth');
	Route::post('menu/editajax','MenuController@ajaxdt')->middleware('auth');

	Route::resource('visit_code', 'VisitCodeController', ['middleware' => 'auth']);
	Route::post('visit_code/ajaxdt','VisitCodeController@ajaxdt')->middleware('auth');
	Route::post('visit_code/editajax','VisitCodeController@editajax')->middleware('auth');
    Route::post('visit_code/visitcode', 'VisitCodeController@storevisit')->middleware('auth')->name('visitcode');

    Route::resource('job_type', 'JobTypeController', ['middleware' => 'auth']);
    Route::post('job_type/ajaxdt','JobTypeController@ajaxdt')->middleware('auth');
    Route::post('job_type/editajax','JobTypeController@editajax')->middleware('auth');
    Route::post('job_type/jobtype', 'JobTypeController@storejob')->middleware('auth')->name('jobtype');

    Route::resource('id_type', 'IdTypeController', ['middleware' => 'auth']);
    Route::post('id_type/ajaxdt','IdTypeController@ajaxdt')->middleware('auth');
    Route::post('id_type/editajax','IdTypeController@editajax')->middleware('auth');
    Route::post('id_type/idtype', 'IdTypeController@storeid')->middleware('auth')->name('idtype');

    Route::resource('country', 'CountryController', ['middleware' => 'auth']);
    Route::post('country/ajaxdt','CountryController@ajaxdt')->middleware('auth');
    Route::post('country/editajax','CountryController@editajax')->middleware('auth');
    Route::post('country/country', 'CountryController@storeCountry')->middleware('auth')->name('country');

    Route::resource('province', 'ProvinceController', ['middleware' => 'auth']);
	Route::post('province/ajaxdt', 'ProvinceController@ajaxdt')->middleware('auth');
	Route::post('province/editajax', 'ProvinceController@editajax')->middleware('auth');

    Route::resource('city', 'CityController', ['middleware' => 'auth']); //
    Route::post('city/ajaxdt', 'CityController@ajaxdt')->middleware('auth');//untuk datatables
    Route::post('city/editajax', 'CityController@editajax')->middleware('auth'); //untuk
	
	Route::get('setting', function () { return view('admin/setting'); })->middleware('auth')->name('setting');
});

Route::prefix('cron')->group(function () {
	Route::get('city', 'CronController@city', ['middleware' => 'auth'])->name('cron_city');
	Route::get('province', 'CronController@province', ['middleware' => 'auth'])->name('cron_province');
	Route::get('backupdb', 'CronController@backupdb', ['middleware' => 'auth'])->name('backupdb');
});

Auth::routes();
