<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->post('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('auth:api')->post('/admission/reservation_store', 'AdmissionController@reservationstore');
Route::middleware('auth:api')->post('/admission/reservation_show_all', 'AdmissionController@reservationshowall');
Route::middleware('auth:api')->post('/admission/reservation_detail', 'AdmissionController@reservationdetail');
Route::middleware('auth:api')->post('/admission/visit_code_show_all', 'AdmissionController@visitcodeshowall');
Route::middleware('auth:api')->post('/admission/doctor_search_by_code', 'AdmissionController@doctorsearchbycode');
Route::middleware('auth:api')->post('/admission/doctor_show_all', 'AdmissionController@doctorshowall');
Route::middleware('auth:api')->post('/admission/init_patient', 'AdmissionController@initpatient');

Route::middleware('auth:api')->post('/patient', 'PatientController@index');
Route::middleware('auth:api')->post('/patient/detail', 'PatientController@detail');
Route::middleware('auth:api')->post('/patient/store', 'PatientController@store');
Route::middleware('auth:api')->post('/patient/update', 'PatientController@store');
//search by patient name
Route::middleware('auth:api')->post('/patient/search_by_name', 'PatientController@searchbyname');
//search by KTP/ID Card
Route::middleware('auth:api')->post('/patient/search_by_idnum', 'PatientController@searchbyid');
