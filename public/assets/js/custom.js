

$( document ).ready(function() {
    // Write your custom Javascript codes here...
    $(function() {
        Materialize.updateTextFields();
    });

    //Date Today
    var d = new Date();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var output = d.getFullYear() + '-' +
        ((''+month).length<2 ? '0' : '') + month + '-' +
        ((''+day).length<2 ? '0' : '') + day;
    $('.date-now').val(output);
    //End of Date Today

    });


$('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 60, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    format: 'yyyy-mm-dd',
    closeOnSelect: true,
    container: 'body',// Close upon selecting a date,
	yearRange: '1950-2000',
	defaultDate: '1980-01-01'
});
$('.datepicker_res').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    minDate: 0, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    format: 'yyyy-mm-dd',
    closeOnSelect: false,
    // container: 'body'// Close upon selecting a date,
});
$('.timepicker').pickatime({
    default: 'now', // Set default time: 'now', '1:30AM', '16:30'
    fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
    twelvehour: false, // Use AM/PM or 24-hour format
    donetext: 'OK', // text for done-button
    cleartext: 'Clear', // text for clear-button
    canceltext: 'Cancel', // Text for cancel-button
    autoclose: false, // automatic close timepicker
    container: 'body',
    ampmclickable: true, // make AM PM clickable
    aftershow: function(){} //Function for after opening timepicker
});

$("#tabsId").on("click", "li", function(evt) {

    evt.preventDefault();
    var id = $(this).data("tabContent");
    $("#tabsContentId")
        .find(".tab-content[data-tab-content='" + id + "']").show()
        .siblings().hide();
});

function  alertLogout () {
    swal({
        title: "Are you sure?",
        text: "You want to logout ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Logout",
        closeOnConfirm: false
    }, function(){
        event.preventDefault();
        document.getElementById('logout-form').submit();
        swal("Loged Out","", "success");
    });

}
