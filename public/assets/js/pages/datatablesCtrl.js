//For Server Side Processing



$(document).ready(function() {
   var dtables = $('.example').DataTable({
        language: {
            searchPlaceholder: 'Search records',
            sSearch: '',
            sLengthMenu: 'Show _MENU_',
            sLength: 'dataTables_length',
            oPaginate: {
                sFirst: '<i class="material-icons">chevron_left</i>',
                sPrevious: '<i class="material-icons">chevron_left</i>',
                sNext: '<i class="material-icons">chevron_right</i>',
                sLast: '<i class="material-icons">chevron_right</i>'
            }
        }
    });
   //For Custom Filter
    // $('#txt_name').on( 'keyup', function () {
    //     dtables
    //         .columns(4)
    //         .search(this.value)
    //         .draw();
    // });

    $('#masterPatient').DataTable( {
        language: {
            searchPlaceholder: 'Search records',
            sSearch: '',
            sLengthMenu: 'Show _MENU_',
            sLength: 'dataTables_length',
            oPaginate: {
                sFirst: '<i class="material-icons">chevron_left</i>',
                sPrevious: '<i class="material-icons">chevron_left</i>',
                sNext: '<i class="material-icons">chevron_right</i>',
                sLast: '<i class="material-icons">chevron_right</i>'
            }
        },
        "processing": true,
        "serverSide": true,
        "ajax":{
            "url": "patient/patient_ajax",
            "dataType": "json",
            "type": "POST",
            "headers": {
                "X-CSRF-TOKEN": "{{ csrf_token() }}"
            }
        },
        "columns": [
            {
             "data": "id",
             render: function (data, type, row, meta) {
             return meta.row + meta.settings._iDisplayStart + 1;
             }
             },

            { "data": "company_id" },
            { "data": "first_name" },
            { "data": "last_name" },
            { "data": "place_birth" },
            { "data": "birth_date" },
            { "data": "sex" },
            { "data": "init_doctor" },
            { "data": "phone" },
            {
                "mRender": function (data, type, row) {
                    return '<a href="detail_patient" class="btn">Detail</a>';
                }
            }

        ],

    } );


    $('#reservation').DataTable( {
        language: {
            searchPlaceholder: 'Search records',
            sSearch: '',
            sLengthMenu: 'Show _MENU_',
            sLength: 'dataTables_length',
            oPaginate: {
                sFirst: '<i class="material-icons">chevron_left</i>',
                sPrevious: '<i class="material-icons">chevron_left</i>',
                sNext: '<i class="material-icons">chevron_right</i>',
                sLast: '<i class="material-icons">chevron_right</i>'
            }
        },
        "processing": true,
        "serverSide": true,
        "ajax":{
            "url": "api/admission/reservation_show_all",
            "dataType": "json",
            "type": "POST",
            "data": {
                "company_code" : "1400",
                "doctor_code": "MIJ",
                "reservation_date"  : "2017-09-12"
            },
            "headers": {
                "authorization": "Bearer uEzFSH1CTVPj0mf2VLRmmtGVvNWUQwBR0u5QykJzFskllHEsEr7Z7dU806YcvyD23k2DgH5WLTYyU86TkepxeQRtY5gSINSuTuXX"
            }
        },
        "columns": [
            /*{
             "data": "id",
             render: function (data, type, row, meta) {
             return meta.row + meta.settings._iDisplayStart + 1;
             }
             },*/

            { "data": "session" },
            { "data": "morula_id" },
            { "data": "first_name" },
            { "data": "last_name" },
            { "data": "birth_date" },
            { "data": "sex" },
            { "data": "mobile" },
            {
                "mRender": function (data, type, row) {
                    return 'Operator';
                }
            },
            { "data": "description" },
            { "data": "visit" },


        ],

    } );

    $('.dataTables_length select').addClass('browser-default');

    $('#reservation_form').submit(function(e){

        e.preventDefault(); // Prevent Default Submission

        $.ajax({
            url: 'api/admission/reservation_show_all',
            type: 'POST',
            headers: {
                "authorization": "Bearer uEzFSH1CTVPj0mf2VLRmmtGVvNWUQwBR0u5QykJzFskllHEsEr7Z7dU806YcvyD23k2DgH5WLTYyU86TkepxeQRtY5gSINSuTuXX"
            },
            data: $(this).serialize() // it will serialize the form data
        })

            .fail(function(){
                alert('Ajax Submit Failed ...');
            });
    });
} );

$('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    format: 'yyyy-mm-dd',
    closeOnSelect: false,
    container: 'body'// Close upon selecting a date,
});
$('.timepicker').pickatime({
    default: 'now', // Set default time: 'now', '1:30AM', '16:30'
    fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
    twelvehour: false, // Use AM/PM or 24-hour format
    donetext: 'OK', // text for done-button
    cleartext: 'Clear', // text for clear-button
    canceltext: 'Cancel', // Text for cancel-button
    autoclose: false, // automatic close timepicker
    container: 'body',
    ampmclickable: true, // make AM PM clickable
    aftershow: function(){} //Function for after opening timepicker
});

$("#tabsId").on("click", "li", function(evt) {

        evt.preventDefault();
        var id = $(this).data("tabContent");
        $("#tabsContentId")
            .find(".tab-content[data-tab-content='" + id + "']").show()
            .siblings().hide();
});


