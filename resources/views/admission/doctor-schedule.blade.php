@extends('layouts.layout')
@section('content')
    <main class="mn-inner">

        <div class="col s12">
                <ul class="tabs tab-demo z-depth-1" style="width: 100%;">
                    <li class="tab col s3"><a href="#docter_practice" class="active">Doctor Practice</a></li>
                    <li class="tab col s3"><a href="#create_schedule">Create Schedule</a></li>
                    <li class="tab col s3"><a href="#monthly_schedule">Monthly Schedule</a></li>
                    <li class="tab col s3"><a href="#apoinment">Apointment</a></li>
                    <li class="tab col s3"><a href="#form_request">Form Request</a></li>
                    <div class="indicator" style="right: 372.75px; left: 0px;"></div></ul>





                    <div id="docter_practice">
                                    <div class="col s12 card">
                                            <ul class="tabs tab-demo z-depth-1" style="width: 100%;">
                                                <li class="tab col s3"><a class="active" href="#sunday">Sunday</a></li>
                                                <li class="tab col s3"><a href="#monday">Monday</a></li>
                                                <li class="tab col s3"><a href="#tuesday">Tuesday</a></li>
                                                <li class="tab col s3"><a href="#wednesday">Wednesday</a></li>
                                                <li class="tab col s3"><a href="#thursday">Thursday</a></li>
                                                <li class="tab col s3"><a href="#friday">Friday</a></li>
                                                <li class="tab col s3"><a href="#saturday">Saturday</a></li>
                                                <div class="indicator" style="right: 372.75px; left: 0px;"></div></ul>



                                                    <div class="card-content">
                                                        <table id="" class="example display responsive-table datatable-example" cellspacing="0" width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th>Dokter</th>
                                                                <th>Spesialisasi</th>
                                                                <th>Jam Praktek</th>
                                                                <th>Ruang Praktek</th>
                                                                <th>Hari</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>Agus</td>
                                                                <td>TEST</td>
                                                                <td>07.00 - 08.00</td>
                                                                <td>Morula IVF</td>
                                                                <td>Senin</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Bambang</td>
                                                                <td>TEST</td>
                                                                <td>07.00 - 08.00</td>
                                                                <td>Morula IVF</td>
                                                                <td>Selasa</td>
                                                            </tr>
                                                            </tbody>

                                                        </table>


                                                    </div>

                                </div>



						</div>

                <div id="create_schedule">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12 m12 card-panel" method="post" id="">

                                <div class="row" style="margin-top: 20px;">

                                    <label class="col lebel">Data</label>
                                    <input type="text" class="col s1" name="company_code">
                                    <input type="text" class="col s2" style="margin-left: 20px;">

                                </div>

                                <div class="row">
                                    <label class="col lebel">Date</label>
                                    <input type="text" class="datepicker_res date-now" name="reservation_date" style="width: 100px;">
                                    <i class="material-icons" style="padding-top: 10px;">date_range</i>

                                    <button style="margin-left: 5%" type="submit" class="waves-effect waves-light btn" href="#">OK</button>
                                    <a style="margin-left: 5%" class="waves-effect waves-light btn" href="#">Print</a>
                                </div>
                            </form>

                        </div>
                     </div>
				</div>


            <div id="monthly_schedule">
                <div class="row">

                    <div class="col s12 card-panel">
                        <div class="row" style="margin-top: 20px;">

                            <label class="col lebel">Data</label>
                            <input type="text" class="col s1" name="company_code">
                            <label class="col lebel">Date</label>
                            <input type="text" class="datepicker_res date-now" name="reservation_date" style="width: 100px;">
                            <i class="material-icons" style="padding-top: 10px;">date_range</i>

                        </div>

                        <div class="row">
                            <button type="submit" class="waves-effect waves-light btn col s1 offset-s6" href="#">OK</button>
                        </div>
                    </div>

                </div>
            </div>



            <div id="apointment">

            </div>


            <div id="form_request">

					<div class="col s12 card">
						<ul class="tabs">
							<li class="tab col s3"><a href="#full_day_leave">Full Day Leave</a></li>
							<li class="tab col s3"><a class="active" href="#session_leave">Session Leave</a></li>
							<li class="tab col s3"><a href="#late">Late</a></li>
							<div class="indicator" style="right: 372.75px; left: 0px;"></div></ul>
					</div>

                <div id="full_day_leave">
                        <div class="row">
                            <div class="card col s6 offset-l3 no-padding-hor">
                                <div class="center-align card-title teal white-text" style="font-size: 20px">Doctor Leave Request Form</div>
                                <div class="card-content">
                                    {{--The Form Here--}}
                                    <form action="{{ route('doctor_leave') }}" method="post" id="newforLeave">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col s12 no-padding">
                                                <label for="height" class="col s2 lebel">Doctor</label>
                                                <select class="form-control col s6" id="doctor" name="doctor">
                                                    @foreach($doctor as $item)
                                                        <option value="{{$item->id}}"> {{$item->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col s12 no-padding">
                                                <label for="height" class="col s1 lebel">Start Date</label>
                                                <input type="text" class="datepicker date_now col s3" name="start">
                                                <i class="material-icons col" style="padding-top: 10px;">date_range</i>

                                                <label for="height" class="col s1 lebel">End Date</label>
                                                <input type="text" class="datepicker date_now col s3" name="end">
                                                <i class="material-icons col" style="padding-top: 10px;">date_range</i>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col s8 no-padding">
                                                <label for="height" class="col s4 lebel">Leave Type</label>
                                                    <select class="col s6 no-padding" name="type_leave">
                                                        <option value="Paid Leave">Paid Leave</option>
                                                        <option value="Unpaid Leave">Unpaid Leave</option>
                                                    </select>
                                                 </div>
                                            </div>
                                  
                                        <div class="row">
                                            <div class="col s12 no-padding">
                                                <label for="height" class="col s2 lebel">Cause</label>
                                                <div class="input-field col s8">
                                                    <textarea id="textarea1" class="materialize-textarea" length="120" name="cause"></textarea>
                                                    <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"></span></div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col s12 no-padding">
                                                <div class="input-field col s8" style="margin-left:310px;">
                                                    <button id="button_add_leave" type="submit" class="waves-effect waves-light btn col s5 offset-s1" href="#">Submit</button></div>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
            </div>


                <div id="session_leave">
                    <div class="row">
                        <div class="card col s6 offset-l3 no-padding-hor">
                            <div class="center-align card-title teal white-text" style="font-size: 20px">Doctor Session Leave Request Form</div>
                                <div class="card-content">
                                    {{--The Form Here--}}
                                    <form action="{{ route('doctor_schedule') }}" method="post" id="newformSession_Leave">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col s12 no-padding">
                                                <label for="height" class="col s2 lebel">Doctor</label>
                                                <select class="form-control col s6" id="doctor" name="doctor">
                                                    @foreach($doctor as $item)
                                                        <option value="{{$item->id}}"> {{$item->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col s12 no-padding">
                                                <label for="height" class="col s2 lebel">Date</label>
                                                <input type="text" class="datepicker date_now col s3" name="date" placeholder="Y/m/d">
                                                <i class="material-icons col" style="padding-top: 10px;">date_range</i>

                                                <label for="height" class="col s2 lebel">Session</label>
                                                <input type="text" name="sess" class="timepicker col s2" placeholder="Hh:mm">
                                                <i class="material-icons" style="padding-top: 10px;">access_time</i>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col s12 no-padding">
                                                <label for="height" class="col s2 lebel">Cause</label>
                                                <div class="input-field col s8">
                                                    <textarea id="textarea1" class="materialize-textarea" length="120" name="cause" placeholder="Cause"></textarea>
                                                    <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"></span></div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col s12 no-padding">
                                                <div class="input-field col s8" style="margin-left:310px;">
                                                    <button type="submit" id="button_add_session_leave" class="waves-effect waves-light btn col s5 offset-s1" href="#">Submit</button></div>
                                            </div>
                                        </div>
                                    </form>

                                    {{--End Form--}}
                            </div>
                         </div>
                    </div>
                </div>

                <div id="late">
                        <div class="row">
                            <div class="card col s6 offset-l3 no-padding-hor">
                                <div class="center-align card-title teal white-text" style="font-size: 20px">Doctor Late Information Form</div>
                                <div class="card-content">
                                    {{--The Form Here--}}
                                    <form action="{{ route('doctor_late') }}" method="post" id="newformLate">
                                        {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <label for="height" class="col s2 lebel">Doctor</label>
                                            <select class="form-control col s6" id="doctor" name="doctor">
                                                @foreach($doctor as $item)
                                                    <option value="{{$item->id}}"> {{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <label for="height" class="col s2 lebel">Date</label>
                                            <input type="text" name="date" class="datepicker date_now col s3" placeholder="Y/m/d">
                                            <i class="material-icons col" style="padding-top: 10px;">date_range</i>

                                            <label for="height" class="col s2 lebel">Session</label>
                                            <input type="text" name="sess" class="timepicker col s3" placeholder="Hh:mm">
                                            <i class="material-icons" style="padding-top: 10px;">access_time</i>
                                        </div>
                                    </div>

                                    <div class="col offset-s5">
                                        <label for="height" class="col s5 lebel" style="margin-left:5px">Start time</label>
                                        <input type="text" name="start" class="timepicker col s5" placeholder="Hh:mm">
                                        <i class="material-icons" style="padding-top: 10px;">access_time</i>
                                    </div>

                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <label for="height" class="col s2 lebel">Cause</label>
                                            <div class="input-field col s8">
                                                <textarea id="textarea1" class="materialize-textarea" length="120" placeholder="Cause" name="cause"></textarea>
                                                <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"></span></div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <div class="input-field col s8" style="margin-left:310px;">
                                                <button id="button_add_late" type="submit" class="waves-effect waves-light btn col s5 offset-s1" href="#">Submit</button></div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
            </div>




        </div>
    </main>
@endsection

@section('js')
    <script>
        //Button add session leave
        $('#button_add_leave').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: '/admission/doctor_leave/request_leave',
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#newforLeave').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) {
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n');
                    } else {
                        var descr = data.description;
                        $(':input','#newforLeave')
                            .not(':button, :submit, :reset, :hidden')
                            .val('');
                    }
                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        $('#button_add_session_leave').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: '/admission/doctor_schedule/request_session_leave',
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#newformSession_Leave').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) {
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n');
                    } else {
                        var descr = data.description;
                        $(':input','#newformSession_Leave')
                            .not(':button, :submit, :reset, :hidden')
                            .val('');
                    }
                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        $('#button_add_late').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: '/admission/doctor_late/request_late',
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#newformLate').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) {
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n');
                    } else {
                        var descr = data.description;
                        $(':input','#newformLate')
                            .not(':button, :submit, :reset, :hidden')
                            .val('');
                    }
                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        function alertPopup(title, text, type){
            swal({ title: title,
                text: text,
                type: type,
                //showConfirmButton: false,
                //timer: 3000,
            })
        }

        @if (Session::has('after_save'))
        var t = "{{ Session::get('after_save.title') }}";
        var txt = "{{ Session::get('after_save.text-1') }} {{ Session::get('after_save.text-2') }}";
        var ty = "{{ Session::get('after_save.alert') }}";
        alertPopup(t, txt,ty);
        @endif
    </script>

@endsection
