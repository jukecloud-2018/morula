@extends('layouts.layout')
@section('content')
	<main class="mn-inner">
		<div class="row">
			<div class="col s12">
				<table>
					<tr>
						<td>
							<div id="doctor_list"></div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</main>

	{{--Modal Reservation Detail--}}

@endsection

@section('js')
	<script>
        $(document).ready(function() {
            $.ajax({
                type: 'POST',
                url: '/admission/doctor_show_all',
                dataType: 'json',
                success: function (data) {
                    var isi = (data.data);
                    var content = '';
                    for (var i = 0; i < isi.length; i++) {
                        content += '<div class="col s3 m3 l3">';
                        content += '<div class="card card-dashboard" id="' + isi[i].id + '">';
                        content += '<div class="card-content">';
                        content += '<div class="row" style="margin-bottom: 0;">';
                        content += '<img class="doctor-dp col s5 no-padding" src="/uploads/doctor/' + isi[i].photo +'">';
                        content += '<div class="col s7">';
                        content += '<span>' + isi[i].name +'</span><br>';
                        content += '<span>Session :</span><br>';
                        content += '<span>Patient :</span><br>';
                        content += '<span>New Attendant :</span><br>';
                        content += '<span>New Patient :</span><br>';
                        content += '</div></div></div>';
                        content += '<div class="card-action footer-doctor light-green accent-3">';
                        content += '</div></div></div>';
                    }

                    $('#doctor_list').html(content);
                    $('#doctor_list').addClass('animated fadeInUp');

                }
            });
        });

	</script>
@endsection