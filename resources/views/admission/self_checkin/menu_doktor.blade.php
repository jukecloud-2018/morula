@extends('layouts.layout-self_checkin')
@section('content')
    <main class="mn-inner">

			<div class="row">
    			<div class="destination">
        			<div class="col s3 center-align offset-m4">
        				 <img src="/assets/images/profile-image-2.png" alt="" class="circle responsive-img">
        				 <p id="welcome">Welcome, Mr. Ucok</p>
        			</div>
        		</div>
    		</div>

    		<div class="row">
    		<div class="col l3 m3 s12">
    			<div class="card">
    				<div class="card-title"></div>
    				<div class="content center-align">
    					<p style="padding:20px;">Anda Terdaftar Pada Nomor</p>
    					<p style="font-size:80px; margin-top:10px; padding-bottom:80px;">01</p>
    				</div>
    			</div>
    		</div>

    		<div class="col l3 m3 s12">
    			<div class="card">
    				<div class="card-title"></div>
    				<div class="content center-align">
    					<p style="padding:20px;">Menemui Doktor</p>
    					<a href=""><img style="width:150px;" src="/assets/images/logo.png"></a>
    					<p style="padding-bottom:35px;">Dr. Ivan Sini, SpOG</p>
    				</div>
    			</div>
    		</div>

    		<div class="col l3 m3 s12">
    			<div class="card">
    				<div class="card-title"></div>
    				<div class="content center-align">
    					<p style="padding-top:20px;">Jadwal Check in</p>
    					<p style="font-size:55px;">08:30</p>
    					<p style="padding-bottom:20px;">AM</p>
    				</div>
    			</div>
    		</div>

    		<div class="col l3 m3 s12">
    			<div class="card">
    				<div class="card-title"></div>
    				<div class="content center-align">
    					<p style="padding-top:20px;">Ruangan</p>
    					<p style="font-size:55px;">206</p>
    					<p style="padding-bottom:20px;">Sixth Floor</p>
    				</div>
    			</div>
    		</div>
    		</div>

    </main>


@endsection

@section('js')
	<script>
 
		$( document ).ready(function(){
			$('.mn-inner').toggleClass('hidden-fixed-sidebar');
	        $('.mn-content').toggleClass('fixed-sidebar-on-hidden');
	        $(document).trigger('fixedSidebarClick');
		});
	</script>		
@endsection