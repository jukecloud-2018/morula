@extends('layouts.layout-self_checkin')
@section('content')
    <main class="mn-inner">

			<div class="row">
    			<div class="destination">
        			<div class="col s3 center-align offset-m4">
        				 <img src="/assets/images/profile-image-2.png" alt="" class="circle responsive-img">
        				 <p id="welcome">Welcome, Mr. Ucok</p>
        				 <p>Please select your destination</p>
        			</div>
        		</div>
    		</div>

    		<div class="row">
    		<div class="col l4 m4 s12 offset-l2">
    			<div class="card">
    				<div class="card-title"></div>
    				<div class="content center-align">
    					<a href=""><img style="width:150px;" src="/assets/images/logo.png"></a>
    					<p>Pharmacy</p>
    					<p style="padding:20px;">This is a square image. Add the "circle" class to it to make it appear circular.</p>
    				</div>
    			</div>
    		</div>

    		<div class="col l4 m4 s12">
    			<div class="card">
    				<div class="card-title"></div>
    				<div class="content center-align">
    					<a href=""><img style="width:150px;" src="/assets/images/logo.png"></a>
    					<p>Nurse</p>
    					<p style="padding:20px;">This is a square image. Add the "circle" class to it to make it appear circular.</p>
    				</div>
    			</div>
    		</div>
    		</div>

    </main>


@endsection

@section('js')
	<script>
 
		$( document ).ready(function(){
			$('.mn-inner').toggleClass('hidden-fixed-sidebar');
	        $('.mn-content').toggleClass('fixed-sidebar-on-hidden');
	        $(document).trigger('fixedSidebarClick');
		});
	</script>		
@endsection