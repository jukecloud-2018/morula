@extends('layouts.layout-self_checkin')
@section('content')
    <main class="mn-inner">

        <div class="row">

		<div class="form-self-checkin">
        	<div class="col l4 m4 s12">
        		<div class="card isi-form-self-checkin">
	        		<div class="card-title center-align">Please Input Your<br>Booking Number</div>
	        		<div class="card-content center-align" >
	        			  <label class="active" for="first_name2">Booking Number</label>
					      <input value="" id="first_name2" type="text" class="validate">
					      <a class="waves-effect waves-light btn">Submit</a>
	        		</div>
        		</div>
        	</div>

        	<div class="col l4 m4 s12">
        		<div class="card isi-form-self-checkin">
	        		<div class="card-title center-align">New Patient Tap Here</div>
	        		<div class="card-content center-align">
	        			<a class="waves-effect waves-light btn btn-self-checkin"><p id="add_new_patient">Add New Patient</p></a>
	        		</div>
        		</div>
        	</div>
		</div>

        </div>

    </main>


@endsection


@section('js')
	<script type="text/javascript">
		$(document).ready(function() {
					$('.mn-inner').toggleClass('hidden-fixed-sidebar');
	                $('.mn-content').toggleClass('fixed-sidebar-on-hidden');
	                $(document).trigger('fixedSidebarClick');
		});
	</script>
@endsection