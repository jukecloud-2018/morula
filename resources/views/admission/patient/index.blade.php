@extends('layouts.layout')

@section('content')
<main class="mn-inner">
	<div class="col s12">
		<div>
			{{--Main Tab Master Patient--}}
			<div id="mp" class="tab-content" style="display:block">
				<div class="card-panel">
				{{--<a class="waves-effect waves-light btn modal-trigger" href="#modal1">Modal</a>--}}
					<table id="masterPatient" class="display responsive-table datatable-example" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Morula ID</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Place Birth</th>
								<th>Date Birth</th>
								<th>F/M</th>
								<th>Doctor</th>
								<th>Phone No And HP</th>
								<th>Actions</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
    <!--/div-->

	{{--Modals--}}
    <div id="newPatient" class="modal  modal-fixed-footer">
        <div class="modal-content">
            <h4 style="font-size: 20px;padding-left: 45%;">New Patient</h4>
            <br>
            <form method="post" id="submit_newPatient">
            <div class="row">
                <label class="col lebel">Session</label>
                <input type="text" name="session" class="timepicker col s1">
                <i class="material-icons" style="padding-top: 10px;">access_time</i>
            </div>

            <div class="row">
                <label class="col lebel">First Name</label>
                <input type="text" name="first_name" class="col s8">
            </div>

            <div class="row">
                <label class="col lebel">Last Name</label>
                <input type="text" name="last_name" class="col s8">
            </div>


            <input type="hidden" name="init_doctor" value="1">
            <input type="hidden" name="id_number" value="12738213">
            <input type="hidden" name="id_type" value="1">
                <input type="hidden" name="reservation_date" value="2017-09-12">
                <input type="hidden" name="visit_code" value="1">


            <div class="row">
                <label class="col lebel">Sex</label>
                <select name="sex" class="col s2">
                    <option value="" disabled selected></option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                </select>
            </div>

            <div class="row">
                <label class="col lebel">Date of Birth</label>
                <input type="text" class="datepicker col" name="birth_date" style="width: 100px;">
                <i class="material-icons" style="padding-top: 10px;">date_range</i>
            </div>

            <div class="row">
                <label class="col lebel">Mobile no 1</label>
                <input type="text" name="mobile" class="col s8">
            </div>

            <div class="row">
                <label class="col lebel">Mobile no 2</label>
                <input type="text" name="mobile_2" class="col s8">
            </div>

            {{--<div class="row">--}}
                {{--<label class="col lebel">Operator</label>--}}
                {{--<input type="text" name="operator" readonly class="col s8">--}}
            {{--</div>--}}

            <div class="row">
                <label class="col lebel">Description</label>
                <textarea class="materialize-textarea col s8" name="description" data-length="120"></textarea>
            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
            <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cencel</a>
        </div>
        </form>
    </div>
</main>
@endsection

@section('js')
<script>
	$(document).ready(function() {
		function pad (str, max) {
		  str = str.toString();
		  return str.length < max ? pad("0" + str, max) : str;
		}
		
	   $('#masterPatient').DataTable( {
			language: {
				searchPlaceholder: 'Search records',
				sSearch: '',
				sLengthMenu: 'Show _MENU_',
				sLength: 'dataTables_length',
				oPaginate: {
					sFirst: '<i class="material-icons">chevron_left</i>',
					sPrevious: '<i class="material-icons">chevron_left</i>',
					sNext: '<i class="material-icons">chevron_right</i>',
					sLast: '<i class="material-icons">chevron_right</i>'
				}
			},
			"processing": true,
			"serverSide": true,
			"ajax":{
				"url": "{{ route('master_patient_ajaxdt') }}",
				"dataType": "json",
				"type": "POST",
				"headers": {
					"X-CSRF-TOKEN": "{{ csrf_token() }}"
				}
			},
			"columns": [
				{
					"data": "id",
						render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
						}
				},
				{
					"mRender": function (data, type, row) {
						return row.company_id + '-' + pad(row.id, 4);
					}
				},
				{ "data": "first_name" },
				{ "data": "last_name" },
				{ "data": "birth_place" },
				{ "data": "birth_date" },
				{ "data": "sex" },
				{ "data": "init_doctor" },
				{ "data": "phone" },
				{
					"mRender": function (data, type, row) {
						return '<a href=\"{{ route("master_patient.index") }}/' + row.id +'\" class="btn">Detail</a>';
					}
				}
			],
		});
		$('.dataTables_length select').addClass('browser-default');

	});	
</script>
@endsection
