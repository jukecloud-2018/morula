@extends('layouts.layout')
@section('content')
    <main class="mn-inner">
		<form enctype="multipart/form-data" action='{{ url("/admission/master_patient/$data->id") }}' method="post">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
			<div class="row">
				<div class="col s12">
					<div class="page-title">Detail Patient</div>
				</div>
				
				<div class="col s12 m12 l12">
					@if (Session::has('after_save'))
						<div class="card red">
							<div class="card-content"> 
								<div class="white-text">{{ $errors->update->first('first_name') }}</div>
								<div class="white-text">{{ $errors->update->first('last_name') }}</div>
								<div class="white-text">{{ $errors->update->first('init_doctor') }}</div>
								<div class="white-text">{{ $errors->update->first('emergency_name') }}</div>
								<div class="white-text">{{ $errors->update->first('emergency_relationship') }}</div>
								<div class="white-text">{{ $errors->update->first('emergency_address') }}</div>
								<div class="white-text">{{ $errors->update->first('emergency_city') }}</div>
								<div class="white-text">{{ $errors->update->first('emergency_postcode') }}</div>
								<div class="white-text">{{ $errors->update->first('emergency_phone') }}</div>
								<div class="white-text">{{ $errors->update->first('emergency_email') }}</div>
								<div class="white-text">{{ $errors->update->first('id_type') }}</div>
								<div class="white-text">{{ $errors->update->first('id_number') }}</div>
								<div class="white-text">{{ $errors->update->first('birth_place') }}</div>
								<div class="white-text">{{ $errors->update->first('birth_date') }}</div>
								<div class="white-text">{{ $errors->update->first('sex') }}</div>
								<div class="white-text">{{ $errors->update->first('body_height') }}</div>
								<div class="white-text">{{ $errors->update->first('blood') }}</div>
								<div class="white-text">{{ $errors->update->first('marital_status') }}</div>
								<div class="white-text">{{ $errors->update->first('married_date') }}</div>
								<div class="white-text">{{ $errors->update->first('religion') }}</div>
								<div class="white-text">{{ $errors->update->first('nationality') }}</div>
								<div class="white-text">{{ $errors->update->first('address') }}</div>
								<div class="white-text">{{ $errors->update->first('city') }}</div>
								<div class="white-text">{{ $errors->update->first('postal_code') }}</div>
								<div class="white-text">{{ $errors->update->first('phone') }}</div>
								<div class="white-text">{{ $errors->update->first('mobile') }}</div>
								<div class="white-text">{{ $errors->update->first('mobile_2') }}</div>
								<div class="white-text">{{ $errors->update->first('office_name') }}</div>
								<div class="white-text">{{ $errors->update->first('office_phone') }}</div>
								<div class="white-text">{{ $errors->update->first('job_type') }}</div>
								<div class="white-text">{{ $errors->update->first('email') }}</div>
							</div>
						</div>
					@endif
				</div>
				<div class="col s12 m12 l4">
					<div class="card">
						<div class="card-content">
							<span class="card-title" style="margin-bottom: 0px;">Basic Info</span><br>
							<div class="row">
								<div class="row">
									<div class="col s6 m6 l6">
										<img class="detail-patient-dp" src="{{ $data->foto != '' ? "/uploads/patient/$data->foto" : "/assets/images/avatar-default.png" }}">
									</div>
									<div class="col m6 s6">
										<div class="row">
											<div class="input-field-detail">
												<input name="first_name" id="first_name" type="text" class="validate" value="{{ old('first_name', $data->first_name) }}">
												<label for="first_name">First Name</label>
											</div>
										</div>
										<div class="row">
											<div class="input-field-detail">
												<input name="last_name" id="last_name" type="text" class="validate" value="{{ old('last_name', $data->last_name) }}">
												<label for="last_name">Last Name</label>
											</div>
										</div>
										<div class="row">
											<div class="input-field-detail">
												@php
													$no_morula = str_pad($data->id, 4, "0", STR_PAD_LEFT);
												@endphp
												<input name="no_morula" value="1400-{{ $no_morula }}" id="no_morula" type="text" class="" readonly>
												<label for="no_morula">No Morula</label>
											</div>
										</div>
										
									</div>
								</div>
								<div class="row">
									<div class="file-field input-field">
										<div class="btn teal lighten-1">
											<span>Upload Foto</span>
											<input name="foto" id="foto" type="file">
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="input-field-detail col s3">
										<input type="hidden" name="init_doctor" value="{{ $data->init_doctor }}">
										<input name="doctor" id="doctor" type="text" class="validate" value="{{ old('doctor_code', $data_doctor->doctor_code) }}">
										<label for="doctor">Doctor</label>
										
									</div>
									<input value="{{ $data_doctor->name }}" type="text" class="col s8" style="margin-left: 20px;" readonly>
								</div>
							</div>
						</div>
					</div>
					
					@php
						if(isset($data->emergency)){
							$emergency_name = $data->emergency->name;
							$emergency_relationship = $data->emergency->relationship;
							$emergency_address = $data->emergency->address;
							$emergency_city = $data->emergency->city;
							$emergency_postcode = $data->emergency->postal_code;
							$emergency_phone = $data->emergency->phone;
							$emergency_email = $data->emergency->email;
						} else {
							$emergency_name = "";
							$emergency_relationship = "";
							$emergency_address = "";
							$emergency_city = "";
							$emergency_postcode = "";
							$emergency_phone = "";
							$emergency_email = "";
						}
					@endphp

					<div class="card">
						<div class="card-content">
							<span class="card-title" style="margin-bottom: 0px;">Emergency Contact</span><br>
							<div class="row">
								<div class="row">
									<div class="input-field-detail col s12">
										<input value="{{ old('emergency_name', $emergency_name) }}" name="emergency_name" type="text" class="validate">
										<label>Name</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field-detail col s12">
										<input value="{{ old('emergency_relationship', $emergency_relationship) }}" name="emergency_relationship" type="text" class="validate">
										<label>Relationship</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field-detail col s12">
										<input value="{{ old('emergency_address', $emergency_address) }}" name="emergency_address" type="text" class="validate">
										<label>Address</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s6" style="height: 35px;margin-top: 0px;">
										<input value="{{ old('emergency_city', $emergency_city) }}" name="emergency_city" id="emergency_city" type="text" class="validate">
										<label>City</label>
									</div>
									<div class="input-field-detail col s6">
										<input value="{{ old('emergency_postcode', $emergency_postcode) }}" name="emergency_postcode" type="text" class="validate">
										<label>Post Code</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field-detail col s12">
										<input value="{{ old('emergency_phone', $emergency_phone) }}" name="emergency_phone" type="text" class="validate">
										<label>Phone Number</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field-detail col s12">
										<input value="{{ old('emergency_email', $emergency_email) }}" name="emergency_email" id="email" type="text" class="validate">
										<label for="email">Email</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col s12 m12 l8">
					<div class="card">
						<div class="card-content">
							<span class="card-title" style="margin-bottom: 0px;">Personal Information</span><br>
							<div class="row">
									<div class="input-field-detail col s4" style="height: 35px;margin-top: 0px;">
										<select name="id_type" id="id_type">
											<option value="" disabled selected>Choose ID Type</option>
											<option value="KTP" {{ old('id_type', $data->id_type) == 'KTP' ? 'selected="selected"' : '' }}>KTP</option>
											<option value="SIM" {{ old('id_type', $data->id_type) == 'SIM' ? 'selected="selected"' : '' }}>SIM</option>
											<option value="Other" {{ old('id_type', $data->id_type) == 'Other' ? 'selected="selected"' : '' }}>Other</option>
										</select>
										<label></label>
									</div>
									<div class="input-field-detail col s8">
										<input value="{{ old('id_number', $data->id_number) }}" type="text" class="validate" name="id_number">
										<label>Identity Number</label>
									</div>
								</div>
							<div class="row">
								<div class="row">
									<div class="input-field-detail col s6">
										<input value="{{ old('birth_place', $data->birth_place) }}" name="birth_place" id="birth_place" type="text" class="validate">
										<label for="birth_place">Birth Place</label>
									</div>
									<div class="input-field-detail col s2">
										<label for="birth_date">Birth Date</label>
										<input value="{{ old('birth_date', $data->birth_date) }}" readonly name="birth_date" id="birth_date" type="text" class="datepicker_res">
									</div>
								</div>
								<div class="row">
									<div class="input-field col s4" style="height: 35px;margin-top: 0px;">
										<select name="sex">
											<option value="" disabled>Choose your option</option>
											<option value="Male" {{ old('sex', $data->sex) == 'Male' ? 'selected="selected"' : '' }}>Male</option>
											<option value="Female" {{ old('sex', $data->sex) == 'Female' ? 'selected="selected"' : '' }}>Female</option>
										</select>
										<label>Sex</label>
									</div>
									<div class="input-field-detail col s4">
										<input value="{{ old('body_height', $data->body_height) }}" name="body_height" id="body_height" type="text" class="validate">
										<label for="body_height">Body Height (cm)</label>
									</div>
									<div class="input-field-detail col s4">
										<input value="{{ old('blood', $data->blood) }}" name="blood" id="blood" type="text" class="validate">
										<label for="blood">Blood Type</label>
									</div>
								</div>
								
								<div class="row">
									<div class="input-field col s6" style="height: 35px;margin-top: 0px;">
										<select name="marital_status">
											<option value="" disabled>Choose your option</option>
											<option value="Married" {{ old('marital_status', $data->marital_status) == 'Married' ? 'selected="selected"' : '' }}>Married</option>
											<option value="Single" {{ old('marital_status', $data->marital_status) == 'Single' ? 'selected="selected"' : '' }}>Single</option>
										</select>
										<label>Marital Status</label>
									</div>
									<div class="input-field-detail col s6">
										<input value="{{ old('married_date', $data->married_date) }}" name="married_date" id="dom" type="text" class="datepicker_res">
										<label for="dom">Date of Married</label>
									</div>
								</div>
								
								<div class="row">
									<div class="input-field-detail col s6" style="height: 35px;margin-top: 0px;">
										<select name="religion">
											<option value="" disabled>Choose Religion</option>
											<option value="Islam" {{ old('religion', $data->religion) == 'Islam' ? 'selected="selected"' : '' }}>Islam</option>
											<option value="Kristen Katolik" {{ old('religion', $data->religion) == 'Kristen Katolik' ? 'selected="selected"' : '' }}>Kristen Katolik</option>
											<option value="Kristen Protestan" {{ old('religion', $data->religion) == 'Kristen Protestan' ? 'selected="selected"' : '' }}>Kristen Protestan</option>
											<option value="Hindu" {{ old('religion', $data->religion) == 'Hindu' ? 'selected="selected"' : '' }}>Hindu</option>
											<option value="Buddha" {{ old('religion', $data->religion) == 'Buddha' ? 'selected="selected"' : '' }}>Buddha</option>
											<option value="Kong Hu Cu" {{ old('religion', $data->religion) == 'Kong Hu Cu' ? 'selected="selected"' : '' }}>Kong Hu Cu</option>
										</select>
										<label></label>
									</div>
									<div class="input-field col s6" style="height: 35px;margin-top: 0px;">
										<select name="nationality">
											<option value="" disabled>Choose your option</option>
											<option value="Indonesian" {{ old('nationality', $data->nationality) == 'Indonesian' ? 'selected="selected"' : '' }}>Indonesian</option>
											<option value="Foreign" {{ old('nationality', $data->nationality) == 'Foreign' ? 'selected="selected"' : '' }}>Foreign</option>
										</select>
										<label>Nationality</label>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col s12 m12 l8">
					<div class="card">
						<div class="card-content">
							<span class="card-title" style="margin-bottom: 0px;">Address &amp; Contact Information</span><br>
							<div class="row">
								<div class="row">
									<div class="input-field-detail col s12">
										<input value="{{ old('address', $data->address) }}" name="address" id="address" type="text" class="validate">
										<label for="address">Address</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s6" style="height: 35px;margin-top: 0px;">
										<select name="city">
											<option value="" disabled selected>Choose your option</option>
											<option value="Jakarta Pusat" {{ old('city', $data->city) == 'Jakarta Pusat' ? 'selected="selected"' : '' }}>Jakarta Pusat</option>
											<option value="Jakarta Barat" {{ old('city', $data->city) == 'Jakarta Barat' ? 'selected="selected"' : '' }}>Jakarta Barat</option>
											<option value="Jakarta Utara" {{ old('city', $data->city) == 'Jakarta Utara' ? 'selected="selected"' : '' }}>Jakarta Utara</option>
											<option value="Jakarta Selatan" {{ old('city', $data->city) == 'Jakarta Selatan' ? 'selected="selected"' : '' }}>Jakarta Selatan</option>
											<option value="Jakarta Timur" {{ old('city', $data->city) == 'Jakarta Timur' ? 'selected="selected"' : '' }}>Jakarta Timur</option>
											<option value="Kep. Seribu"{{ old('city', $data->city) == 'Kep. Seribu' ? 'selected="selected"' : '' }}>Kep. Seribu</option>
										</select>
										<label>City</label>
									</div>
									<div class="input-field-detail col s6">
										<input value="{{ old('postal_code', $data->postal_code) }}" type="text" class="validate" name="postal_code">
										<label>Postal Code</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field-detail col s4">
										<input value="{{ old('phone', $data->phone) }}" type="text" class="validate" name="phone">
										<label>Phone</label>
									</div>
									<div class="input-field-detail col s4">
										<input value="{{ old('mobile', $data->mobile) }}" type="text" class="validate" name="mobile">
										<label>Mobile</label>
									</div>
									<div class="input-field-detail col s4">
										<input value="{{ old('mobile_2', $data->mobile_2) }}" type="text" class="validate" name="mobile_2">
										<label>Mobile 2</label>
									</div>
								</div>
								
								<div class="row">
									<div class="input-field col s6" style="height: 35px;margin-top: 0px;">
										<input value="{{ old('office_name', $data->office_name) }}" type="text" class="validate" name="office_name">
										<label>Office Name</label>
									</div>
									<div class="input-field-detail col s6">
										<input value="{{ old('office_phone', $data->office_phone) }}" type="text" class="validate" name="office_phone">
										<label>Office Phone</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field-detail col s12">
										<input value="{{ old('job_type', $data->job_type) }}" type="text" class="validate" name="job_type">
										<label>Job Type</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field-detail col s12">
										<input name="email" value="{{ old('email', $data->email) }}" id="email" type="email" class="validate">
										<label for="email">Email</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			
			</div>
			<div class="partner row">
				<div class="col s12 m12 l12">
					<div class="card">
						
						<div class="card-content">
							<span class="card-title" style="margin-bottom: 0px;">Data Partner</span><br>
							<div class="row">
								
								<div class="input-field col s2" style="height: 35px;margin-top: 0px;">
									<select>
										<option value="" disabled>Choose your option</option>
										<option value="1">Wife 1</option>
										<option value="2">Wife 2</option>
									</select>
								</div>
								<div class="input-field-detail col s3">
									<input name="morula_num_part1" id="morula_num_part1" type="text" class="validate">
									<label for="morula_num_part1">Morula No.</label>
								</div>
								<div class="input-field-detail col s3">
									<input name="first_name_part1" id="first_name_part1" type="text" class="validate">
									<label for="first_name_part1">First Name</label>
								</div>
								<div class="input-field-detail col s3">
									<input name="last_name_part1" id="last_name_part1" type="text" class="validate">
									<label for="last_name_part1">Last Name</label>
								</div>
							</div>
						</div>
					</div>
				
				
					<div class="col s12 m12 l4">
						<div class="card">
							<div class="card-content">
								<span class="card-title" style="margin-bottom: 0px;">Place &amp; Date Birth</span><br>
								<div class="row">
									<div class="row">
										<div class="input-field-detail col s6">
											<input id="" type="text" class="validate">
											<label for="">Place</label>
										</div>
										<div class="input-field-detail col s6">
											<input type="text" class="validate">
											<label>Date</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field-detail col s12">
											<input type="text" class="validate">
											<label>Blood Type</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12" style="height: 35px;margin-top: 0px;">
											<select>
												<option value="" disabled selected></option>

											</select>
											<label>Religion</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12" style="height: 35px;margin-top: 0px;">
											<select>
												<option value="" disabled selected></option>
											</select>
											<label>Citizen</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col s12 m12 l8">
						<div class="card">
							<div class="card-content">
								<span class="card-title" style="margin-bottom: 0px;">Address &amp; Contact Information</span><br>
								<div class="row">
									<div class="row">
										<div class="input-field-detail col s12">
											<input type="text" class="validate">
											<label>Address</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s6" style="height: 35px;margin-top: 0px;">
											<select>
												<option value="" disabled selected></option>
											</select>
											<label>City</label>
										</div>
										<div class="input-field-detail col s6">
											<input type="text" class="validate">
											<label>Post Code</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field-detail col s6">
											<input type="text" class="validate">
											<label>Phone</label>
										</div>
										<div class="input-field-detail col s6">
											<input type="text" class="validate">
											<label>HP</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field-detail col s12">
											<input type="text" class="validate">
											<label>Identity Number</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s6" style="height: 35px;margin-top: 0px;">
											<select>
												<option value="" disabled selected></option>
											</select>
											<label>Job</label>
										</div>
										<div class="input-field-detail col s6">
											<input type="text" class="validate">
											<label>Office No</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field-detail col s12">
											<input id="email" type="text" class="validate">
											<label for="email">Email</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="col s6 m6 l6">
						<a href="{{ route('master_patient.index') }}" class="waves-effect waves-light btn white m-b-xs">Back</a>&nbsp;
						<button type="submit" class="waves-effect waves-light btn m-b-xs">Save</button>&nbsp;
					</div>
					<div class="col s6 m6 l6 right-align">
						<button type="button" class="waves-effect waves-light btn m-b-xs blue" id="add_partner">Add Partner</button>&nbsp;&nbsp;
						<button type="button" class="waves-effect waves-light btn m-b-xs green">Print</button>&nbsp;&nbsp;
						<button type="button" class="waves-effect waves-light btn m-b-xs grey">Label</button>
					</div>
				</div>
			</div>
		</form>
    </main>
@endsection
@section('js')
<script src="/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script>
	$(document).ready(function() {
		$(".masked").inputmask();
		// PrettyPrint
		$('pre').addClass('prettyprint');
		prettyPrint();
		
		
	});
	
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('.detail-patient-dp').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#foto").change(function(){
		readURL(this);
	});
	
	$('#add_partner').click(function(){
		$('.partner:last').clone()
								  .find("input:text").val("").end()
								  .appendTo('.partner:last');
	});
	
	
	@if (Session::has('after_save'))
		function alertSuccess(){
			swal({ title: "{{ Session::get('after_save.title') }}",
				text: "{{ Session::get('after_save.text-1') }} {{ Session::get('after_save.text-2') }}",
				type: "{{ Session::get('after_save.alert') }}",
				showConfirmButton: false,
				timer: 2000,
			})
		}
		alertSuccess();
	@endif
</script>
@endsection