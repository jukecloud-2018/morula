@extends('layouts.layout')

@section('content')
<main class="mn-inner">
	<div class="col s12">
		<div>
			
			{{--Main Tab Daily Patient--}}
			<div id="dp" class="tab-content" style="display:block">
				<div class="card-panel">
				<label for="daily_patient" class="active" style="font-size: 1rem;">Date :</label>&nbsp;
				<input type="text" id="daily_patient" class="datepicker" style="width: 150px;">
				</div>
			</div>

			
		</div>
	</div>
    
</main>
@endsection

@section('js')
<script>
	
</script>
@endsection
