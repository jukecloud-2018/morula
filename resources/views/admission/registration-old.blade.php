@extends('layouts.layout')

@section('content')
<main class="mn-inner">
	<div class="col s12">
		<div>
			<ul class="tabs tab-demo z-depth-1" id="tabsId" style="width: 100%;">
				<li class="tab col s3"><a href="#res">Reservation</a></li>
				<li id="registration" class="tab col s3"><a class="" href="#reg">Registration</a></li>
				<li class="tab col s3"><a href="#dp">Daily Patient</a></li>
				<li id="masterpasien" class="tab col s3"><a href="#mp" class="">Master Patient</a></li>
				<div class="indicator" style="right: 372.75px; left: 0px;"></div>
			</ul>

			{{--Main Tab Reservation--}}
			<div id="res">
				<div class="col s12">
					<div>
						{{--<ul class="tabs tab-demo z-depth-1" style="width: 100%;">--}}
						{{--<li class="tab col s3"><a class="" href="#regis">Registration</a></li>--}}
						{{--<div class="indicator" style="right: 372.75px; left: 0px;"></div></ul>--}}

						<div id="regis" >

							<div class="card-content">
								<div class="row">
									<form class="col s12 m12 card-panel" method="post" id="reservation_form">

										<div class="row" style="margin-top: 20px;">

												<label class="col lebel">Company</label>
												<input type="text" value="1400" class="col s1" name="company_code" readonly>
												<input type="text" value="Morula IVF Jakarta" class="col s2" style="margin-left: 20px;" readonly>


												<label class="col lebel s1">Doctor</label>

												<input value="IVN" type="text" class="col s1" id="doctor_code" onblur="doctorSearch()" name="doctor_code">

												<input value="dr. Ivan Rizal Sini, Sp.OG" type="text" class="col s2" id="doctor_name" style="margin-left: 20px;" readonly>

										</div>

										<div class="row">
											<label class="col lebel">Date Reservation</label>
											<input type="text" class="datepicker_res date-now" name="reservation_date" style="width: 100px;">
											<i class="material-icons" style="padding-top: 10px;">date_range</i>

											<button style="margin-left: 5%" type="submit" class="waves-effect waves-light btn" href="#">OK</button>
											<a style="margin-left: 5%" class="waves-effect waves-light btn" href="#">Print</a>
										</div>
									</form>

									{{--<div class="col s6">--}}
										{{--<a style="margin-left: 5%" class="waves-effect waves-light btn modal-trigger" href="#newPatient">New Patient</a>--}}
										{{--<a style="margin-left: 5%" class="waves-effect waves-light btn modal-trigger" href="#newPatient">Old Patient</a>--}}
									{{--</div>--}}

								</div>
								<div class="card-panel" id="data_reservation" style="display:none">
									<a style="margin-left: 5%;" class="waves-effect waves-light btn modal-trigger" href="#newPatient"><i class="material-icons">add</i></a>
									<table id="reservation" class="display responsive-table datatable-example" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>Session</th>
												<th>Morula No</th>
												<th>First Name</th>
												<th>Last Name</th>
												<th>Date of Birth</th>
												<th>Sex</th>
												<th>Mobile No</th>
												<th>Operator</th>
												<th>Description</th>
												<th>Visit</th>
												<th>Actions</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			{{--Main Tab Registration--}}
			<div id="reg" class="tab-content">
				<div class="row">
					<div class="col s12 m12 l4">
						<div class="card-panel">
							<table class="striped scroll" id="doctor_list">
								<thead>
									<tr>
										<th>Doctor</th>
										<th>Specialization</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col s12 m12 l4">
						<div class="card">
							<div class="card-content">

								<span class="card-title">Consultation</span>
								<div class="row" style="margin-bottom: 0px">
									<label class="col lebel" style="text-align: left;margin-right: 0px;width: 8em;">Appointment</label>
									<input type="text" class="timepicker col" id="date_from" style="width: 63px;" >

									<label class="col lebel s1">To</label>
									<input type="text" class="timepicker col" id="date_to" style="width: 63px;">
								</div>
								<div class="row" style="margin-bottom: 0px">
									<label class="col lebel" style="text-align: left;margin-right: 0px;width: 8em;">Count</label>
									<input type="text" class="col s2" id="ttl_patient">
								</div>
								<div class="row">
									<a class="waves-effect waves-light btn col" href="#">Init Patient</a>
									<a style="margin-left: 2%;" class="waves-effect waves-light btn col" href="#">Morula Patient</a>
									<a style="margin-left: 2%;" class="waves-effect waves-light btn col" href="#">Daily Patient</a>
								</div>
							</div>
						</div>
					</div>

					<div class="col s12 m12 l4">
						<div class="card">
							<div class="card-content">

								<span class="card-title">Doctor</span>
								<div class="row" style="margin-bottom: 0px">
									<label class="col lebel" style="text-align: left;margin-right: 0px;width: 8em;">Name</label>
									<input type="text" class="col s4" id="dokter">
								</div>
								<div class="row" style="margin-bottom: 0px">
									<label class="col lebel" style="text-align: left;margin-right: 0px;width: 8em;">Specialist</label>
									<input type="text" class="col s4" id="specialist">
								</div>
								<div class="row" style="margin-bottom: 0px">
									<label class="col lebel" style="text-align: left;margin-right: 0px;width: 8em;">Status</label>
									<input type="text" class="col s4" id="status">
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="card-panel">
					<table class="example display responsive-table datatable-example">
						<thead>
							<tr>
								<th>NO</th>
								<th>Session</th>
								<th>MR NO</th>
								<th>Full Name</th>
								<th>Sex</th>
								<th>Date of Birth</th>
								<th>Attendance</th>
								<th>Register No</th>
								<th>Phone Number</th>
								<th>Repl. Doctor</th>
								<th>Age</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>01</td>
								<td>07:00</td>
								<td>123</td>
								<td>Agus Setiawan</td>
								<td>M</td>
								<td>00</td>
								<td>-</td>
								<td>-</td>
								<td>-</td>
								<td>-</td>
								<td>-</td>
								<td>-</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			{{--Main Tab Daily Patient--}}
			<div id="dp" class="tab-content">
				<div class="card-panel">
				<label for="daily_patient" class="active" style="font-size: 1rem;">Date :</label>&nbsp
				<input type="text" id="daily_patient" class="datepicker" style="width: 150px;">
				</div>
			</div>

			{{--Main Tab Master Patient--}}
			<div id="mp" class="tab-content">
				<div class="card-panel">
				{{--<a class="waves-effect waves-light btn modal-trigger" href="#modal1">Modal</a>--}}
					<table id="masterPatient" class="display responsive-table datatable-example" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Morula ID</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Place Birth</th>
								<th>Date Birth</th>
								<th>F/M</th>
								<th>Doctor</th>
								<th>Phone No And HP</th>
								<th>Actions</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
    <!--/div-->

	{{--Modals--}}
    <div id="newPatient" class="modal  modal-fixed-footer">
        <div class="modal-content">
            <h4 style="font-size: 20px;padding-left: 45%;">New Patient</h4>
            <br>
            <form method="post" id="submit_newPatient">
            <div class="row">
                <label class="col lebel">Session</label>
                <input type="text" name="session" class="timepicker col s1">
                <i class="material-icons" style="padding-top: 10px;">access_time</i>
            </div>

            <div class="row">
                <label class="col lebel">First Name</label>
                <input type="text" name="first_name" class="col s8">
            </div>

            <div class="row">
                <label class="col lebel">Last Name</label>
                <input type="text" name="last_name" class="col s8">
            </div>


            <input type="hidden" name="init_doctor" value="1">
            <input type="hidden" name="id_number" value="12738213">
            <input type="hidden" name="id_type" value="1">
                <input type="hidden" name="reservation_date" value="2017-09-12">
                <input type="hidden" name="visit_code" value="1">


            <div class="row">
                <label class="col lebel">Sex</label>
                <select name="sex" class="col s2">
                    <option value="" disabled selected></option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                </select>
            </div>

            <div class="row">
                <label class="col lebel">Date of Birth</label>
                <input type="text" class="datepicker col" name="birth_date" style="width: 100px;">
                <i class="material-icons" style="padding-top: 10px;">date_range</i>
            </div>

            <div class="row">
                <label class="col lebel">Mobile no 1</label>
                <input type="text" name="mobile" class="col s8">
            </div>

            <div class="row">
                <label class="col lebel">Mobile no 2</label>
                <input type="text" name="mobile_2" class="col s8">
            </div>

            {{--<div class="row">--}}
                {{--<label class="col lebel">Operator</label>--}}
                {{--<input type="text" name="operator" readonly class="col s8">--}}
            {{--</div>--}}

            <div class="row">
                <label class="col lebel">Description</label>
                <textarea class="materialize-textarea col s8" name="description" data-length="120"></textarea>
            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
            <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cencel</a>
        </div>
        </form>
    </div>
</main>
@endsection

@section('js')
<script>
	$(document).ready(function() {

	   var dtables = $('.example').DataTable({
			language: {
				searchPlaceholder: 'Search records',
				sSearch: '',
				sLengthMenu: 'Show _MENU_',
				sLength: 'dataTables_length',
				oPaginate: {
					sFirst: '<i class="material-icons">chevron_left</i>',
					sPrevious: '<i class="material-icons">chevron_left</i>',
					sNext: '<i class="material-icons">chevron_right</i>',
					sLast: '<i class="material-icons">chevron_right</i>'
				}
			}
		});
		/*For Custom Filter
		$('#txt_name').on( 'keyup', function () {
			 dtables
				 .columns(4)
				 .search(this.value)
				 .draw();
		 });*/

		//Submit New Patient Reservation
		$('#reservation_form').submit(function(e){
			$("#reservation").DataTable().destroy();
			e.preventDefault(); // Prevent Default Submission
			$("#data_reservation").hide();
			$("#data_reservation").show();
			var theData = $(this).serializeArray();
		$('#reservation').DataTable( {
			language: {
				searchPlaceholder: 'Search records',
				sSearch: '',
				sLengthMenu: 'Show _MENU_',
				sLength: 'dataTables_length',
				oPaginate: {
					sFirst: '<i class="material-icons">chevron_left</i>',
					sPrevious: '<i class="material-icons">chevron_left</i>',
					sNext: '<i class="material-icons">chevron_right</i>',
					sLast: '<i class="material-icons">chevron_right</i>'
				}
			},
			"processing": true,
			"serverSide": true,
			"ajax":{
				"url": "/api/admission/reservation_show_all",
				"dataType": "json",
				"type": "POST",
				"data": function(d) {
					var frm_data = theData;
					$.each(frm_data, function(key, val) {
						d[val.name] = val.value;
					});
				},
				"headers": {
					"authorization": "Bearer uEzFSH1CTVPj0mf2VLRmmtGVvNWUQwBR0u5QykJzFskllHEsEr7Z7dU806YcvyD23k2DgH5WLTYyU86TkepxeQRtY5gSINSuTuXX"
				}
			},
			"columns": [
				/*{
				 "data": "id",
				 render: function (data, type, row, meta) {
				 return meta.row + meta.settings._iDisplayStart + 1;
				 }
				 },*/

				{ "data": "session", "render": function(data, type, row){
						return row.session + '<a style="margin-left: 5%;" class="btn-floating modal-trigger"  href="#newPatient"><i class="material-icons">add</i></a>';
					}
				},
				{ "data": "morula_id" },
				{ "data": "first_name" },
				{ "data": "last_name" },
				{ "data": "birth_date" },
				{ "data": "sex" },
				{ "data": "mobile" },
				{
					"mRender": function (data, type, row) {
						return 'Operator';
					}
				},
				{ "data": "description" },
				{ "data": "visit" },
				{
					"mRender": function () {
						return '<a href="/detail_patient" class="btn-floating"><i class="material-icons">zoom_in</i></a><a style="margin-left: 5%;" href="" class="btn-floating"><i class="material-icons">delete</i></a>';
					}
				}


			],

		} );
			$('.dataTables_length select').addClass('browser-default');

		});

		//Submit New Patient Reservation
		$('#submit_newPatient').submit(function(e){

			e.preventDefault(); // Prevent Default Submission

			$.ajax({
				url: '/api/admission/reservation_store',
				type: 'POST',
				headers:{"Authorization": 'Bearer uEzFSH1CTVPj0mf2VLRmmtGVvNWUQwBR0u5QykJzFskllHEsEr7Z7dU806YcvyD23k2DgH5WLTYyU86TkepxeQRtY5gSINSuTuXX'},
				data: $(this).serialize() // it will serialize the form data
			})
			/*.done(function(data){
				$('#form-content').fadeOut('slow', function(){
					$('#form-content').fadeIn('slow').html(data);
				});
			})*/
			.fail(function(){
				alert('Ajax Submit Failed ...');
			});
		});

		$(window).on('hashchange',function(){
			$('h1').text(location.hash.slice(1));
		});

		$("#registration").on("click", function(evt) {
			doctorShowAll();
		});


		$("#masterpasien").on("click", function(evt) {

			/*evt.preventDefault();
			 var id = $(this).data("tabContent");
			 $("#tabsContentId")
			 .find(".tab-content[data-tab-content='" + id + "']").show()
			 .siblings().hide();*/

			$("#masterPatient").DataTable().destroy();
			$('#masterPatient').DataTable( {
				language: {
					searchPlaceholder: 'Search records',
					sSearch: '',
					sLengthMenu: 'Show _MENU_',
					sLength: 'dataTables_length',
					oPaginate: {
						sFirst: '<i class="material-icons">chevron_left</i>',
						sPrevious: '<i class="material-icons">chevron_left</i>',
						sNext: '<i class="material-icons">chevron_right</i>',
						sLast: '<i class="material-icons">chevron_right</i>'
					}
				},
				"processing": true,
				"serverSide": true,
				"ajax":{
					"url": "/admission/patient_ajax",
					"dataType": "json",
					"type": "POST",
					"headers": {
						"X-CSRF-TOKEN": "{{ csrf_token() }}"
					}
				},
				"columns": [
					{
						"data": "id",
						render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
						}
					},

					{ "data": "company_id"},
					{ "data": "first_name" },
					{ "data": "last_name" },
					{ "data": "birth_place" },
					{ "data": "birth_date" },
					{ "data": "sex" },
					{ "data": "init_doctor" },
					{ "data": "phone" },
					{
						"mRender": function (data, type, row) {
							return '<a href="/admission/patient/' + row.id +'" class="btn">Detail</a>';
						}
					}

				],



			});
			$('.dataTables_length select').addClass('browser-default');

		});

	} );

	function doctorSearch(){
		var doctorCode = $('#doctor_code').val();
		var getData;
		$.ajax({
			type: 'POST',
			url: '/api/admission/doctor_search_by_code',
			data: {
				'code' : doctorCode,
			},
			headers:{"Authorization": 'Bearer uEzFSH1CTVPj0mf2VLRmmtGVvNWUQwBR0u5QykJzFskllHEsEr7Z7dU806YcvyD23k2DgH5WLTYyU86TkepxeQRtY5gSINSuTuXX'},
			dataType: 'json',
			success: function(data) {
				var isi = data.data;
				$('#doctor_name').val(isi);

			}

		});

	};

	function doctorShowAll(){
		//var doctorCode = $('#doctor_code').val();

		$.ajax({
			type: 'POST',
			url: '/api/admission/doctor_show_all',

			headers:{"Authorization": 'Bearer uEzFSH1CTVPj0mf2VLRmmtGVvNWUQwBR0u5QykJzFskllHEsEr7Z7dU806YcvyD23k2DgH5WLTYyU86TkepxeQRtY5gSINSuTuXX'},
			dataType: 'json',
			success: function(data) {
				var isi = (data.data);
				console.log(isi.length);
				var content = '';
				//content += '<tbody>'; -- **superfluous**
				for (var i = 0; i < isi.length; i++) {
					content += '<tr id="' + isi[i].id + '">';
					//content += '<td><input id="check_' + json[i].ID + '" name="check_' + json[i].ID + '" type="checkbox" value="' + json[i].ID + '" autocomplete=OFF /></td>';
					content += '<td>' + isi[i].name + '</td>';
					content += '<td>' + isi[i].specialization + '</td>';
					content += '<td>' + isi[i].status + '</td>';
					//content += '<td>' + json[i].District + '</td>';
					//content += '<td>' + json[i].Population + '</td>';
					//content += '<td><a href="#" class="edit">Edit</a> <a href="#" class="delete">Delete</a></td>';
					content += '</tr>';
				}
			   // content += '</tbody>';-- **superfluous**
				//$('table tbody').replaceWith(content);  **incorrect..**
				$('#doctor_list tbody').html(content);

			}
		});

	};
</script>
@endsection
