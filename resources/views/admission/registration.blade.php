@extends('layouts.layout')
@section('content')
    <main class="mn-inner">

        <div class="row">
            <div class="col s12">

                <table>
                    <tr>
                        <td>
                        <div id="doctor_list">

                        </div>
                        </td>
                    </tr>
                </table>



            </div>
        </div>


    </main>

    {{--Modal New Patient--}}
    <div id="newPatient" class="modal  modal-fixed-footer">
        <div class="modal-content">
            <h4 style="font-size: 20px;padding-left: 45%;">New Patient</h4>
            <br>
            <form method="post" id="submit_newPatient">
                <div class="row">
                    <label class="col lebel">Session</label>
                    <input type="text" name="session" class="timepicker col s1">
                    <i class="material-icons" style="padding-top: 10px;">access_time</i>
                </div>

                <div class="row">
                    <label class="col lebel">First Name</label>
                    <input type="text" name="first_name" class="col s8">
                </div>

                <div class="row">
                    <label class="col lebel">Last Name</label>
                    <input type="text" name="last_name" class="col s8">
                </div>


                <input type="hidden" name="init_doctor" value="1">
                <input type="hidden" name="id_number" value="12738213">
                <input type="hidden" name="id_type" value="1">
                <input type="hidden" name="reservation_date" value="2017-09-12">
                <input type="hidden" name="visit_code" value="1">


                <div class="row">
                    <label class="col lebel">Sex</label>
                    <select name="sex" class="col s2">
                        <option value="" disabled selected></option>
                        <option value="1">Male</option>
                        <option value="2">Female</option>
                    </select>
                </div>

                <div class="row">
                    <label class="col lebel">Date of Birth</label>
                    <input type="text" class="datepicker col" name="birth_date" style="width: 100px;">
                    <i class="material-icons" style="padding-top: 10px;">date_range</i>
                </div>

                <div class="row">
                    <label class="col lebel">Mobile no 1</label>
                    <input type="text" name="mobile" class="col s8">
                </div>

                <div class="row">
                    <label class="col lebel">Mobile no 2</label>
                    <input type="text" name="mobile_2" class="col s8">
                </div>

                <div class="row">
                    <label class="col lebel">Description</label>
                    <textarea class="materialize-textarea col s8" name="description" data-length="120"></textarea>
                </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
            <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cencel</a>
        </div>
        </form>
    </div>

    {{--Modals Search Patient--}}
    <div id="searchPatient" class="modal  modal-fixed-footer">
        <div class="modal-content">
            <h4 style="font-size: 20px;padding-left: 40%;">Search Patient</h4>
            <br>
            <form method="post" id="">
                <div class="row">
                    <label class="col lebel">Date of Birth</label>
                    <input type="text" class="datepicker col" name="birth_date" style="width: 100px;">
                    <i class="material-icons col" style="padding-top: 10px;">date_range</i>
                    <label class="col s1 lebel">Name</label>
                    <input type="text" name="first_name" class="col s3">
                    <a class="btn-floating waves-effect waves-light" style="margin-left: 20px;"><i class="material-icons">search</i></a>
                </div>

                <div class="row">
                    <table class="responsive-table highlight">
                        <thead>
                            <th>Name</th>
                            <th>Birth Date</th>
                            <th>Address</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Dodi</td>
                                <td>1989-27-04</td>
                                <td>Cempaka Putih</td>
                                <td><a class="btn-floating waves-effect waves-light"><i class="material-icons">add</i></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br>
                <br>
                <div class="row">
                    <a class="btn waves-effect waves-light col offset-s5" onclick="addNewPatient()"><i class="material-icons left">add</i>New Patient</a>
                </div>


        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-blue btn-flat ">Close</a>
        </div>
        </form>
    </div>
@endsection

@section('js')
    <script>
        function new_patient(){
            $('#searchPatient').openModal();
        }
        function addNewPatient(){
            $('#searchPatient').closeModal();
            $('#newPatient').openModal();
        }
        $(document).ready(function() {
            //var doctorCode = $('#doctor_code').val();

            $.ajax({
                type: 'POST',
                url: '/api/admission/doctor_show_all',

                headers: {"Authorization": 'Bearer uEzFSH1CTVPj0mf2VLRmmtGVvNWUQwBR0u5QykJzFskllHEsEr7Z7dU806YcvyD23k2DgH5WLTYyU86TkepxeQRtY5gSINSuTuXX'},
                dataType: 'json',
                success: function (data) {
                    var isi = (data.data);
                    //console.log(isi.length);
                    var content = '';
                    //content += '<tbody>'; -- **superfluous**
                    for (var i = 0; i < isi.length; i++) {

                    content += '<div class="col s3 m3 l3">';
                        content += '<div class="card card-dashboard" id="' + isi[i].id + '">';
                        content += '<div class="card-content">';
                        content += '<div class="row" style="margin-bottom: 0;">';
                        content += '<img class="doctor-dp col s5 no-padding" src="/uploads/doctor/' + isi[i].photo +'">';
                        content += '<div class="col s7">';
                        content += '<span>' + isi[i].name +'</span><br>';
                        content += '<span>Session :</span><br>';
                        content += '<span>Patient :</span><br>';
                        content += '<span>New Attendant :</span><br>';
                        content += '<span>New Patient :</span><br>';
                        content += '</div></div></div>';
                        content += '<div class="card-action footer-doctor light-green accent-3">';
                        content += '</div></div></div>';


                    }
                    // content += '</tbody>';-- **superfluous**
                    //$('table tbody').replaceWith(content);  **incorrect..**
                    $('#doctor_list').html(content);
                    $('#doctor_list').addClass('animated fadeInUp');

                }
            });
        });

    </script>
    @endsection