@extends('layouts.layout')
@section('content')
    <main class="mn-inner">

        <div class="row">
            <div class="col s12">
                <div class="page-title">Detail Patient</div>
            </div>

            <div class="col s12 m12 l4">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title" style="margin-bottom: 0px;">Basic Info</span><br>
                        <div class="row">

                                <div class="row">

                                    <div class="col s6">
                                        <img class="detail-patient-dp" src="{{url('/assets/images/sample.jpg')}}">
                                    </div>
                                        <div class="col s6">
                                        <div class="row">
                                            <div class="input-field-detail">
                                                <input id="name" type="text" class="validate">
                                                <label for="name">Name</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field-detail">
                                                <input id="no_morula" type="text" class="validate">
                                                <label for="no_morula">No Morula</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field-detail">
                                                <input id="no_morula_ivf" type="text" class="validate">
                                                <label for="no_morula_ivf">No Morula(IVF)</label>
                                            </div>
                                        </div>

                                    </div>

                                    </div>
                            <div class="row">
                            <a style="margin-left: 5px;" class="waves-effect waves-light btn" href="#">Upload Photo</a>
                            <a style="margin-left: 10%;width: 140px;" class="waves-effect waves-light btn" href="#">Take Photo</a>
                            </div>
                                    <div class="row">
                                        <div class="input-field-detail col s3">
                                        <input id="doctor" type="text" class="validate">
                                        <label for="doctor">Doctor</label>
                                        </div>
                                        <input type="text" class="col s6" style="margin-left: 20px;" disabled>
                                    </div>


                                </div>

                    </div>
                </div>

                <div class="card">
                    <div class="card-content">
                        <span class="card-title" style="margin-bottom: 0px;">Emergency Contact</span><br>
                        <div class="row">

                            <div class="row">
                                <div class="input-field-detail col s12">
                                    <input type="text" class="validate">
                                    <label>Name</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field-detail col s12">
                                    <input type="text" class="validate">
                                    <label>Relationship</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field-detail col s12">
                                    <input type="text" class="validate">
                                    <label>Address</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s6" style="height: 35px;margin-top: 0px;">
                                    <select>
                                        <option value="" disabled selected></option>
                                    </select>
                                    <label>City</label>
                                </div>
                                <div class="input-field-detail col s6">
                                    <input type="text" class="validate">
                                    <label>Post Code</label>
                                </div>
                            </div>


                            <div class="row">
                                <div class="input-field-detail col s12">
                                    <input type="text" class="validate">
                                    <label>Phone Number</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field-detail col s12">
                                    <input id="email" type="text" class="validate">
                                    <label for="email">Email</label>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

         </div>


            <div class="col s12 m12 l4">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title" style="margin-bottom: 0px;">Place & Date Birt,Sex</span><br>
                        <div class="row">

                                <div class="row">
                                    <div class="input-field-detail col s12">
                                        <input id="place" type="text" class="validate">
                                        <label for="place">Place</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field-detail col s6">
                                        <input id="" type="text" class="validate">
                                        <label for="">Date</label>
                                    </div>
                                    <div class="input-field col s6" style="height: 35px;margin-top: 0px;">
                                        <select>
                                            <option value="" disabled selected></option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                        </select>
                                        <label>Sex</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field-detail col s6">
                                        <input id="height" type="text" class="validate">
                                        <label for="height">Height</label>
                                    </div>
                                    <div class="input-field-detail col s6">
                                        <input id="blood" type="text" class="validate">
                                        <label for="blood">Blood</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12" style="height: 35px;margin-top: 0px;">
                                        <select>
                                            <option value="" disabled selected></option>
                                            <option value="1">Married</option>
                                            <option value="2">Single</option>
                                        </select>
                                        <label>Status</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field-detail col s12">
                                    <input id="dom" type="text" class="validate">
                                    <label for="dom">Date of Married</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12" style="height: 35px;margin-top: 0px;">
                                        <select>
                                            <option value="" disabled selected></option>

                                        </select>
                                        <label>Religion</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12" style="height: 35px;margin-top: 0px;">
                                        <select>
                                            <option value="" disabled selected></option>
                                        </select>
                                        <label>Citizen</label>
                                    </div>
                                </div>


                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-content">
                        <span class="card-title" style="margin-bottom: 0px;">Place & Date Birth</span><br>
                        <div class="row">

                            <div class="row">
                                <div class="input-field-detail col s6">
                                    <input id="" type="text" class="validate">
                                    <label for="">Place</label>
                                </div>
                                <div class="input-field-detail col s6">
                                    <input type="text" class="validate">
                                    <label>Date</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field-detail col s12">
                                    <input type="text" class="validate">
                                    <label>Blood Type</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12" style="height: 35px;margin-top: 0px;">
                                    <select>
                                        <option value="" disabled selected></option>

                                    </select>
                                    <label>Religion</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12" style="height: 35px;margin-top: 0px;">
                                    <select>
                                        <option value="" disabled selected></option>
                                    </select>
                                    <label>Citizen</label>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>

            <div class="col s12 m12 l4">
                <div class="card">
                    <div class="card-content">
                        <span class="card-title" style="margin-bottom: 0px;">Address,City,Post Code, Phone No</span><br>
                        <div class="row">

                            <div class="row">
                                <div class="input-field-detail col s12">
                                    <input id="address" type="text" class="validate">
                                    <label for="address">Address</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s6" style="height: 35px;margin-top: 0px;">
                                    <select>
                                        <option value="" disabled selected></option>
                                    </select>
                                    <label>City</label>
                                </div>
                                <div class="input-field-detail col s6">
                                    <input type="text" class="validate">
                                    <label>Post Code</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field-detail col s6">
                                    <input type="text" class="validate">
                                    <label>Phone</label>
                                </div>
                                <div class="input-field-detail col s6">
                                    <input type="text" class="validate">
                                    <label>HP</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field-detail col s12">
                                    <input type="text" class="validate">
                                    <label>Identity Number</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s6" style="height: 35px;margin-top: 0px;">
                                    <select>
                                        <option value="" disabled selected></option>
                                    </select>
                                    <label>Job</label>
                                </div>
                                <div class="input-field-detail col s6">
                                    <input type="text" class="validate">
                                    <label>Office No</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field-detail col s12">
                                    <input id="email" type="text" class="validate">
                                    <label for="email">Email</label>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-content">
                        <span class="card-title" style="margin-bottom: 0px;">Address,City,Post Code, Phone No</span><br>
                        <div class="row">

                            <div class="row">
                                <div class="input-field-detail col s12">
                                    <input type="text" class="validate">
                                    <label>Address</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s6" style="height: 35px;margin-top: 0px;">
                                    <select>
                                        <option value="" disabled selected></option>
                                    </select>
                                    <label>City</label>
                                </div>
                                <div class="input-field-detail col s6">
                                    <input type="text" class="validate">
                                    <label>Post Code</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field-detail col s6">
                                    <input type="text" class="validate">
                                    <label>Phone</label>
                                </div>
                                <div class="input-field-detail col s6">
                                    <input type="text" class="validate">
                                    <label>HP</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field-detail col s12">
                                    <input type="text" class="validate">
                                    <label>Identity Number</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s6" style="height: 35px;margin-top: 0px;">
                                    <select>
                                        <option value="" disabled selected></option>
                                    </select>
                                    <label>Job</label>
                                </div>
                                <div class="input-field-detail col s6">
                                    <input type="text" class="validate">
                                    <label>Office No</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field-detail col s12">
                                    <input id="email" type="text" class="validate">
                                    <label for="email">Email</label>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>

            </div>


    </div>




    </main>


@endsection