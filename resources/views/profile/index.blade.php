@extends('layouts.layout')
@section('content')
	<main class="mn-inner">
		<div class="col s12">

								<form enctype="multipart/form-data" action='{{ route("profile_update") }}' class="col s12 m12" method="post" id="user_form_edit">
								{{ csrf_field() }}
									<div class="card-content">
										<div class="card-panel" id="data_reservation">
											<div id="newPatient" class="">
												<div class="modal-content">
													<h4 style="font-size: 20px;padding-left: 45%;">Edit User</h4><br>
													<div class="row">
														<div class="cropFrame col offset-l4" style="width: 150px;height: 150px;">
														<img id="upload-image" class="upload-dp cropImage" src="{{ $user->photo != '' ? "/uploads/user/$user->photo" : "/assets/images/avatar-default.png" }}">
														</div>
													</div>
													<div class="row ">
														<label class="col lebel"></label>
														<div class="file-field input-field">
															<div class="btn teal lighten-1">
																<span>Photo Upload</span>
																<input name="photo" id="photo" type="file">
															</div>
															<div class="file-path-wrapper">
																<input class="col s6 file-path validate" type="text">
															</div>
														</div>
													</div>
													<div class="row">
														<label class="col lebel">Username</label>
														<input value="{{ old('username', $user->username) }}" type="text" placeholder="Username" class="col s8" disabled>
														<span class="label label-danger red-text text-darken-1">{{ $errors->update->first('username') }}</span>
													</div>
													<div class="row">
														<label class="col lebel">Password</label>
														<input value="" type="password" name="passw" placeholder="Kosongkan jika tidak ingin mengubah password" class="col s8">
														<span class="label label-danger red-text text-darken-1">{{ $errors->update->first('password') }}</span>
													</div>
													<div class="row">
														<label class="col lebel">Full Name</label>
														<input value="{{ old('name', $user->name) }}" type="text" name="name" placeholder="Full Name" class="col s8">
														<span class="label label-danger red-text text-darken-1">{{ $errors->update->first('name') }}</span>
													</div>
													<div class="row">
														<label class="col lebel">Email</label>
														<input value="{{ old('email', $user->email) }}" type="text" name="email" placeholder="Email" class="col s8">
														<span class="label label-danger red-text text-darken-1">{{ $errors->update->first('email') }}</span>
													</div>
													<div class="row">
														<label class="col lebel">Phone</label>
														<input value="{{ old('phone', $user->phone) }}" type="text" name="phone" placeholder="Phone" class="col s8">
														<span class="label label-danger red-text text-darken-1">{{ $errors->update->first('phone') }}</span>
													</div>
													<div class="row">
														<label class="col lebel">Description</label>
														<textarea class="materialize-textarea col s8" name="description" data-length="120">{{ old('description', $user->description) }}</textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="row" style="margin-top: 20px;">
											</div>
											<div class="row">
												<a style="margin-left: 5%;" class="waves-effect waves-light btn modal-trigger" href="{{ url('/') }}">Back</a>
												<button style="margin-left: 5%" type="submit" class="waves-effect waves-light btn" href="#">Save</button>
											</div>
										</div>
									</div>
								</form>



		</div>

		{{--Modals--}}
		
	</main>
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $('#upload-image').cropbox({
                width: 120,
                height: 160
            }, function() {
                //on load
                console.log('Url: ' + this.getDataURL());
            }).on('cropbox', function(e, data) {
                console.log('crop window: ' + data);
            });
        });


		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#upload-image').attr('src', e.target.result);
				};
				reader.readAsDataURL(input.files[0]);
			}
		}

		$("#photo").change(function(){
			readURL(this);
		});
		
		@if (Session::has('after_save'))
            function alertSuccess(){
				swal({ title: "{{ Session::get('after_save.title') }}",
					text: "{{ Session::get('after_save.text-1') }} {{ Session::get('after_save.text-2') }}",
					type: "{{ Session::get('after_save.alert') }}",
					showConfirmButton: false,
					timer: 2000,
				})
			}
			alertSuccess();
        @endif
	</script>
@endsection
