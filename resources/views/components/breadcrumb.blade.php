<ol class="breadcrumb">
	<li><a href="{{ route('home') }}"><span class="fa fa-home"></span> Home</a></li>
	@php
	$segments	= '';
	$num		= count(Request::segments());
	$i			= 0;
	foreach(Request::segments() as $segment) {
		$segments .= '/'.$segment;
		$i++;
		if($i != $num) {
			echo "<li><a href='". url($segments) ."'>". ucfirst(trans($segment)) ."</a></li>";
		} else {
			echo "<li>". ucfirst(trans($segment)) ."</li>";
		}
	}
	@endphp
</ol>