@extends('layouts.layout')
@section('content')
    <main class="mn-inner">

        <div class="row">
            <div class="card hoverable col s4 offset-s4 card-search-patient">

                <div class="card-content">
                    <div class="row">
                        <div class="input-field center-align">
                        Please Insert Morula ID for Existing Patient
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field card-nurse">
                            <input type="text" >
                            <label>Morula ID</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field card-nurse">
                            <input type="text" disabled>
                            <label>Nama Patient</label>
                        </div>
                    </div>

                    <div class="row">
                        <a href="#" class="btn waves-effect col s4 offset-s4 cream">Submit</a>
                    </div>
                    <div class="row">
                        <a href="#" class="btn waves-effect col s6 offset-s3 teal accent-4">New Patient</a>
                    </div>

                </div>

            </div>
        </div>

    </main>


@endsection