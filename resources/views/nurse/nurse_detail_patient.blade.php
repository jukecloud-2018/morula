@extends('layouts.layout')
@section('content')
    <main class="mn-inner">

        <div class="row">
            <div class="col s12 m12 l3"></div>
        <div class="col s12 m12 l6">
            <div class="card">
                <div class="card-content">
                    <span class="card-title" style="margin-bottom: 0px;">PLACE & DATE BIRTH, SEX</span><br>

                    <div class="row">

                            <div class="col s4">
                                <img class="detail-patient-dp" src="{{url('assets/images/sample.jpg')}}">
                            </div>
                            <div class="col s8">
                                <div class="row">
                                    <div class="input-field-detail col s12">
                                        <input value="Cempaka Putih Jakarta Pusat" id="place" type="text" class="validate">
                                        <label for="place">Place</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field-detail col s6">
                                        <input value="2017-09-22" id="" type="text" class="validate">
                                        <label for="">Date</label>
                                    </div>
                                    <div class="input-field col s6" style="height: 35px;margin-top: 0px;">
                                        <select>
                                            <option value="" disabled></option>
                                            <option value="1">Male</option>
                                            <option value="2" selected>Female</option>
                                        </select>
                                        <label>Sex</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field-detail col s6">
                                        <input value="178" id="height" type="text" class="validate">
                                        <label for="height">Height</label>
                                    </div>
                                    <div class="input-field-detail col s6">
                                        <input value="O" id="blood" type="text" class="validate">
                                        <label for="blood">Blood</label>
                                    </div>
                                </div>

                            </div>


                        </div>

                    <div class="row">

                        <div class="row">
                            <div class="input-field col s3" style="height: 35px;margin-top: 0px;">
                                <select>
                                    <option value="" disabled></option>
                                    <option value="1" selected>Married</option>
                                    <option value="2">Single</option>
                                </select>
                                <label>Status</label>
                            </div>

                            <div class="input-field-detail col s3">
                                <input value="2010-09-27" id="dom" type="text" class="validate">
                                <label for="dom">Date of Married</label>
                            </div>


                            <div class="input-field col s3" style="height: 35px;margin-top: 0px;">
                                <select>
                                    <option value="" disabled></option>
                                    <option value="1" disabled selected>Muslim</option>

                                </select>
                                <label>Religion</label>
                            </div>

                            <div class="input-field col s3" style="height: 35px;margin-top: 0px;">
                                <select>
                                    <option value="1" disabled selected>Indonesian</option>
                                </select>
                                <label>Citizen</label>
                            </div>

                        </div>




                      </div>

                    </div>

                <div class="card-action teal lighten-2">

                    <div class="row white-input ">
                        <div class="row">
                            <div class="col s6">
                                <label for="height" class="col lebel white-text">Height</label>
                                <input type="number" class="text-center col s4 white-text validate">
                            </div>
                            <label class="white-text lebel col s1" style="padding-left: 0px;margin-right: 0px">Weight</label>
                            <div class="col s5">
                                <input type="number" class="text-center col s5 white-text validate">
                            </div>
                        </div>
                    </div>

                    <div class="row white-input ">
                        <div class="row">
                            <div class="col s6">
                                <label for="height" class="col lebel white-text">Body Mass Index</label>
                                <input type="text" class="text-center col s4 white-text validate">
                            </div>
                            <i class="material-icons white-text col s1" style="padding-top: 20px;">chevron_right</i>
                            <div class="col s5">
                                <input type="text" class="text-center col s5 white-text validate">
                            </div>
                        </div>
                    </div>

                    <div class="row white-input ">
                        <div class="row">
                            <div class="col s6">
                                <label for="height" class="col lebel white-text">Blood Pressure</label>
                                <input type="text" class="text-center col s4 white-text validate">
                            </div>
                            <i class="material-icons white-text col s1" style="padding-top: 20px;">/</i>
                            <div class="col s5">
                                <input type="text" class="text-center col s5 white-text validate">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col s6" style="margin-left: 150px;">
                        <a class="col waves-effect waves-light btn white">Save</a>
                        <a class="col offset-s6 waves-effect waves-light btn white">Cencel</a>
                        </div>
                    </div>

                </div>


                </div>

             </div>
        </div>


        </div>

    </main>


@endsection