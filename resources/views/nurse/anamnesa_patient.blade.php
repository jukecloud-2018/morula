@extends('layouts.layout')
@section('content')
    <main class="mn-inner">

        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="teal accent-3 center-align card-title">Anamnesa Umum</div>
                </div>

{{--Wife--}}
                <div class="col s6 no-padding-left">
                <div class="card">
                    <div class="center-align card-title teal accent-3">Wife</div>
                        <div class="card-content">
                            {{--The Form Here--}}
                            <div class="row">
                                <div class="col s12 l2 no-padding">
                                    <label for="height" class="col s4 lbl no-padding-left">Usia</label>
                                    <input type="number" class="col s6 validate no-padding">
                                </div>

                                <div class="col s3 no-padding">
                                    <label for="height" class="col s7 lbl no-padding">Lama Menikah</label>
                                    <input type="number" class="col s4 validate no-padding">
                                </div>

                                <div class="col s12 l2">
                                    <label for="height" class="col s4 lbl">P</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>
                                <div class="col s12 l2">
                                    <label for="height" class="col s4 lbl">A</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>
                                <div class="col s12 l2">
                                    <label for="height" class="col s4 lbl">E</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s12 l2 no-padding">
                                    <label for="height" class="col s4 lbl no-padding-left">KB</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>

                                <div class="col s3 no-padding-hor">
                                    <label for="height" class="col s4 lbl">Lama</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>

                                <div class="col s12 l5 no-padding-hor">
                                    <label for="height" class="col s4 lbl">Dimana</label>
                                    <input type="text" class="col s4 validate no-padding">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s12 l3 no-padding">
                                    <label for="height" class="col s5 lbl no-padding-left">Siklus Haid</label>
                                    <input type="number" class="col s6 validate no-padding">
                                </div>

                                <div class="col s3 no-padding-hor">
                                    <label for="height" class="col s4 lbl">Lama</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>

                                <div class="col s12 l5 no-padding-hor">
                                    <label for="height" class="col s4 lbl">Dism</label>
                                    <input type="text" class="col s4 validate no-padding">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s3 no-padding">
                                    <label for="height" class="col s5 lbl no-padding-hor">HPHT</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>

                                <div class="col s4 no-padding">
                                    <label for="height" class="col s5 lbl no-padding-right">R.Keputihan</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>

                                <div class="col s5 no-padding">
                                    <label for="height" class="col s5 lbl no-padding-hor">R.Nyeri Perut Bawah</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>
                            </div>


                            <div class="row">
                                <div class="col s4">
                                    <label for="height" class="col s5 lbl no-padding-hor">R.Operasi</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>
                                <div class="col s4">
                                    <label for="height" class="col s5 lbl no-padding-hor">Kapan</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>
                                <div class="col s4">
                                    <label for="height" class="col s5 lbl no-padding-hor">Di</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s4">
                                    <label for="height" class="col s5 lbl no-padding-hor">Jenis Operasi</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>
                                <div class="col s4">
                                    <label for="height" class="col s5 lbl no-padding-hor">R.P.Serius</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>
                                <div class="col s4">
                                    <label for="height" class="col s5 lbl no-padding-hor">RPK</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s4">
                                    <label for="height" class="col s5 lbl no-padding-hor">Merokok</label>
                                    <input type="text" class="col s5 validate no-padding">
                                </div>
                                <div class="col s3">
                                    <label for="height" class="col s5 lbl no-padding-hor">Jumlah</label>
                                    <input type="number" class="col s5 validate no-padding">
                                </div>
                                <div class="col s5">
                                    <label for="height" class="col s5 lbl no-padding-hor">Minum Alkohol</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>
                            </div>


                            <div class="row">
                                <div class="col s6">
                                    <label for="height" class="col s5 lbl no-padding-hor">Obat-obatan Rutin</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>
                                <div class="col s6">
                                    <label for="height" class="col s3 lbl no-padding-hor">R.Alergi</label>
                                    <input type="text" class="col s6 validate no-padding">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s4">
                                    <label for="height" class="col s4 lbl no-padding-hor">R.Pap Smear</label>
                                    <input type="text" class="col s7 validate no-padding">
                                </div>
                                <div class="col s4">
                                    <label for="height" class="col s4 lbl no-padding-hor">Kapan</label>
                                    <input type="text" class="col s7 validate no-padding">
                                </div>
                                <div class="col s4">
                                    <label for="height" class="col s4 lbl no-padding-hor">Di</label>
                                    <input type="text" class="col s7 validate no-padding">
                                </div>
                            </div>

                            
                        </div>
                </div>
                </div>


{{--Husband--}}
                <div class="col s6 no-padding-right">
                <div class="card">
                    <div class="center-align card-title teal accent-3">Husband</div>
                    <div class="card-content">
                        {{--The Form Here--}}
                        <div class="row">
                            <div class="col s12 l2 no-padding">
                                <label for="height" class="col s4 lbl" style="padding-left: 0px;">Usia</label>
                                <input type="number" class="col s6 validate no-padding">
                            </div>

                            <div class="col s3 no-padding">
                                <label for="height" class="col s7 lbl no-padding">Lama Menikah</label>
                                <input type="number" class="col s4 validate no-padding">
                            </div>

                            <div class="col s12 l2">
                                <label for="height" class="col s4 lbl">P</label>
                                <input type="text" class="col s6 validate no-padding">
                            </div>
                            <div class="col s12 l2">
                                <label for="height" class="col s4 lbl">A</label>
                                <input type="text" class="col s6 validate no-padding">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s12 l2 no-padding">
                                <label for="height" class="col s4 lbl" style="padding-left: 0px;">SA</label>
                                <input type="text" class="col s6 validate no-padding">
                            </div>

                            <div class="col s3 no-padding-hor">
                                <label for="height" class="col s4 lbl">Kapan</label>
                                <input type="text" class="col s6 validate no-padding">
                            </div>

                            <div class="col s12 l5 no-padding-hor">
                                <label for="height" class="col s4 lbl">Dimana</label>
                                <input type="text" class="col s4 validate no-padding">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s4 no-padding">
                                <label for="height" class="col s5 lbl no-padding-hor">Hasil SA</label>
                                <input type="number" class="col s5 validate no-padding">
                            </div>

                            <div class="col s2 no-padding-left">
                                <input type="text" class="validate no-padding">
                            </div>

                            <div class="col s2 no-padding-right">
                                <input type="text" class="validate no-padding">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s6">
                                <label for="height" class="col s4 lbl no-padding-hor">R.P.K</label>
                                <input type="text" class="col s6 validate no-padding">
                            </div>
                            <div class="col s6">
                                <label for="height" class="col s4 lbl no-padding-hor">R.P. Serius</label>
                                <input type="text" class="col s6 validate no-padding">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s6">
                                <label for="height" class="col s4 lbl no-padding-hor">R.O Perut</label>
                                <input type="text" class="col s6 validate no-padding">
                            </div>
                            <div class="col s6">
                                <label for="height" class="col s4 lbl no-padding-hor">R.Trauma Genitalia</label>
                                <input type="text" class="col s6 validate no-padding">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s6">
                                <label for="height" class="col s4 lbl no-padding-hor">R.Infeksi Genitalia</label>
                                <input type="text" class="col s6 validate no-padding">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col s4">
                                <label for="height" class="col s5 lbl no-padding-hor">Merokok</label>
                                <input type="text" class="col s5 validate no-padding">
                            </div>
                            <div class="col s3">
                                <label for="height" class="col s5 lbl no-padding-hor">Jumlah</label>
                                <input type="number" class="col s5 validate no-padding">
                            </div>
                            <div class="col s5">
                                <label for="height" class="col s5 lbl no-padding-hor">Minum Alkohol</label>
                                <input type="text" class="col s6 validate no-padding">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col s6">
                                <label for="height" class="col s5 lbl no-padding-hor">Obat-obatan Rutin</label>
                                <input type="text" class="col s6 validate no-padding">
                            </div>
                            <div class="col s6">
                                <label for="height" class="col s3 lbl no-padding-hor">R.Alergi</label>
                                <input type="text" class="col s6 validate no-padding">
                            </div>
                        </div>


                    </div>
                </div>
                </div>

            </div>

            <div class="col s12">
                <div class="col offset-s4">
                <a href="#" class="btn waves-effect white">Save</a>
                <a href="#" class="btn waves-effect white" style="margin-left: 45px;">Print</a>
                <a href="#" class="btn waves-effect white" style="margin-left: 45px;">Cencel</a>
                </div>
            </div>


        </div>

    </main>


@endsection