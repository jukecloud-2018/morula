<aside id="slide-out" class="side-nav fixed">

    <div class="side-nav-wrapper">

        <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">

            <li class="no-padding {{{ ((Request::segment(2)=='user' || Request::segment(2)=='') ? 'active' : '') }}}"><a href="{{route('user.index')}}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/UserIcon_green.png"><br>User</a></li>

			<li class="no-padding {{{ ((Request::segment(2)=='role') ? 'active' : '') }}}"><a href="{{route('role.index')}}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/RoleIcon_green.png"><br>Role</a></li>
			
			<li class="no-padding {{{ ((Request::segment(2)=='doctor') ? 'active' : '') }}}"><a href="{{route('doctor.index')}}"><img class="sidebar-icon" src="/assets/images/icons/morula/light/Doctor_LightBrown.png"><br>Doctor</a></li>
			
			<li class="no-padding {{{ ((Request::segment(2)=='place') ? 'active' : '') }}}"><a href="{{route('place')}}"><img class="sidebar-icon" src="/assets/images/icons/morula/light/Place_LightBrown.png"><br>Place</a></li>
			
			<li class="no-padding {{{ ((Request::segment(2)=='menu') ? 'active' : '') }}}"><a href="{{route('menu.index')}}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/MenuSettingIcon_green.png"><br>Menu</a></li>

        </ul>

    </div>

</aside>
@section('js-sidebar')
    <script>
        new PerfectScrollbar('.side-nav');
    </script>
@endsection