<aside id="slide-out" class="side-nav fixed">
    <div class="side-nav-wrapper">
        <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">


            <li class="no-padding {{{ Request::segment(1)=='' || Request::segment(2)=='registration' || Request::segment(1)==''? 'active' : '' }}}"><a href="{{route('registration.index')}}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/Registration_green.png">Registration</a></li>

			<li class="no-padding"><a href="#new_registration" class="modal-trigger"><img class="sidebar-icon" src="/assets/images/icons/morula/white/NewRegistration_green.png"><br>New Registration</a></li>

			<li class="no-padding {{{ ((Request::segment(2)=='daily_patient') ? 'active' : '') }}}"><a href="{{ route('daily_patient') }}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/DailyPatient_green.png"><br>Daily Patient</a></li>

			<li class="no-padding {{{ ((Request::segment(2)=='master_patient') ? 'active' : '') }}}"><a href="{{ route('master_patient.index') }}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/MasterPatient_green.png"><br>Master Patient</a></li>

            <li class="no-padding {{{ ((Request::segment(2)=='doctor_schedule') ? 'active' : '') }}}"><a href="{{ route('doctor_schedule') }}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/DoctorSchedule_green.png">Doctor Schedule</a></li>

            <li class="no-padding {{{ ((Request::segment(2)=='program') ? 'active' : '') }}}"><a href="{{route('program')}}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/Program_green.png">Program</a></li>

            <li class="no-padding {{{ ((Request::segment(2)=='information') ? 'active' : '') }}}"><a href="{{route('information')}}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/Information_green.png">Information</a></li>

        </ul>
    </div>
</aside>

{{--Modals--}}
<div id="new_registration" class="modal modal-fixed-footer">
	<form method="post" id="submit_new_registration">
		<div class="modal-content">
			<h4>New Registration</h4>
			<br>
			<div class="row">
				<label class="col lebel">Visit</label>
				<select name="visist" class="col s4">
					<option value="Consultation">Consultation</option>
					<option value="IVF">IVF</option>
					<option value="TC">TC</option>
					<option value="OI">OI</option>
					<option value="Pregnancy Check">Pregnancy Check</option>
				</select>
			</div>
			<div class="row">
				<label class="col lebel">Doctor</label>
				<select name="doctor_code" class="doctor-list js-states browser-default" tabindex="-1" style="width: 50%" id="doctor-list">
					<option disabled selected></option>
				</select>
			</div>
			<div class="row">
				<label class="col lebel">Date</label>
				<input type="text" class="datepicker col" name="date" style="width: 100px;">
				<i class="material-icons" style="padding-top: 10px;">date_range</i>
			</div>
			<div class="row">
				<label class="col lebel">Session</label>
				<input placeholder="hh:mm" type="text" name="session" class="timepicker col s4">
				<i class="material-icons" style="padding-top: 10px;">access_time</i>
				
			</div>
			<div class="row">
				<label class="col lebel">&nbsp;</label>
				<input placeholder="hh:mm (Additional Session)" type="text" name="session_add" class="timepicker col s4">
				<i class="material-icons" style="padding-top: 10px;">access_time</i>
			</div>

			<div class="row">
				<label class="col lebel">Patient Name</label>
				<input type="hidden" id="patient_id" name="patient_id" readonly>
				<input type="text" id="patient_name" name="patient_name" class="find_patient col s6" readonly>
				
				<a href="#find_patient" class="modal-trigger find_patient waves-effect waves-light btn"><i class="material-icons">search</i></a>
				
				<a href="#new_patient" class="modal-trigger waves-effect waves-light btn"><i class="material-icons">add</i></a>
			</div>
			
			<div class="row">
				<label for="attendance" class="col lebel">Attendance</label>
				<input type="checkbox" name="attendance" id="attendance">
			</div>
		</div>
		<div class="modal-footer">
			<button type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
			<a href="javascript:;" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
		</div>
	</form>
</div>

<div id="find_patient" class="modal modal-fixed-footer">
	<form method="post" id="data_patient_form">
		<div class="modal-content">
			<h4>Find Patient</h4>
			
			<div class="row">
				
				<div class="row" style="margin-top: 20px;">
					<label class="col" style="padding-top: 10px;font-size: 14px;float: left;margin-right: 1em;text-align: right;"><i class="material-icons prefix">date_range</i></label>
					<input id="birth_date_find" value="1990-01-31" placeholder="Birth Date" type="text" class="datepicker col s2" name="birth_date">

					<label class="col" style="padding-top: 10px;font-size: 14px;float: left;margin-right: 1em;text-align: right;"><i class="material-icons prefix">account_circle</i></label>
					<input id="name_find" value="perdana" type="text" class="col s4" id="name" name="name" placeholder="First or Last Name" style="">
					
					<button id="find" style="margin-left: 3px" type="button" class="waves-effect waves-light btn" href="javascript:;">Find</button>
					
				</div>
				
				<div class="card-panel">
					<table id="data_patient_find" class="display responsive-table datatable-example" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Name</th>
								<th>Birth Date</th>
								<th>Address</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button id="new_patient_button" style="margin-left: 3px" type="button" class="waves-effect waves-light btn" href="javascript:;">Create New Patient</button>
		</div>
	</form>
</div>

<div id="new_patient" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>New Patient</h4>
		<form method="post" id="submit_newPatient">
		<!--div class="row">
			<label class="col lebel">Session</label>
			<input type="text" name="session" class="timepicker col s1">
			<i class="material-icons" style="padding-top: 10px;">access_time</i>
		</div-->

		<div class="row">
			<label class="col lebel">First Name</label>
			<input type="text" name="first_name" class="col s8">
		</div>

		<div class="row">
			<label class="col lebel">Last Name</label>
			<input type="text" name="last_name" class="col s8">
		</div>


		<input type="hidden" name="init_doctor" value="1">
		<input type="hidden" name="id_number" value="12738213">
		<input type="hidden" name="id_type" value="1">
		<input type="hidden" name="reservation_date" value="2017-09-12">
		<input type="hidden" name="visit_code" value="1">


		<div class="row">
			<label class="col lebel">Sex</label>
			<select name="sex" class="col s2">
				<option value="" disabled selected></option>
				<option value="1">Male</option>
				<option value="2">Female</option>
			</select>
		</div>

		<div class="row">
			<label class="col lebel">Date of Birth</label>
			<input type="text" class="datepicker col" name="birth_date" style="width: 100px;">
			<i class="material-icons" style="padding-top: 10px;">date_range</i>
		</div>

		<div class="row">
			<label class="col lebel">Mobile no 1</label>
			<input type="text" name="mobile" class="col s8">
		</div>

		<div class="row">
			<label class="col lebel">Mobile no 2</label>
			<input type="text" name="mobile_2" class="col s8">
		</div>

		{{--<div class="row">--}}
			{{--<label class="col lebel">Operator</label>--}}
			{{--<input type="text" name="operator" readonly class="col s8">--}}
		{{--</div>--}}

		<div class="row">
			<label class="col lebel">Description</label>
			<textarea class="materialize-textarea col s8" name="description" data-length="120"></textarea>
		</div>

	</div>
	<div class="modal-footer">
		<button type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
		<a href="javascript:;" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
	</div>
	</form>
</div>

@section('js-sidebar')
<script>
	$(document).ready(function() {
		var ps = new PerfectScrollbar('.side-nav');
		
		$('.find_patient').click(function(){
			$('#find_patient').openModal();
		});
		
		$('#find').click(function(){
			find_dt_patient();
		});
		
		$('#new_patient_button').click(function(){
			console.log('add_patient c');
			$('#find_patient').closeModal();
			$('#new_patient').openModal();
		});
		
		var dtables = $('#data_patient_find').DataTable({
			language: {
				searchPlaceholder: 'Search records',
				sSearch: '',
				sLengthMenu: 'Show _MENU_',
				sLength: 'dataTables_length',
				oPaginate: {
					sFirst: '<i class="material-icons">chevron_left</i>',
					sPrevious: '<i class="material-icons">chevron_left</i>',
					sNext: '<i class="material-icons">chevron_right</i>',
					sLast: '<i class="material-icons">chevron_right</i>'
				}
			}
		});
		
		$('#doctor_code').keyup(function(){
			console.log('keyup');
			doctorSearch();
		});
		
		$('#doctor-list').click(function(){
			console.log('data_doctor');
			$.ajax({
				type: 'POST',
				url: '/admission/doctor_list',
				dataType: 'json',
				async: false,
				success: function (data_doctor) {
					console.log(data_doctor);
					$(".doctor-list").select2({
						data: data_doctor
					});
				}
			});
		});
		
		$.ajax({
			type: 'POST',
			url: '/admission/doctor_list',
			dataType: 'json',
			async: false,
			success: function (data_doctor) {
				console.log(data_doctor);
				$(".doctor-list").select2({
					data: data_doctor
				});
			}
		});
		
		//$(".doctor-list").select2();
	});
	
	function find_dt_patient(){
		$("#data_patient_find").DataTable().destroy();
		
		$('#data_patient_find').DataTable( {
			language: {
				searchPlaceholder: 'Search records',
				sSearch: '',
				sLengthMenu: 'Show _MENU_',
				sLength: 'dataTables_length',
				oPaginate: {
					sFirst: '<i class="material-icons">chevron_left</i>',
					sPrevious: '<i class="material-icons">chevron_left</i>',
					sNext: '<i class="material-icons">chevron_right</i>',
					sLast: '<i class="material-icons">chevron_right</i>'
				}
			},
			"processing": true,
			"serverSide": true,
			"select" : true,
			"ajax":{
				"url": "{{ route('search_by_name_dt') }}",
				"dataType": "json",
				"type": "POST",
				"data": {
					'name' : $('#name_find').val(),
					'birth_date' : $('#birth_date_find').val(),
				}
			},
			"columns": [
				{ "mRender": function(data, type, row){
						return row.first_name + " " + row.last_name;
					}
				},
				{ "data": "birth_date" },
				{ "data": "address" },
				{
					"mRender": function (data, type, row, meta) {
						return '<a class="btn-floating" title="Add This Patient" onclick="add_patient(`' + row.id + '`, `' + row.first_name + ' ' + row.last_name + '`,`' + row.company_id + '`)"><i class="material-icons">add</i></a>';
					}
				}
			],
		});
		
		$('.dataTables_length select').addClass('browser-default');
	}
	
	function pad(str, max) {
		str = str.toString();
		return str.length < max ? pad("0" + str, max) : str;
	}
	
	function add_patient(id,name,company){
		console.log(name);
		$('#patient_id').val(id);
		$('#patient_name').val(name + ' ( ' + company + '-' + pad(id, 4) + ' )');
		$('#find_patient').closeModal();
	}
	
	function new_patient(){
		console.log('add_patient f');
		$('#new_patient').openModal();
	}
	
	function doctorSearch(){
		var doctorCode = $('#doctor_code').val();
		if(doctorCode.length < 3) return;
		$('#doctor_code').val(doctorCode.toUpperCase());
		
		$.ajax({
			type: 'POST',
			url: '/admission/doctor_search_by_code',
			data: {
				'code' : doctorCode,
			},
			dataType: 'json',
			success: function(data) {
				var isi = data.data;
				$('#doctor_name').val(isi);
			}
		});
	};
</script>
@endsection