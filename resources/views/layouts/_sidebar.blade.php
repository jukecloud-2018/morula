<aside id="slide-out" class="side-nav fixed ">
    <div class="side-nav-wrapper">
        <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">

            <li class="no-padding {{{ Request::segment(2)=='' || Request::segment(2)=='registration' || Request::segment(1)=='' ? 'active' : '' }}}"><a href="{{route('registration')}}"><img class="sidebar-icon" src="/assets/images/icons/morula/light/Registration_LightBrown.svg">Registration</a></li>

            <li class="no-padding {{{ ((Request::segment(2)=='doctor_schedule') ? 'active' : '') }}}"><a href="{{ route('doctor_schedule') }}"><img class="sidebar-icon" src="/assets/images/icons/morula/light/DoctorSchedule_LightBrown.svg">Doctor Schedule</a></li>

            <li class="no-padding {{{ ((Request::segment(2)=='program') ? 'active' : '') }}}"><a href="{{route('program')}}"><img class="sidebar-icon" src="/assets/images/icons/morula/light/Patient_LightBrown.svg">Program</a></li>

            <li class="no-padding {{{ ((Request::segment(2)=='information') ? 'active' : '') }}}"><a href="{{route('information')}}"><img class="sidebar-icon" src="/assets/images/icons/morula/light/Forum_LightBrown.svg">Information</a></li>

        </ul>
    </div>
</aside>
@section('js-sidebar')
    <script>
        new PerfectScrollbar('.side-nav');
    </script>
@endsection