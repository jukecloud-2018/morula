{{--top-navbar bellow here--}}

<header class="mn-header navbar-fixed">
	<nav class="morula">
		<div class="nav-wrapper row">
			<section class="navigation-toggle">
			   <a href="javascript:;" class="hide-this .hide-on-med-and-down" data-activates="slide-out">
				   @php
						
						$icon_head = '';
						if(Request::segment(1) == 'admin')
							$icon_head = 'UserType_Administrator.png';
						else if(Request::segment(1) == 'admission')
							$icon_head = 'UserType_Admission.png';
						else if(Request::segment(1) == 'nurse')
							$icon_head = 'UserType_Nurse copy.png';
						else if(Request::segment(1) == 'doctor')
							$icon_head = 'UserType_Doctor.png';
						else if(Request::segment(1) == 'cashier')
							$icon_head = 'UserType_Cashier.png';
						else if(Request::segment(1) == 'finance')
							$icon_head = 'UserType_Finance.png';
						else if(Request::segment(1) == 'ga')
							$icon_head = 'UserType_General.png';
						else if(Request::segment(1) == 'lab')
							$icon_head = 'UserType_Laboratory.png';
						else
							$icon_head = 'UserType_Administrator.png';

				   @endphp
				   <img style="height: 80px; width: 80;" src="/assets/images/icons/morula/green/{{ $icon_head }}" alt="">
				</a>
			</section>
			<div class="header-title">
				<div class="" style="height: 25px;"><a data-position="right" data-delay="50" data-tooltip="Select Module" class='dropdown-button tooltipped' data-activates='dropdown2'>{{ucwords(trans(Request::segment(1)))}}</a></div>
				<ul id='dropdown2' class='dropdown-content'>
					<li><a href="/admission">Admission</a></li>
					<li class="divider"></li>
					<li><a href="/doctor">Doctor</a></li>
					<li class="divider"></li>
					<li><a href="/cashier">Cashier</a></li>
					<li class="divider"></li>
					<li><a href="{{ url('/nurse') }}">Nurse</a></li>
					<li class="divider"></li>
					<li><a href="/admin">Admin</a></li>
				</ul>
				<span class="s12">
					@php
						$segments   = '';
						$num        = count(Request::segments());
						$i          = 0;
						foreach(Request::segments() as $segment) {
							$title = str_replace('_',' ',$segment);
							$segments .= '/'.$segment;
							$i++;
							if($i != $num) {
								echo "<a class='breadcrumb' href='". url($segments) ."'>". ucwords(trans($title)) ."</a>";
							} else {
								echo "<a class='breadcrumb'>". ucwords(trans($title)). "</a>";
							}
						}
					@endphp

				</span>

			</div>
					

			{{--<button class="button-logout right" id="logout-btn" onclick="alertLogout()"><img style="width:40px;height:40px;" class="sidebar-icon" src="/assets/images/icons/morula/light/SignOut_LightBrown.png"><br>Logout</button>--}}
			<div class="user white-text">Hi, <a href="/profile">{{ Auth::user()->name }}</a></div>
			
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			{{ csrf_field() }}
			</form>
		</div>
	</nav>
</header>