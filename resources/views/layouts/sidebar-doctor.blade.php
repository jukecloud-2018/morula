<aside id="slide-out" class="side-nav fixed">
    <div class="side-nav-wrapper">
        <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">

            <li class="no-padding {{{ Request::segment(2)=='' || Request::segment(2)=='registration' || Request::segment(1)=='' ? 'active' : '' }}}"><a href="{{url('doctor/patient')}}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/Patient_green-1.png">Patient</a></li>

            <li class="no-padding {{{ ((Request::segment(2)=='doctor_schedule') ? 'active' : '') }}}"><a href="{{url('doctor/calendar')}}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/DoctorSchedule_green.png">Calendar</a></li>

            <li class="no-padding {{{ ((Request::segment(2)=='doctor_schedule') ? 'active' : '') }}}"><a href="{{url('doctor/form_request')}}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/DoctorSchedule_green.png">Request</a></li>

        </ul>
    </div>
</aside>
@section('js-sidebar')
    <script>
        new PerfectScrollbar('.side-nav');
    </script>
@endsection