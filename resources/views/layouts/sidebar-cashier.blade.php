<aside id="slide-out" class="side-nav fixed">
    <div class="side-nav-wrapper">
        <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">

            <li class="no-padding {{{ Request::segment(2)=='' || Request::segment(2)=='cashier' || Request::segment(1)=='' ? 'active' : '' }}}"><a href="{{ url('cashier/cashier') }}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/Cashier_green.png">Cashier</a></li>

            <li class="no-padding {{{ ((Request::segment(2)=='doctor_schedule') ? 'active' : '') }}}"><a href="{{ url('cashier/selling_transaction') }}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/SellingTransaction_green.png">Selling Transaction</a></li>

            <li class="no-padding {{{ ((Request::segment(2)=='exchange_labour_price') ? 'active' : '') }}}"><a href="{{ url('cashier/exchange_labour_price') }}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/exchangePrice_green.png/">Exchange Labour Price</a></li>
			
			<li class="no-padding {{{ ((Request::segment(2)=='table') ? 'active' : '') }}}"><a href="{{ url('cashier/table') }}"><img style="margin-left:8px;" class="sidebar-icon" src="/assets/images/icons/morula/white/Table_green.png">Table</a></li>
			
			<li class="no-padding {{{ ((Request::segment(2)=='cashier_transaction') ? 'active' : '') }}}"><a href="{{ url('cashier/cashier_transaction') }}"><img class="sidebar-icon" src="/assets/images/icons/morula/white/cashierTransaction_green.png">Cashier Transaction</a></li>
        </ul>
    </div>
</aside>
@section('js-sidebar')
    <script>
        new PerfectScrollbar('.side-nav');
    </script>
@endsection