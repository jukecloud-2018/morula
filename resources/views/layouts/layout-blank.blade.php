<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Title -->
        <title>Morula</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="/assets/plugins/materialize/css/materialize.css"/>
        <link type="text/css" rel="stylesheet" href="/assets/plugins/materialize/css/timepicker.css"/>
        <link type="text/css" rel="stylesheet" href="/assets/css/material-icons.css"/>
        <link type="text/css" rel="stylesheet" href="/assets/css/perfect-scrollbar.css"/>
        <link href="/assets/plugins/metrojs/MetroJs.min.css" rel="stylesheet">
        <link href="/assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet">
        <link href="/assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="/assets/plugins/google-code-prettify/prettify.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/plugins/jquery.nestable/nestable.css" type="text/css" rel="stylesheet">
        <!-- Theme Styles -->
        <link href="/assets/css/alpha.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

        <![endif]-->
    </head>
    <body class="loaded">
        <div class="mn-content fixed-sidebar">
			<!-- TOP NAVIGATION BAR -->
			@include('layouts.top-navbar');
			<!-- END OF TOP NAVIGATION BAR -->

			<!-- SIDE BAR -->
			@include('layouts.sidebar-blank');
			<!-- END OF SIDE BAR -->    
			<!-- CONTEN HERE -->
			@yield('content')
			<!-- END OF CONTENT -->
        </div>
		
		<!-- Javascripts -->
        <script src="/assets/plugins/jquery/jquery-2.2.0.min.js"></script>
        <script src="/assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="/assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
        <script src="/assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="/assets/plugins/waypoints/jquery.waypoints.min.js"></script>
        <script src="/assets/plugins/counter-up-master/jquery.counterup.min.js"></script>
        <script src="/assets/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>
        <script src="/assets/plugins/chart.js/chart.min.js"></script>
        <script src="/assets/plugins/flot/jquery.flot.min.js"></script>
        <script src="/assets/plugins/flot/jquery.flot.time.min.js"></script>
        <script src="/assets/plugins/flot/jquery.flot.symbol.min.js"></script>
        <script src="/assets/plugins/flot/jquery.flot.resize.min.js"></script>
        <script src="/assets/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="/assets/plugins/curvedlines/curvedLines.js"></script>
        <script src="/assets/plugins/peity/jquery.peity.min.js"></script>
        <script src="/assets/plugins/google-code-prettify/prettify.js"></script>
        <script src="/assets/plugins/sweetalert/sweetalert.min.js"></script>
        <script src="/assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
        <script src="/assets/plugins/materialize/js/timepicker.js"></script>
        <script src="/assets/js/alpha.js"></script>
        <script src="/assets/js/perfect-scrollbar.js"></script>
        <script src="/assets/js/custom.js"></script>
        <script src="/assets/js/pages/dashboard.js"></script>
        <script src="/assets/plugins/jquery.nestable/jquery.nestable.js"></script>



        @yield('js')
    </body>
</html>