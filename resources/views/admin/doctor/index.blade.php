@extends('layouts.layout')
@section('content')
	<main class="mn-inner">
		<div class="col s12">
			<div>
				<div id="res">
					<div class="col s12">
						<div>
							<div id="regis" >
								<div class="card-content">

									<div class="card-panel" id="data_reservation">
										<table id="doctor" class="display responsive-table datatable-example" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>No.</th>
													<th>Foto</th>
													<th>Doctor Name</th>
													<th>Specialization</th>
													<th>Code</th>
													<th>NIP</th>
													<th>Shift</th>
													<th>Status</th>
													<th>Actions</th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		{{--Modals New--}}
		<div id="new" class="modal modal-fixed-footer">
			<form enctype="multipart/form-data" action="/admin/doctor" method="post" id="newform">
			{{ csrf_field() }}
				<div class="modal-content">
					<h4 style="font-size: 20px;padding-left: 45%;">New User</h4><br>
					<div class="row">
						<label class="col lebel">Doctor Name</label>
						<input type="text" name="name" placeholder="Doctor Name" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Specialization</label>
						<div class="input-field col s8">
							<select class="browser-default" id="specialization" name="specialization">
								@foreach($specialization as $item)
									<option value="{{$item->name}}" >{{$item->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row">
						<label class="col lebel">Doctor Code</label>
						<input maxlength="3" type="text" name="doctor_code" placeholder="Doctor Code (3 Character)" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">NIP</label>
						<input type="text" name="nip" placeholder="NIP" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Shift</label>
						<div class="input-field col s8">
							<select class="browser-default" name="shift" id="shift">
								<option value="Pagi">Pagi</option>
								<option value="Sore">Sore</option>
							</select>
						</div>
					</div>
					<div class="row">
						<label class="col lebel">Status</label>
						<div class="input-field col s8">
							<select class="browser-default" name="status" id="status">
								<option value="Tetap">Tetap</option>
								<option value="Lainnya">Lainnya</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="cropFrame" style="width: 150px;height: 150px; margin-left:165px">
						<img id="upload-image" class="upload-dp cropImage" src="/assets/images/avatar-default.png">
						</div> 
					</div>
					<div class="row ">
						<label class="col lebel"></label>
						<div class="file-field input-field">
							<div class="btn teal lighten-1">
								<span>Photo Upload</span>
								<input name="photo" id="photo_new" type="file">
							</div>
							<div class="file-path-wrapper">
								<input class="col s6 file-path validate" type="text">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button id="button_add" type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
					<a href="javascript:;" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
				</div>
			</form>
		</div>

		{{--Modals Details--}}
		<div id="details" class="modal modal-fixed-footer">
			<form enctype="multipart/form-data" action="" method="post" id="edit">
				{{ method_field('PUT') }}
				{{ csrf_field() }}
				<div class="modal-content">
					<h4 style="font-size: 20px;padding-left: 45%;">Edit</h4><br>
					<div class="row">
						<label class="col lebel">Doctor Name</label>
						<input type="text" name="name" placeholder="Doctor Name" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Specialization</label>
						<div class="input-field col s8">
							<select class="browser-default" id="specialization" name="specialization">
								@foreach($specialization as $item)
									<option value="{{$item->name}}" >{{$item->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="row">
						<label class="col lebel">Doctor Code</label>
						<input maxlength="3" type="text" name="doctor_code" placeholder="Doctor Code (3 Character)" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">NIP</label>
						<input type="text" name="nip" placeholder="NIP" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Shift</label>
						<div class="input-field col s8">
							<select class="browser-default" name="shift" id="shift">
								<option value="Pagi">Pagi</option>
								<option value="Sore">Sore</option>
							</select>
						</div>
					</div>
					<div class="row">
						<label class="col lebel">Status</label>
						<div class="input-field col s8">
							<select class="browser-default" name="status" id="status">
								<option value="Tetap">Tetap</option>
								<option value="Lainnya">Lainnya</option>
							</select>
						</div>
					</div>
					<div class="row">
						<label class="col lebel"></label>
						<img id="img_detail" class="detail-patient-dp" src="/assets/images/avatar-default.png">
					</div>
					<div class="row ">
						<label class="col lebel"></label>
						<div class="file-field input-field">
							<div class="btn teal lighten-1">
								<span>Photo Upload</span>
								<input name="photo" id="photo_detail" type="file">
							</div>
							<div class="file-path-wrapper">
								<input id="caption" class="col s6 file-path validate" type="text">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button id="button_edit" type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
					<a href="javascript:;" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
				</div>
			</form>
		</div>


	</main>
@endsection

@section('js')
	<script>
        function add_new() {
            $('#new').openModal('open');
        }
		$(document).ready(function () {
            $('#upload-image').cropbox({
                width: 120,
                height: 160
            }, function() {
                //on load
                console.log('Url: ' + this.getDataURL());
            }).on('cropbox', function(e, data) {
                console.log('crop window: ' + data);
            });
        });

		$(document).ready(function() {
			getdata();
			$('.dataTables_length select').addClass('browser-default');
		});
		
		function getdata(){
			$('#doctor').DataTable( {
                dom : 'lf<"#add_new">rtip',
				language: {
					searchPlaceholder: 'Search records',
					sSearch: '',
					sLengthMenu: 'Show _MENU_',
					sLength: 'dataTables_length',
					oPaginate: {
						sFirst: '<i class="material-icons">chevron_left</i>',
						sPrevious: '<i class="material-icons">chevron_left</i>',
						sNext: '<i class="material-icons">chevron_right</i>',
						sLast: '<i class="material-icons">chevron_right</i>'
					}
				},
				"processing": true,
				"serverSide": true,
				"ajax":{
					"url": "/admin/doctor/ajaxdt",
					"dataType": "json",
					"type": "POST",
				},
				"order": [[ 0, "desc" ]],
				"columns": [
					{
						"data": "id",
						render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
						}
					},
					{ 
						"data": function(data){
							if(data.photo == null)
								return "<img src='/assets/images/avatar-default.png' width='20px' class='preview img-responsive inline-block' />";
							else
								return "<img src='/uploads/doctor/" + data.photo + "' width='20px' class='preview img-responsive inline-block' />";
						}
					},
					{ "data": "name" },
					{ "data": "specialization" },
					{ "data": "doctor_code" },
					{ "data": "nip" },
					{ "data": "shift" },
					{ "data": "status" },
					{
						"mRender": function (data, type, row, meta) {
							return '<a class="btn-floating" onclick="detail(`' + row.id + '`)"><i class="material-icons">zoom_in</i></a><a id="' + row.id + '" style="margin-left: 5%;" class="btn-floating" onclick="del(`' + row.id + '`)"><i class="material-icons">delete</i></a>';
						}
					}
				],
			});
            $("#add_new").html('<a style="margin-left: 2%;margin-top: 25px;" class="waves-effect waves-light btn modal-trigger" onclick="add_new()">Add</a>');
		}
		
		function detail(id){
			$.ajax({
				type: 'POST',
				url: '/admin/doctor/editajax',
				data: {
					'id' : id,
				},
				async: false,
				dataType: 'json',
				success: function(data) {
					var isi = (data.data);
					var photo = '/uploads/doctor/' + isi.photo;
					
					if(isi.photo == null)
						photo = "/assets/images/avatar-default.png";
					
					$('#edit #shift').prop('selectedIndex',0);
					$('#edit #status').prop('selectedIndex',0);
					$('#edit').attr('action', '/admin/doctor/' + isi.id);
					$('#edit input[name="name"]').val(isi.name);
					$('#edit #specialization option[value="' + isi.specialization + '"]').attr('selected', 'selected');
					$('#edit input[name="doctor_code"]').val(isi.doctor_code);
					$('#edit input[name="nip"]').val(isi.nip);
					$('#edit #shift option[value="' + isi.shift + '"]').attr('selected', 'selected');
					$('#edit #status option[value="' + isi.status + '"]').attr('selected', 'selected');
					$('#edit #photo_detail').val('');
					$('#edit #caption').val('');
					$('#edit #img_detail').attr("src", photo);
				}

			});
            $('#details').openModal();
        }
		
		function del(id) {
			swal({
				title: "Are you sure?",
				text: "Delete this doctor?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Confirm",
				closeOnConfirm: true
			}, function(){
				event.preventDefault();
				$.ajax({
					type: "DELETE",
					url: "/admin/doctor/" + id,
					dataType:"json",  
					success: function(data){
						if(data.success == true){
							$('#doctor').DataTable().destroy();
							getdata();
						}
					}
				});
				return false;
			});
		}
		
		$('#button_add').click(function(e) {
			e.preventDefault();
			
			var $form    = $('form#newform'),
				formData = new FormData(),
				params   = $form.serializeArray(),
				files    = $form.find('[name="photo"]')[0].files;

			$.each(files, function(i, file) {
				formData.append('photo', file);
			});

			$.each(params, function(i, val) {
				formData.append(val.name, val.value);
			});
			
			$.ajax({
				url: '/admin/doctor',
				type: 'post',
				dataType: 'json',
				async: false,
				contentType: false,
				processData: false,
				data: formData,
				success: function(data) {
							var desc = new Array();
							if(data.status != 200){
								$.each(data.description, function(i,d) {
									desc.push(d[0]);
								});
								var descr = desc.join('\n');
							} else {
								var descr = data.description;
								$('#doctor').DataTable().destroy();
								getdata();
								$(':input','#newform')
									.not('select, :button, :submit, :reset, :hidden')
									.val('');
								$(".detail-patient-dp").attr("src","/assets/images/avatar-default.png");
							}
							
							alertPopup(data.title, descr, data.alert);
						},
				error: function(d) {
					console.log('error');
				}
			});
		});
		
		$('#button_edit').click(function(e) {
			e.preventDefault();
			
			var $form    = $('form#edit'),
				formData = new FormData(),
				params   = $form.serializeArray(),
				files    = $form.find('[name="photo"]')[0].files;

			$.each(files, function(i, file) {
				formData.append('photo', file);
			});

			$.each(params, function(i, val) {
				formData.append(val.name, val.value);
			});
			
			$.ajax({
				url: $('#edit').attr('action'),
				type: 'post',
				dataType: 'json',
				async: false,
				contentType: false,
				processData: false,
				data: formData,
				success: function(data) {
							var desc = new Array();
							if(data.status != 200){
								$.each(data.description, function(i,d) {
									desc.push(d[0]);
								});
								var descr = desc.join('\n');
							} else {
								var descr = data.description;
								$('#doctor').DataTable().destroy();
								getdata();
							}
							
							alertPopup(data.title, descr, data.alert);
						},
				error: function(d) {
					console.log('error');
				}
			});
		});
		
		function alertPopup(title, text, type){
			swal({ title: title,
				text: text,
				type: type,
				//showConfirmButton: false,
				//timer: 3000,
			})
		}
		
        @if (Session::has('after_save'))
			var t = "{{ Session::get('after_save.title') }}";
			var txt = "{{ Session::get('after_save.text-1') }} {{ Session::get('after_save.text-2') }}";
			var ty = "{{ Session::get('after_save.alert') }}";
			alertPopup(t, txt,ty);
        @endif
		
		function readURL(input, id_img) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$(id_img).attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}

		$("#photo_new").change(function(){
			readURL(this, '#img_new');
		});
		
		$("#photo_detail").change(function(){
			readURL(this, '#img_detail');
		});
	</script>
@endsection
