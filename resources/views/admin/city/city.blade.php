@extends('layouts.layout')
@section('content')
	<main class="mn-inner">
		<div class="col s12">
			<div>
				<div id="res">
					<div class="col s12">
						<div>
							<div id="regis" >
								<div class="card-content">
									<div class="row">
										<form class="col s12 m12 card-panel" method="post" id="reservation_form">
											<div class="row" style="margin-top: 20px;"></div>
											<div class="row">
												<label class="col lebel"><i class="material-icons prefix">account_circle</i></label>
												<input type="text" class="col s2" id="name" name="name" placeholder="Name City" style="margin-left: 20px;">
												<button style="margin-left: 5%" type="submit" class="waves-effect waves-light btn" href="#">Find</button>
												<a style="margin-left: 5%" class="waves-effect waves-light btn" href="#">Print</a>
												<a style="margin-left: 5%;" class="waves-effect waves-light btn modal-trigger" href="#newData">Add</a>
											</div>
										</form>
									</div>

									<div class="card-panel" id="data_reservation">
										<table id="data" class="display responsive-table datatable-example" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>No.</th>
													<th>City Name</th>
													<th>Province Name</th>
													<th>Actions</th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		{{--Modals New--}}
		<div id="newData" class="modal modal-fixed-footer">
			<form action="{{ route('city.index') }}" method="post" id="newform">
			{{ csrf_field() }}
				<div class="modal-content">
					<h4 style="font-size: 20px;padding-left: 45%;">New City</h4><br>
					
					<div class="row">
						<label class="col label">City Name</label>
						<input type="text" name="name" placeholder="City Name" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Province Name</label>
						<select class="form-control" id="province" name="province">
							@foreach($province as $item)
								<option value="{{$item->id}}"> {{$item->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button id="button_add" type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
					<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
				</div>
			</form>
		</div>

		{{--Modals Details--}}
		<div id="dataDetails" class="modal  modal-fixed-footer">
			<form action="" method="post" id="edit">
				{{ method_field('PUT') }}
				{{ csrf_field() }}
				<div class="modal-content">
					<h4 style="font-size: 20px;padding-left: 45%;">Edit City</h4><br>
					
					<div class="row">
						<label class="col label">City Name</label>
						<input type="text" name="name" placeholder="City Name" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Province Name</label>
						<div class="input-field col s8">
							<select class="browser-default" id="province" name="province">
								@foreach($province as $item)
									<option value="{{$item->id}}" > {{$item->name}}</option>
								@endforeach
							</select>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button id="button_edit" type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
					<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
				</div>
			</form>
		</div>

	</main>
@endsection

@section('js')
	<script>
		$(document).ready(function() {
			getdata();
			$('.dataTables_length select').addClass('browser-default');
		});
		//ini ajax untuk nampilin data
		function getdata(){
			$('#data').DataTable( {
                language: {
                    searchPlaceholder: 'Search records',
					sSearch: '',
                    sLengthMenu: 'Show _MENU_',
                    sLength: 'dataTables_length',
					oPaginate: {
						sFirst: '<i class="material-icons">chevron_left</i>',
						sPrevious: '<i class="material-icons">chevron_left</i>',
						sNext: '<i class="material-icons">chevron_right</i>',
						sLast: '<i class="material-icons">chevron_right</i>'
					}
				},
				"processing": true,
				"serverSide": true,
				"ajax":{
					"url": "/admin/city/ajaxdt",
					"dataType": "json",
					"type": "POST",
				},
                'order': [[ 0, 'desc' ]],
				"columns": [
					{
						"data": "id",
						render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
						}
					},
					{ "data": "name" },
                    {
                        "mRender": function (data, type, row, meta) {
                            return row.province.name;
                        }
                    },
					{
						"mRender": function (data, type, row, meta) {
							return '<a class="btn-floating" onclick="detail(`' + row.id + '`)"><i class="material-icons">zoom_in</i></a><a id="' + row.id + '" style="margin-left: 5%;" class="btn-floating" onclick="del(`' + row.id + '`)"><i class="material-icons">delete</i></a>';
						}
					}
				],
			});
            $("div.toolbar").html('<a style="margin-left: 5%;" class="waves-effect waves-light btn modal-trigger" href="#newData">Add</a>');
		}
		//ini ajax detail
		function detail(id){
			$.ajax({
				type: 'POST',
				url: '/admin/city/editajax',
				data: {
					'id' : id,
				},
				async: false,
				dataType: 'json',
				success: function(data) {
					var isi = (data.data);
					$('#edit').attr('action', '/admin/city/' + isi.id);
					$('#edit input[name="name"]').val(isi.name);
					$('#edit option[value="' + isi.province_id +'"]').attr('selected', 'selected');
				}

			});
            $('#dataDetails').openModal();
        }
		
		function del(id) {
			swal({
				title: "Are you sure?",
				text: "Delete this city?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Confirm",
				closeOnConfirm: true
			}, function(){
				event.preventDefault();
				$.ajax({
					type: "DELETE",
					url: "/admin/city/" + id,
					dataType:"json",  
					success: function(data){
						if(data.success == true){
							//location.reload(true);
							$('#data').DataTable().destroy();
							getdata();
						}
					}
				});
				return false;
			});
		}

        //Button add City
        $('#button_add').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: '/admin/city',
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#newform').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) { //i=index, d=nama array,description=objek json
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n'); // join = gabungin array ke text, \n = pemisahnya
                    } else {
                        var descr = data.description;
                        $('#data').DataTable().destroy();
                        getdata();
                        $(':input','#newform')
                            .not(':button, :submit, :reset, :hidden')
                            .val('');
                    }
                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        //Button Submit edit city
        $('#button_edit').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: $('#edit').attr('action'),
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#edit').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) {
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n');
                    } else {
                        var descr = data.description;
                        $('#data').DataTable().destroy();
                        getdata();
                    }
                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        function alertPopup(title, text, type){
            swal({ title: title,
                text: text,
                type: type,
                //showConfirmButton: false,
                //timer: 3000,
            })
        }

		@if (Session::has('after_save'))
			var t = "{{ Session::get('after_save.title') }}";
			var txt = "{{ Session::get('after_save.text-1') }} {{ Session::get('after_save.text-2') }}";
			var ty = "{{ Session::get('after_save.alert') }}";
        alertPopup(t, txt,ty);
		@endif
	</script>
@endsection
