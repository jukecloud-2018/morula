@extends('layouts.layout')
@section('content')
	<main class="mn-inner">
		<div class="col s12">
			<div>
				<ul class="tabs tab-demo z-depth-1" id="tabsId" style="width: 100%;">
                    <li class="tab col s3"><a href="#country">Country</a></li>
                    <li class="tab col s3"><a href="#prov">Province</a></li>
					<li class="tab col s3"><a href="#city">City</a></li>
					<div class="indicator" style="right: 372.75px; left: 0px;"></div>
				</ul>

				{{--Main tab Province--}}
				<div id="country">
					<div class="col s12">
						<div>
							<div id="regis" >
								<div class="card-content">
									{{--star view Country--}}
									<div class="card-panel" id="data_reservation">
										<table id="dataCountry" class="display responsive-table datatable-example" cellspacing="0" width="100%">
											<thead>
											<tr>
												<th>No.</th>
												<th>Country Name</th>
												<th>Actions</th>
											</tr>
											</thead>
										</table>
									</div>
									{{--akhir dari tampilan Country--}}
								</div>
							</div>
						</div>
					</div>
				</div>
				{{--Akhir dari tab Country--}}

                {{--Main tab Province--}}
				<div id="prov">
					<div class="col s12">
						<div>
							<div id="regis" >
								<div class="card-content">
									{{--star view province--}}
									<div class="card-panel" id="data_reservation">
										<table id="dataProvince" class="display responsive-table datatable-example" cellspacing="0" width="100%">
											<thead>
											<tr>
												<th>No.</th>
												<th>Province Name</th>
												<th>Actions</th>
											</tr>
											</thead>
										</table>
									</div>
									{{--akhir dari tampilan province--}}
								</div>
							</div>
						</div>
					</div>
				</div>
				{{--Akhir dari tab Province--}}

				{{--Main Tab City--}}
				<div id="city">
					<div class="col s12">
						<div>
							<div id="regis" >
								<div class="card-content">
									{{--star view city--}}
									<div class="card-panel" id="data_reservation">
										<table id="dataCity" class="display responsive-table datatable-example" cellspacing="0" width="100%">
											<thead>
											<tr>
												<th>No.</th>
												<th>City Name</th>
												<th>Province Name</th>
												<th>Actions</th>
											</tr>
											</thead>
										</table>
									</div>
									{{--akhir dari tampilan city--}}
								</div>
							</div>
						</div>
					</div>
				</div>
				{{--Akhir tab City--}}


			</div>
		</div>

	{{--Modals--}}
	<!--    {{--Modals New Country--}}-->
		<div id="newDataCountry" class="modal modal-fixed-footer">
			<form action="{{ route('country.index') }}" method="post" id="newformCountry">
				{{ csrf_field() }}
				<div class="modal-content">
					<h4 style="font-size: 20px;padding-left: 45%;">New Country</h4><br>
					<div class="row">
						<label class="col label">Country Name </label>
						<input type="text" name="name" placeholder="Country Name" class="col s8">
					</div>
				</div>
				<div class="modal-footer">
					<button id="button_addCountry" type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
					<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
				</div>
			</form>
		</div>

	<!--    {{--Modals Details Country--}}-->
		<div id="dataDetailsCountry" class="modal  modal-fixed-footer">
			<form action="" method="post" id="editCountry">
				{{ method_field('PUT') }}
				{{ csrf_field() }}
				<div class="modal-content">
					<h4 style="font-size: 20px;padding-left: 45%;">Edit Country</h4><br>
					<div class="row">
						<label class="col label">Country Name</label>
						<input type="text" name="name" placeholder="Country Name" class="col s8">
					</div>
				</div>
				<div class="modal-footer">
					<button id="button_editCountry" type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
					<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
				</div>
			</form>
		</div>

<!--    {{--Modals New Province--}}-->
		<div id="newDataProvince" class="modal modal-fixed-footer">
			<form action="{{ route('province.index') }}" method="post" id="newformProvince">
				{{ csrf_field() }}
				<div class="modal-content">
					<h4 style="font-size: 20px;padding-left: 45%;">New Province</h4><br>
					<div class="row">
						<label class="col label">Province Name </label>
						<input type="text" name="name" placeholder="Province Name" class="col s8">
					</div>
				</div>
				<div class="modal-footer">
					<button id="button_add" type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
					<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
				</div>
			</form>
		</div>

	<!--    {{--Modals Details Province--}}-->
		<div id="dataDetailsProvince" class="modal  modal-fixed-footer">
			<form action="" method="post" id="editProvince">
				{{ method_field('PUT') }}
				{{ csrf_field() }}
				<div class="modal-content">
					<h4 style="font-size: 20px;padding-left: 45%;">Edit Province</h4><br>
					<div class="row">
						<label class="col label">Province Name</label>
						<input type="text" name="name" placeholder="Province Name" class="col s8">
					</div>
				</div>
				<div class="modal-footer">
					<button id="button_edit" type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
					<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
				</div>
			</form>
		</div>

		{{--Modals New City--}}
		<div id="newDataCity" class="modal modal-fixed-footer">
			<form action="{{ route('city.index') }}" method="post" id="newformCity">
				{{ csrf_field() }}
				<div class="modal-content">
					<h4 style="font-size: 20px;padding-left: 45%;">New City</h4><br>

					<div class="row">
						<label class="col label">City Name</label>
						<input type="text" name="name" placeholder="City Name" class="col s8">
					</div>
					<div class="row">
						<label class="col label">Province Name</label>
						<select class="form-control" id="province" name="province">
							@foreach($province as $item)
								<option value="{{$item->id}}"> {{$item->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button id="button_add2" type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
					<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
				</div>
			</form>
		</div>

		{{--Modals Details City--}}
		<div id="dataDetailsCity" class="modal  modal-fixed-footer">
			<form action="" method="post" id="editCity">
				{{ method_field('PUT') }}
				{{ csrf_field() }}
				<div class="modal-content">
					<h4 style="font-size: 20px;padding-left: 45%;">Edit City</h4><br>
					<div class="row">
						<label class="col label">City Name</label>
						<input type="text" name="name" placeholder="City Name" class="col s8">
					</div>
					<div class="row">
						<label class="col label">Province Name</label>
						<div class="input-field col s8">
							<select class="browser-default" id="province" name="province">
								@foreach($province as $item)
									<option value="{{$item->id}}" > {{$item->name}}</option>
								@endforeach
							</select>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button id="button_edit2" type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
					<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
				</div>
			</form>
		</div>
	</main>
@endsection

{{--ajak dimulai dari sini--}}
@section('js')
	<script>
        function add_newCountry() {
            $('#newDataCountry').openModal('open');
        }
        $(document).ready(function() {
            getdataCountry();
            $('.dataTables_length select').addClass('browser-default');
        });
        //ini ajax untuk nampilin data province
        function getdataCountry(){
            $('#dataCountry').DataTable( {
                dom : 'lf<"#add_newCountry">rtip',
                language: {
                    searchPlaceholder: 'Search records',
                    sSearch: '',
                    sLengthMenu: 'Show _MENU_',
                    sLength: 'dataTables_length',
                    oPaginate: {
                        sFirst: '<i class="material-icons">chevron_left</i>',
                        sPrevious: '<i class="material-icons">chevron_left</i>',
                        sNext: '<i class="material-icons">chevron_right</i>',
                        sLast: '<i class="material-icons">chevron_right</i>'
                    }
                },
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url": "/admin/country/ajaxdt",
                    "dataType": "json",
                    "type": "POST",
                },
                'order': [[ 0, 'desc' ]],
                "columns": [
                    {
                        "data": "id",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { "data": "name" },
                    {
                        "mRender": function (data, type, row, meta) {
                            return '<a class="btn-floating" onclick="detailCountry(`' + row.id + '`)"><i class="material-icons">zoom_in</i></a><a id="' + row.id + '" style="margin-left: 5%;" class="btn-floating" onclick="delCountry(`' + row.id + '`)"><i class="material-icons">delete</i></a>';
                        }
                    }
                ],
            });
            $("#add_newCountry").html('<a style="margin-left: 2%;margin-top: 25px;" class="waves-effect waves-light btn modal-trigger" onclick="add_newCountry()">Add</a>');
        }

        //ini ajax detail atau edit province
        function detailCountry(id){
            $.ajax({
                type: 'POST',
                url: '/admin/country/editajax',
                data: {
                    'id' : id,
                },
                async: false,
                dataType: 'json',
                success: function(data) {
                    var isi = (data.data);
                    $('#editCountry').attr('action', '/admin/country/' + isi.id);
                    $('#editCountry input[name="name"]').val(isi.name);
                }
            });
            $('#dataDetailsCountry').openModal();
        }

        //Function delete province
        function delCountry(id) {
            swal({
                title: "Are you sure?",
                text: "Delete this country?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Confirm",
                closeOnConfirm: true
            }, function(){
                event.preventDefault();
                $.ajax({
                    type: "DELETE",
                    url: "/admin/country/" + id,
                    dataType:"json",
                    success: function(data){
                        if(data.success == true){
                            //location.reload(true);
                            $('#dataCountry').DataTable().destroy();
                            getdataCountry();
                        }
                    }
                });
                return false;
            });
        }

        //Button add Province
        $('#button_addCountry').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: '/admin/country/country',
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#newformCountry').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) { //i=index, d=nama array,description=objek json
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n'); // join = gabungin array ke text, \n = pemisahnya
                    } else {
                        var descr = data.description;
                        $('#dataCountry').DataTable().destroy();
                        getdataCountry();
                        $(':input','#newformCountry')
                            .not(':button, :submit, :reset, :hidden')
                            .val('');
                    }

                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        //Button Submit edit Country
        $('#button_editCountry').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: $('#editCountry').attr('action'),
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#editCountry').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) {
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n');
                    } else {
                        var descr = data.description;
                        $('#dataCountry').DataTable().destroy();
                        getdataCountry();
                    }
                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        //ini jquery province
        function add_newProvince() {
            $('#newDataProvince').openModal('open');
        }
        $(document).ready(function() {
            getdataProvince();
            $('.dataTables_length select').addClass('browser-default');
        });
        //ini ajax untuk nampilin data province
        function getdataProvince(){
            $('#dataProvince').DataTable( {
                dom : 'lf<"#add_newProvince">rtip',
                language: {
                    searchPlaceholder: 'Search records',
                    sSearch: '',
                    sLengthMenu: 'Show _MENU_',
                    sLength: 'dataTables_length',
                    oPaginate: {
                        sFirst: '<i class="material-icons">chevron_left</i>',
                        sPrevious: '<i class="material-icons">chevron_left</i>',
                        sNext: '<i class="material-icons">chevron_right</i>',
                        sLast: '<i class="material-icons">chevron_right</i>'
                    }
                },
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url": "/admin/province/ajaxdt",
                    "dataType": "json",
                    "type": "POST",
                },
                'order': [[ 0, 'desc' ]],
                "columns": [
                    {
                        "data": "id",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { "data": "name" },
                    {
                        "mRender": function (data, type, row, meta) {
                            return '<a class="btn-floating" onclick="detailProvince(`' + row.id + '`)"><i class="material-icons">zoom_in</i></a><a id="' + row.id + '" style="margin-left: 5%;" class="btn-floating" onclick="delProvince(`' + row.id + '`)"><i class="material-icons">delete</i></a>';
                        }
                    }
                ],
            });
            $("#add_newProvince").html('<a style="margin-left: 2%;margin-top: 25px;" class="waves-effect waves-light btn modal-trigger" onclick="add_newProvince()">Add</a>');
        }

        //ini ajax detail atau edit province
        function detailProvince(id){
            $.ajax({
                type: 'POST',
                url: '/admin/province/editajax',
                data: {
                    'id' : id,
                },
                async: false,
                dataType: 'json',
                success: function(data) {
                    var isi = (data.data);
                    $('#editProvince').attr('action', '/admin/province/' + isi.id);
                    $('#editProvince input[name="name"]').val(isi.name);
                }
            });
            $('#dataDetailsProvince').openModal();
        }

		//Function delete province
        function delProvince(id) {
            swal({
                title: "Are you sure?",
                text: "Delete this province?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Confirm",
                closeOnConfirm: true
            }, function(){
                event.preventDefault();
                $.ajax({
                    type: "DELETE",
                    url: "/admin/province/" + id,
                    dataType:"json",
                    success: function(data){
                        if(data.success == true){
                            //location.reload(true);
                            $('#dataProvince').DataTable().destroy();
                            getdataProvince();
                        }
                    }
                });
                return false;
            });
        }

        //Button add Province
        $('#button_add').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: '/admin/province',
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#newformProvince').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) { //i=index, d=nama array,description=objek json
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n'); // join = gabungin array ke text, \n = pemisahnya
                    } else {
                        var descr = data.description;
                        $('#dataProvince').DataTable().destroy();
                        getdataProvince();
                        $(':input','#newformProvince')
                            .not(':button, :submit, :reset, :hidden')
                            .val('');
                    }

                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        //Button Submit edit Province
        $('#button_edit').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: $('#editProvince').attr('action'),
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#editProvince').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) {
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n');
                    } else {
                        var descr = data.description;
                        $('#dataProvince').DataTable().destroy();
                        getdataProvince();
                    }
                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        //ini jquery city
        function add_newCity() {
            $('#newDataCity').openModal('open');
        }
        $(document).ready(function() {
            getdataCity();
            $('.dataTables_length select').addClass('browser-default');
        });
        //ini ajax untuk nampilin data City
        function getdataCity(){
            $('#dataCity').DataTable( {
                dom : 'lf<"#add_newCity">rtip',
                language: {
                    searchPlaceholder: 'Search records',
                    sSearch: '',
                    sLengthMenu: 'Show _MENU_',
                    sLength: 'dataTables_length',
                    oPaginate: {
                        sFirst: '<i class="material-icons">chevron_left</i>',
                        sPrevious: '<i class="material-icons">chevron_left</i>',
                        sNext: '<i class="material-icons">chevron_right</i>',
                        sLast: '<i class="material-icons">chevron_right</i>'
                    }
                },
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url": "/admin/city/ajaxdt",
                    "dataType": "json",
                    "type": "POST",
                },
                'order': [[ 0, 'desc' ]],
                "columns": [
                    {
                        "data": "id",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { "data": "name" },
                    {
                        "mRender": function (data, type, row, meta) {
//                            console.log(row);
                            return row.province.name;
                        }
                    },
                    {
                        "mRender": function (data, type, row, meta) {
                            return '<a class="btn-floating" onclick="detailCity(`' + row.id + '`)"><i class="material-icons">zoom_in</i></a><a id="' + row.id + '" style="margin-left: 5%;" class="btn-floating" onclick="delCity(`' + row.id + '`)"><i class="material-icons">delete</i></a>';
                        }
                    }
                ],
            });
            $("#add_newCity").html('<a style="margin-left: 2%;margin-top: 25px;" class="waves-effect waves-light btn modal-trigger" onclick="add_newCity()">Add</a>');
        }

        //Function detail atau edit City
        function detailCity(id){
            $.ajax({
                type: 'POST',
                url: '/admin/city/editajax',
                data: {
                    'id' : id,
                },
                async: false,
                dataType: 'json',
                success: function(data) {
                    var isi = (data.data);
                    $('#editCity').attr('action', '/admin/city/' + isi.id);
                    $('#editCity input[name="name"]').val(isi.name);
                    $('#editCity option[value="' + isi.province_id +'"]').attr('selected', 'selected');
                }
            });
            $('#dataDetailsCity').openModal();
        }

		//Function delete city
        function delCity(id) {
            swal({
                title: "Are you sure?",
                text: "Delete this city?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Confirm",
                closeOnConfirm: true
            }, function(){
                event.preventDefault();
                $.ajax({
                    type: "DELETE",
                    url: "/admin/city/" + id,
                    dataType:"json",
                    success: function(data){
                        if(data.success == true){
                            //location.reload(true);
                            $('#dataCity').DataTable().destroy();
                            getdataCity();
                        }
                    }
                });
                return false;
            });
        }

        //Button add City
        $('#button_add2').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: '/admin/city',
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#newformCity').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) { //i=index, d=nama array,description=objek json
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n'); // join = gabungin array ke text, \n = pemisahnya
                    } else {
                        var descr = data.description;
                        $('#dataCity').DataTable().destroy();
                        getdataCity();
                        $(':input','#newformCity')
                            .not(':button, :submit, :reset, :hidden')
                            .val('');
                    }

                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        //Button Submit edit City
        $('#button_edit2').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: $('#editCity').attr('action'),
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#editCity').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) {
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n');
                    } else {
                        var descr = data.description;
                        $('#dataCity').DataTable().destroy();
                        getdataCity();
                    }
                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        function alertPopup(title, text, type){
            swal({ title: title,
                text: text,
                type: type,
                //showConfirmButton: false,
                //timer: 3000,
            })
        }

        @if (Session::has('after_save'))
            var t = "{{ Session::get('after_save.title') }}";
            var txt = "{{ Session::get('after_save.text-1') }} {{ Session::get('after_save.text-2') }}";
            var ty = "{{ Session::get('after_save.alert') }}";
            alertPopup(t, txt,ty);
        @endif
	</script>
@endsection
