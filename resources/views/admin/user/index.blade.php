@extends('layouts.layout')
@section('content')
	<main class="mn-inner">
		<div class="col s12">
			<div>
				<div id="res">
					<div class="col s12">
						<div>
							<div id="regis" >
								<div class="card-content">


									<div class="card-panel" id="data_reservation">
										<table id="user" class="display responsive-table datatable-example" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>No.</th>
													<th>Foto</th>
													<th>Username</th>
													<th>Name</th>
													<th>Email</th>
													<th>Phone</th>
													<th>Description</th>
													<th>Actions</th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		{{--Modals New--}}
		<div id="newUser" class="modal modal-fixed-footer">
			<form action="{{ route('user.index') }}" method="post" id="newuser">
			{{ csrf_field() }}
				<div class="modal-content">
					<h4 style="font-size: 20px;padding-left: 45%;">New User</h4><br>
					<div class="row">
						<label class="col lebel">Username</label>
						<input type="text" name="username" placeholder="Username" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Password</label>
						<input type="password" name="password" placeholder="Password" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Full Name</label>
						<input type="text" name="name" placeholder="Full Name" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Email</label>
						<input type="text" name="email" placeholder="Email" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Phone</label>
						<input type="text" name="phone" placeholder="Phone" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Description</label>
						<textarea class="materialize-textarea col s8" name="description" data-length="120"></textarea>
					</div>
					<div class="row">
						@foreach($roles as $item)
							<label class="col lebel">{{ $item->id == $max[0]->id ? 'Roles' : '&nbsp;' }}</label>
							<p class="p-v-xs">
								<input id="rolenew_{{ $item->id }}" name="roles[]" type="checkbox" class="filled-in" value="{{ $item->id }}" />
								<label for="rolenew_{{ $item->id }}">{{ $item->name }}</label>
							</p>
						@endforeach
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
					<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
				</div>
			</form>
		</div>

		{{--Modals Details--}}
		<div id="userDetails" class="modal modal-fixed-footer">
			<form action="" method="post" id="edituser">
				{{ method_field('PUT') }}
				{{ csrf_field() }}
				<div class="modal-content">
					<h4 style="font-size: 20px;padding-left: 45%;">Edit User</h4><br>
					<div class="row">
						<label class="col lebel">Username</label>
						<input type="text" name="username" placeholder="Username" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Password</label>
						<input type="password" name="password" placeholder="Leave blank if don't want to change password" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Full Name</label>
						<input type="text" name="name" placeholder="Full Name" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Email</label>
						<input type="text" name="email" placeholder="Email" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Phone</label>
						<input type="text" name="phone" placeholder="Phone" class="col s8">
					</div>
					<div class="row">
						<label class="col lebel">Description</label>
						<textarea class="materialize-textarea col s8" name="description" data-length="120"></textarea>
					</div>
					<div class="row">
						@foreach($roles as $item)
							<label class="col lebel">{{ $item->id == $max[0]->id ? 'Roles' : '&nbsp;' }}</label>
							<p class="p-v-xs">
								<input name="roles[]" type="checkbox" class="filled-in role-edit" id="role_{{ $item->id }}" value="{{ $item->id }}" />
								<label for="role_{{ $item->id }}">{{ $item->name }}</label>
							</p>
						@endforeach
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
					<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
				</div>
			</form>
		</div>


	</main>
@endsection

@section('js')
	<script>
        function add_new() {
            $('#newUser').openModal('open');
        }
		$(document).ready(function() {
			getdata();
			$('.dataTables_length select').addClass('browser-default');
		});
		
		function getdata(){
			$('#user').DataTable( {
                dom : 'lf<"#add_new">rtip',
				language: {
					searchPlaceholder: 'Search records',
					sSearch: '',
					sLengthMenu: 'Show _MENU_',
					sLength: 'dataTables_length',
					oPaginate: {
						sFirst: '<i class="material-icons">chevron_left</i>',
						sPrevious: '<i class="material-icons">chevron_left</i>',
						sNext: '<i class="material-icons">chevron_right</i>',
						sLast: '<i class="material-icons">chevron_right</i>'
					}
				},
				"processing": true,
				"serverSide": true,
				"ajax":{
					"url": "/admin/user/ajaxdt",
					"dataType": "json",
					"type": "POST",
				},
				"columns": [
					{
						"data": "id",
						render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
						}
					},
					{ 
						"data": function(data){
							if(data.photo == null)
								return "<img src='/assets/images/avatar-default.png' width='20px' class='preview img-responsive inline-block' />";
							else
								return "<img src='/uploads/user/" + data.photo + "' width='20px' class='preview img-responsive inline-block' />";
						}
					},
					{ "data": "username" },
					{ "data": "name" },
					{ "data": "email" },
					{ "data": "phone" },
					{ "data": "description" },
					{
						"mRender": function (data, type, row, meta) {
							return '<a class="btn-floating" onclick="detail(`' + row.id + '`)"><i class="material-icons">zoom_in</i></a><a id="' + row.id + '" style="margin-left: 5%;" class="btn-floating" onclick="del(`' + row.id + '`)"><i class="material-icons">delete</i></a>';
						}
					}
				],
			});
			$("#add_new").html('<a style="margin-left: 2%;margin-top: 25px;" class="waves-effect waves-light btn modal-trigger" onclick="add_new()">Add</a>');
		}
		
		function detail(id){
			$.ajax({
				type: 'POST',
				url: '/admin/user/editajax',
				data: {
					'id' : id,
				},
				async: false,
				dataType: 'json',
				success: function(data) {
					var isi = (data.data);
					
					$('#edituser').attr('action', '/admin/user/' + isi.id);
					$('#edituser input[name="username"]').val(isi.username);
					$('#edituser input[name="name"]').val(isi.name);
					$('#edituser input[name="email"]').val(isi.email);
					$('#edituser input[name="phone"]').val(isi.phone);
					$('#edituser textarea[name="description"]').val(isi.description);
					
					$("input[id*='role_'").prop('checked', false);
					
					console.log(isi.roles);
					if(isi.roles != null){
						var roles = isi.roles.split(",");
						
						roles.forEach(function(item){
							console.log('#role_' + item);
							$('#role_' + item).prop('checked', true);
						});
					}
				}

			});
            $('#userDetails').openModal();
        }
		
		function del(id) {
			swal({
				title: "Are you sure?",
				text: "Delete this user?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Confirm",
				closeOnConfirm: true
			}, function(){
				event.preventDefault();
				$.ajax({
					type: "DELETE",
					url: "/admin/user/" + id,
					dataType:"json",  
					success: function(data){
						if(data.success == true){
							//location.reload(true);
							$('#user').DataTable().destroy();
							getdata();
						}
					}
				});
				return false;
			});
		}
		
        @if (Session::has('after_save'))
            function alertPopup(){
				swal({ title: "{{ Session::get('after_save.title') }}",
					text: "{{ Session::get('after_save.text-1') }} {{ Session::get('after_save.text-2') }}",
					type: "{{ Session::get('after_save.alert') }}",
					showConfirmButton: false,
					timer: 2000,
				})
			}
			alertPopup();
        @endif
	</script>
@endsection
