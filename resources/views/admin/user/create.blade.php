@extends('layouts.layout-admin')
@section('content')
	<main class="mn-inner">
		<div class="col s12">
			<div>
				<div id="res">
					<div class="col s12">
						<div>
							<div id="regis" >
								<form action="{{ route('user.index') }}" class="col s12 m12 card-panel" method="post" id="user_form_add">
									<div class="card-content">
										<div class="card-panel" id="data_reservation">
											<div id="newPatient" class="">
												<form action="{{ route('user.index') }}" method="post" id="newuser">
												{{ csrf_field() }}
													<div class="modal-content">
														<h4 style="font-size: 20px;padding-left: 45%;">New User</h4><br>
														<div class="row">
															<label class="col lebel">Username</label>
															<input value="{{ old('username') }}" type="text" name="username" placeholder="Username" class="col s8">
															<span class="label label-danger red-text text-darken-1">{{ $errors->add->first('username') }}</span>
														</div>
														<div class="row">
															<label class="col lebel">Password</label>
															<input value="" type="password" name="password" placeholder="Password" class="col s8">
															<span class="label label-danger red-text text-darken-1">{{ $errors->add->first('password') }}</span>
														</div>
														<div class="row">
															<label class="col lebel">Full Name</label>
															<input value="{{ old('name') }}" type="text" name="name" placeholder="Full Name" class="col s8">
															<span class="label label-danger red-text text-darken-1">{{ $errors->add->first('name') }}</span>
														</div>
														<div class="row">
															<label class="col lebel">Email</label>
															<input value="{{ old('email') }}" type="email" name="email" placeholder="Email" class="col s8 validate">
															<span class="label label-danger red-text text-darken-1">{{ $errors->add->first('email') }}</span>
														</div>
														<div class="row">
															<label class="col lebel">Phone</label>
															<input value="{{ old('phone') }}" type="text" name="phone" placeholder="Phone" class="col s8">
															<span class="label label-danger red-text text-darken-1">{{ $errors->add->first('phone') }}</span>
														</div>
														<div class="row">
															<label class="col lebel">Description</label>
															<textarea class="materialize-textarea col s8" name="description" data-length="120">{{ old('description') }}</textarea>
														</div>
														<div class="row">
															@foreach($roles as $item)
																<label class="col lebel">{{ $item->id == $max[0]->id ? 'Roles' : '&nbsp;' }}</label>
																<p class="p-v-xs">
																	<input name="roles[]" type="checkbox" class="filled-in" id="role_{{ $item->id }}" value="{{ $item->id }}" />
																	<label for="role_{{ $item->id }}">{{ $item->name }}</label>
																</p>
															@endforeach
														</div>
													</div>
												</form>
											</div>
										</div>
										<div class="row">
											<div class="row" style="margin-top: 20px;">
											</div>
											<div class="row">
												<a style="margin-left: 5%;" class="waves-effect waves-light btn modal-trigger" href="{{ route('user.index') }}">Back</a>
												<button style="margin-left: 5%" type="submit" class="waves-effect waves-light btn" href="#">Save</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{{--End Modals--}}
	</main>
@endsection

@section('js')
	<script>
		$(document).ready(function() {
			$('#user').DataTable( {
				language: {
					searchPlaceholder: 'Search records',
					sSearch: '',
					sLengthMenu: 'Show _MENU_',
					sLength: 'dataTables_length',
					oPaginate: {
						sFirst: '<i class="material-icons">chevron_left</i>',
						sPrevious: '<i class="material-icons">chevron_left</i>',
						sNext: '<i class="material-icons">chevron_right</i>',
						sLast: '<i class="material-icons">chevron_right</i>'
					}
				},
				"processing": true,
				"serverSide": true,
				"ajax":{
					"url": "user/ajaxdt",
					"dataType": "json",
					"type": "POST",
					/*"data": function(d) {
						var frm_data = theData;
						$.each(frm_data, function(key, val) {
							d[val.name] = val.value;
						});
					},*/
					/*"headers": {
						"authorization": "Bearer uEzFSH1CTVPj0mf2VLRmmtGVvNWUQwBR0u5QykJzFskllHEsEr7Z7dU806YcvyD23k2DgH5WLTYyU86TkepxeQRtY5gSINSuTuXX"
					}*/
				},
				"columns": [
					{
						"data": "id",
						render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
						}
					},
					{ "data": "username" },
					{ "data": "name" },
					{ "data": "email" },
					{ "data": "phone" },
					{ "data": "description" },
					{
						"mRender": function () {
							return '<a href="/detail_patient" class="btn-floating"><i class="material-icons">zoom_in</i></a><a style="margin-left: 5%;" href="" class="btn-floating"><i class="material-icons">delete</i></a>';
						}
					}
				],
			});
			
			$('.dataTables_length select').addClass('browser-default');

		});
		
		@if (Session::has('after_save'))
            function alertSuccess(){
				swal({ title: "{{ Session::get('after_save.title') }}",
					text: "{{ Session::get('after_save.text-1') }} {{ Session::get('after_save.text-2') }}",
					type: "{{ Session::get('after_save.alert') }}",
					showConfirmButton: false,
					timer: 2000,
				})
			}
			alertSuccess();
        @endif
	</script>
@endsection
