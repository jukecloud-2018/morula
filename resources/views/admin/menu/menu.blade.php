@extends('layouts.layout')
@section('content')
    <main class="mn-inner">
        <div class="col s12">
            <div>
                <ul class="tabs tab-demo z-depth-1" id="tabsId" style="width: 100%;">
                    <li class="tab col s3"><a href="#menu">Menu</a></li>
                    <li class="tab col s3"><a href="#jobtype">Job Type</a></li>
                    <div class="indicator" style="right: 372.75px; left: 0px;"></div>
                </ul>

                {{--Main tab menu--}}
                <div id="menu">
                    <div class="col s12">
                        <div>
                            <div id="regis" >
                                <div class="card-content">
                                    {{--star view menu--}}
                                    <div class="card-panel" id="data_reservation">
                                        <table id="dataMenu" class="display responsive-table datatable-example" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Menu Name</th>
                                                <th>Sort</th>
                                                <th>Parent</th>
                                                <th>Roles</th>
                                                <th>URL</th>
                                                <th>Active Classes</th>
                                                <th>Icon</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    {{--akhir dari tampilan menu--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--Akhir dari tab menu--}}

                {{--Main Tab jobtype--}}
                <div id="jobtype">
                    <div class="col s12">
                        <div>
                            <div id="regis" >
                                <div class="card-content">
                                    {{--star view jobtype--}}
                                    <div class="card-panel" id="data_reservation">
                                        <table id="dataJob" class="display responsive-table datatable-example" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Job Type</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    {{--akhir dari tampilan jobtype--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--Akhir tab jobtype--}}

            </div>
        </div>

        {{--Modals New Menu--}}
        <div id="newMenu" class="modal modal-fixed-footer">
            <form action="{{ route('menu.index') }}" method="post" id="newformMenu">
                {{ csrf_field() }}
                <div class="modal-content">
                    <h4 style="font-size: 20px;padding-left: 45%;">New Menu</h4><br>
                    <div class="row">
                        <label class="col lebel">Menu Name</label>
                        <input type="text" name="name" placeholder="Menu Name" class="col s8">
                    </div>
                    <div class="row">
                        <label class="col lebel">Sort</label>
                        <input type="text" name="sort" placeholder="Sort" class="col s8">
                    </div>
                    <div class="row">
                        <label class="col lebel">Parent</label>
                        <input type="text" name="parent_id" placeholder="Parent" class="col s8">
                    </div>
                    <div class="row">
                        @foreach($roles as $item)
                            <label class="col lebel">{{ $item->id == $max[0]->id ? 'Roles' : '&nbsp;' }}</label>
                            <p class="p-v-xs">
                                <input id="rolenew_{{ $item->id }}" name="roles[]" type="checkbox" class="filled-in" value="{{ $item->id }}" />
                                <label for="rolenew_{{ $item->id }}">{{ $item->name }}</label>
                            </p>
                        @endforeach
                    </div>
                    <div class="row">
                        <label class="col lebel">URL</label>
                        <input type="text" name="url" placeholder="URL" class="col s8">
                    </div>
                    <div class="row">
                        <label class="col lebel">Active Classes</label>
                        <input type="text" name="active_classes" placeholder="Active Classes" class="col s8">
                    </div>
                    <div class="row">
                        <label class="col lebel">Icon</label>
                        <input type="text" name="icon" placeholder="Icon" class="col s8">
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="button_addMenu" type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
                    <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
                </div>
            </form>
        </div>

        {{--Modals Details Menu--}}
        <div id="dataDetailsMenu" class="modal  modal-fixed-footer">
            <form action="" method="post" id="editmenu">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="modal-content">
                    <h4 style="font-size: 20px;padding-left: 45%;">Edit Menu</h4><br>
                    <div class="modal-content">
                        <div class="row">
                            <label class="col lebel">Menu Name</label>
                            <input type="text" name="name" placeholder="Menu Name" class="col s8">
                        </div>
                        <div class="row">
                            <label class="col lebel">Sort</label>
                            <input type="text" name="sort" placeholder="Sort" class="col s8">
                        </div>
                        <div class="row">
                            <label class="col lebel">Parent</label>
                            <input type="text" name="parent_id" placeholder="Parent" class="col s8">
                        </div>
                        <div class="row">
                            @foreach($roles as $item)
                                <label class="col lebel">{{ $item->id == $max[0]->id ? 'Roles' : '&nbsp;' }}</label>
                                <p class="p-v-xs">
                                    <input id="rolenew_{{ $item->id }}" name="roles[]" type="checkbox" class="filled-in" value="{{ $item->id }}" />
                                    <label for="rolenew_{{ $item->id }}">{{ $item->name }}</label>
                                </p>
                            @endforeach
                        </div>
                        <div class="row">
                            <label class="col lebel">URL</label>
                            <input type="text" name="url" placeholder="URL" class="col s8">
                        </div>
                        <div class="row">
                            <label class="col lebel">Active Classes</label>
                            <input type="text" name="active_classes" placeholder="Active Classes" class="col s8">
                        </div>
                        <div class="row">
                            <label class="col lebel">Icon</label>
                            <input type="text" name="icon" placeholder="Icon" class="col s8">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="button_editMenu" type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
                    <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
                </div>
            </form>
        </div>

        {{--Modals New Job Type--}}
        <div id="newJob" class="modal modal-fixed-footer">
            <form action="{{ route('job_type.index') }}" method="post" id="newformJob">
                {{ csrf_field() }}
                <div class="modal-content">
                    <h4 style="font-size: 20px;padding-left: 45%;">New Job Type</h4><br>
                    <div class="row">
                        <label class="col lebel">Job Type Name</label>
                        <input type="text" name="name" placeholder="Job Type" class="col s8">
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="button_addJob" type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
                    <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
                </div>
            </form>
        </div>

        {{--Modals Details Job Type--}}
        <div id="dataDetailsJob" class="modal  modal-fixed-footer">
            <form action="" method="post" id="editJob">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="modal-content">
                    <h4 style="font-size: 20px;padding-left: 45%;">Edit Visit Code</h4><br>
                    <div class="modal-content">
                        <div class="row">
                            <label class="col lebel">Job Type Name</label>
                            <input type="text" name="name" placeholder="Job Type" class="col s8">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="button_editJob" type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
                    <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
                </div>
            </form>
        </div>

    </main>
@endsection

@section('js')
    <script>
        function add_new() {
            $('#newMenu').openModal('open');
        }
        $(document).ready(function() {
            getdataMenu();
            $('.dataTables_length select').addClass('browser-default');
        });
        //ini ajax untuk nampilin data menu
        function getdataMenu(){
            $('#dataMenu').DataTable( {
                dom : 'lf<"#add_new">rtip',
                language: {
                    searchPlaceholder: 'Search records',
                    sSearch: '',
                    sLengthMenu: 'Show _MENU_',
                    sLength: 'dataTables_length',
                    oPaginate: {
                        sFirst: '<i class="material-icons">chevron_left</i>',
                        sPrevious: '<i class="material-icons">chevron_left</i>',
                        sNext: '<i class="material-icons">chevron_right</i>',
                        sLast: '<i class="material-icons">chevron_right</i>'
                    }
                },
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url": "/admin/menu/ajaxdt",
                    "dataType": "json",
                    "type": "POST",
                },
                "columns": [
                    { "data": "id",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { "data": "sort" },
                    { "data": "parent_id" },
                    { "data": "name" },
                    { "data": "roles" },
                    { "data": "url" },
                    { "data": "active_classes" },
                    { "data": "icon" },
                    {
                        "mRender": function (data, type, row, meta) {
                            return '<a class="btn-floating" onclick="detailMenu(`' + row.id + '`)"><i class="material-icons">zoom_in</i></a><a id="' + row.id + '" style="margin-left: 5%;" class="btn-floating" onclick="delMenu(`' + row.id + '`)"><i class="material-icons">delete</i></a>';
                        }
                    }
                ],
            });
            $("#add_new").html('<a style="margin-left: 2%;margin-top: 25px;" class="waves-effect waves-light btn modal-trigger" onclick="add_new()">Add</a>');
        }
        //ini ajax detail
        function detailMenu(id){
            $.ajax({
                type: 'POST',
                url: '/admin/menu/editajax',
                data: {
                    'id' : id,
                },
                async: false,
                dataType: 'json',
                success: function(data) {
                    var isi = (data.data);
                    $('#editmenu').attr('action', '/admin/menu/' + isi.id);
                    $('#editmenu input[name="name"]').val(isi.name);
//                    $('#editmenu input[name="sort"]').val(isi.sort);
//                    $('#editmenu input[name="parent_id"]').val(isi.parent_id);
//                    $('#editmenu input[name="url"]').val(isi.url);
//                    $('#editmenu input[name="active_classes"]').val(isi.active_classes);
//                    $('#editmenu input[name="icon"]').val(isi.icon);

                    $("input[id*='role_'").prop('checked', false);

                    console.log(isi.roles);
                    if(isi.roles != null){
                        var roles = isi.roles.split(",");

                        roles.forEach(function(item){
                            console.log('#role_' + item);
                            $('#role_' + item).prop('checked', true);
                        });
                    }
                }
            });
            $('#dataDetailsMenu').openModal();
        }

        function delMenu(id) {
            swal({
                title: "Are you sure?",
                text: "Delete this menu?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Confirm",
                closeOnConfirm: true
            }, function(){
                event.preventDefault();
                $.ajax({
                    type: "DELETE",
                    url: "/admin/menu/" + id,
                    dataType:"json",
                    success: function(data){
                        if(data.success == true){
                            //location.reload(true);
                            $('#dataMenu').DataTable().destroy();
                            getdataMenu();
                        }
                    }
                });
                return false;
            });
        }

        //Button add Menu
        $('#button_addMenu').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: '/admin/menu',
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#newformMenu').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) {
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n');
                    } else {
                        var descr = data.description;
                        $('#dataMenu').DataTable().destroy();
                        getdataMenu();
                        $(':input','#newformMenu')
                            .not(':button, :submit, :reset, :hidden')
                            .val('');
                    }
                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        //Button Submit edit menu
        $('#button_editMenu').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: $('#editmenu').attr('action'),
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#editmenu').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) {
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n');
                    } else {
                        var descr = data.description;
                        $('#dataMenu').DataTable().destroy();
                        getdataMenu();
                    }
                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        //New Job Type
        function add_newJob() {
            $('#newJob').openModal('open');
        }
        $(document).ready(function() {
            getdataJob();
            $('.dataTables_length select').addClass('browser-default');
        });
        //ini ajax untuk nampilin data
        function getdataJob(){
            $('#dataJob').DataTable( {
                dom : 'lf<"#add_newJob">rtip',
                language: {
                    searchPlaceholder: 'Search records',
                    sSearch: '',
                    sLengthMenu: 'Show _MENU_',
                    sLength: 'dataTables_length',
                    oPaginate: {
                        sFirst: '<i class="material-icons">chevron_left</i>',
                        sPrevious: '<i class="material-icons">chevron_left</i>',
                        sNext: '<i class="material-icons">chevron_right</i>',
                        sLast: '<i class="material-icons">chevron_right</i>'
                    }
                },
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url": "/admin/job_type/ajaxdt",
                    "dataType": "json",
                    "type": "POST",
                },
                'order': [[ 0, 'desc' ]],
                "columns": [
                    { "data": "id",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { "data": "name" },
                    {
                        "mRender": function (data, type, row, meta) {
                            return '<a class="btn-floating" onclick="detailJob(`' + row.id + '`)"><i class="material-icons">zoom_in</i></a><a id="' + row.id + '" style="margin-left: 5%;" class="btn-floating" onclick="delJob(`' + row.id + '`)"><i class="material-icons">delete</i></a>';
                        }
                    }
                ],
            });
            $("#add_newJob").html('<a style="margin-left: 2%;margin-top: 25px;" class="waves-effect waves-light btn modal-trigger" onclick="add_newJob()">Add</a>');
        }
        //ini ajax detail
        function detailJob(id){
            $.ajax({
                type: 'POST',
                url: '/admin/job_type/editajax',
                data: {
                    'id' : id,
                },
                async: false,
                dataType: 'json',
                success: function(data) {
                    var isi = (data.data);
                    $('#editJob').attr('action', '/admin/job_type/' + isi.id);
                    $('#editJob input[name="name"]').val(isi.name);
                }
            });
            $('#dataDetailsJob').openModal();
        }

        function delJob(id) {
            swal({
                title: "Are you sure?",
                text: "Delete this job type?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Confirm",
                closeOnConfirm: true
            }, function(){
                event.preventDefault();
                $.ajax({
                    type: "DELETE",
                    url: "/admin/job_type/" + id,
                    dataType:"json",
                    success: function(data){
                        if(data.success == true){
                            $('#dataJob').DataTable().destroy();
                            getdataJob();
                        }
                    }
                });
                return false;
            });
        }

        //Button add Menu
        $('#button_addJob').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: '/admin/job_type/jobtype',
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#newformJob').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) {
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n');
                    } else {
                        var descr = data.description;
                        $('#dataJob').DataTable().destroy();
                        getdataJob();
                        $(':input','#newformJob')
                            .not(':button, :submit, :reset, :hidden')
                            .val('');
                    }
                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        //Button Submit edit job
        $('#button_editJob').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: $('#editJob').attr('action'),
                type: 'post',
                dataType: 'json',
                async: false,
                data: $('form#editJob').serialize(),
                success: function(data) {
                    var desc = new Array();
                    if(data.status != 200){
                        $.each(data.description, function(i,d) {
                            desc.push(d[0]);
                        });
                        var descr = desc.join('\n');
                    } else {
                        var descr = data.description;
                        $('#dataJob').DataTable().destroy();
                        getdataJob();
                    }
                    alertPopup(data.title, descr, data.alert);
                },
                error: function(d) {
                    console.log('error');
                }
            });
        });

        function alertPopup(title, text, type){
            swal({ title: title,
                text: text,
                type: type,
            })
        }
                @if (Session::has('after_save'))
        var t = "{{ Session::get('after_save.title') }}";
        var txt = "{{ Session::get('after_save.text-1') }} {{ Session::get('after_save.text-2') }}";
        var ty = "{{ Session::get('after_save.alert') }}";
        alertPopup(t, txt,ty);
        @endif
    </script>
@endsection
