@extends('layouts.layout')
@section('content')
	<main class="mn-inner">
		<div class="col s12">
			<div>
				<div id="res">
					<div class="col s12">
						<div>
							<div id="regis" >
								<div class="card-content">

									<div class="card-panel" id="data_reservation">
										<table id="data" class="display responsive-table datatable-example" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>No.</th>
													<th>Name</th>
													<th>Description</th>
													<th>Actions</th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		{{--Modals New--}}
		<div id="newData" class="modal modal-fixed-footer">
			<form action="{{ route('role.index') }}" method="post" id="new">
			{{ csrf_field() }}
				<div class="modal-content">
					<h4 style="font-size: 20px;padding-left: 45%;">New Role</h4><br>
					
					<div class="row">
						<label class="col lebel">Role Name</label>
						<input type="text" name="name" placeholder="Role Name" class="col s8">
					</div>
					
					<div class="row">
						<label class="col lebel">Description</label>
						<textarea class="materialize-textarea col s8" name="description" data-length="120"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
					<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
				</div>
			</form>
		</div>

		{{--Modals Details--}}
		<div id="dataDetails" class="modal  modal-fixed-footer">
			<form action="" method="post" id="edit">
				{{ method_field('PUT') }}
				{{ csrf_field() }}
				<div class="modal-content">
					<h4 style="font-size: 20px;padding-left: 45%;">Edit Role</h4><br>
					
					<div class="row">
						<label class="col lebel">Role Name</label>
						<input type="text" name="name" placeholder="Role Name" class="col s8">
					</div>
					
					<div class="row">
						<label class="col lebel">Description</label>
						<textarea class="materialize-textarea col s8" name="description" data-length="120"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="modal-action modal-close waves-effect waves-blue btn-flat ">Submit</button>
					<a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Cancel</a>
				</div>
			</form>
		</div>


	</main>
@endsection

@section('js')
	<script>
        function add_new() {
            $('#newData').openModal('open');
        }
		$(document).ready(function() {
			getdata();
			$('.dataTables_length select').addClass('browser-default');
		});
		
		function getdata(){
			$('#data').DataTable( {
                dom : 'lf<"#add_new">rtip',
				language: {
					searchPlaceholder: 'Search records',
					sSearch: '',
					sLengthMenu: 'Show _MENU_',
					sLength: 'dataTables_length',
					oPaginate: {
						sFirst: '<i class="material-icons">chevron_left</i>',
						sPrevious: '<i class="material-icons">chevron_left</i>',
						sNext: '<i class="material-icons">chevron_right</i>',
						sLast: '<i class="material-icons">chevron_right</i>'
					}
				},
				"processing": true,
				"serverSide": true,
				"ajax":{
					"url": "/admin/role/ajaxdt",
					"dataType": "json",
					"type": "POST",
				},
				"columns": [
					{
						"data": "id",
						render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
						}
					},
					{ "data": "name" },
					{ "data": "description" },
					{
						"mRender": function (data, type, row, meta) {
							return '<a class="btn-floating" onclick="detail(`' + row.id + '`)"><i class="material-icons">zoom_in</i></a><a id="' + row.id + '" style="margin-left: 5%;" class="btn-floating" onclick="del(`' + row.id + '`)"><i class="material-icons">delete</i></a>';
						}
					}
				],
			});
            $("#add_new").html('<a style="margin-left: 2%;margin-top: 25px;" class="waves-effect waves-light btn modal-trigger" onclick="add_new()">Add</a>');
		}
		
		function detail(id){
			$.ajax({
				type: 'POST',
				url: '/admin/role/editajax',
				data: {
					'id' : id,
				},
				async: false,
				dataType: 'json',
				success: function(data) {
					var isi = (data.data);
					$('#edit').attr('action', '/admin/role/' + isi.id);
					$('#edit input[name="name"]').val(isi.name);
					$('#edit textarea[name="description"]').val(isi.description);
				}

			});
            $('#dataDetails').openModal();
        }
		
		function del(id) {
			swal({
				title: "Are you sure?",
				text: "Delete this role?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Confirm",
				closeOnConfirm: true
			}, function(){
				event.preventDefault();
				$.ajax({
					type: "DELETE",
					url: "/admin/role/" + id,
					dataType:"json",  
					success: function(data){
						if(data.success == true){
							//location.reload(true);
							$('#data').DataTable().destroy();
							getdata();
						}
					}
				});
				return false;
			});
		}
		
        @if (Session::has('after_save'))
            function alertPopup(){
				swal({ title: "{{ Session::get('after_save.title') }}",
					text: "{{ Session::get('after_save.text-1') }} {{ Session::get('after_save.text-2') }}",
					type: "{{ Session::get('after_save.alert') }}",
					showConfirmButton: false,
					timer: 2000,
				})
			}
			alertPopup();
        @endif
	</script>
@endsection
