@extends('layouts.layout-admin')
@section('content')
	<main class="mn-inner">
		<div class="col s12">
			<div>
				<div id="res">
					<div class="col s12">
						<div>
							<div id="regis" >
								<form action='{{ url("admin/role/$role->id") }}' class="col s12 m12 card-panel" method="post" id="form_edit">
								{{ method_field('PUT') }}
								{{ csrf_field() }}
									<div class="card-content">
										<div class="card-panel" id="data_reservation">
											<div id="newPatient" class="">
												<div class="modal-content">
													<h4 style="font-size: 20px;padding-left: 45%;">Edit Role</h4><br>
													
													<div class="row">
														<label class="col lebel">Full Name</label>
														<input value="{{ old('name', $role->name) }}" type="text" name="name" placeholder="Full Name" class="col s8">
														<span class="label label-danger red-text text-darken-1">{{ $errors->update->first('name') }}</span>
													</div>
													
													<div class="row">
														<label class="col lebel">Description</label>
														<textarea class="materialize-textarea col s8" name="description" data-length="120">{{ old('description', $role->description) }}</textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="row" style="margin-top: 20px;">
											</div>
											<div class="row">
												<a style="margin-left: 5%;" class="waves-effect waves-light btn modal-trigger" href="{{ route('role.index') }}">Back</a>
												<button style="margin-left: 5%" type="submit" class="waves-effect waves-light btn" href="#">Save</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		{{--Modals--}}
		
	</main>
@endsection

@section('js')
	<script>
		@if (Session::has('after_save'))
            function alertSuccess(){
				swal({ title: "{{ Session::get('after_save.title') }}",
					text: "{{ Session::get('after_save.text-1') }} {{ Session::get('after_save.text-2') }}",
					type: "{{ Session::get('after_save.alert') }}",
					showConfirmButton: false,
					timer: 2000,
				})
			}
			alertSuccess();
        @endif
	</script>
@endsection
