@extends('layouts.layout')
@section('content')
    <main class="mn-inner">

        <div class="row">

            <div class="col s12">
                <div class="page-title">Calendar</div>
            </div>
            <div class="col s12 m12 l12">
                <div class="card">
                    <div class="card-content">
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>

        </div>

    </main>


@endsection

@section('js')
    <script type="text/javascript">
        $( document ).ready(function() {
            var today = new Date();
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                disableDragging: true,
                minTime: "09:00:00",
                maxTime: "16:00:00",
                defaultView: 'agendaWeek',
                defaultDate: today,
                editable: false,
                eventLimit: true, // allow "more" link when too many events
                events: [
                    {
                        title: 'Reni Wahyuni',
                        start: '2017-10-19T09:00:00',
                        end: '2017-10-19T10:30:00',
                        description: 'RSIA Cikini'
                    },
                    {
                        title: 'Anggun Prasasti',
                        description: 'long description',
                        start: '2017-10-19T11:00:00',
                        end: '2017-10-19T12:30:00'
                    },
                    {
                        title: 'Rini',
                        description: 'long description',
                        start: '2017-10-18T10:00:00',
                        end: '2017-10-18T11:30:00'
                    }

                ],
                eventRender: function(event, element) {
                    element.find('.fc-title').append("<br/>" + event.description);
                }
            });

        });
    </script>
@endsection