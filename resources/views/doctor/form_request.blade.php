@extends('layouts.layout')
@section('content')
    <main class="mn-inner">

        <div class="col s12">
            <ul class="tabs tab-demo z-depth-1" style="width: 100%;">
                <li class="tab col s3"><a href="#full_day_leave">Full Day Leave</a></li>
                <li id="registration" class="tab col s3"><a class="" href="#session_leave">Session Leave</a></li>
                <li class="tab col s3"><a href="#late">Late</a></li>
                <div class="indicator" style="right: 372.75px; left: 0px;"></div>
            </ul>

        </div>


                <div id="full_day_leave">
                        <div class="row">
                            <div class="card col s6 offset-l3 no-padding-hor">
                                <div class="center-align card-title teal white-text" style="font-size: 20px">Doctor Leave Request Form</div>
                                <div class="card-content">
                                    {{--The Form Here--}}
                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <label for="height" class="col s2 lebel">Doctor</label>
                                            <input type="text" class="col s8 validate no-padding">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <label for="height" class="col s2 lebel">Start Date</label>
                                            <input type="text" class="datepicker date_now col s3">
                                            <i class="material-icons col" style="padding-top: 10px;">date_range</i>

                                            <label for="height" class="col s2 lebel">End Date</label>
                                            <input type="text" class="datepicker date_now col s3">
                                            <i class="material-icons col" style="padding-top: 10px;">date_range</i>                                            
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col s8 no-padding">
                                            <label for="height" class="col s4 lebel">Leave Type</label>
                                                <select class="col s6 no-padding">
                                                    <option value="1">tes</option>
                                                    <option value="2">tes2</option>
                                                </select>
                                             </div>
                                        </div>
                                  

                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <label for="height" class="col s2 lebel">Cause</label>
                                            <div class="input-field col s8">
                                                <textarea id="textarea1" class="materialize-textarea" length="120"></textarea>
                                                <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"></span></div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <div class="input-field col s8" style="margin-left:310px;">
                                                <button type="submit" class="waves-effect waves-light btn col s5 offset-s1" href="#">Submit</button></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
            </div>


                <div id="session_leave">
                    <div class="row">
                        <div class="card col s6 offset-l3 no-padding-hor">
                            <div class="center-align card-title teal white-text" style="font-size: 20px">Doctor Session Leave Request Form</div>
                                <div class="card-content">
                                    {{--The Form Here--}}
                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <label for="height" class="col s2 lebel">Doctor</label>
                                            <input type="text" class="col s8 validate no-padding">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <label for="height" class="col s2 lebel">Date</label>
                                            <input type="text" class="datepicker date_now col s3">
                                            <i class="material-icons col" style="padding-top: 10px;">date_range</i>

                                            <label for="height" class="col s2 lebel">Session</label>
                                            <select class="col s3">
                                                <option value="1">tes</option>
                                                <option value="2">tes2</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <label for="height" class="col s2 lebel">Cause</label>
                                            <div class="input-field col s8">
                                                <textarea id="textarea1" class="materialize-textarea" length="120"></textarea>
                                                <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"></span></div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <div class="input-field col s8" style="margin-left:310px;">
                                                <button type="submit" class="waves-effect waves-light btn col s5 offset-s1" href="#">Submit</button></div>
                                        </div>
                                    </div>

                            </div>
                         </div>
                    </div>
                </div>

                <div id="late">
                        <div class="row">
                            <div class="card col s6 offset-l3 no-padding-hor">
                                <div class="center-align card-title teal white-text" style="font-size: 20px">Doctor Late Information Form</div>
                                <div class="card-content">
                                    {{--The Form Here--}}
                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <label for="height" class="col s2 lebel">Doctor</label>
                                            <input type="text" class="col s8 validate no-padding">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <label for="height" class="col s2 lebel">Date</label>
                                            <input type="text" class="datepicker date_now col s3">
                                            <i class="material-icons col" style="padding-top: 10px;">date_range</i>

                                            <label for="height" class="col s2 lebel">Session</label>
                                            <select class="col s3">
                                                <option value="1">tes</option>
                                                <option value="2">tes2</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col offset-s5">
                                        <label for="height" class="col s5 lebel" style="margin-left:15px">Start time</label>
                                        <select class="col s5 no-padding">
                                            <option value="1">tes</option>
                                            <option value="2">tes2</option>
                                        </select>
                                    </div>

                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <label for="height" class="col s2 lebel">Cause</label>
                                            <div class="input-field col s8">
                                                <textarea id="textarea1" class="materialize-textarea" length="120"></textarea>
                                                <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"></span></div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col s12 no-padding">
                                            <div class="input-field col s8" style="margin-left:310px;">
                                                <button type="submit" class="waves-effect waves-light btn col s5 offset-s1" href="#">Submit</button></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
            </div>

    </main>


@endsection