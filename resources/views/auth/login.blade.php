@extends('layouts.apps')

@section('content')


	<div class="mn-content valign-wrapper">
		<main class="mn-inner container" style="padding-top: 0px;">
			<img src="assets/images/icons/morula/logo-morulla.png" style="margin-left: 41%;padding-bottom: 5%;">
			<div class="valign">
				<div class="row">
					<div class="col s12 m6 l4 offset-l4 offset-m3" style="margin-left:28%;">

						<div class="card-content-login">
							<span class="card-title"></span>
							<div class="row">
								<form class="col s12" method="POST" style="margin-top: 4em;" action="{{ route('login') }}">
								{{ csrf_field() }}
									<div class="input-field col s12" style="padding: 0 2rem;">
										<input name="username" id="username" type="text" class="validate" style="background: white; border-radius: 5px;" placeholder="Username" value="{{ old('username') }}">

									</div>
									<div class="input-field col s12" style="padding: 0 2rem;">
										<input name="password" id="password" type="password" class="validate" style="background: white;border-radius: 5px;" placeholder="Password">

									</div>
									<div class="col s12 right-align m-t-sm" style="padding-right: 2rem;">
										<!--     <a href="sign-up.html" class="waves-effect waves-light btn teal">sign up</a> -->
										<button type="submit" class="waves-effect waves-light btn-flat cream">sign in</button>
									</div>
								</form>
							</div>
						</div>

					</div>
				</div>
			</div>
		</main>
	</div>

@endsection
