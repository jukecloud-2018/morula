<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Title -->
    <title>Morula Login</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta charset="UTF-8">
    <meta name="description" content="Responsive Admin Dashboard Template" />
    <meta name="keywords" content="admin,dashboard" />
    <meta name="author" content="Steelcoders" />

    <!-- Styles -->
    <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.css"/>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">


    <!-- Theme Styles -->
    <link href="assets/css/alpha.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="signin-page" style="background-color: #FFF;">
<div class="mn-content valign-wrapper">
    <main class="mn-inner container" style="padding-top: 0px;">
        <img src="assets/images/icons/morula/logo-morulla.png" style="margin-left: 41%;padding-bottom: 5%;">
        <div class="valign">
            <div class="row">
                <div class="col s12 m6 l4 offset-l4 offset-m3" style="margin-left:28%;">

                    <div class="card-content-login">
                        <span class="card-title"></span>
                        <div class="row">
                            <form class="col s12" style="margin-top: 4em;">
                                <div class="input-field col s12" style="padding: 0 2rem;">
                                    <input id="email" type="email" class="validate" style="background: white; border-radius: 5px;" placeholder="Username">

                                </div>
                                <div class="input-field col s12" style="padding: 0 2rem;">
                                    <input id="password" type="password" class="validate" style="background: white;border-radius: 5px;" placeholder="Password">

                                </div>
                                <div class="col s12 right-align m-t-sm" style="padding-right: 2rem;">
                                    <!--     <a href="sign-up.html" class="waves-effect waves-light btn teal">sign up</a> -->
                                    <a href="index.html" class="waves-effect waves-light btn-flat cream">sign in</a>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </main>
</div>

<!-- Javascripts -->
<script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="assets/plugins/materialize/js/materialize.min.js"></script>
<script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
<script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="assets/js/alpha.min.js"></script>

</body>
</html>