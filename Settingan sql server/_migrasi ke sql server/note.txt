1. instal sql server 2014 express
2. buat database
3. import database dari mysql -> sqlserver
  -caranya di generating script pake tool SMSA
  -setelah itu import biasa ke sqlserver
4. setelah berhasil import, maka copy file dibawah ini ke xampp/php/ext
  - php_pdo_sqlsrv_56_ts.ddl
  - php_sqlsrv_56_ts.ddl
5. edit file php.ini di directory xampp/php
6. tambahkan code berikut dibawah extension
  - ;extension=php_pdo_sqlsrv_56_ts.ddl
  - ;extension=php_sqlsrv_56_ts.ddl
  kalo ada (;) itu komentar
7. restart xampp control panel -> service apache, if cannot start:
  - stop service SQL Server Reporting Service
  - atur properties ganti pilihan (automatic->manual)
8. jalankan apache
10. tambahin virtual host
C:\xampp\apache\conf\extra\httpd-vhosts.conf
tambahan jika masih tidak bisa:
1. cek dulu xampp/apache/conf/extra/httpd-vhosts.conf
  -tambahkan script berikut:
  <VirtualHost *:80>
    ServerAdmin admin@localhost
    DocumentRoot "E:/xampp/htdocs"
    ServerName localhost
</VirtualHost>

<VirtualHost *:80>
    ServerAdmin admin@morula.dev
    DocumentRoot "E:/xampp/htdocs/morula-devel/public"
    ServerName morula.dev
    ErrorLog "logs/morula.dev-error.log"
    CustomLog "logs/morula.dev.access.log" common
</VirtualHost>

11. setting name host
C:\Windows\System32\drivers\etc\host
127.0.0.2 morula (contoh)



  