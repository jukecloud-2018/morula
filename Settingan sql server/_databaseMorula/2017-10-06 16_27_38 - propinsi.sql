-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.26-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table k_auto2000.xpropinsi
DROP TABLE IF EXISTS `xpropinsi`;
CREATE TABLE IF NOT EXISTS `xpropinsi` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xpropinsi: ~34 rows (approximately)
DELETE FROM `xpropinsi`;
/*!40000 ALTER TABLE `xpropinsi` DISABLE KEYS */;
INSERT INTO `xpropinsi` (`id`, `nama`) VALUES
	(11, 'ACEH'),
	(51, 'BALI'),
	(36, 'BANTEN'),
	(17, 'BENGKULU'),
	(34, 'DI YOGYAKARTA'),
	(31, 'DKI JAKARTA'),
	(75, 'GORONTALO'),
	(15, 'JAMBI'),
	(32, 'JAWA BARAT'),
	(33, 'JAWA TENGAH'),
	(35, 'JAWA TIMUR'),
	(61, 'KALIMANTAN BARAT'),
	(63, 'KALIMANTAN SELATAN'),
	(62, 'KALIMANTAN TENGAH'),
	(64, 'KALIMANTAN TIMUR'),
	(65, 'KALIMANTAN UTARA'),
	(19, 'KEPULAUAN BANGKA BELITUNG'),
	(21, 'KEPULAUAN RIAU'),
	(18, 'LAMPUNG'),
	(81, 'MALUKU'),
	(82, 'MALUKU UTARA'),
	(52, 'NUSA TENGGARA BARAT'),
	(53, 'NUSA TENGGARA TIMUR'),
	(94, 'PAPUA'),
	(91, 'PAPUA BARAT'),
	(14, 'RIAU'),
	(76, 'SULAWESI BARAT'),
	(73, 'SULAWESI SELATAN'),
	(72, 'SULAWESI TENGAH'),
	(74, 'SULAWESI TENGGARA'),
	(71, 'SULAWESI UTARA'),
	(13, 'SUMATERA BARAT'),
	(16, 'SUMATERA SELATAN'),
	(12, 'SUMATERA UTARA');
/*!40000 ALTER TABLE `xpropinsi` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
