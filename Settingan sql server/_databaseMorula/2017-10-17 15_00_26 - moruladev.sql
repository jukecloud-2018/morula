-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.26-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table moruladev.t_doctor_request_late
DROP TABLE IF EXISTS `t_doctor_request_late`;
CREATE TABLE IF NOT EXISTS `t_doctor_request_late` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(10) DEFAULT NULL,
  `date` date NOT NULL,
  `session` time DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `cause` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `updated_by` int(3) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table moruladev.t_doctor_request_late: 0 rows
DELETE FROM `t_doctor_request_late`;
/*!40000 ALTER TABLE `t_doctor_request_late` DISABLE KEYS */;
INSERT INTO `t_doctor_request_late` (`id`, `doctor_id`, `date`, `session`, `start_time`, `cause`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 2, '2017-10-18', '14:54:00', '15:54:00', NULL, 1, NULL, '2017-10-17 14:55:48', '2017-10-17 14:55:48');
/*!40000 ALTER TABLE `t_doctor_request_late` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
