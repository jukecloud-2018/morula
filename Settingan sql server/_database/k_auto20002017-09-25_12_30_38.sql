-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table k_auto2000.msoal_assesment_test_d
DROP TABLE IF EXISTS `msoal_assesment_test_d`;
CREATE TABLE IF NOT EXISTS `msoal_assesment_test_d` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idH` int(11) NOT NULL,
  `idSoalAssesment` int(11) NOT NULL,
  `jawaban` varchar(1) NOT NULL,
  `kunciJawaban` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `h_id_m_soalassesment_id` (`idH`,`idSoalAssesment`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.msoal_assesment_test_d: ~0 rows (approximately)
DELETE FROM `msoal_assesment_test_d`;
/*!40000 ALTER TABLE `msoal_assesment_test_d` DISABLE KEYS */;
INSERT INTO `msoal_assesment_test_d` (`id`, `idH`, `idSoalAssesment`, `jawaban`, `kunciJawaban`) VALUES
	(13, 3, 1, 'D', 'E'),
	(14, 3, 2, 'E', 'E'),
	(15, 3, 3, 'A', 'E'),
	(16, 3, 5, 'C', 'E'),
	(17, 3, 4, 'B', 'E'),
	(18, 3, 6, 'D', 'E'),
	(19, 4, 1, 'A', 'E'),
	(20, 4, 2, 'B', 'E'),
	(21, 4, 3, 'C', 'E'),
	(22, 4, 5, 'D', 'E'),
	(23, 4, 4, 'E', 'E'),
	(24, 4, 6, '', 'E'),
	(25, 5, 1, 'A', 'E'),
	(26, 5, 2, 'A', 'E'),
	(27, 5, 3, 'A', 'E'),
	(28, 5, 5, 'A', 'E'),
	(29, 5, 4, '', 'E'),
	(30, 5, 6, '', 'E'),
	(31, 6, 1, 'B', 'E'),
	(32, 6, 2, 'B', 'E'),
	(33, 6, 3, '', 'E'),
	(34, 6, 5, 'B', 'E'),
	(35, 6, 4, '', 'E'),
	(36, 6, 6, '', 'E');
/*!40000 ALTER TABLE `msoal_assesment_test_d` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.msoal_assesment_test_h
DROP TABLE IF EXISTS `msoal_assesment_test_h`;
CREATE TABLE IF NOT EXISTS `msoal_assesment_test_h` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idResource` int(11) NOT NULL,
  `jumlahSoal` int(11) NOT NULL,
  `jumlahBenar` int(11) NOT NULL,
  `jumlahSalah` int(11) NOT NULL,
  `jumlahTidakDiJawab` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.msoal_assesment_test_h: ~0 rows (approximately)
DELETE FROM `msoal_assesment_test_h`;
/*!40000 ALTER TABLE `msoal_assesment_test_h` DISABLE KEYS */;
INSERT INTO `msoal_assesment_test_h` (`id`, `idUser`, `idResource`, `jumlahSoal`, `jumlahBenar`, `jumlahSalah`, `jumlahTidakDiJawab`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(3, 11, 1, 6, 1, 5, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 11, 1, 6, 1, 4, 1, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 11, 1, 6, 0, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 11, 1, 6, 0, 3, 3, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `msoal_assesment_test_h` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
