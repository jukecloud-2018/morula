-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.26-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table grahitadb.t_evaluasi
DROP TABLE IF EXISTS `t_evaluasi`;
CREATE TABLE IF NOT EXISTS `t_evaluasi` (
  `id_evaluasi` int(11) NOT NULL AUTO_INCREMENT,
  `id_siswa` int(11) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `nilai_mtk` int(11) NOT NULL,
  `nilai_bi` int(11) NOT NULL,
  `nilai_ipa` int(11) NOT NULL,
  `keterangan` enum('Paham','Belum Paham','','') NOT NULL,
  PRIMARY KEY (`id_evaluasi`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Dumping data for table grahitadb.t_evaluasi: 33 rows
DELETE FROM `t_evaluasi`;
/*!40000 ALTER TABLE `t_evaluasi` DISABLE KEYS */;
INSERT INTO `t_evaluasi` (`id_evaluasi`, `id_siswa`, `nama_siswa`, `nilai_mtk`, `nilai_bi`, `nilai_ipa`, `keterangan`) VALUES
	(1, 1, 'Tia Tiara', 70, 80, 80, ''),
	(2, 2, 'Faza Oktaviera Nur', 70, 78, 80, ''),
	(3, 3, 'Ahmad Rizki Januar', 75, 80, 75, ''),
	(4, 4, 'Nurul Fitriani', 67, 85, 80, ''),
	(5, 5, 'M. Herdi Gunawan', 67, 80, 75, ''),
	(6, 6, 'Muhammad Salman Nurfaiz', 70, 80, 75, ''),
	(7, 7, 'Indra Lesmana', 75, 75, 70, ''),
	(8, 8, 'Muhammad Alham Nur Arief', 73, 75, 75, ''),
	(9, 9, 'Rizky Rivaldi Muchtar', 70, 80, 75, ''),
	(10, 10, 'Heri Sam', 70, 85, 75, ''),
	(11, 11, 'Radika Krisna Anugrah', 68, 80, 70, ''),
	(12, 12, 'Marlina Triawan', 70, 75, 75, ''),
	(13, 13, 'Widiya Pratiwi Siswandini', 75, 85, 70, ''),
	(14, 14, 'Udan Gustiawan', 73, 80, 80, ''),
	(15, 15, 'Sifa LAtifah', 70, 80, 75, ''),
	(16, 16, 'Irfan Kurniawan', 75, 75, 73, ''),
	(17, 17, 'Nurfitri Cahyani Gumelar', 70, 75, 75, ''),
	(18, 18, 'M. Ilham Faturrahman', 75, 75, 80, ''),
	(19, 19, 'Hendra Lesmana', 68, 85, 80, ''),
	(20, 20, 'Agus Supriadi', 75, 80, 75, ''),
	(21, 21, 'Sopian', 68, 75, 75, ''),
	(22, 22, 'Septi Wahyuningsih', 70, 70, 70, ''),
	(23, 23, 'Irfan Apriliansyah', 70, 80, 75, ''),
	(24, 24, 'Juniawan', 75, 80, 73, ''),
	(25, 25, 'Luna', 75, 80, 75, ''),
	(26, 26, 'Ilham Kusuma', 73, 75, 70, ''),
	(27, 27, 'Lina Marlina', 75, 73, 85, ''),
	(28, 28, 'Eka Setiawan', 75, 75, 80, ''),
	(29, 29, 'Jajang Pamungkas', 70, 80, 80, ''),
	(30, 30, 'Edi Maryadi', 70, 80, 75, ''),
	(31, 31, 'Wahid Setiawan', 75, 75, 75, ''),
	(32, 32, 'Yogi Triansyah', 73, 75, 70, ''),
	(33, 33, 'Ernawati', 70, 80, 70, '');
/*!40000 ALTER TABLE `t_evaluasi` ENABLE KEYS */;

-- Dumping structure for table grahitadb.t_guru
DROP TABLE IF EXISTS `t_guru`;
CREATE TABLE IF NOT EXISTS `t_guru` (
  `id_guru` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) CHARACTER SET utf8 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 NOT NULL,
  `nama` varchar(30) CHARACTER SET utf8 NOT NULL,
  `foto` varchar(100) NOT NULL,
  `alamat_guru` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `notelepon_guru` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `keterangan` enum('Guru Belajar Kelompok 1','Guru Belajar Kelompok 2','Guru Belajar Kelompok 3','Guru Belajar Kelompok 4','Guru Belajar Kelompok 5','Guru Belajar Kelompok 6') CHARACTER SET utf8 DEFAULT NULL,
  `keyy` varchar(100) NOT NULL,
  PRIMARY KEY (`id_guru`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table grahitadb.t_guru: 7 rows
DELETE FROM `t_guru`;
/*!40000 ALTER TABLE `t_guru` DISABLE KEYS */;
INSERT INTO `t_guru` (`id_guru`, `nip`, `password`, `nama`, `foto`, `alamat_guru`, `notelepon_guru`, `keterangan`, `keyy`) VALUES
	(1, '196212171987032001', 'e10adc3949ba59abbe56e057f20f883e', 'Ani Aniangsih', '', 'Jl. Arcamanik No 3', '081232466344', 'Guru Belajar Kelompok 1', '4e5dc2d77b3584791b9ca5bde669c663'),
	(2, '197011172007011003', 'e10adc3949ba59abbe56e057f20f883e', 'Dian Kusnadi', '', 'Jl. Sukaluyu No 4', '089778663882', 'Guru Belajar Kelompok 2', '28c1a568f206b214904957ae1fc9d6b6'),
	(3, '197602242008012001', 'e10adc3949ba59abbe56e057f20f883e', 'Hilyatul Wadihah', '', 'Jl. Cibeunying no 2', '085734687598', 'Guru Belajar Kelompok 3', '11c619de155b49b1cfdeb290ff17a932'),
	(4, '123456789012345678', 'e10adc3949ba59abbe56e057f20f883e', 'Kartika', '', 'Jl. Setiabudi No 2', '087880923456', 'Guru Belajar Kelompok 4', '9efebb3d7d059bff092842bf31dd2816'),
	(5, '098765432109876543', 'e10adc3949ba59abbe56e057f20f883e', 'Tatat N', '', 'Jl. Cendrawasih No 5', '0822465529987', 'Guru Belajar Kelompok 5', 'd6af28e71caf4866e7d4109dc9fe6719'),
	(6, '123456789009876543', 'e10adc3949ba59abbe56e057f20f883e', 'Galih', '', 'Jl. Cimenyan No 1', '087665774772', 'Guru Belajar Kelompok 6', '96ee99458a033a39a33a40bd130e544c'),
	(10, '10112652', 'e10adc3949ba59abbe56e057f20f883e', 'Imam Nur Arifin', 'file_1483589740.JPG', 'Jl. Sukamantri 1 No 143/144 D RT 03/ RW 10', '098663615371', 'Guru Belajar Kelompok 1', 'd91ca65e9b0f652e93260287fe1c55bd');
/*!40000 ALTER TABLE `t_guru` ENABLE KEYS */;

-- Dumping structure for table grahitadb.t_siswa
DROP TABLE IF EXISTS `t_siswa`;
CREATE TABLE IF NOT EXISTS `t_siswa` (
  `id_siswa` int(11) NOT NULL AUTO_INCREMENT,
  `id_guru` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `foto` varchar(50) NOT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `jenis_kel` enum('L','P') NOT NULL,
  PRIMARY KEY (`id_siswa`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Dumping data for table grahitadb.t_siswa: 33 rows
DELETE FROM `t_siswa`;
/*!40000 ALTER TABLE `t_siswa` DISABLE KEYS */;
INSERT INTO `t_siswa` (`id_siswa`, `id_guru`, `nama`, `foto`, `alamat`, `jenis_kel`) VALUES
	(1, 1, 'Tia Tiara', 'file_1483588624.png', 'Jl. Cilendi No 2 RT 05/ RW 09 Hegarmanah Bandung', 'P'),
	(2, 1, 'Faza Oktaviera Nur', 'file_1483588679.png', 'Jl. Sekelminung Kaler No 101 RT 04/ RW 09 Bandung', 'P'),
	(3, 1, 'Ahmad Rizki Januar', '', 'Bandung', 'L'),
	(4, 1, 'Nurul Fitriani', 'file_1483588719.png', 'Jl. Ciburial Indah Kp. Lebak Siuh No 5 RT 05/RW 01', 'P'),
	(5, 1, 'M. Herdi Gunawan', 'file_1483588739.png', 'Jl. Sukasari 2 No 82 Bandung', 'L'),
	(6, 2, 'Muhammad Salman Nurfaiz', 'file_1483588770.png', 'Jl. Cibeunying Kolot V No.19 Bandung', 'L'),
	(7, 2, 'Indra Lesmana', 'file_1483588783.png', 'Jl. Mers Dirgahayu Bogeng Kalor RT 01/RW 22 Bandun', 'L'),
	(8, 2, 'Muhammad Alham Nur Arief', 'file_1483588819.png', 'Komplek Vila Bukit HA 31 No A-12 RT 05/RW 18 Kel. ', 'L'),
	(9, 2, 'Rizky Rivaldi Muchtar', 'file_1483588842.png', 'Jl. Sadang Serang Gang Palem II No 1 RT 01/RW 13 K', 'L'),
	(10, 2, 'Heri Sam', 'file_1483588856.png', 'Jl. Katamso Gang Sakiluluegan No 16a RT 06/RW 09 B', 'L'),
	(11, 3, 'Radika Krisna Anugrah', 'file_1483588969.png', 'Jl. Pramuka VIII Blok H No 107 RT 08/RW 13 Bandung', 'L'),
	(12, 3, 'Marlina Triawan', 'file_1483588885.png', 'Jl. Cirapuhan No 32 RT 04/RW 06 Kec. CImenyan Band', 'P'),
	(13, 3, 'Widiya Pratiwi Siswandini', 'file_1483588938.png', 'Bojong Kalor No 61 RT 01/RW 01 Kel. Cibeunying Kec', 'P'),
	(14, 3, 'Udan Gustiawan', 'file_1483588920.png', 'Jl. Raya Golf Dago Atas No 37 CIrapuhan RT 04/RW 0', 'L'),
	(15, 3, 'Sifa Latifah', 'file_1483589014.png', 'Jl. Cigadung Kaler 1 No 50 RT 01/RW 04 Kel.Cigadun', 'P'),
	(16, 4, 'Irfan Kurniawan', 'file_1483589031.png', 'Jl. Cibeunying Kolot No 48 RT 07/RW 21 Kel. Sadang', 'L'),
	(17, 4, 'Nurfitri Cahyani Gumelar', 'file_1483589053.png', 'Kp. Cirapuhan RT 04/RW 06', 'P'),
	(18, 4, 'M. Ilham Faturrahman', 'file_1483589071.png', 'Cijontang Indah No 7 RT 04/RW 08 Kec. Cimenyan', 'L'),
	(19, 4, 'Hendra Lesmana', 'file_1483589093.png', 'Jl. Mars Bojong Kalor RT 01/RW 22 Kel.Cibeunying K', 'L'),
	(20, 4, 'Agus Supriadi', '', 'Jl. Sukamantri 1 No 144', 'L'),
	(21, 5, 'Sopian', '', 'Jl. Cisalak No 3', 'L'),
	(22, 5, 'Septi Wahyuningsih', '', 'Jl. Cisarua No 33', 'P'),
	(23, 5, 'Irfan Apriliansyah', '', 'Jl. Tagog Apu Padalarang no 122', 'L'),
	(24, 5, 'Juniawan', '', 'Jl. Raya Majalengka no 2', 'L'),
	(25, 5, 'Luna', '', 'Jl. Raya Padalarang no 10', 'P'),
	(26, 5, 'Ilham Kusuma', '', 'Komplek Buah Batu Indah No 125', 'L'),
	(27, 6, 'Lina Marlina', '', 'Jl. Cihaliwung No 15', 'P'),
	(28, 6, 'Eka Setiawan', '', 'Jl. MArtadinata no 199', 'L'),
	(29, 6, 'Jajang Pamungkas', '', 'Jl, Setiabudi no 11', 'L'),
	(30, 6, 'Edi Maryadi', '', 'Jl. Kiarcondong no 122', 'L'),
	(31, 6, 'Wahid Setiawan', '', 'Jl. cimekar no 32', 'L'),
	(32, 6, 'Yogi Triansyah', '', 'Jl. Cikapundung no 56', 'L'),
	(33, 1, 'Ernawati', '', 'Jl. Pasirkoja no 144', 'P');
/*!40000 ALTER TABLE `t_siswa` ENABLE KEYS */;

-- Dumping structure for table grahitadb.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(10) NOT NULL AUTO_INCREMENT,
  `nama_user` varchar(30) NOT NULL,
  `nip` varchar(30) DEFAULT NULL,
  `email_user` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `alamat_user` varchar(50) NOT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `id_user` (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table grahitadb.user: 1 rows
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id_user`, `nama_user`, `nip`, `email_user`, `password`, `alamat_user`) VALUES
	(1, 'Imam Nur Arifin', '10112652', 'fairplay_ina@yahoo.co.id', '21232f297a57a5a743894a0e4a801fc3', 'Jl. Kubangsari No 33');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
