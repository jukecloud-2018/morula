-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.26-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table k_auto2000.mapp
DROP TABLE IF EXISTS `mapp`;
CREATE TABLE IF NOT EXISTS `mapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `icon` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mapp: ~0 rows (approximately)
DELETE FROM `mapp`;
/*!40000 ALTER TABLE `mapp` DISABLE KEYS */;
/*!40000 ALTER TABLE `mapp` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mapproval_status
DROP TABLE IF EXISTS `mapproval_status`;
CREATE TABLE IF NOT EXISTS `mapproval_status` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `minPowerlevel` int(1) NOT NULL DEFAULT '0' COMMENT 'minimum powerlevel yang boleh akses',
  `info` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='approval status for resource, actions, ecercise, dll\r\n';

-- Dumping data for table k_auto2000.mapproval_status: ~0 rows (approximately)
DELETE FROM `mapproval_status`;
/*!40000 ALTER TABLE `mapproval_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `mapproval_status` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mcara_kirim_info
DROP TABLE IF EXISTS `mcara_kirim_info`;
CREATE TABLE IF NOT EXISTS `mcara_kirim_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mcara_kirim_info: ~0 rows (approximately)
DELETE FROM `mcara_kirim_info`;
/*!40000 ALTER TABLE `mcara_kirim_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `mcara_kirim_info` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mexercise_pilihan
DROP TABLE IF EXISTS `mexercise_pilihan`;
CREATE TABLE IF NOT EXISTS `mexercise_pilihan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL DEFAULT '0',
  `idExercise` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mexercise_pilihan: ~0 rows (approximately)
DELETE FROM `mexercise_pilihan`;
/*!40000 ALTER TABLE `mexercise_pilihan` DISABLE KEYS */;
/*!40000 ALTER TABLE `mexercise_pilihan` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mexercise_tipe
DROP TABLE IF EXISTS `mexercise_tipe`;
CREATE TABLE IF NOT EXISTS `mexercise_tipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mexercise_tipe: ~0 rows (approximately)
DELETE FROM `mexercise_tipe`;
/*!40000 ALTER TABLE `mexercise_tipe` DISABLE KEYS */;
/*!40000 ALTER TABLE `mexercise_tipe` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mfeedback
DROP TABLE IF EXISTS `mfeedback`;
CREATE TABLE IF NOT EXISTS `mfeedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUserPengirim` int(11) NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `isi` text NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mfeedback: ~0 rows (approximately)
DELETE FROM `mfeedback`;
/*!40000 ALTER TABLE `mfeedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `mfeedback` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mfeedback_reply
DROP TABLE IF EXISTS `mfeedback_reply`;
CREATE TABLE IF NOT EXISTS `mfeedback_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idFeedback` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `isi` text,
  `idUserPengirim` int(11) DEFAULT NULL,
  `isLast` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mfeedback_reply: ~0 rows (approximately)
DELETE FROM `mfeedback_reply`;
/*!40000 ALTER TABLE `mfeedback_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `mfeedback_reply` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mfilter
DROP TABLE IF EXISTS `mfilter`;
CREATE TABLE IF NOT EXISTS `mfilter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `filterQuery` text,
  `url` text,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mfilter: ~0 rows (approximately)
DELETE FROM `mfilter`;
/*!40000 ALTER TABLE `mfilter` DISABLE KEYS */;
/*!40000 ALTER TABLE `mfilter` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mforum_kategori
DROP TABLE IF EXISTS `mforum_kategori`;
CREATE TABLE IF NOT EXISTS `mforum_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `urut` int(1) DEFAULT '1000',
  `idParent` int(11) DEFAULT NULL,
  `idParentTags` varchar(200) DEFAULT NULL COMMENT 'auto',
  `lvl` int(1) DEFAULT NULL COMMENT 'auto',
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.mforum_kategori: ~0 rows (approximately)
DELETE FROM `mforum_kategori`;
/*!40000 ALTER TABLE `mforum_kategori` DISABLE KEYS */;
/*!40000 ALTER TABLE `mforum_kategori` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mforum_reaction
DROP TABLE IF EXISTS `mforum_reaction`;
CREATE TABLE IF NOT EXISTS `mforum_reaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `icon` varchar(200) DEFAULT NULL,
  `shortcut` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.mforum_reaction: ~0 rows (approximately)
DELETE FROM `mforum_reaction`;
/*!40000 ALTER TABLE `mforum_reaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `mforum_reaction` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mforum_tag
DROP TABLE IF EXISTS `mforum_tag`;
CREATE TABLE IF NOT EXISTS `mforum_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.mforum_tag: ~0 rows (approximately)
DELETE FROM `mforum_tag`;
/*!40000 ALTER TABLE `mforum_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `mforum_tag` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mforum_tipe
DROP TABLE IF EXISTS `mforum_tipe`;
CREATE TABLE IF NOT EXISTS `mforum_tipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.mforum_tipe: ~0 rows (approximately)
DELETE FROM `mforum_tipe`;
/*!40000 ALTER TABLE `mforum_tipe` DISABLE KEYS */;
/*!40000 ALTER TABLE `mforum_tipe` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo
DROP TABLE IF EXISTS `minfo`;
CREATE TABLE IF NOT EXISTS `minfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isActive` int(1) NOT NULL,
  `title` varchar(200) NOT NULL,
  `isi` text NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `idCarakiriminfo` int(11) NOT NULL,
  `radioTo` int(1) NOT NULL COMMENT '1=all user, 2=custom, 3=batch',
  `excludeSuspended` int(1) DEFAULT NULL,
  `idPerson` text COMMENT 'php serialized',
  `idFilter` int(11) DEFAULT NULL,
  `idBatch` int(11) DEFAULT NULL,
  `emailFrom` varchar(100) DEFAULT NULL,
  `isRepeating` int(1) DEFAULT NULL,
  `jamMuncul` time DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.minfo: ~0 rows (approximately)
DELETE FROM `minfo`;
/*!40000 ALTER TABLE `minfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_log
DROP TABLE IF EXISTS `minfo_log`;
CREATE TABLE IF NOT EXISTS `minfo_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idInfo` int(11) NOT NULL,
  `val` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.minfo_log: ~0 rows (approximately)
DELETE FROM `minfo_log`;
/*!40000 ALTER TABLE `minfo_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_log` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_penerima
DROP TABLE IF EXISTS `minfo_penerima`;
CREATE TABLE IF NOT EXISTS `minfo_penerima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idInfo` int(11) DEFAULT NULL,
  `idMember` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.minfo_penerima: ~0 rows (approximately)
DELETE FROM `minfo_penerima`;
/*!40000 ALTER TABLE `minfo_penerima` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_penerima` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_person_log
DROP TABLE IF EXISTS `minfo_person_log`;
CREATE TABLE IF NOT EXISTS `minfo_person_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPerson` int(11) NOT NULL,
  `idInfo` int(11) NOT NULL,
  `idInforespon` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.minfo_person_log: ~0 rows (approximately)
DELETE FROM `minfo_person_log`;
/*!40000 ALTER TABLE `minfo_person_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_person_log` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_person_penerima
DROP TABLE IF EXISTS `minfo_person_penerima`;
CREATE TABLE IF NOT EXISTS `minfo_person_penerima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idInfo` int(11) NOT NULL,
  `idInforespon` int(11) NOT NULL,
  `idPerson` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.minfo_person_penerima: ~0 rows (approximately)
DELETE FROM `minfo_person_penerima`;
/*!40000 ALTER TABLE `minfo_person_penerima` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_person_penerima` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_respon
DROP TABLE IF EXISTS `minfo_respon`;
CREATE TABLE IF NOT EXISTS `minfo_respon` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.minfo_respon: ~0 rows (approximately)
DELETE FROM `minfo_respon`;
/*!40000 ALTER TABLE `minfo_respon` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_respon` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_user_penerima
DROP TABLE IF EXISTS `minfo_user_penerima`;
CREATE TABLE IF NOT EXISTS `minfo_user_penerima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idInfo` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idInforespon` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.minfo_user_penerima: ~0 rows (approximately)
DELETE FROM `minfo_user_penerima`;
/*!40000 ALTER TABLE `minfo_user_penerima` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_user_penerima` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mjenis_resource
DROP TABLE IF EXISTS `mjenis_resource`;
CREATE TABLE IF NOT EXISTS `mjenis_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `urut` int(1) DEFAULT '1000',
  `pilihan` varchar(200) DEFAULT NULL,
  `isFile` int(11) DEFAULT '1' COMMENT 'kalo 1=perlu upload, 0 gak perlu upload',
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mjenis_resource: ~0 rows (approximately)
DELETE FROM `mjenis_resource`;
/*!40000 ALTER TABLE `mjenis_resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `mjenis_resource` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mmodule
DROP TABLE IF EXISTS `mmodule`;
CREATE TABLE IF NOT EXISTS `mmodule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `urut` int(11) DEFAULT '100',
  `icon` varchar(200) DEFAULT 'http://olahdana.com/qx/aset/img/etc/icon.png',
  `info` text,
  `isDeletable` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mmodule: ~0 rows (approximately)
DELETE FROM `mmodule`;
/*!40000 ALTER TABLE `mmodule` DISABLE KEYS */;
/*!40000 ALTER TABLE `mmodule` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mposisi
DROP TABLE IF EXISTS `mposisi`;
CREATE TABLE IF NOT EXISTS `mposisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `icon` varchar(200) DEFAULT 'http://olahdana.com/qx/aset/img/etc/icon.png',
  `urut` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mposisi: ~0 rows (approximately)
DELETE FROM `mposisi`;
/*!40000 ALTER TABLE `mposisi` DISABLE KEYS */;
/*!40000 ALTER TABLE `mposisi` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mresource
DROP TABLE IF EXISTS `mresource`;
CREATE TABLE IF NOT EXISTS `mresource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `icon` varchar(200) DEFAULT NULL COMMENT 'url icon',
  `info` text,
  `value` varchar(200) DEFAULT NULL,
  `isi` text,
  `idJenisresource` int(11) NOT NULL,
  `idResourcegroup` int(11) DEFAULT NULL,
  `idH` int(11) DEFAULT NULL,
  `qtyPlayed` int(11) NOT NULL DEFAULT '0',
  `urut` int(11) NOT NULL,
  `idApprovalstatus` int(1) NOT NULL,
  `durasi` time NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=latin1 COMMENT='resource ini digunakan oleh module apa saja yang membutuhkan.\r\n\r\nm_resourcegroup_id= adalah kode module yang membutuhkan.\r\n\r\ncontoh misalnya resource ini digunakan oleh SliderHome1, maka \r\n1. buat new record SliderHome1 di table m_resourcegroup \r\n2. id tersebut yang digunakan untuk m_resourcegroup_id \r\n';

-- Dumping data for table k_auto2000.mresource: ~0 rows (approximately)
DELETE FROM `mresource`;
/*!40000 ALTER TABLE `mresource` DISABLE KEYS */;
/*!40000 ALTER TABLE `mresource` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mresource_group
DROP TABLE IF EXISTS `mresource_group`;
CREATE TABLE IF NOT EXISTS `mresource_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='cara menggunakan table ini:\r\ntipe Extends:\r\n1. buat controller di dalam module resource\r\n2. extends controller resource\r\n3. tambahkan hidden field m_resourcegroup_id, dengan field_value_default=id record di table ini \r\n\r\ntipe Children (header-detail):\r\n1. buat controller header\r\n2. buat link-ajax detail yang href=resource\r\n3. set m_resourcegroup_id= id di table ini\r\n4. set h_id = id milik table yang bersangkutan\r\n\r\ncontoh tipe Children:\r\n1. controller module\r\n2. memiliki banyak resource, maka ada link detail resource seperti berikut:\r\nresource?m_resourcegroup_id=2&h_id=100\r\nm_resourcegroup_id = 2 = adalah isi di table ini, 2 = module\r\nh_id=100 = berarti m_module_id=100';

-- Dumping data for table k_auto2000.mresource_group: ~0 rows (approximately)
DELETE FROM `mresource_group`;
/*!40000 ALTER TABLE `mresource_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `mresource_group` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mresource_user_view
DROP TABLE IF EXISTS `mresource_user_view`;
CREATE TABLE IF NOT EXISTS `mresource_user_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idResource` int(11) NOT NULL,
  `isSudahPlayVideo` int(1) NOT NULL DEFAULT '0',
  `isSudahLulusTestAssesment` int(1) NOT NULL DEFAULT '0',
  `nilaiTestAssesmentLulus` int(2) NOT NULL DEFAULT '0',
  `nilaiTestAssesmentTerbaik` int(2) NOT NULL DEFAULT '0',
  `nilaiTestAssesmentTerakhir` int(2) NOT NULL DEFAULT '0',
  `idUser` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='untuk mencatat apakah user \r\n1. sudah melihat?\r\n2. sudah test soal assesment?\r\n3. sudah lulus soal test assesment?\r\n';

-- Dumping data for table k_auto2000.mresource_user_view: ~0 rows (approximately)
DELETE FROM `mresource_user_view`;
/*!40000 ALTER TABLE `mresource_user_view` DISABLE KEYS */;
/*!40000 ALTER TABLE `mresource_user_view` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.msoal_assesment
DROP TABLE IF EXISTS `msoal_assesment`;
CREATE TABLE IF NOT EXISTS `msoal_assesment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idResource` int(11) NOT NULL,
  `soal` text NOT NULL,
  `iconsoal` varchar(200) DEFAULT NULL,
  `opsia` text,
  `icona` varchar(200) DEFAULT NULL,
  `opsib` text,
  `iconb` varchar(200) DEFAULT NULL,
  `opsic` text,
  `iconc` varchar(200) DEFAULT NULL,
  `opsid` text,
  `icond` varchar(200) DEFAULT NULL,
  `opsie` text,
  `icone` varchar(200) DEFAULT NULL,
  `kuncijawaban` enum('A','B','C','D','E') NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4572 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='ini adalah soal assesment setelah user menonton video di dalam module.\r\njadi relasinya \r\nmodule (one) ---> (many) resource \r\nresource (one) ----> (many) soal assesment  \r\n';

-- Dumping data for table k_auto2000.msoal_assesment: ~0 rows (approximately)
DELETE FROM `msoal_assesment`;
/*!40000 ALTER TABLE `msoal_assesment` DISABLE KEYS */;
/*!40000 ALTER TABLE `msoal_assesment` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.msoal_assesment_hasil_test_d
DROP TABLE IF EXISTS `msoal_assesment_hasil_test_d`;
CREATE TABLE IF NOT EXISTS `msoal_assesment_hasil_test_d` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idH` int(11) NOT NULL,
  `idSoalassesment` int(11) NOT NULL,
  `jawaban` varchar(1) NOT NULL,
  `kuncijawaban` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `h_id_m_soalassesment_id` (`idH`,`idSoalassesment`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.msoal_assesment_hasil_test_d: ~0 rows (approximately)
DELETE FROM `msoal_assesment_hasil_test_d`;
/*!40000 ALTER TABLE `msoal_assesment_hasil_test_d` DISABLE KEYS */;
/*!40000 ALTER TABLE `msoal_assesment_hasil_test_d` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.msoal_assesment_hasil_test_h
DROP TABLE IF EXISTS `msoal_assesment_hasil_test_h`;
CREATE TABLE IF NOT EXISTS `msoal_assesment_hasil_test_h` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idResource` int(11) NOT NULL,
  `jumlahSoal` int(11) NOT NULL,
  `jumlahBenar` int(11) NOT NULL,
  `jumlahSalah` int(11) NOT NULL,
  `jumlahTidakdijawab` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.msoal_assesment_hasil_test_h: ~0 rows (approximately)
DELETE FROM `msoal_assesment_hasil_test_h`;
/*!40000 ALTER TABLE `msoal_assesment_hasil_test_h` DISABLE KEYS */;
/*!40000 ALTER TABLE `msoal_assesment_hasil_test_h` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.x365_hari
DROP TABLE IF EXISTS `x365_hari`;
CREATE TABLE IF NOT EXISTS `x365_hari` (
  `tgl` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.x365_hari: ~0 rows (approximately)
DELETE FROM `x365_hari`;
/*!40000 ALTER TABLE `x365_hari` DISABLE KEYS */;
/*!40000 ALTER TABLE `x365_hari` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xbulan
DROP TABLE IF EXISTS `xbulan`;
CREATE TABLE IF NOT EXISTS `xbulan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xbulan: ~0 rows (approximately)
DELETE FROM `xbulan`;
/*!40000 ALTER TABLE `xbulan` DISABLE KEYS */;
/*!40000 ALTER TABLE `xbulan` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xemail_queue
DROP TABLE IF EXISTS `xemail_queue`;
CREATE TABLE IF NOT EXISTS `xemail_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromName` varchar(50) DEFAULT NULL,
  `fromEmail` varchar(50) DEFAULT NULL,
  `toName` varchar(50) DEFAULT NULL,
  `toEmail` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `body` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xemail_queue: ~0 rows (approximately)
DELETE FROM `xemail_queue`;
/*!40000 ALTER TABLE `xemail_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `xemail_queue` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xgcm
DROP TABLE IF EXISTS `xgcm`;
CREATE TABLE IF NOT EXISTS `xgcm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gcm` varchar(200) NOT NULL,
  `json` text NOT NULL,
  `isSending` int(1) DEFAULT '0',
  `sentAt` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `fcmAtauGcm` varchar(3) DEFAULT 'GCM',
  `googleApiKey` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xgcm: ~0 rows (approximately)
DELETE FROM `xgcm`;
/*!40000 ALTER TABLE `xgcm` DISABLE KEYS */;
/*!40000 ALTER TABLE `xgcm` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xgroup_hak
DROP TABLE IF EXISTS `xgroup_hak`;
CREATE TABLE IF NOT EXISTS `xgroup_hak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idGroupUser` int(11) NOT NULL,
  `idModule` int(11) NOT NULL,
  `idModuleAction` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xgroup_hak: ~0 rows (approximately)
DELETE FROM `xgroup_hak`;
/*!40000 ALTER TABLE `xgroup_hak` DISABLE KEYS */;
/*!40000 ALTER TABLE `xgroup_hak` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xgroup_module
DROP TABLE IF EXISTS `xgroup_module`;
CREATE TABLE IF NOT EXISTS `xgroup_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `urut` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xgroup_module: ~0 rows (approximately)
DELETE FROM `xgroup_module`;
/*!40000 ALTER TABLE `xgroup_module` DISABLE KEYS */;
/*!40000 ALTER TABLE `xgroup_module` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xgroup_user
DROP TABLE IF EXISTS `xgroup_user`;
CREATE TABLE IF NOT EXISTS `xgroup_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `urut` int(11) DEFAULT NULL,
  `powerlevel` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xgroup_user: ~0 rows (approximately)
DELETE FROM `xgroup_user`;
/*!40000 ALTER TABLE `xgroup_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `xgroup_user` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xhari
DROP TABLE IF EXISTS `xhari`;
CREATE TABLE IF NOT EXISTS `xhari` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xhari: ~0 rows (approximately)
DELETE FROM `xhari`;
/*!40000 ALTER TABLE `xhari` DISABLE KEYS */;
/*!40000 ALTER TABLE `xhari` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xjenis_kelamin
DROP TABLE IF EXISTS `xjenis_kelamin`;
CREATE TABLE IF NOT EXISTS `xjenis_kelamin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `urut` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xjenis_kelamin: ~0 rows (approximately)
DELETE FROM `xjenis_kelamin`;
/*!40000 ALTER TABLE `xjenis_kelamin` DISABLE KEYS */;
/*!40000 ALTER TABLE `xjenis_kelamin` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xkota
DROP TABLE IF EXISTS `xkota`;
CREATE TABLE IF NOT EXISTS `xkota` (
  `id` int(11) NOT NULL,
  `idPropinsi` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_propinsi_id_nama` (`idPropinsi`,`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table k_auto2000.xkota: ~0 rows (approximately)
DELETE FROM `xkota`;
/*!40000 ALTER TABLE `xkota` DISABLE KEYS */;
/*!40000 ALTER TABLE `xkota` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xlog
DROP TABLE IF EXISTS `xlog`;
CREATE TABLE IF NOT EXISTS `xlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caller` varchar(200) DEFAULT NULL,
  `isi` text NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xlog: ~0 rows (approximately)
DELETE FROM `xlog`;
/*!40000 ALTER TABLE `xlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `xlog` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xmodule
DROP TABLE IF EXISTS `xmodule`;
CREATE TABLE IF NOT EXISTS `xmodule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idGroupModule` int(11) DEFAULT '0',
  `class` varchar(200) NOT NULL DEFAULT '',
  `nama` varchar(200) NOT NULL DEFAULT '',
  `isAdaIndex` int(1) DEFAULT NULL,
  `isAdaAddnew` int(1) DEFAULT NULL,
  `isAdaEdit` int(1) DEFAULT NULL,
  `isAdaDelete` int(1) DEFAULT NULL,
  `isEditable` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xmodule: ~0 rows (approximately)
DELETE FROM `xmodule`;
/*!40000 ALTER TABLE `xmodule` DISABLE KEYS */;
/*!40000 ALTER TABLE `xmodule` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xmodule_action
DROP TABLE IF EXISTS `xmodule_action`;
CREATE TABLE IF NOT EXISTS `xmodule_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `urut` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xmodule_action: ~0 rows (approximately)
DELETE FROM `xmodule_action`;
/*!40000 ALTER TABLE `xmodule_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `xmodule_action` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xmodule_group_user
DROP TABLE IF EXISTS `xmodule_group_user`;
CREATE TABLE IF NOT EXISTS `xmodule_group_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idModule` int(11) NOT NULL,
  `idGroupUser` int(11) NOT NULL,
  `isBolehIndex` int(1) DEFAULT NULL,
  `isBolehAddnew` int(1) DEFAULT NULL,
  `isBolehEdit` int(1) DEFAULT NULL,
  `isBolehDelete` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=805 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xmodule_group_user: ~0 rows (approximately)
DELETE FROM `xmodule_group_user`;
/*!40000 ALTER TABLE `xmodule_group_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `xmodule_group_user` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xperson
DROP TABLE IF EXISTS `xperson`;
CREATE TABLE IF NOT EXISTS `xperson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL DEFAULT '0',
  `nama` varchar(100) NOT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `hp` varchar(100) DEFAULT NULL,
  `ktp` varchar(100) DEFAULT NULL,
  `ttlTempat` varchar(100) DEFAULT NULL,
  `ttlTgl` date DEFAULT NULL,
  `idJenisKelamin` int(11) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `kodepos` varchar(10) DEFAULT NULL,
  `idKota` int(11) DEFAULT NULL,
  `info` varchar(200) DEFAULT NULL,
  `idJabatan` int(11) DEFAULT NULL,
  `idForum` int(11) DEFAULT NULL,
  `isSuspended` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='\r\n';

-- Dumping data for table k_auto2000.xperson: ~3 rows (approximately)
DELETE FROM `xperson`;
/*!40000 ALTER TABLE `xperson` DISABLE KEYS */;
INSERT INTO `xperson` (`id`, `idUser`, `nama`, `foto`, `hp`, `ktp`, `ttlTempat`, `ttlTgl`, `idJenisKelamin`, `alamat`, `kodepos`, `idKota`, `info`, `idJabatan`, `idForum`, `isSuspended`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 1, 'auto', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 2, 'sistem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 3, 'ngasal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `xperson` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xperson_blokir
DROP TABLE IF EXISTS `xperson_blokir`;
CREATE TABLE IF NOT EXISTS `xperson_blokir` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPersonBy` int(11) NOT NULL,
  `idPersonBlokir` int(11) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_person_id_by_u_person_id_blokir` (`idPersonBy`,`idPersonBlokir`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='yang diblokir disini, nggak akan muncul lagi di pencarian/request anak dll';

-- Dumping data for table k_auto2000.xperson_blokir: ~3 rows (approximately)
DELETE FROM `xperson_blokir`;
/*!40000 ALTER TABLE `xperson_blokir` DISABLE KEYS */;
INSERT INTO `xperson_blokir` (`id`, `idPersonBy`, `idPersonBlokir`, `info`, `createdAt`) VALUES
	(1, 1, 2, NULL, '0000-00-00 00:00:00'),
	(2, 1, 3, NULL, '0000-00-00 00:00:00'),
	(3, 2, 3, NULL, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `xperson_blokir` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xpropinsi
DROP TABLE IF EXISTS `xpropinsi`;
CREATE TABLE IF NOT EXISTS `xpropinsi` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xpropinsi: ~0 rows (approximately)
DELETE FROM `xpropinsi`;
/*!40000 ALTER TABLE `xpropinsi` DISABLE KEYS */;
/*!40000 ALTER TABLE `xpropinsi` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser
DROP TABLE IF EXISTS `xuser`;
CREATE TABLE IF NOT EXISTS `xuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `hp` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `gcm` varchar(200) DEFAULT NULL,
  `imei` varchar(200) DEFAULT NULL,
  `idGroupUser` int(11) DEFAULT NULL,
  `isNonAktif` int(1) DEFAULT NULL,
  `refCodeBy` varchar(50) DEFAULT NULL,
  `refCode` varchar(50) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `lastSeenTime` datetime DEFAULT NULL,
  `lastSeenUrl` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `hp_unik` (`hp`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xuser: ~0 rows (approximately)
DELETE FROM `xuser`;
/*!40000 ALTER TABLE `xuser` DISABLE KEYS */;
/*!40000 ALTER TABLE `xuser` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser_log
DROP TABLE IF EXISTS `xuser_log`;
CREATE TABLE IF NOT EXISTS `xuser_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `loginTime` datetime NOT NULL,
  `logoutTime` datetime DEFAULT NULL,
  `lastSeen` datetime NOT NULL,
  `url` varchar(200) NOT NULL,
  `isLast` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xuser_log: ~0 rows (approximately)
DELETE FROM `xuser_log`;
/*!40000 ALTER TABLE `xuser_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `xuser_log` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser_periode
DROP TABLE IF EXISTS `xuser_periode`;
CREATE TABLE IF NOT EXISTS `xuser_periode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `periode` int(11) NOT NULL,
  `lamaHari` int(11) NOT NULL,
  `tglStart` date NOT NULL,
  `tglEnd` date NOT NULL,
  `isPeriodeBerjalan` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xuser_periode: ~0 rows (approximately)
DELETE FROM `xuser_periode`;
/*!40000 ALTER TABLE `xuser_periode` DISABLE KEYS */;
/*!40000 ALTER TABLE `xuser_periode` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser_reset_password
DROP TABLE IF EXISTS `xuser_reset_password`;
CREATE TABLE IF NOT EXISTS `xuser_reset_password` (
  `idUser` int(11) DEFAULT NULL,
  `k` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='app baca table ini. jika ada member_id nya, maka: hapus record table ini yang member id dia, lalu log out.\r\n';

-- Dumping data for table k_auto2000.xuser_reset_password: ~0 rows (approximately)
DELETE FROM `xuser_reset_password`;
/*!40000 ALTER TABLE `xuser_reset_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `xuser_reset_password` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser_unconfirmed
DROP TABLE IF EXISTS `xuser_unconfirmed`;
CREATE TABLE IF NOT EXISTS `xuser_unconfirmed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idGroupUser` int(11) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `hp` varchar(50) DEFAULT NULL,
  `apiCode` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `gcm` varchar(200) DEFAULT NULL,
  `imei` varchar(200) DEFAULT NULL,
  `kodeAktivasi` varchar(200) DEFAULT NULL,
  `refCodeBy` varchar(50) DEFAULT NULL,
  `tglDikirimEmail` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `hp` (`hp`)
) ENGINE=InnoDB AUTO_INCREMENT=678 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.xuser_unconfirmed: ~0 rows (approximately)
DELETE FROM `xuser_unconfirmed`;
/*!40000 ALTER TABLE `xuser_unconfirmed` DISABLE KEYS */;
/*!40000 ALTER TABLE `xuser_unconfirmed` ENABLE KEYS */;

-- Dumping structure for procedure k_auto2000.xxgenerate_365_hari
DROP PROCEDURE IF EXISTS `xxgenerate_365_hari`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `xxgenerate_365_hari`(
	IN `tahun` INT





)
BEGIN
	DECLARE crt_date DATE;
	declare start_date date;
	declare end_date date;

create table if not exists x365_hari (tgl date);
delete from x365_hari where date_format(tgl, '%Y') = tahun ;

	set start_date = makedate(tahun, 1);
	set end_date = concat(tahun, '-12-31'); 
 
	SET crt_date=start_date;
	WHILE crt_date <= end_date DO
		INSERT INTO x365_hari VALUES(crt_date);
		SET crt_date = ADDDATE(crt_date, INTERVAL 1 DAY);
	END WHILE;
END//
DELIMITER ;

-- Dumping structure for function k_auto2000.xxrandom_number
DROP FUNCTION IF EXISTS `xxrandom_number`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `xxrandom_number`(
	`vmin` int,
	`vmax` int




) RETURNS int(11)
    DETERMINISTIC
BEGIN 
  DECLARE hasil int;
  SET hasil = floor(vmin+ (rand() * (vmax-vmin)));
  RETURN hasil;
END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
