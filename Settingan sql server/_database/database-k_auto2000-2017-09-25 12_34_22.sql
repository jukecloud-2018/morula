-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.26-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table k_auto2000.mapp
DROP TABLE IF EXISTS `mapp`;
CREATE TABLE IF NOT EXISTS `mapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `icon` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mapp: ~0 rows (approximately)
DELETE FROM `mapp`;
/*!40000 ALTER TABLE `mapp` DISABLE KEYS */;
/*!40000 ALTER TABLE `mapp` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mapproval_status
DROP TABLE IF EXISTS `mapproval_status`;
CREATE TABLE IF NOT EXISTS `mapproval_status` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `minPowerlevel` int(1) NOT NULL DEFAULT '0' COMMENT 'minimum powerlevel yang boleh akses',
  `info` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='approval status for resource, actions, ecercise, dll\r\n';

-- Dumping data for table k_auto2000.mapproval_status: ~2 rows (approximately)
DELETE FROM `mapproval_status`;
/*!40000 ALTER TABLE `mapproval_status` DISABLE KEYS */;
INSERT INTO `mapproval_status` (`id`, `nama`, `minPowerlevel`, `info`) VALUES
	(1, 'Unpublished', 1, 'Unpublished'),
	(2, 'Published', 2, 'Published');
/*!40000 ALTER TABLE `mapproval_status` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mcabang
DROP TABLE IF EXISTS `mcabang`;
CREATE TABLE IF NOT EXISTS `mcabang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `icon` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mcabang: ~0 rows (approximately)
DELETE FROM `mcabang`;
/*!40000 ALTER TABLE `mcabang` DISABLE KEYS */;
/*!40000 ALTER TABLE `mcabang` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mcabang_periode
DROP TABLE IF EXISTS `mcabang_periode`;
CREATE TABLE IF NOT EXISTS `mcabang_periode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCabang` int(11) NOT NULL,
  `periode` int(11) NOT NULL,
  `lamaHari` int(11) NOT NULL,
  `tglStart` date NOT NULL,
  `tglEnd` date NOT NULL,
  `isPeriodeBerjalan` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mcabang_periode: ~0 rows (approximately)
DELETE FROM `mcabang_periode`;
/*!40000 ALTER TABLE `mcabang_periode` DISABLE KEYS */;
/*!40000 ALTER TABLE `mcabang_periode` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mcara_kirim_info
DROP TABLE IF EXISTS `mcara_kirim_info`;
CREATE TABLE IF NOT EXISTS `mcara_kirim_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mcara_kirim_info: ~0 rows (approximately)
DELETE FROM `mcara_kirim_info`;
/*!40000 ALTER TABLE `mcara_kirim_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `mcara_kirim_info` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mexercise
DROP TABLE IF EXISTS `mexercise`;
CREATE TABLE IF NOT EXISTS `mexercise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `idTipe` int(11) NOT NULL,
  `readOnlyText` text NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mexercise: ~9 rows (approximately)
DELETE FROM `mexercise`;
/*!40000 ALTER TABLE `mexercise` DISABLE KEYS */;
INSERT INTO `mexercise` (`id`, `nama`, `idTipe`, `readOnlyText`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 'Vision Board', 1, '', '2017-09-22 14:45:12', NULL, NULL, NULL, NULL, NULL),
	(2, 'To Do List', 2, '', '2017-09-22 14:48:09', NULL, NULL, NULL, NULL, NULL),
	(3, 'Jurnal Syukur', 2, '', '2017-09-22 14:48:37', NULL, NULL, NULL, NULL, NULL),
	(4, 'Countinue Education', 2, '', '2017-09-22 14:49:01', NULL, NULL, NULL, NULL, NULL),
	(5, 'Self Reflection', 3, '', '2017-09-22 14:49:20', NULL, NULL, NULL, NULL, NULL),
	(6, 'To Do List Evaluation', 2, '', '2017-09-22 14:49:51', NULL, NULL, NULL, NULL, NULL),
	(7, 'Change Your Respon', 2, '', '2017-09-22 14:52:05', NULL, '2017-09-22 14:54:59', 11, NULL, NULL),
	(8, 'Joyful Giving', 2, '', '2017-09-22 14:52:25', NULL, NULL, NULL, NULL, NULL),
	(9, 'Connecting With People Skill', 2, '', '2017-09-22 14:52:41', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `mexercise` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mexercise_pilihan
DROP TABLE IF EXISTS `mexercise_pilihan`;
CREATE TABLE IF NOT EXISTS `mexercise_pilihan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `idExercise` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama_idExercise` (`nama`,`idExercise`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mexercise_pilihan: ~35 rows (approximately)
DELETE FROM `mexercise_pilihan`;
/*!40000 ALTER TABLE `mexercise_pilihan` DISABLE KEYS */;
INSERT INTO `mexercise_pilihan` (`id`, `nama`, `idExercise`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 'Pilihan 1 	Change Your Respon', 7, '2017-09-22 16:28:59', NULL, '2017-09-25 11:48:07', 11, NULL, NULL),
	(2, 'Pilihan 1 Connecting With People Skill', 9, '2017-09-22 16:29:53', NULL, '2017-09-25 11:48:51', 11, NULL, NULL),
	(3, 'Pilihan 2 	Change Your Respon', 7, '2017-09-22 17:10:33', NULL, '2017-09-25 11:48:12', 11, NULL, NULL),
	(4, 'Pilihan 3 To Do List', 2, '2017-09-22 17:11:20', NULL, '2017-09-25 12:28:38', 11, NULL, NULL),
	(5, 'Pilihan 1 Joyful Giving', 8, '2017-09-25 11:46:56', NULL, '2017-09-25 11:47:12', 11, NULL, NULL),
	(6, 'Pilihan 2 Joyful Giving', 8, '2017-09-25 11:47:19', NULL, NULL, NULL, NULL, NULL),
	(7, 'Pilihan 3 Joyful Giving', 8, '2017-09-25 11:47:29', NULL, NULL, NULL, NULL, NULL),
	(8, 'Pilihan 4 Joyful Giving', 8, '2017-09-25 11:47:36', NULL, NULL, NULL, NULL, NULL),
	(9, 'Pilihan 5 Joyful Giving', 8, '2017-09-25 11:47:41', NULL, NULL, NULL, NULL, NULL),
	(10, 'Pilihan 3 	Change Your Respon', 7, '2017-09-25 11:48:21', NULL, NULL, NULL, NULL, NULL),
	(11, 'Pilihan 4 	Change Your Respon', 7, '2017-09-25 11:48:29', NULL, NULL, NULL, NULL, NULL),
	(12, 'Pilihan 5 	Change Your Respon', 7, '2017-09-25 11:48:36', NULL, NULL, NULL, NULL, NULL),
	(13, 'Pilihan 2 Connecting With People Skill', 9, '2017-09-25 11:48:57', NULL, NULL, NULL, NULL, NULL),
	(14, 'Pilihan 3 Connecting With People Skill', 9, '2017-09-25 11:49:02', NULL, NULL, NULL, NULL, NULL),
	(15, 'Pilihan 4 Connecting With People Skill', 9, '2017-09-25 11:49:08', NULL, NULL, NULL, NULL, NULL),
	(16, 'Pilihan 5 Connecting With People Skill', 9, '2017-09-25 11:49:13', NULL, NULL, NULL, NULL, NULL),
	(17, 'Pilihan 1 Countinue Education', 4, '2017-09-25 11:49:46', NULL, NULL, NULL, NULL, NULL),
	(18, 'Pilihan 2 Countinue Education', 4, '2017-09-25 11:49:55', NULL, NULL, NULL, NULL, NULL),
	(19, 'Pilihan 3 Countinue Education', 4, '2017-09-25 11:50:00', NULL, NULL, NULL, NULL, NULL),
	(20, 'Pilihan 4 Countinue Education', 4, '2017-09-25 11:50:05', NULL, NULL, NULL, NULL, NULL),
	(21, 'Pilihan 5 Countinue Education', 4, '2017-09-25 11:50:10', NULL, '2017-09-25 11:50:21', 11, NULL, NULL),
	(22, 'Pilihan 1 Jurnal Syukur', 3, '2017-09-25 11:50:38', NULL, NULL, NULL, NULL, NULL),
	(23, 'Pilihan 2 Jurnal Syukur', 3, '2017-09-25 11:50:49', NULL, NULL, NULL, NULL, NULL),
	(24, 'Pilihan 3 Jurnal Syukur', 3, '2017-09-25 11:50:56', NULL, NULL, NULL, NULL, NULL),
	(25, 'Pilihan 4 Jurnal Syukur', 3, '2017-09-25 11:51:04', NULL, NULL, NULL, NULL, NULL),
	(26, 'Pilihan 5 Jurnal Syukur', 3, '2017-09-25 11:51:09', NULL, NULL, NULL, NULL, NULL),
	(27, 'Pilihan 1 To Do List', 2, '2017-09-25 12:28:21', NULL, NULL, NULL, NULL, NULL),
	(28, 'Pilihan 2 To Do List', 2, '2017-09-25 12:28:27', NULL, NULL, NULL, NULL, NULL),
	(29, 'Pilihan 4 To Do List', 2, '2017-09-25 12:28:44', NULL, NULL, NULL, NULL, NULL),
	(30, 'Pilihan 5 To Do List', 2, '2017-09-25 12:28:50', NULL, NULL, NULL, NULL, NULL),
	(31, 'Pilihan 1 To Do List Evaluation', 6, '2017-09-25 12:29:08', NULL, NULL, NULL, NULL, NULL),
	(32, 'Pilihan 2To Do List Evaluation', 6, '2017-09-25 12:29:16', NULL, NULL, NULL, NULL, NULL),
	(33, 'Pilihan 3 To Do List Evaluation', 6, '2017-09-25 12:29:26', NULL, NULL, NULL, NULL, NULL),
	(34, 'Pilihan 4 To Do List Evaluation', 6, '2017-09-25 12:29:32', NULL, NULL, NULL, NULL, NULL),
	(35, 'Pilihan 5 To Do List Evaluation', 6, '2017-09-25 12:29:38', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `mexercise_pilihan` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mexercise_read_only_text
DROP TABLE IF EXISTS `mexercise_read_only_text`;
CREATE TABLE IF NOT EXISTS `mexercise_read_only_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `idExercise` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama_idExercise` (`nama`,`idExercise`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mexercise_read_only_text: ~5 rows (approximately)
DELETE FROM `mexercise_read_only_text`;
/*!40000 ALTER TABLE `mexercise_read_only_text` DISABLE KEYS */;
INSERT INTO `mexercise_read_only_text` (`id`, `nama`, `idExercise`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(3, 'Pilihan 1 Self Reflection', 5, '2017-09-25 11:28:46', NULL, '2017-09-25 11:52:01', 11, NULL, NULL),
	(4, 'Pilihan 2 Self Reflection', 5, '2017-09-25 11:29:08', NULL, '2017-09-25 12:27:39', 11, NULL, NULL),
	(5, 'Pilihan 3 Self Reflection', 5, '2017-09-25 11:29:18', NULL, '2017-09-25 12:27:48', 11, NULL, NULL),
	(6, 'Pilihan 4 Self Reflection', 5, '2017-09-25 12:27:54', NULL, NULL, NULL, NULL, NULL),
	(7, 'Pilihan 5 Self Reflection', 5, '2017-09-25 12:28:06', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `mexercise_read_only_text` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mexercise_tipe
DROP TABLE IF EXISTS `mexercise_tipe`;
CREATE TABLE IF NOT EXISTS `mexercise_tipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `iconButton` varchar(50) NOT NULL,
  `url` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mexercise_tipe: ~3 rows (approximately)
DELETE FROM `mexercise_tipe`;
/*!40000 ALTER TABLE `mexercise_tipe` DISABLE KEYS */;
INSERT INTO `mexercise_tipe` (`id`, `nama`, `iconButton`, `url`) VALUES
	(1, 'Vision Board', 'fa-upload', 'vision_board?filter_idExercise={id}'),
	(2, 'Pilihan', 'fa-list', 'exercise/pilihan?filter_idExercise={id}&show_close_button_on_index=1'),
	(3, 'Read Only Text', 'fa-font', 'exercise/read_only_text?filter_idExercise={id}');
/*!40000 ALTER TABLE `mexercise_tipe` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mfeedback
DROP TABLE IF EXISTS `mfeedback`;
CREATE TABLE IF NOT EXISTS `mfeedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUserPengirim` int(11) NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `isi` text NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mfeedback: ~0 rows (approximately)
DELETE FROM `mfeedback`;
/*!40000 ALTER TABLE `mfeedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `mfeedback` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mfeedback_reply
DROP TABLE IF EXISTS `mfeedback_reply`;
CREATE TABLE IF NOT EXISTS `mfeedback_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idFeedback` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `isi` text,
  `idUserPengirim` int(11) DEFAULT NULL,
  `isLast` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mfeedback_reply: ~0 rows (approximately)
DELETE FROM `mfeedback_reply`;
/*!40000 ALTER TABLE `mfeedback_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `mfeedback_reply` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mfilter
DROP TABLE IF EXISTS `mfilter`;
CREATE TABLE IF NOT EXISTS `mfilter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `filterQuery` text,
  `url` text,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mfilter: ~0 rows (approximately)
DELETE FROM `mfilter`;
/*!40000 ALTER TABLE `mfilter` DISABLE KEYS */;
/*!40000 ALTER TABLE `mfilter` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mforum_kategori
DROP TABLE IF EXISTS `mforum_kategori`;
CREATE TABLE IF NOT EXISTS `mforum_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `urut` int(1) DEFAULT '1000',
  `idParent` int(11) DEFAULT NULL,
  `idParentTags` varchar(200) DEFAULT NULL COMMENT 'auto',
  `lvl` int(1) DEFAULT NULL COMMENT 'auto',
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.mforum_kategori: ~0 rows (approximately)
DELETE FROM `mforum_kategori`;
/*!40000 ALTER TABLE `mforum_kategori` DISABLE KEYS */;
/*!40000 ALTER TABLE `mforum_kategori` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mforum_reaction
DROP TABLE IF EXISTS `mforum_reaction`;
CREATE TABLE IF NOT EXISTS `mforum_reaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `icon` varchar(200) DEFAULT NULL,
  `shortcut` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.mforum_reaction: ~0 rows (approximately)
DELETE FROM `mforum_reaction`;
/*!40000 ALTER TABLE `mforum_reaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `mforum_reaction` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mforum_tag
DROP TABLE IF EXISTS `mforum_tag`;
CREATE TABLE IF NOT EXISTS `mforum_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.mforum_tag: ~0 rows (approximately)
DELETE FROM `mforum_tag`;
/*!40000 ALTER TABLE `mforum_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `mforum_tag` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mforum_tipe
DROP TABLE IF EXISTS `mforum_tipe`;
CREATE TABLE IF NOT EXISTS `mforum_tipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.mforum_tipe: ~0 rows (approximately)
DELETE FROM `mforum_tipe`;
/*!40000 ALTER TABLE `mforum_tipe` DISABLE KEYS */;
/*!40000 ALTER TABLE `mforum_tipe` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo
DROP TABLE IF EXISTS `minfo`;
CREATE TABLE IF NOT EXISTS `minfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isActive` int(1) NOT NULL,
  `title` varchar(200) NOT NULL,
  `isi` text NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `idCarakiriminfo` int(11) NOT NULL,
  `radioTo` int(1) NOT NULL COMMENT '1=all user, 2=custom, 3=batch',
  `excludeSuspended` int(1) DEFAULT NULL,
  `idPerson` text COMMENT 'php serialized',
  `idFilter` int(11) DEFAULT NULL,
  `idBatch` int(11) DEFAULT NULL,
  `emailFrom` varchar(100) DEFAULT NULL,
  `isRepeating` int(1) DEFAULT NULL,
  `jamMuncul` time DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.minfo: ~0 rows (approximately)
DELETE FROM `minfo`;
/*!40000 ALTER TABLE `minfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_log
DROP TABLE IF EXISTS `minfo_log`;
CREATE TABLE IF NOT EXISTS `minfo_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idInfo` int(11) NOT NULL,
  `val` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.minfo_log: ~0 rows (approximately)
DELETE FROM `minfo_log`;
/*!40000 ALTER TABLE `minfo_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_log` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_penerima
DROP TABLE IF EXISTS `minfo_penerima`;
CREATE TABLE IF NOT EXISTS `minfo_penerima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idInfo` int(11) DEFAULT NULL,
  `idMember` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.minfo_penerima: ~0 rows (approximately)
DELETE FROM `minfo_penerima`;
/*!40000 ALTER TABLE `minfo_penerima` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_penerima` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_person_log
DROP TABLE IF EXISTS `minfo_person_log`;
CREATE TABLE IF NOT EXISTS `minfo_person_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPerson` int(11) NOT NULL,
  `idInfo` int(11) NOT NULL,
  `idInforespon` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.minfo_person_log: ~0 rows (approximately)
DELETE FROM `minfo_person_log`;
/*!40000 ALTER TABLE `minfo_person_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_person_log` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_person_penerima
DROP TABLE IF EXISTS `minfo_person_penerima`;
CREATE TABLE IF NOT EXISTS `minfo_person_penerima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idInfo` int(11) NOT NULL,
  `idInforespon` int(11) NOT NULL,
  `idPerson` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.minfo_person_penerima: ~0 rows (approximately)
DELETE FROM `minfo_person_penerima`;
/*!40000 ALTER TABLE `minfo_person_penerima` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_person_penerima` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_respon
DROP TABLE IF EXISTS `minfo_respon`;
CREATE TABLE IF NOT EXISTS `minfo_respon` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.minfo_respon: ~0 rows (approximately)
DELETE FROM `minfo_respon`;
/*!40000 ALTER TABLE `minfo_respon` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_respon` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_user_penerima
DROP TABLE IF EXISTS `minfo_user_penerima`;
CREATE TABLE IF NOT EXISTS `minfo_user_penerima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idInfo` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idInforespon` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.minfo_user_penerima: ~0 rows (approximately)
DELETE FROM `minfo_user_penerima`;
/*!40000 ALTER TABLE `minfo_user_penerima` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_user_penerima` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mjabatan
DROP TABLE IF EXISTS `mjabatan`;
CREATE TABLE IF NOT EXISTS `mjabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `icon` varchar(200) DEFAULT 'http://olahdana.com/qx/aset/img/etc/icon.png',
  `urut` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mjabatan: ~0 rows (approximately)
DELETE FROM `mjabatan`;
/*!40000 ALTER TABLE `mjabatan` DISABLE KEYS */;
/*!40000 ALTER TABLE `mjabatan` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mjenis_resource
DROP TABLE IF EXISTS `mjenis_resource`;
CREATE TABLE IF NOT EXISTS `mjenis_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `urut` int(1) DEFAULT '1000',
  `pilihan` varchar(200) DEFAULT NULL,
  `isFile` int(11) DEFAULT '1' COMMENT 'kalo 1=perlu upload, 0 gak perlu upload',
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mjenis_resource: ~9 rows (approximately)
DELETE FROM `mjenis_resource`;
/*!40000 ALTER TABLE `mjenis_resource` DISABLE KEYS */;
INSERT INTO `mjenis_resource` (`id`, `nama`, `info`, `urut`, `pilihan`, `isFile`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 'Images', NULL, 1, 'img', 1, NULL, NULL, '2016-05-20 17:56:20', 11, NULL, NULL),
	(2, 'Video', NULL, 2, 'video', 1, NULL, NULL, '2016-05-20 17:56:25', 11, NULL, NULL),
	(3, 'Audio', NULL, 6, 'audio', 1, NULL, NULL, '2016-05-23 14:02:15', 11, NULL, NULL),
	(4, 'Video - Youtube', NULL, 2, 'youtube', 0, NULL, NULL, '2016-05-20 17:57:12', 11, NULL, NULL),
	(5, 'Artikel - File Offices', 'Catatan untuk Artikel Office', 3, 'office', 1, NULL, NULL, '2016-05-26 14:23:11', 11, NULL, NULL),
	(7, 'Artikel', NULL, 3, 'textarea', 0, NULL, NULL, '2016-05-26 13:01:10', 11, NULL, NULL),
	(8, 'Webpage / URL', NULL, 7, 'url', 0, NULL, NULL, '2016-05-20 17:56:53', 11, NULL, NULL),
	(9, 'File - PDF', NULL, 1000, 'pdf', 1, '2016-05-20 17:59:54', 11, '2016-05-23 13:55:34', 11, NULL, NULL),
	(10, 'Trailler', NULL, 2, 'trailler', 1, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `mjenis_resource` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mkunci_jawaban
DROP TABLE IF EXISTS `mkunci_jawaban`;
CREATE TABLE IF NOT EXISTS `mkunci_jawaban` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mkunci_jawaban: ~5 rows (approximately)
DELETE FROM `mkunci_jawaban`;
/*!40000 ALTER TABLE `mkunci_jawaban` DISABLE KEYS */;
INSERT INTO `mkunci_jawaban` (`id`, `nama`) VALUES
	(1, 'A'),
	(2, 'B'),
	(3, 'C'),
	(4, 'D'),
	(5, 'E');
/*!40000 ALTER TABLE `mkunci_jawaban` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mmodule
DROP TABLE IF EXISTS `mmodule`;
CREATE TABLE IF NOT EXISTS `mmodule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `urut` int(11) DEFAULT '100',
  `icon` varchar(200) DEFAULT 'http://olahdana.com/qx/aset/img/etc/icon.png',
  `info` text,
  `isDeletable` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mmodule: ~3 rows (approximately)
DELETE FROM `mmodule`;
/*!40000 ALTER TABLE `mmodule` DISABLE KEYS */;
INSERT INTO `mmodule` (`id`, `nama`, `urut`, `icon`, `info`, `isDeletable`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 'Module 1', 100, 'http://localhost/juke/cmsauto2000/upload/icon/module/file.jpg', NULL, NULL, '2017-09-20 12:55:17', NULL, '2017-09-20 14:03:44', 11, NULL, NULL),
	(3, 'Module 3', 101, NULL, NULL, NULL, '2017-09-20 13:48:43', NULL, NULL, NULL, NULL, NULL),
	(4, 'Module 4', 102, NULL, NULL, NULL, '2017-09-20 13:48:55', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `mmodule` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mmodule_resource
DROP TABLE IF EXISTS `mmodule_resource`;
CREATE TABLE IF NOT EXISTS `mmodule_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idModule` int(11) DEFAULT NULL COMMENT '1',
  `idResource` int(11) DEFAULT NULL COMMENT 'm',
  `info` varchar(200) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='1 record bisa memiliki beberapa resource.\r\nsaat membuat resource, langsung dipilih ini mau buat siapa? mau buat action? buat motivasi? buat inspirasi? buat tip?\r\n\r\ntapi dari master action / motivasi / inspirasi / tip bisa juga menambahkan resource dari daftar resource.';

-- Dumping data for table k_auto2000.mmodule_resource: ~0 rows (approximately)
DELETE FROM `mmodule_resource`;
/*!40000 ALTER TABLE `mmodule_resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `mmodule_resource` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mposisi
DROP TABLE IF EXISTS `mposisi`;
CREATE TABLE IF NOT EXISTS `mposisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `icon` varchar(200) DEFAULT 'http://olahdana.com/qx/aset/img/etc/icon.png',
  `urut` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mposisi: ~0 rows (approximately)
DELETE FROM `mposisi`;
/*!40000 ALTER TABLE `mposisi` DISABLE KEYS */;
/*!40000 ALTER TABLE `mposisi` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mresource
DROP TABLE IF EXISTS `mresource`;
CREATE TABLE IF NOT EXISTS `mresource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `icon` varchar(200) DEFAULT NULL COMMENT 'url icon',
  `info` text,
  `value` varchar(200) DEFAULT NULL,
  `isi` text,
  `idJenisResource` int(11) NOT NULL,
  `idResourceGroup` int(11) DEFAULT NULL,
  `idH` int(11) DEFAULT NULL,
  `qtyPlayed` int(11) NOT NULL DEFAULT '0',
  `urut` int(11) NOT NULL,
  `idApprovalstatus` int(1) NOT NULL,
  `durasi` time NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COMMENT='resource ini digunakan oleh module apa saja yang membutuhkan.\r\n\r\nm_resourcegroup_id= adalah kode module yang membutuhkan.\r\n\r\ncontoh misalnya resource ini digunakan oleh SliderHome1, maka \r\n1. buat new record SliderHome1 di table m_resourcegroup \r\n2. id tersebut yang digunakan untuk m_resourcegroup_id \r\n';

-- Dumping data for table k_auto2000.mresource: ~6 rows (approximately)
DELETE FROM `mresource`;
/*!40000 ALTER TABLE `mresource` DISABLE KEYS */;
INSERT INTO `mresource` (`id`, `nama`, `icon`, `info`, `value`, `isi`, `idJenisResource`, `idResourceGroup`, `idH`, `qtyPlayed`, `urut`, `idApprovalstatus`, `durasi`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 'video A module 1b', 'http://localhost/juke/cmsauto2000/upload/all/file_1.jpg', NULL, 'http://localhost/juke/cmsauto2000/upload/video/file.mp4', NULL, 2, 2, 1, 0, 0, 1, '00:00:00', '2017-09-20 14:25:20', NULL, '2017-09-22 14:09:21', 11, NULL, NULL),
	(2, 'video B module 2', NULL, NULL, 'https://premium.esq165.co.id/upload/sementara/video/1.mkv', NULL, 2, 2, 1, 0, 0, 1, '00:00:00', '2017-09-20 15:09:16', NULL, '2017-09-20 15:32:10', 11, NULL, NULL),
	(4, 'video C untuk module 4', NULL, NULL, 'https://premium.esq165.co.id/upload/sementara/video/1.mkv', NULL, 2, 2, 4, 0, 0, 1, '00:00:00', '2017-09-20 15:31:51', NULL, '2017-09-20 15:32:16', 11, NULL, NULL),
	(5, 'Video C module 1', NULL, NULL, 'https://premium.esq165.co.id/upload/sementara/video/1.mkv', NULL, 2, 2, 1, 0, 0, 1, '00:00:00', '2017-09-20 15:33:22', NULL, NULL, NULL, NULL, NULL),
	(6, 'Slider 1e', NULL, NULL, 'http://esqx.esq165.co.id/upload/sementara/video/file_587f28fd90475.mp4', NULL, 2, 1, NULL, 0, 0, 1, '00:00:00', '2017-09-20 16:58:40', NULL, '2017-09-20 17:37:06', 11, NULL, NULL),
	(7, 'Slider 2', NULL, NULL, 'http://esqx.esq165.co.id/upload/sementara/video/file_587f28fd90475.mp4', NULL, 2, 1, NULL, 0, 0, 1, '00:00:00', '2017-09-20 17:06:10', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `mresource` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mresource_group
DROP TABLE IF EXISTS `mresource_group`;
CREATE TABLE IF NOT EXISTS `mresource_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='cara menggunakan table ini:\r\ntipe Extends:\r\n1. buat controller di dalam module resource\r\n2. extends controller resource\r\n3. tambahkan hidden field m_resourcegroup_id, dengan field_value_default=id record di table ini \r\n\r\ntipe Children (header-detail):\r\n1. buat controller header\r\n2. buat link-ajax detail yang href=resource\r\n3. set m_resourcegroup_id= id di table ini\r\n4. set h_id = id milik table yang bersangkutan\r\n\r\ncontoh tipe Children:\r\n1. controller module\r\n2. memiliki banyak resource, maka ada link detail resource seperti berikut:\r\nresource?m_resourcegroup_id=2&h_id=100\r\nm_resourcegroup_id = 2 = adalah isi di table ini, 2 = module\r\nh_id=100 = berarti m_module_id=100';

-- Dumping data for table k_auto2000.mresource_group: ~0 rows (approximately)
DELETE FROM `mresource_group`;
/*!40000 ALTER TABLE `mresource_group` DISABLE KEYS */;
INSERT INTO `mresource_group` (`id`, `nama`) VALUES
	(1, 'Slider Home 1');
/*!40000 ALTER TABLE `mresource_group` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mresource_user_view
DROP TABLE IF EXISTS `mresource_user_view`;
CREATE TABLE IF NOT EXISTS `mresource_user_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idResource` int(11) NOT NULL,
  `isSudahPlayVideo` int(1) NOT NULL DEFAULT '0',
  `isSudahLulusTestAssesment` int(1) NOT NULL DEFAULT '0',
  `nilaiTestAssesmentLulus` int(2) NOT NULL DEFAULT '0',
  `nilaiTestAssesmentTerbaik` int(2) NOT NULL DEFAULT '0',
  `nilaiTestAssesmentTerakhir` int(2) NOT NULL DEFAULT '0',
  `idUser` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='untuk mencatat apakah user \r\n1. sudah melihat?\r\n2. sudah test soal assesment?\r\n3. sudah lulus soal test assesment?\r\n';

-- Dumping data for table k_auto2000.mresource_user_view: ~0 rows (approximately)
DELETE FROM `mresource_user_view`;
/*!40000 ALTER TABLE `mresource_user_view` DISABLE KEYS */;
/*!40000 ALTER TABLE `mresource_user_view` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.msoal_assesment
DROP TABLE IF EXISTS `msoal_assesment`;
CREATE TABLE IF NOT EXISTS `msoal_assesment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idResource` int(11) NOT NULL,
  `soal` text NOT NULL,
  `iconsoal` varchar(200) DEFAULT NULL,
  `opsia` text,
  `icona` varchar(200) DEFAULT NULL,
  `opsib` text,
  `iconb` varchar(200) DEFAULT NULL,
  `opsic` text,
  `iconc` varchar(200) DEFAULT NULL,
  `opsid` text,
  `icond` varchar(200) DEFAULT NULL,
  `opsie` text,
  `icone` varchar(200) DEFAULT NULL,
  `kunciJawaban` enum('A','B','C','D','E') NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='ini adalah soal assesment setelah user menonton video di dalam module.\r\njadi relasinya \r\nmodule (one) ---> (many) resource \r\nresource (one) ----> (many) soal assesment  \r\n';

-- Dumping data for table k_auto2000.msoal_assesment: ~2 rows (approximately)
DELETE FROM `msoal_assesment`;
/*!40000 ALTER TABLE `msoal_assesment` DISABLE KEYS */;
INSERT INTO `msoal_assesment` (`id`, `idResource`, `soal`, `iconsoal`, `opsia`, `icona`, `opsib`, `iconb`, `opsic`, `iconc`, `opsid`, `icond`, `opsie`, `icone`, `kunciJawaban`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 1, 'soal 1 untuk video A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A', '2017-09-20 15:45:44', NULL, NULL, NULL, NULL, NULL),
	(2, 1, 'soal 2 untuk video A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B', '2017-09-20 15:53:47', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `msoal_assesment` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.msoal_assesment_hasil_test_d
DROP TABLE IF EXISTS `msoal_assesment_hasil_test_d`;
CREATE TABLE IF NOT EXISTS `msoal_assesment_hasil_test_d` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idH` int(11) NOT NULL,
  `idSoalassesment` int(11) NOT NULL,
  `jawaban` varchar(1) NOT NULL,
  `kuncijawaban` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `h_id_m_soalassesment_id` (`idH`,`idSoalassesment`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.msoal_assesment_hasil_test_d: ~0 rows (approximately)
DELETE FROM `msoal_assesment_hasil_test_d`;
/*!40000 ALTER TABLE `msoal_assesment_hasil_test_d` DISABLE KEYS */;
/*!40000 ALTER TABLE `msoal_assesment_hasil_test_d` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.msoal_assesment_hasil_test_h
DROP TABLE IF EXISTS `msoal_assesment_hasil_test_h`;
CREATE TABLE IF NOT EXISTS `msoal_assesment_hasil_test_h` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idResource` int(11) NOT NULL,
  `jumlahSoal` int(11) NOT NULL,
  `jumlahBenar` int(11) NOT NULL,
  `jumlahSalah` int(11) NOT NULL,
  `jumlahTidakdijawab` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.msoal_assesment_hasil_test_h: ~0 rows (approximately)
DELETE FROM `msoal_assesment_hasil_test_h`;
/*!40000 ALTER TABLE `msoal_assesment_hasil_test_h` DISABLE KEYS */;
/*!40000 ALTER TABLE `msoal_assesment_hasil_test_h` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mvision_board
DROP TABLE IF EXISTS `mvision_board`;
CREATE TABLE IF NOT EXISTS `mvision_board` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `info` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idUser_nama` (`idUser`,`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mvision_board: ~0 rows (approximately)
DELETE FROM `mvision_board`;
/*!40000 ALTER TABLE `mvision_board` DISABLE KEYS */;
/*!40000 ALTER TABLE `mvision_board` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.x365_hari
DROP TABLE IF EXISTS `x365_hari`;
CREATE TABLE IF NOT EXISTS `x365_hari` (
  `tgl` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.x365_hari: ~0 rows (approximately)
DELETE FROM `x365_hari`;
/*!40000 ALTER TABLE `x365_hari` DISABLE KEYS */;
/*!40000 ALTER TABLE `x365_hari` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xbulan
DROP TABLE IF EXISTS `xbulan`;
CREATE TABLE IF NOT EXISTS `xbulan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xbulan: ~0 rows (approximately)
DELETE FROM `xbulan`;
/*!40000 ALTER TABLE `xbulan` DISABLE KEYS */;
/*!40000 ALTER TABLE `xbulan` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xemail_queue
DROP TABLE IF EXISTS `xemail_queue`;
CREATE TABLE IF NOT EXISTS `xemail_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromName` varchar(50) DEFAULT NULL,
  `fromEmail` varchar(50) DEFAULT NULL,
  `toName` varchar(50) DEFAULT NULL,
  `toEmail` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `body` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xemail_queue: ~0 rows (approximately)
DELETE FROM `xemail_queue`;
/*!40000 ALTER TABLE `xemail_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `xemail_queue` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xgcm
DROP TABLE IF EXISTS `xgcm`;
CREATE TABLE IF NOT EXISTS `xgcm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gcm` varchar(200) NOT NULL,
  `json` text NOT NULL,
  `isSending` int(1) DEFAULT '0',
  `sentAt` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `fcmAtauGcm` varchar(3) DEFAULT 'GCM',
  `googleApiKey` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xgcm: ~0 rows (approximately)
DELETE FROM `xgcm`;
/*!40000 ALTER TABLE `xgcm` DISABLE KEYS */;
/*!40000 ALTER TABLE `xgcm` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xgroup_hak
DROP TABLE IF EXISTS `xgroup_hak`;
CREATE TABLE IF NOT EXISTS `xgroup_hak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idGroupUser` int(11) NOT NULL,
  `idModule` int(11) NOT NULL,
  `idModuleAction` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xgroup_hak: ~0 rows (approximately)
DELETE FROM `xgroup_hak`;
/*!40000 ALTER TABLE `xgroup_hak` DISABLE KEYS */;
/*!40000 ALTER TABLE `xgroup_hak` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xgroup_module
DROP TABLE IF EXISTS `xgroup_module`;
CREATE TABLE IF NOT EXISTS `xgroup_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `info` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xgroup_module: ~0 rows (approximately)
DELETE FROM `xgroup_module`;
/*!40000 ALTER TABLE `xgroup_module` DISABLE KEYS */;
/*!40000 ALTER TABLE `xgroup_module` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xgroup_user
DROP TABLE IF EXISTS `xgroup_user`;
CREATE TABLE IF NOT EXISTS `xgroup_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `urut` int(11) DEFAULT NULL,
  `powerlevel` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xgroup_user: ~2 rows (approximately)
DELETE FROM `xgroup_user`;
/*!40000 ALTER TABLE `xgroup_user` DISABLE KEYS */;
INSERT INTO `xgroup_user` (`id`, `nama`, `urut`, `powerlevel`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(10, 'member', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL),
	(30, 'admin', 2, 3, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `xgroup_user` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xhari
DROP TABLE IF EXISTS `xhari`;
CREATE TABLE IF NOT EXISTS `xhari` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xhari: ~0 rows (approximately)
DELETE FROM `xhari`;
/*!40000 ALTER TABLE `xhari` DISABLE KEYS */;
/*!40000 ALTER TABLE `xhari` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xjenis_kelamin
DROP TABLE IF EXISTS `xjenis_kelamin`;
CREATE TABLE IF NOT EXISTS `xjenis_kelamin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `urut` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xjenis_kelamin: ~0 rows (approximately)
DELETE FROM `xjenis_kelamin`;
/*!40000 ALTER TABLE `xjenis_kelamin` DISABLE KEYS */;
/*!40000 ALTER TABLE `xjenis_kelamin` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xkota
DROP TABLE IF EXISTS `xkota`;
CREATE TABLE IF NOT EXISTS `xkota` (
  `id` int(11) NOT NULL,
  `idPropinsi` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_propinsi_id_nama` (`idPropinsi`,`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table k_auto2000.xkota: ~0 rows (approximately)
DELETE FROM `xkota`;
/*!40000 ALTER TABLE `xkota` DISABLE KEYS */;
/*!40000 ALTER TABLE `xkota` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xlog
DROP TABLE IF EXISTS `xlog`;
CREATE TABLE IF NOT EXISTS `xlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caller` varchar(200) DEFAULT NULL,
  `isi` text NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xlog: ~0 rows (approximately)
DELETE FROM `xlog`;
/*!40000 ALTER TABLE `xlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `xlog` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xmodule
DROP TABLE IF EXISTS `xmodule`;
CREATE TABLE IF NOT EXISTS `xmodule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idGroupModule` int(11) DEFAULT '0',
  `class` varchar(200) NOT NULL DEFAULT '',
  `nama` varchar(200) NOT NULL DEFAULT '',
  `isAdaIndex` int(1) DEFAULT NULL,
  `isAdaAddnew` int(1) DEFAULT NULL,
  `isAdaEdit` int(1) DEFAULT NULL,
  `isAdaDelete` int(1) DEFAULT NULL,
  `isEditable` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xmodule: ~12 rows (approximately)
DELETE FROM `xmodule`;
/*!40000 ALTER TABLE `xmodule` DISABLE KEYS */;
INSERT INTO `xmodule` (`id`, `idGroupModule`, `class`, `nama`, `isAdaIndex`, `isAdaAddnew`, `isAdaEdit`, `isAdaDelete`, `isEditable`) VALUES
	(1, 0, 'xxuser', 'Xxuser', 1, 1, 1, 1, NULL),
	(2, 0, 'awal', 'Awal', 1, 1, 1, 1, NULL),
	(3, 0, 'module', 'Module', 1, 1, 1, 1, NULL),
	(4, 0, 'resource', 'Resource', 1, 1, 1, 1, NULL),
	(5, 0, 'vids', 'Vids', 1, 1, 1, 1, NULL),
	(6, 0, 'jenis_resource', 'Jenis_resource', 1, 1, 1, 1, NULL),
	(7, 0, 'soal_assesment', 'Soal_assesment', 1, 1, 1, 1, NULL),
	(8, 0, 'slider_home1', 'Slider_home1', 1, 1, 1, 1, NULL),
	(9, 0, 'pilihan', 'Pilihan', 1, 1, 1, 1, NULL),
	(10, 0, 'tipe', 'Tipe', 1, 1, 1, 1, NULL),
	(11, 0, 'exercise', 'Exercise', 1, 1, 1, 1, NULL),
	(12, 0, 'read_only_text', 'Read_only_text', 1, 1, 1, 1, NULL);
/*!40000 ALTER TABLE `xmodule` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xmodule_action
DROP TABLE IF EXISTS `xmodule_action`;
CREATE TABLE IF NOT EXISTS `xmodule_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `urut` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xmodule_action: ~0 rows (approximately)
DELETE FROM `xmodule_action`;
/*!40000 ALTER TABLE `xmodule_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `xmodule_action` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xmodule_group_user
DROP TABLE IF EXISTS `xmodule_group_user`;
CREATE TABLE IF NOT EXISTS `xmodule_group_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idModule` int(11) NOT NULL,
  `idGroupUser` int(11) NOT NULL,
  `isBolehIndex` int(1) DEFAULT NULL,
  `isBolehAddnew` int(1) DEFAULT NULL,
  `isBolehEdit` int(1) DEFAULT NULL,
  `isBolehDelete` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xmodule_group_user: ~12 rows (approximately)
DELETE FROM `xmodule_group_user`;
/*!40000 ALTER TABLE `xmodule_group_user` DISABLE KEYS */;
INSERT INTO `xmodule_group_user` (`id`, `idModule`, `idGroupUser`, `isBolehIndex`, `isBolehAddnew`, `isBolehEdit`, `isBolehDelete`) VALUES
	(1, 1, 30, 1, 1, 1, 1),
	(2, 2, 30, 1, 1, 1, 1),
	(3, 3, 30, 1, 1, 1, 1),
	(4, 4, 30, 1, 1, 1, 1),
	(5, 5, 30, 1, 1, 1, 1),
	(6, 6, 30, 1, 1, 1, 1),
	(7, 7, 30, 1, 1, 1, 1),
	(8, 8, 30, 1, 1, 1, 1),
	(9, 9, 30, 1, 1, 1, 1),
	(10, 10, 30, 1, 1, 1, 1),
	(11, 11, 30, 1, 1, 1, 1),
	(12, 12, 30, 1, 1, 1, 1);
/*!40000 ALTER TABLE `xmodule_group_user` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xperson
DROP TABLE IF EXISTS `xperson`;
CREATE TABLE IF NOT EXISTS `xperson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL DEFAULT '0',
  `nama` varchar(100) NOT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `hp2` varchar(100) DEFAULT NULL,
  `ktp` varchar(100) DEFAULT NULL,
  `ttlTempat` varchar(100) DEFAULT NULL,
  `ttlTgl` date DEFAULT NULL,
  `idJenisKelamin` int(11) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `kodepos` varchar(10) DEFAULT NULL,
  `idKota` int(11) DEFAULT NULL,
  `info` varchar(200) DEFAULT NULL,
  `idJabatan` int(11) DEFAULT NULL,
  `idForum` int(11) DEFAULT NULL,
  `isNonAktif` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COMMENT='\r\n';

-- Dumping data for table k_auto2000.xperson: ~4 rows (approximately)
DELETE FROM `xperson`;
/*!40000 ALTER TABLE `xperson` DISABLE KEYS */;
INSERT INTO `xperson` (`id`, `idUser`, `nama`, `foto`, `hp2`, `ktp`, `ttlTempat`, `ttlTgl`, `idJenisKelamin`, `alamat`, `kodepos`, `idKota`, `info`, `idJabatan`, `idForum`, `isNonAktif`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 1, 'auto', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 2, 'sistem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 3, 'ngasal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 11, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `xperson` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xperson_blokir
DROP TABLE IF EXISTS `xperson_blokir`;
CREATE TABLE IF NOT EXISTS `xperson_blokir` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPersonBy` int(11) NOT NULL,
  `idPersonBlokir` int(11) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_person_id_by_u_person_id_blokir` (`idPersonBy`,`idPersonBlokir`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='yang diblokir disini, nggak akan muncul lagi di pencarian/request anak dll';

-- Dumping data for table k_auto2000.xperson_blokir: ~3 rows (approximately)
DELETE FROM `xperson_blokir`;
/*!40000 ALTER TABLE `xperson_blokir` DISABLE KEYS */;
INSERT INTO `xperson_blokir` (`id`, `idPersonBy`, `idPersonBlokir`, `info`, `createdAt`) VALUES
	(1, 1, 2, NULL, '0000-00-00 00:00:00'),
	(2, 1, 3, NULL, '0000-00-00 00:00:00'),
	(3, 2, 3, NULL, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `xperson_blokir` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xpropinsi
DROP TABLE IF EXISTS `xpropinsi`;
CREATE TABLE IF NOT EXISTS `xpropinsi` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xpropinsi: ~0 rows (approximately)
DELETE FROM `xpropinsi`;
/*!40000 ALTER TABLE `xpropinsi` DISABLE KEYS */;
/*!40000 ALTER TABLE `xpropinsi` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser
DROP TABLE IF EXISTS `xuser`;
CREATE TABLE IF NOT EXISTS `xuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `hp` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `gcm` varchar(200) DEFAULT NULL,
  `imei` varchar(200) DEFAULT NULL,
  `idGroupUser` int(11) DEFAULT NULL,
  `isNonAktif` int(1) DEFAULT NULL,
  `refCodeBy` varchar(50) DEFAULT NULL,
  `refCode` varchar(50) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `lastSeenTime` datetime DEFAULT NULL,
  `lastSeenUrl` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `hp_unik` (`hp`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xuser: ~3 rows (approximately)
DELETE FROM `xuser`;
/*!40000 ALTER TABLE `xuser` DISABLE KEYS */;
INSERT INTO `xuser` (`id`, `email`, `username`, `hp`, `password`, `gcm`, `imei`, `idGroupUser`, `isNonAktif`, `refCodeBy`, `refCode`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`, `lastSeenTime`, `lastSeenUrl`) VALUES
	(1, 'auto@gmail.com', 'auto', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'sistem@gmail.com', 'sistem', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 'admin@gmail.com', 'admin', '11', '$P$B5i5MklbTxUcDX2HrPTRYU9DvbdYxK1', NULL, NULL, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-25 12:33:00', 'http://localhost/juke/cmsauto2000/exercise');
/*!40000 ALTER TABLE `xuser` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser_log
DROP TABLE IF EXISTS `xuser_log`;
CREATE TABLE IF NOT EXISTS `xuser_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `loginTime` datetime NOT NULL,
  `logoutTime` datetime DEFAULT NULL,
  `lastSeen` datetime NOT NULL,
  `url` varchar(200) NOT NULL,
  `isLast` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xuser_log: ~0 rows (approximately)
DELETE FROM `xuser_log`;
/*!40000 ALTER TABLE `xuser_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `xuser_log` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser_periode
DROP TABLE IF EXISTS `xuser_periode`;
CREATE TABLE IF NOT EXISTS `xuser_periode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `periode` int(11) NOT NULL,
  `lamaHari` int(11) NOT NULL,
  `tglStart` date NOT NULL,
  `tglEnd` date NOT NULL,
  `isPeriodeBerjalan` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xuser_periode: ~0 rows (approximately)
DELETE FROM `xuser_periode`;
/*!40000 ALTER TABLE `xuser_periode` DISABLE KEYS */;
/*!40000 ALTER TABLE `xuser_periode` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser_reset_password
DROP TABLE IF EXISTS `xuser_reset_password`;
CREATE TABLE IF NOT EXISTS `xuser_reset_password` (
  `idUser` int(11) DEFAULT NULL,
  `k` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='app baca table ini. jika ada member_id nya, maka: hapus record table ini yang member id dia, lalu log out.\r\n';

-- Dumping data for table k_auto2000.xuser_reset_password: ~0 rows (approximately)
DELETE FROM `xuser_reset_password`;
/*!40000 ALTER TABLE `xuser_reset_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `xuser_reset_password` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser_unconfirmed
DROP TABLE IF EXISTS `xuser_unconfirmed`;
CREATE TABLE IF NOT EXISTS `xuser_unconfirmed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idGroupUser` int(11) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `hp` varchar(50) DEFAULT NULL,
  `apiCode` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `gcm` varchar(200) DEFAULT NULL,
  `imei` varchar(200) DEFAULT NULL,
  `kodeAktivasi` varchar(200) DEFAULT NULL,
  `refCodeBy` varchar(50) DEFAULT NULL,
  `tglDikirimEmail` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `hp` (`hp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.xuser_unconfirmed: ~0 rows (approximately)
DELETE FROM `xuser_unconfirmed`;
/*!40000 ALTER TABLE `xuser_unconfirmed` DISABLE KEYS */;
/*!40000 ALTER TABLE `xuser_unconfirmed` ENABLE KEYS */;

-- Dumping structure for procedure k_auto2000.xxgenerate_365_hari
DROP PROCEDURE IF EXISTS `xxgenerate_365_hari`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `xxgenerate_365_hari`(
	IN `tahun` INT





)
BEGIN
	DECLARE crt_date DATE;
	declare start_date date;
	declare end_date date;

create table if not exists x365_hari (tgl date);
delete from x365_hari where date_format(tgl, '%Y') = tahun ;

	set start_date = makedate(tahun, 1);
	set end_date = concat(tahun, '-12-31'); 
 
	SET crt_date=start_date;
	WHILE crt_date <= end_date DO
		INSERT INTO x365_hari VALUES(crt_date);
		SET crt_date = ADDDATE(crt_date, INTERVAL 1 DAY);
	END WHILE;
END//
DELIMITER ;

-- Dumping structure for function k_auto2000.xxrandom_number
DROP FUNCTION IF EXISTS `xxrandom_number`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `xxrandom_number`(
	`vmin` int,
	`vmax` int




) RETURNS int(11)
    DETERMINISTIC
BEGIN 
  DECLARE hasil int;
  SET hasil = floor(vmin+ (rand() * (vmax-vmin)));
  RETURN hasil;
END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
