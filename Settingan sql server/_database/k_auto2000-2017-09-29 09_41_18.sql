-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.26-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table k_auto2000.mapp
DROP TABLE IF EXISTS `mapp`;
CREATE TABLE IF NOT EXISTS `mapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `icon` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mapp: ~0 rows (approximately)
DELETE FROM `mapp`;
/*!40000 ALTER TABLE `mapp` DISABLE KEYS */;
/*!40000 ALTER TABLE `mapp` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mapproval_status
DROP TABLE IF EXISTS `mapproval_status`;
CREATE TABLE IF NOT EXISTS `mapproval_status` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `minPowerlevel` int(1) NOT NULL DEFAULT '0' COMMENT 'minimum powerlevel yang boleh akses',
  `info` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='approval status for resource, actions, ecercise, dll\r\n';

-- Dumping data for table k_auto2000.mapproval_status: ~2 rows (approximately)
DELETE FROM `mapproval_status`;
/*!40000 ALTER TABLE `mapproval_status` DISABLE KEYS */;
INSERT INTO `mapproval_status` (`id`, `nama`, `minPowerlevel`, `info`) VALUES
	(1, 'Unpublished', 1, 'Unpublished'),
	(2, 'Published', 2, 'Published');
/*!40000 ALTER TABLE `mapproval_status` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mcabang
DROP TABLE IF EXISTS `mcabang`;
CREATE TABLE IF NOT EXISTS `mcabang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `idVoucher` int(11) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`),
  UNIQUE KEY `idVoucher` (`idVoucher`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mcabang: ~3 rows (approximately)
DELETE FROM `mcabang`;
/*!40000 ALTER TABLE `mcabang` DISABLE KEYS */;
INSERT INTO `mcabang` (`id`, `nama`, `idVoucher`, `info`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 'Cabang 1', 1, 'keterangan cabang 1', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Cabang 2', 2, 'keterangan cabang 2', NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'Cabang 3', 3, 'keterangan cabang 3', NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `mcabang` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mcabang_member
DROP TABLE IF EXISTS `mcabang_member`;
CREATE TABLE IF NOT EXISTS `mcabang_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCabang` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `createdBy` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mcabang_member: ~0 rows (approximately)
DELETE FROM `mcabang_member`;
/*!40000 ALTER TABLE `mcabang_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `mcabang_member` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mcabang_periode
DROP TABLE IF EXISTS `mcabang_periode`;
CREATE TABLE IF NOT EXISTS `mcabang_periode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCabang` int(11) NOT NULL,
  `periode` int(11) NOT NULL,
  `lamaHari` int(11) NOT NULL,
  `tglStart` date NOT NULL,
  `tglEnd` date NOT NULL,
  `isPeriodeBerjalan` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='sementara nggak dipakai\r\nkarena periode seakrang yang kita pakai periode bulanan aja, biar gampang filternya bulanan\r\ndengan begitu otomatis nggak dibutuhkan lagi table ini\r\n\r\nkita akan pakai lagi table ini jika nanti menemukan alasan yang bagus kenapa butuh table periode \r\n';

-- Dumping data for table k_auto2000.mcabang_periode: ~3 rows (approximately)
DELETE FROM `mcabang_periode`;
/*!40000 ALTER TABLE `mcabang_periode` DISABLE KEYS */;
INSERT INTO `mcabang_periode` (`id`, `idCabang`, `periode`, `lamaHari`, `tglStart`, `tglEnd`, `isPeriodeBerjalan`) VALUES
	(1, 1, 1, 30, '2017-09-25', '2017-10-25', 1),
	(2, 2, 1, 30, '2017-09-25', '2017-10-25', 1),
	(3, 3, 1, 30, '2017-09-25', '2017-10-25', 1);
/*!40000 ALTER TABLE `mcabang_periode` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mcara_kirim_info
DROP TABLE IF EXISTS `mcara_kirim_info`;
CREATE TABLE IF NOT EXISTS `mcara_kirim_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mcara_kirim_info: ~0 rows (approximately)
DELETE FROM `mcara_kirim_info`;
/*!40000 ALTER TABLE `mcara_kirim_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `mcara_kirim_info` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mexercise
DROP TABLE IF EXISTS `mexercise`;
CREATE TABLE IF NOT EXISTS `mexercise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `icon` varchar(200) NOT NULL,
  `idTipe` int(11) NOT NULL,
  `urut` int(11) DEFAULT '0',
  `readOnlyText` text NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mexercise: ~9 rows (approximately)
DELETE FROM `mexercise`;
/*!40000 ALTER TABLE `mexercise` DISABLE KEYS */;
INSERT INTO `mexercise` (`id`, `nama`, `icon`, `idTipe`, `urut`, `readOnlyText`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 'Vision Board', 'http://localhost/juke/cmsauto2000/upload/exercise/icon/exercise/file_8.png', 1, 9, '<p><br></p>', '2017-09-22 14:45:12', NULL, '2017-09-25 15:47:06', 11, NULL, NULL),
	(2, 'To Do List', 'http://localhost/juke/cmsauto2000/upload/exercise/icon/exercise/file_6.png', 2, 4, '<p><br></p>', '2017-09-22 14:48:09', NULL, '2017-09-25 15:46:52', 11, NULL, NULL),
	(3, 'Jurnal Syukur', 'http://localhost/juke/cmsauto2000/upload/exercise/icon/exercise/file_4.png', 2, 5, '<p><br></p>', '2017-09-22 14:48:37', NULL, '2017-09-25 15:46:06', 11, NULL, NULL),
	(4, 'Countinue Education', 'http://localhost/juke/cmsauto2000/upload/exercise/icon/exercise/file_2.png', 2, 6, '<p><br></p>', '2017-09-22 14:49:01', NULL, '2017-09-25 15:45:47', 11, NULL, NULL),
	(5, 'Self Reflection', 'http://localhost/juke/cmsauto2000/upload/exercise/icon/exercise/file_5.png', 3, 8, '<p><br></p>', '2017-09-22 14:49:20', NULL, '2017-09-25 15:46:40', 11, NULL, NULL),
	(6, 'To Do List Evaluation', 'http://localhost/juke/cmsauto2000/upload/exercise/icon/exercise/file_7.png', 2, 10, '<p><br></p>', '2017-09-22 14:49:51', NULL, '2017-09-25 15:46:59', 11, NULL, NULL),
	(7, 'Change Your Respon', 'http://localhost/juke/cmsauto2000/upload/exercise/icon/exercise/file.png', 2, 11, '<p><br></p>', '2017-09-22 14:52:05', NULL, '2017-09-25 15:45:24', 11, NULL, NULL),
	(8, 'Joyful Giving', 'http://localhost/juke/cmsauto2000/upload/exercise/icon/exercise/file_3.png', 2, 12, '<p><br></p>', '2017-09-22 14:52:25', NULL, '2017-09-25 15:45:57', 11, NULL, NULL),
	(9, 'Connecting With People Skill', 'http://localhost/juke/cmsauto2000/upload/exercise/icon/exercise/file_1.png', 2, 13, '<p><br></p>', '2017-09-22 14:52:41', NULL, '2017-09-25 15:45:34', 11, NULL, NULL);
/*!40000 ALTER TABLE `mexercise` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mexercise_pilihan
DROP TABLE IF EXISTS `mexercise_pilihan`;
CREATE TABLE IF NOT EXISTS `mexercise_pilihan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `idExercise` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama_idExercise` (`nama`,`idExercise`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mexercise_pilihan: ~35 rows (approximately)
DELETE FROM `mexercise_pilihan`;
/*!40000 ALTER TABLE `mexercise_pilihan` DISABLE KEYS */;
INSERT INTO `mexercise_pilihan` (`id`, `nama`, `idExercise`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 'Pilihan 1 	Change Your Respon', 7, '2017-09-22 16:28:59', NULL, '2017-09-25 11:48:07', 11, NULL, NULL),
	(2, 'Pilihan 1 Connecting With People Skill', 9, '2017-09-22 16:29:53', NULL, '2017-09-25 11:48:51', 11, NULL, NULL),
	(3, 'Pilihan 2 	Change Your Respon', 7, '2017-09-22 17:10:33', NULL, '2017-09-25 11:48:12', 11, NULL, NULL),
	(4, 'Pilihan 3 To Do List', 2, '2017-09-22 17:11:20', NULL, '2017-09-25 12:28:38', 11, NULL, NULL),
	(5, 'Pilihan 1 Joyful Giving', 8, '2017-09-25 11:46:56', NULL, '2017-09-25 11:47:12', 11, NULL, NULL),
	(6, 'Pilihan 2 Joyful Giving', 8, '2017-09-25 11:47:19', NULL, NULL, NULL, NULL, NULL),
	(7, 'Pilihan 3 Joyful Giving', 8, '2017-09-25 11:47:29', NULL, NULL, NULL, NULL, NULL),
	(8, 'Pilihan 4 Joyful Giving', 8, '2017-09-25 11:47:36', NULL, NULL, NULL, NULL, NULL),
	(9, 'Pilihan 5 Joyful Giving', 8, '2017-09-25 11:47:41', NULL, NULL, NULL, NULL, NULL),
	(10, 'Pilihan 3 	Change Your Respon', 7, '2017-09-25 11:48:21', NULL, NULL, NULL, NULL, NULL),
	(11, 'Pilihan 4 	Change Your Respon', 7, '2017-09-25 11:48:29', NULL, NULL, NULL, NULL, NULL),
	(12, 'Pilihan 5 	Change Your Respon', 7, '2017-09-25 11:48:36', NULL, NULL, NULL, NULL, NULL),
	(13, 'Pilihan 2 Connecting With People Skill', 9, '2017-09-25 11:48:57', NULL, NULL, NULL, NULL, NULL),
	(14, 'Pilihan 3 Connecting With People Skill', 9, '2017-09-25 11:49:02', NULL, NULL, NULL, NULL, NULL),
	(15, 'Pilihan 4 Connecting With People Skill', 9, '2017-09-25 11:49:08', NULL, NULL, NULL, NULL, NULL),
	(16, 'Pilihan 5 Connecting With People Skill', 9, '2017-09-25 11:49:13', NULL, NULL, NULL, NULL, NULL),
	(17, 'Pilihan 1 Countinue Education', 4, '2017-09-25 11:49:46', NULL, NULL, NULL, NULL, NULL),
	(18, 'Pilihan 2 Countinue Education', 4, '2017-09-25 11:49:55', NULL, NULL, NULL, NULL, NULL),
	(19, 'Pilihan 3 Countinue Education', 4, '2017-09-25 11:50:00', NULL, NULL, NULL, NULL, NULL),
	(20, 'Pilihan 4 Countinue Education', 4, '2017-09-25 11:50:05', NULL, NULL, NULL, NULL, NULL),
	(21, 'Pilihan 5 Countinue Education', 4, '2017-09-25 11:50:10', NULL, '2017-09-25 11:50:21', 11, NULL, NULL),
	(22, 'Pilihan 1 Jurnal Syukur', 3, '2017-09-25 11:50:38', NULL, NULL, NULL, NULL, NULL),
	(23, 'Pilihan 2 Jurnal Syukur', 3, '2017-09-25 11:50:49', NULL, NULL, NULL, NULL, NULL),
	(24, 'Pilihan 3 Jurnal Syukur', 3, '2017-09-25 11:50:56', NULL, NULL, NULL, NULL, NULL),
	(25, 'Pilihan 4 Jurnal Syukur', 3, '2017-09-25 11:51:04', NULL, NULL, NULL, NULL, NULL),
	(26, 'Pilihan 5 Jurnal Syukur', 3, '2017-09-25 11:51:09', NULL, NULL, NULL, NULL, NULL),
	(27, 'Pilihan 1 To Do List', 2, '2017-09-25 12:28:21', NULL, NULL, NULL, NULL, NULL),
	(28, 'Pilihan 2 To Do List', 2, '2017-09-25 12:28:27', NULL, NULL, NULL, NULL, NULL),
	(29, 'Pilihan 4 To Do List', 2, '2017-09-25 12:28:44', NULL, NULL, NULL, NULL, NULL),
	(30, 'Pilihan 5 To Do List', 2, '2017-09-25 12:28:50', NULL, NULL, NULL, NULL, NULL),
	(31, 'Pilihan 1 To Do List Evaluation', 6, '2017-09-25 12:29:08', NULL, NULL, NULL, NULL, NULL),
	(32, 'Pilihan 2To Do List Evaluation', 6, '2017-09-25 12:29:16', NULL, NULL, NULL, NULL, NULL),
	(33, 'Pilihan 3 To Do List Evaluation', 6, '2017-09-25 12:29:26', NULL, NULL, NULL, NULL, NULL),
	(34, 'Pilihan 4 To Do List Evaluation', 6, '2017-09-25 12:29:32', NULL, NULL, NULL, NULL, NULL),
	(35, 'Pilihan 5 To Do List Evaluation', 6, '2017-09-25 12:29:38', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `mexercise_pilihan` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mexercise_read_only_text
DROP TABLE IF EXISTS `mexercise_read_only_text`;
CREATE TABLE IF NOT EXISTS `mexercise_read_only_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `idExercise` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama_idExercise` (`nama`,`idExercise`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mexercise_read_only_text: ~5 rows (approximately)
DELETE FROM `mexercise_read_only_text`;
/*!40000 ALTER TABLE `mexercise_read_only_text` DISABLE KEYS */;
INSERT INTO `mexercise_read_only_text` (`id`, `nama`, `idExercise`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(3, 'Pilihan 1 Self Reflection', 5, '2017-09-25 11:28:46', NULL, '2017-09-25 11:52:01', 11, NULL, NULL),
	(4, 'Pilihan 2 Self Reflection', 5, '2017-09-25 11:29:08', NULL, '2017-09-25 12:27:39', 11, NULL, NULL),
	(5, 'Pilihan 3 Self Reflection', 5, '2017-09-25 11:29:18', NULL, '2017-09-25 12:27:48', 11, NULL, NULL),
	(6, 'Pilihan 4 Self Reflection', 5, '2017-09-25 12:27:54', NULL, NULL, NULL, NULL, NULL),
	(7, 'Pilihan 5 Self Reflection', 5, '2017-09-25 12:28:06', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `mexercise_read_only_text` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mexercise_tipe
DROP TABLE IF EXISTS `mexercise_tipe`;
CREATE TABLE IF NOT EXISTS `mexercise_tipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `iconButton` varchar(50) NOT NULL,
  `url` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='id pilihan harus fixed: \r\nid: 2 = pilihan ';

-- Dumping data for table k_auto2000.mexercise_tipe: ~3 rows (approximately)
DELETE FROM `mexercise_tipe`;
/*!40000 ALTER TABLE `mexercise_tipe` DISABLE KEYS */;
INSERT INTO `mexercise_tipe` (`id`, `nama`, `iconButton`, `url`) VALUES
	(1, 'Vision Board', 'fa-upload', 'exercise/vision_board?show_close_button_on_index=1'),
	(2, 'Pilihan', 'fa-list', 'exercise/pilihan?filter_idExercise={id}&show_close_button_on_index=1'),
	(3, 'Read Only Text', 'fa-font', 'exercise/read_only_text?filter_idExercise={id}');
/*!40000 ALTER TABLE `mexercise_tipe` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mfeedback
DROP TABLE IF EXISTS `mfeedback`;
CREATE TABLE IF NOT EXISTS `mfeedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUserPengirim` int(11) NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `isi` text NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mfeedback: ~0 rows (approximately)
DELETE FROM `mfeedback`;
/*!40000 ALTER TABLE `mfeedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `mfeedback` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mfeedback_reply
DROP TABLE IF EXISTS `mfeedback_reply`;
CREATE TABLE IF NOT EXISTS `mfeedback_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idFeedback` int(11) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `isi` text,
  `idUserPengirim` int(11) DEFAULT NULL,
  `isLast` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mfeedback_reply: ~0 rows (approximately)
DELETE FROM `mfeedback_reply`;
/*!40000 ALTER TABLE `mfeedback_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `mfeedback_reply` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mfilter
DROP TABLE IF EXISTS `mfilter`;
CREATE TABLE IF NOT EXISTS `mfilter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `filterQuery` text,
  `url` text,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mfilter: ~0 rows (approximately)
DELETE FROM `mfilter`;
/*!40000 ALTER TABLE `mfilter` DISABLE KEYS */;
/*!40000 ALTER TABLE `mfilter` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mforum_kategori
DROP TABLE IF EXISTS `mforum_kategori`;
CREATE TABLE IF NOT EXISTS `mforum_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `urut` int(1) DEFAULT '1000',
  `idParent` int(11) DEFAULT NULL,
  `idParentTags` varchar(200) DEFAULT NULL COMMENT 'auto',
  `lvl` int(1) DEFAULT NULL COMMENT 'auto',
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.mforum_kategori: ~0 rows (approximately)
DELETE FROM `mforum_kategori`;
/*!40000 ALTER TABLE `mforum_kategori` DISABLE KEYS */;
/*!40000 ALTER TABLE `mforum_kategori` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mforum_reaction
DROP TABLE IF EXISTS `mforum_reaction`;
CREATE TABLE IF NOT EXISTS `mforum_reaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `icon` varchar(200) DEFAULT NULL,
  `shortcut` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.mforum_reaction: ~0 rows (approximately)
DELETE FROM `mforum_reaction`;
/*!40000 ALTER TABLE `mforum_reaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `mforum_reaction` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mforum_tag
DROP TABLE IF EXISTS `mforum_tag`;
CREATE TABLE IF NOT EXISTS `mforum_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.mforum_tag: ~0 rows (approximately)
DELETE FROM `mforum_tag`;
/*!40000 ALTER TABLE `mforum_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `mforum_tag` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mforum_tipe
DROP TABLE IF EXISTS `mforum_tipe`;
CREATE TABLE IF NOT EXISTS `mforum_tipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.mforum_tipe: ~0 rows (approximately)
DELETE FROM `mforum_tipe`;
/*!40000 ALTER TABLE `mforum_tipe` DISABLE KEYS */;
/*!40000 ALTER TABLE `mforum_tipe` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo
DROP TABLE IF EXISTS `minfo`;
CREATE TABLE IF NOT EXISTS `minfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isActive` int(1) NOT NULL,
  `title` varchar(200) NOT NULL,
  `isi` text NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `idCarakiriminfo` int(11) NOT NULL,
  `radioTo` int(1) NOT NULL COMMENT '1=all user, 2=custom, 3=batch',
  `excludeSuspended` int(1) DEFAULT NULL,
  `idPerson` text COMMENT 'php serialized',
  `idFilter` int(11) DEFAULT NULL,
  `idBatch` int(11) DEFAULT NULL,
  `idCabang` int(11) DEFAULT NULL,
  `emailFrom` varchar(100) DEFAULT NULL,
  `isRepeating` int(1) DEFAULT NULL,
  `jamMuncul` time DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.minfo: ~0 rows (approximately)
DELETE FROM `minfo`;
/*!40000 ALTER TABLE `minfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_log
DROP TABLE IF EXISTS `minfo_log`;
CREATE TABLE IF NOT EXISTS `minfo_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idInfo` int(11) NOT NULL,
  `val` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.minfo_log: ~0 rows (approximately)
DELETE FROM `minfo_log`;
/*!40000 ALTER TABLE `minfo_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_log` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_penerima
DROP TABLE IF EXISTS `minfo_penerima`;
CREATE TABLE IF NOT EXISTS `minfo_penerima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idInfo` int(11) DEFAULT NULL,
  `idMember` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.minfo_penerima: ~0 rows (approximately)
DELETE FROM `minfo_penerima`;
/*!40000 ALTER TABLE `minfo_penerima` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_penerima` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_person_log
DROP TABLE IF EXISTS `minfo_person_log`;
CREATE TABLE IF NOT EXISTS `minfo_person_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPerson` int(11) NOT NULL,
  `idInfo` int(11) NOT NULL,
  `idInforespon` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.minfo_person_log: ~0 rows (approximately)
DELETE FROM `minfo_person_log`;
/*!40000 ALTER TABLE `minfo_person_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_person_log` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_person_penerima
DROP TABLE IF EXISTS `minfo_person_penerima`;
CREATE TABLE IF NOT EXISTS `minfo_person_penerima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idInfo` int(11) NOT NULL,
  `idInforespon` int(11) NOT NULL,
  `idPerson` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.minfo_person_penerima: ~0 rows (approximately)
DELETE FROM `minfo_person_penerima`;
/*!40000 ALTER TABLE `minfo_person_penerima` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_person_penerima` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_respon
DROP TABLE IF EXISTS `minfo_respon`;
CREATE TABLE IF NOT EXISTS `minfo_respon` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.minfo_respon: ~0 rows (approximately)
DELETE FROM `minfo_respon`;
/*!40000 ALTER TABLE `minfo_respon` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_respon` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minfo_user_penerima
DROP TABLE IF EXISTS `minfo_user_penerima`;
CREATE TABLE IF NOT EXISTS `minfo_user_penerima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idInfo` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idInforespon` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.minfo_user_penerima: ~0 rows (approximately)
DELETE FROM `minfo_user_penerima`;
/*!40000 ALTER TABLE `minfo_user_penerima` DISABLE KEYS */;
/*!40000 ALTER TABLE `minfo_user_penerima` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minspirasi
DROP TABLE IF EXISTS `minspirasi`;
CREATE TABLE IF NOT EXISTS `minspirasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `tgl` date DEFAULT NULL,
  `icon` varchar(200) DEFAULT 'http://olahdana.com/qx/aset/img/etc/icon.png',
  `quote` varchar(200) DEFAULT NULL,
  `content` text,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.minspirasi: ~105 rows (approximately)
DELETE FROM `minspirasi`;
/*!40000 ALTER TABLE `minspirasi` DISABLE KEYS */;
INSERT INTO `minspirasi` (`id`, `title`, `tgl`, `icon`, `quote`, `content`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 'Bahagia Itu Sederhana', '2017-01-16', 'https://qx.esq165.co.id/dev/upload/sementara/img/file_58105282914ba.jpg', 'Orang hadir di hidupmu karena sebuah alasan. Mereka berimu bahagia dan kecewa. Ada yg sesaat, tapi ada yg selamanya.', 'Orang hadir di hidupmu karena sebuah alasan. Mereka berimu bahagia dan kecewa. Ada yg sesaat, tapi ada yg selamanya.\r\n\r\nOrang hadir di hidupmu karena sebuah alasan. Mereka berimu bahagia dan kecewa. Ada yg sesaat, tapi ada yg selamanya. Orang hadir di hidupmu karena sebuah alasan. Mereka berimu bahagia dan kecewa. Ada yg sesaat, tapi ada yg selamanya.', '2016-06-20 18:04:39', 41, '2016-10-26 13:51:48', 11, NULL, NULL),
	(2, 'Sahabat Sejati', '2017-01-17', 'http://olahdana.com/qx/upload/sementara/img/file_5767f26b0cd31.jpg', 'Terima kasih telah menjadi sahabat terbaik. Terbaik dari yang terbaik. Luar biasa!', 'Terima kasih telah menjadi sahabat terbaik. Terbaik dari yang terbaik. Terima kasih telah menjadi sahabat terbaik. Terbaik dari yang terbaik. Terima kasih telah menjadi sahabat terbaik. Terbaik dari yang terbaik.\r\n\r\nTerima kasih telah menjadi sahabat terbaik. Terbaik dari yang terbaik.Terima kasih telah menjadi sahabat terbaik. Terbaik dari yang terbaik. Terima kasih telah menjadi sahabat terbaik. Terbaik dari yang terbaik.', '2016-06-20 18:17:52', 41, '2016-06-20 20:41:01', 12, NULL, NULL),
	(3, 'Minum Air Putih', '2017-01-18', 'https://qx.esq165.co.id/dev/upload/sementara/img/file_5810521536ddf.jpg', 'Tubuh kita terdiri dari 90% lebih air. Dan bagian terbanyak adalah otak. Karena itu kita harus banyak meminum air agar otak aman.', 'Tubuh kita terdiri dari 90% lebih air. Dan bagian terbanyak adalah otak. Karena itu kita harus banyak meminum air agar otak aman.', '2016-06-29 15:23:01', 12, '2017-02-06 12:09:03', 11, NULL, NULL),
	(4, 'the title', '2017-01-19', 'https://qx.esq165.co.id/dev/upload/sementara/img/file_581052f1e7cdd.jpg', 'kote', 'content here', '2016-10-18 15:25:35', 4, '2016-10-26 13:53:40', 11, NULL, NULL),
	(5, 'title 13', '2017-01-20', 'https://qx.esq165.co.id/dev/upload/sementara/img/file_58105308905ad.jpg', 'kuote 13', 'content 13', '2016-10-18 15:28:48', 4, '2016-10-26 13:54:03', 11, NULL, NULL),
	(10, 'title tgl 12 esqx', '2017-01-25', NULL, 'quote 12 esqzx', 'content 12 esqxz', '2016-11-07 20:14:36', 364, NULL, NULL, NULL, NULL),
	(11, 'title 13 esqx', '2017-01-26', 'http://localhost/k/qx/upload/sementara/img/file_58207f38ba629.jpg', 'quote 13 esqx', 'content 13 esqx', '2016-11-07 20:18:56', 364, NULL, NULL, NULL, NULL),
	(20, 'Prasangka Baik', '2017-02-04', 'https://qx.esq165.co.id/upload/sementara/img/file_5889f186a1546.jpg', 'Prasangka Baik', 'Berprasangka baik pada orang lain akan mendorong dan menciptakan kondisi untuk saling percaya, saling mendukung, tebuka dan koorperatif.\r\n\r\n- Ary Ginanjar Agustian -', '2017-01-26 19:57:02', 11, '2017-01-26 19:59:57', 11, NULL, NULL),
	(21, 'BETAPA INDAHNYA JALAN TUHAN', '2017-02-05', 'https://qx.esq165.co.id/upload/sementara/img/file_5898033e976d1.jpg', 'Sahabat ESQ yang Allah cintai. Di hari Jumat yang penuh barokah ini, semoga kita semua selalu mendapatkan limpahan rahmat yang tiada henti dari Allah SWT.', 'Sahabat ESQ yang Allah cintai. Di hari Jumat yang penuh barokah ini, semoga kita semua selalu mendapatkan limpahan rahmat yang tiada henti dari Allah SWT. Tentu saja, limpahan rahmat itupun tak serta merta datang mendadak. Semua itu diperoleh dari usaha kita memperjuangkannya.\r\n.\r\nSahabat ESQ, untuk Anda yang merasa saat ini sedang berjuang keras untuk menggapai hidup bahagia, hal itupun pernah juga dirasakan oleh Nabi Ibrahim As. Taukah apa perjuangan terbesar yang Nabi Ibrahim lakukan sepanjang hidupnya? Sahabat ESQ ada yang tau? Untuk sama-sama mengetahuinya, mari kita simak sebuah kisah yang akan saya ceritakan.\r\n.\r\nKetika usia Nabi Ibrahim As telah lanjut, ia belum juga dikaruniai seorang anak. Ia sangat ingin memperoleh anak laki-laki. Sekian lama menunggu, ia pun tak dikaruniai anak. Doa pun tak pernah putus ia panjatkan kepada Allah agar ia bisa segera diberi seorang anak laki-laki.\r\n.\r\nAkhirnya, penantian manisnya pun berujung indah. Allah mengabulkan doanya. Lahirlah seorang bayi laki-laki dari istrinya, Siti Hajar. Bayi itu kemudian diberi nama Ismail. Ismail tumbuh menjadi anak sholeh yang senantiasa hormat kepada orang tuanya dan ia pun sangat taat beribadah.\r\n.\r\nNamun pada suatu hari, ia bermimpi. Ia diperintah oleh Allah untuk mengorbankan dan menyembelih anak semata wayangnya, Ismail. Bayangkah Sahabat ESQ, bagaimana rasanya ketika Tuhan memerintah Nabi Ibrahim untik menyembelih Ismail. Sebagai manusia normal, tentu saja ia bingung. Apalagi setan terus menggodanya untuk tidak menuruti perintah Allah.\r\n.\r\nAkhirnya, Nabi Ibrahim pun mengambil keputusan besar untuk mengikuti peri tah Allah. Nabi Ibrahim berbicara kepada Ismail tentang perintah Allah yang ia dapat dari mimpinya itu. Nabi Ibrahim tercengang mendengar jawaban Ismail. Ismail berkata "Ayah, sekiranya ini perintah Allah maka lakukanlah"\r\n.\r\nKemudian Ibrahim pun menyiapkan segala sesuatunya untuk melaksanakan perintah Allah. Sepertu yang kita ketahui Sahabat ESQ, atas izin dan perintah Allah, akhirnya Allah menukar dan mengganti pengorbanan Ismail dengan seekor domba dan Ismail diselamatkan.\r\n.\r\nMasya Allah...\r\n.\r\nApa kira-kira yang akan Sahabat ESQ lakukan jika ada di posisi super pelik seperti yang dihadapi oleh Nabi Ibrahim?\r\n.\r\nApabila Sahabat ESQ tidak memahami maknanya, mungkin kita pun akan mengatakan bahwa Allah itu kejam. Namun, kisah Nabi Ibrahim tersebut justru menunjukkan kasih sayang Allah kepada hambanya dengan menunjukkan makna kehidupan yang sebenarnya.\r\n.\r\nMencintai ataupun be-\'ilah\' kepada anak, istri, keluarga, harta, uang, jabatan ataupun kepentingan dunia lainnya, hanya akan mengakibatkan kesulitan dan kerusakan di muka bumi ini. \r\n.\r\nBagaimana Sahabat ESQ, makna apa yang bisa Anda petik dari Kisah Nabi Ibrahim di atas?\r\n.\r\nSahabat ESQ boleh sharing pendapat Anda di kolom komentar di bawah ini. Semoga makin banyak lagi orang yang mendapat manfaat dari kisah-kisah penuh makna ini.\r\n(Sumber Buku "ESQ Power" )\r\nSalam 165\r\nAry Ginanjar Agustian', '2017-02-06 12:03:17', 11, '2017-02-06 12:08:05', 11, NULL, NULL),
	(22, 'BAGAIMANA MENANAMKAN PARADIGMA POSITIF?', '2017-02-06', 'https://qx.esq165.co.id/upload/sementara/img/file_58981edd7a5df.jpg', 'MENANAMKAN PARADIGMA POSITIF', 'BAGAIMANA MENANAMKAN PARADIGMA POSITIF? \r\n.\r\nAssalamualaikum Sahabat ESQ di seluruh penjuru tanah air.\r\nSelamat siang menjelang sore.\r\n.\r\nBagaimana kesibukkan hari ini? Semoga sibuknya kita di hari ini membawa berkah untuk kita dan keluarga yaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\n.\r\nSahabat ESQ yang dirahmati oleh Allah, menjelang sore ini, ijinkan saya untuk berbagi sebuah cerita yang mungkin bisa menjadi inspirasi bagi Sahabat ESQ untuk menjalani kehidupan. Kisah nyata dari sosok yang benar-benar dicintai oleh Allah dan menjadi panutan semua umat Islam di seluruh dunia. YaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦kisah sore ini datang dari pengalaman Nabi Muhammad SAW. \r\n.\r\nTahukah Sahabat ESQ, bahwa Nabi Muhammad SAW hampir setiap hari melewati pasar untuk menuju rumahnya. Di pasar itu, biasanya melewati seorang pengemis Yahudi buta yang selalu mengatakan, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Muhammad pembohong, Muhammad pembohong!ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Suatu saat, ketika Nabi Muhammad SAW lewat pasar itu, iya tidak melihat pengemis buta itu. Nabi Muhammas bertanya kepada orang-orang ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Ke manakah penemis buta itu?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Orang-orang di sana menjawab, bahwa ia sedang sakit.\r\n.\r\nRasulullah SAW langsung mengunjungi rujmah orang buta itu hanya untuk menjenguk dan melihat keadaan sang pengemis itu. Pengemis buta itu sangat terharu melihat keagungan dan kemuliaan sifat yang dimiliki oleh Rasulullah SAW. Hingga pada suatu ketika, pengemis buta itu memutuskan untuk memeluk agama Islam. Kebaikan hati dan kemuliaat sifat yang ditunjukkan oleh Rasulullah bisa membuka hati seseorang yang terbelenggu pemikiran negatif.\r\n.\r\nKonsep khalifah yang mengikuti suara hati fitrah telah dicontohkan oleh Rasulullah SAW. Ia menyadari bahwa dirinya menjadi sumber integritas, bukan persepsi dari bentukkan lingkungan. Sehingga ia adalah subyek, bukan obyek dari lingkungan sekitar karena Rasulullah merupakan wakil Allah yang Agung. \r\n.\r\nSikap yang ditunjukkan Rasulullah memperlihatkan bahwa ia mampu menembus belenggu yang bisa menutupi suara hati fitrah. Belenggu yang dikalahkan oleh suara hatinya itu bernama ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“KemarahanÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â.\r\n.\r\nRasulullah mampu untuk tidak membiarkan dirinya didominasi oleh emosi kemarahan. Ia tetap bisa mengikuti suara hatinya, suara Ilahiah milik Allah untuk ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“menolong dan berempatiÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â kepada sesame makhluk Allah. Ia menjadi raja bagi dirinya sendiri. Tetap berpikir positif, mempertahankan bahwa ia harus bersikap baik dengan apapun dan siapapun yang ditemuinya sekalipun orang itu membenci dirinya.\r\n.\r\nSubhanallahÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦begitu mulia sifat Rasulullah. \r\n.\r\nSahabat ESQ, bisakah kita mencontoh sifat mulia yang ditunjukkan oleh Rasulullah?\r\n.\r\nSiapa yang mau mendapatkan syafaat dari Rasulullah ketika hari kiamat tiba?\r\n.\r\nCoba Sahabat ESQ berikan komen di bawah ini. \r\n.\r\n. \r\n(Kisah ini bisa dibaca juga dibuku saya "ESQ Power")\r\n. \r\nSemoga bermanfaat. \r\nSalam 165\r\n. \r\nAry Ginanjar Agustian\r\n. \r\n. \r\nPs : Bagaimana menurut Sahabat ESQ? Komen, share dan tag orang-orang yang Anda cintai agar lebih banyak lagi orang yang bisa mencontoh sifat-sifat mulia yang dimiliki oleh Rasulullah SAW. Aamin..', '2017-02-06 14:00:34', 11, '2017-02-06 14:00:59', 11, NULL, NULL),
	(23, 'MENGAPA PESAWAT TERBANG MENCAPAI TUJUAN MEREKA?', '2017-02-07', 'https://qx.esq165.co.id/upload/sementara/img/file_589823abcd5bb.jpg', 'Ada sebuah pelajaran hebat yang dapat dipelajari dari cara pesawat terbang mencapai tujuannya. Saat sebuah pesawat take off, pilot akan mengatur kendali menjadi auto pilot (pilot otomatis).', 'Ada sebuah pelajaran hebat yang dapat dipelajari dari cara pesawat terbang mencapai tujuannya. Saat sebuah pesawat take off, pilot akan mengatur kendali menjadi auto pilot (pilot otomatis).\r\n.\r\nPilot otomatis itu akan mengunci sebuah negara tujuan (misalnya Hawaii) dan komputer merencanakan jalur penerbangan yang akan diambil. Komputer pesawat itu akan terus mengunci sasarannya, memandu pesawat pada jalur penerbangannya.\r\n.\r\nNamun, tahukan Anda bahwa pesawat tidak selalu melewati jalur penerbangannya? Selama perjalanan, akan ada angina dan tekanan udara yang mendorong pesawat keluar dari jalur penerbangannya. Namun meski demikian, pesawat tetap sampai ke tujuannya.\r\n.\r\nIni karena saat pesawat keluar dari jalur yang semestinya, komputer pilot otomatis akan mendeteksi sebuah penyimpangan antara arahnya saat itu dan jalur penerbangan yang diinginkan. Ia akan mengalkulasi perubahan yang terjadi dan memberi tanda kepada sistem avionik (penerbangan) pesawat untuk mengarahkan pesawat kembali ke jalur yang benar.\r\n.\r\nBeberapa saat kemudian, pesawat kembali keluar jalur akibat angin dan tekanan udara. Sekali lagi, komputer pesawat segera mendeteksi kalau pesawat keluar dari jalurnya dan melakukan penyesuaian yang diperlukan untuk kembali ke jalur yang benar lagi. Hal ini terjadi terus-menerus selama perjalanan. Tahukan Anda bahwa pesawat keluar dari jalurnya selama 70%-80% waktu penerbangan? Meski demikian, ia tetap mendarat di Hawaii.\r\n.\r\nPrinsip yang sama berlaku dalam usaha kita mencapai tujuan-tujuan dalam hidup. Dalam kehidupan, hal-hal tidak pernah selalu berjalan sesuai rencana. Kenyataannya, ditemukan bahwa 70%-80% rencana-rencana yang dibuat dalam karier, bisnis, dan kehidupan pribadi tidak pernah berjalan sesuai rencana. Ada banyak kejadian dan situasi tidak terduga yang mendorong kita keluar dari jalur juga.\r\n.\r\nMeski demikian, kita tetap dapat mencapai tujuan kita jika kita tetap berfokus padanya dan terus-menerus menyesuaikan strategi dan rencana-rencana kita, serta mengembalikan diri kita ke jalur yang menuju tujuan kita. Jika pesawat itu tidak terkunci pada tujuannya, ia tidak akan pernah sampai ke Hawaii. Serupa dengan itu, jika kita tidak tetap terkunci pada tujuan-tujuan kita, kita akan terlempar keluar dari jalur oleh begitu banyaknya pengalihan perhatian, dan menemukan diri kita mendarat di sebuah tujuan yang tidak pernah ingin kita capai atau diam terpaku, tidak pergi ke mana-mana.\r\n.\r\nJadi tetaplah terkunci pada tujuan-tujuan Anda dan jangan pernah merasa takut saat berbagai hal tidak berjalan sesuai rencana. Selama Anda tetap kembali ke jalur Anda dan terus bergerak maju, Anda akan sampai ke tempat yang ingin Anda tuju.\r\n.\r\nSumber: Winning The Game Of Life, Adam Khoo', '2017-02-06 14:20:16', 11, NULL, NULL, NULL, NULL),
	(24, 'IKAN KECIL DAN AIR', '2017-02-08', 'https://qx.esq165.co.id/upload/sementara/img/file_589826b1e59ad.jpg', 'Suatu hari ada seorang ayah dan anak sedang duduk berbincang - bincang di tepi sungai...', 'Suatu hari ada seorang ayah dan anak sedang duduk berbincang-bincang di tepi sungai...\r\nSang Ayah berkata kepada anaknya, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Lihatlah anakku, air begitu penting dalam kehidupan ini, tanpa air kita semua akan mati.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nPada saat bersamaan, ternyata ada seekor ikan kecil yang mendengar percakapan itu dari bawah permukaan air, ikan kecil itu mendadak gelisah dan ingin tahu apakah air itu, yang katanya begitu penting dalam kehidupan ini.\r\n.\r\nAkhirnya ikan kecil itu berenang dari hulu sampai ke hilir sungai sambil bertanya kepada setiap ikan yang ditemuinya, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Hai tahukah kamu di mana tempat air berada? Aku telah mendengar percakapan manusia bahwa tanpa air kehidupan akan mati.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nTernyata semua ikan yang telah ditanya tidak mengetahui di mana air itu, si ikan kecil itu semakin kebingungan, lalu ia berenang menuju mata air untuk bertemu dengan ikan sepuh yang sudah berpengalaman, kepada ikan sepuh itu ikan kecil ini menanyakan hal yang sama, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Dimakah air?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nIkan sepuh itu menjawab dengan bijak, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Tak usah gelisah anakku, air itu telah mengelilingimu, sehingga kamu bahkan tidak menyadari kehadirannya. Memang benar, tanpa air kita semua akan mati.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nLalu ikan kecil itu terdiam karena ternyata selama ini yang ia pertanyakan ada di dekatnya bahkan disekelilingnyaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\n.\r\nApa arti cerita tersebut bagi kita?\r\n.\r\nSebagai manusia kadang-kadang kita mengalami situasi yang sama seperti ikan kecil, mencari kesana kemari tentang KEHIDUPAN dan KEBAHAGIAAN, padahal ia sedang menjalaninya, bahkan kebahagiaan sedang melingkupinya sampai-sampai ia sendiri tidak menyadarinyaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\n.\r\nSudahkah Anda bersyukur hari ini?\r\nBukan bahagia yang membuat kita bersyukur tapi bersyukur yang membuat kita bahagiaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦!', '2017-02-06 14:34:46', 11, NULL, NULL, NULL, NULL),
	(25, 'FILOSOFI ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â', '2017-02-09', 'https://qx.esq165.co.id/upload/sementara/img/file_589828ad28d65.jpg', 'DIAM membuat kita MATI..! BERGERAK membuat kita HIDUPÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡', 'Dalam masakan Jepang, kita tahu bahwa ikan salmon akan lebih enak untuk dinikmati jika ikan tersebut masih dalam keadaan hidup saat hendak diolah untuk disajikan. Jauh lebih nikmat dibandingkan dengan ikan salmon yang sudah diawetkan dengan es..\r\nItu sebabnya para nelayan selalu memasukkan salmon tangkapannya ke suatu kolam buatan agar dalam perjalanan menuju daratan salmon-salmon tersebut tetap hidup. Meskipun demikian pada kenyataannya banyak salmon yang mati di kolam buatan tersebut.\r\n.\r\nBagaimana cara mereka menyiasatinya?\r\n.\r\nPara nelayan itu memasukkan seekor HIU KECIL di kolam tersebut... Ajaib..! Hiu kecil tersebut MEMAKSA salmon-salmon itu terus bergerak agar jangan sampai dimangsaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\nAkibatnya jumlah salmon yang mati justru menjadi sangat sedikit..!\r\n.\r\nApa maknanya?\r\nDIAM membuat kita MATI..! BERGERAK membuat kita HIDUPÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦!\r\n.\r\nApa yang membuat kita diam?\r\nSaat tidak ada TANTANGAN dalam hidup dan saat kita berada dalam ZONA NYAMANÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦!\r\n.\r\nSituasi seperti ini kerap membuat kita terlena. Begitu terlenanya sehingga kita tidak sadar bahwa kita telah mati..!!!\r\nIronis, bukan..???\r\n.\r\nBanyak hal yang bisa membuat kita bergerak diantaranya, TANTANGAN, IMPIAN, VISI, KETAKUTAN (ikan salmon bergerak karena ketakutan) dan lain-lain..\r\n.\r\n.\r\nSaat tantangan datang secara otomatis naluri kita membuat kita bergerak aktif dan berusaha mengatasi semua tantangan hidup itu. Ingatlah bahwa kita akan bisa belajar banyak dalam hidup ini bukan pada saat keadaan nyaman, tapi justru pada saat kita menghadapi tantangan. Mungkin hiu-hiu kecil itu bisa berbentuk siapa dan apa saja dalam hidup kita.', '2017-02-06 14:45:18', 11, NULL, NULL, NULL, NULL),
	(26, 'FILOSOFI BERSEPEDA', '2017-02-10', 'https://qx.esq165.co.id/upload/sementara/img/file_58982a044946d.jpg', '"ItÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢s about the jou', 'Anda yang suka bersepeda pasti tahu sekali akan cerita berikut iniÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\nKetika sedang bersepeda pasti kita akan menemui jalanan MENANJAK dan MENURUN\r\nSaat sedang MENANJAK, janganlah Anda terlalu bernafsu mencapai puncak... Atur nafas Anda atur tenaga dan konstankan putaran supaya efektif mencapai puncak dan konsentrasi tetap ada untuk menghadapi jalanan menurun\r\n.\r\nSaat sedang MENURUN Anda janganlah kaget hingga terlalu cepat menarik remÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦ Anda akan terjungkal dan makin terpuruk\r\nIkuti alur jalannya, seimbangkan remnya, ambil momentum putarannya hingga saat Anda menanjak Anda tidak membuang banyak tenaga\r\n.\r\nBersepeda itu bukan masalah jumlah kilometer, tapi lebih pada menikmati setiap kayuhan untuk mendapatkan tiap kilometernya. Begitupula kehidupan, hidup menarik bukan karena jumlah umur, tapi bagaimana kita MENIKMATI setiap detik untuk mendapatkan umur tersebutÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\n.\r\nBersepeda juga bukan masalah sepeda atau komponen yang ada di dalamnya, tapi bagaimana menggunakan sepeda dan komponen tersebut untuk mendapatkan perjalanan yang menarik yang bisa kita nikmati, bisa kita ceritakan, bukan hanya menggunakan sepeda untuk kita banggakan harganyaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\n.\r\nBegitupula kehidupan\r\nKehidupan bukan masalah HARTA yang kita dapatkan, tapi bagaimana MEMAKNAI HARTA yang kita miliki untuk membuat hidup kita lebih berharga secara BATIN, bukan hanya secara nominal\r\n.\r\nAda pepatah jawa bilang, "urip kuwi golek jeneng... ojo golek jenang." Terjemahan\r\nbebasnya, "Hidup itu cari namaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦ bukan cari makan", maksudnya hidup itu harus\r\nbermanfaat (bagi orang banyak) sehingga membuat nama yang baik, bukan hidup hanya\r\ncari harta tapi tak membuat perbedaan apa-apa\r\n.\r\nSama dengan sepeda, buat apa punya sepeda kalau cerita yang kita punya hanya pada\r\nsaat kita membelinya, bukan pada saat menaikinya.\r\nBukankah menaikinya itu terlihat dan terasa lebih menarik...?\r\n.\r\n"ItÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢s about the journey, not the destination... because life is a journey"', '2017-02-06 14:47:54', 11, NULL, NULL, NULL, NULL),
	(27, 'MEMIMPIN DIRI', '2017-02-11', 'https://qx.esq165.co.id/upload/sementara/img/file_58982e1106005.jpg', 'KEPEMIMPINAN LAHIR DARI PROSES INTERNAL ( LEADERSHIP FROM THE INSIDE OUT ).', 'Frederick Agung merupakan Raja Prusia yang sangat terkenal, suatu hari berjalan-jalan di pinggiran kota Berlin. Ia bertemu dengan seorang laki-laki tua yang sedang berjalan ke arahnya, kemudian ia bertanya :\r\n.\r\n.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Kau siapaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â tanya Frederick.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Saya raja,ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â jawab sang laki-laki tua.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Raja?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Frederick tertawa. ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Atas kerajaan mana kau memerintah?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Atas diri saya sendiri,ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â jawab laki-laki tua itu dengan bangga.\r\n.\r\n.\r\nPotongan perbincangan diatas mengingatkan kita bahwa seorang pemimpin harus mampu memimpin dirinya sendiri sebelum memimpin orang lain. Apabila seseorang telah mampu memimpin diri, berarti dia telah berhasil menjelajahi dirinya sendiri, mengenal secara mendalam siapa dirinya. Sebelum memimpin ke luar, ia telah mampu memimpin ke dalam.\r\n.\r\n.\r\nMuhammad SAW, sebelum menjadi nabi dan pemimpin besar, telah berhasil memimpin diri sendiri. Sehingga, pada usia belia, telah mendapat gelar dalam hal integritas dan kejujuran, dan ia mendapat gelar ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Al-AminÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â dari masyarakat Quraisy yang kelak menjadi penentang utamanya. Pengenalan diri secara intens dilakukan di Gua Hiro, yang selanjutnya mengantarkan pada pengenalan Sang Pencipta.\r\n.\r\n.\r\nBegitu pula dengan yang terjadi pada Nelson Mandela menceritakan bahwa selama penderitaan 27 tahun dalam penjara pemerintah Apartheid, justru melahirkan perubahan dalam dirinya. Ia mengalami perubahan karakter, dan memeperoleh kedamaian dalam dirinya. Sehingga, ia menjadi manusia yang mampu mengendalikan diri, memaafkan orang yang memusuhinya. Karena itulah, Mandela kemudian diakui sebagai pemimpin sejati.\r\n.\r\n.\r\nMempimpin diri adalah pekerjaan yang berat. Tak mudah bagi seseorang untuk selalu mampu memimpin diri sendiri melawan penjajahan hawa nafsu. Hal ini berkaitan dengan kedisiplinan diri, yaitu mencapai apa yang sungguh-sungguh diharapkan, dan melakukan yang tidak diinginkan. Musuh yang paling berat untuk ditaklukan adalah diri sendiri.\r\n.\r\n.\r\nSeperti yang dikatakan oleh penulis buku terkenal, Kenneth Blanchard, bahwa kepemimpinan dimulai dari dalam hati, dan keluar untuk melayani mereka yang dipimpinnya. Kepemimpinan adalah transformasi internal dalam diri seseorang. Sesuatu yang tumbuh dan berkembang dari dalam diri seseorang. KEPEMIMPINAN LAHIR DARI PROSES INTERNAL (LEADERSHIP FROM THE INSIDE OUT).\r\n.\r\n.\r\nKarena itulah, ketika usai Perang Badar, RASULULLAH MENGATAKAN BAHWA "MASIH ADA PEPERANGAN YANG LEBIH BESAR, YAITU PERANG MELAWAN DIRI SENDIRI. ITULAH PERANG SEPANJANG HAYAT, YAITU MENGENDALIKAN DAN MEMIMPIN DIRI KE JALAN KEBENARAN.', '2017-02-06 15:05:02', 11, NULL, NULL, NULL, NULL),
	(29, 'KISAH MATA LALAT VS MATA LEBAH', '2017-02-13', NULL, 'Apa yang anda PIKIRKAN akan menghasilkan apa yang Anda LIHAT, dan apa yang Anda lihat akan menghasilkan apa yang anda PEROLEH.', 'Anda tentu sangat mengenal hewan LALAT dan LEBAH. Lalat senang berada di tempat kotor dan berbau busuk. Tidak ada lalat yang senang di tempat yang bersih dan wangi. Hingga apabila ada taman bunga yang luas, namun ada SATU bunga yang busuk, lalat akan mencari satu bunga yang busuk itu. Ya! di manapun lalat hanya akan mencari yang busuk!\r\n.\r\nSedangkan lebah senang berada di antara bunga-bunga yang indah dan harum mewangi. Tidak ada lebah yang senang di tempat yang kotor dan berbau busuk. Hingga apabila ada tumpukan sampah, namun ada satu tangkai bunga yang wangi disana, lebah akan mencari SATU tangkai bunga harum tersebut. Di mana pun lebah akan tetap mencari bunga yang wangi!\r\n.\r\n.\r\nApa hikmah yang bisa diambil dari kisah tersebut?\r\nLihatlah ketetapan Allah pada mata lalat dan mata lebah.\r\nMata lalat selalu melihat keburukan meski di sekelilingnya terdapat banyak kebaikan\r\nMata lebah selalu melihat kebaikan meski di sekelilingnya terdapat banyak keburukan\r\nDengan mata lalat kita akan selalu melihat kekurangan dan hati kita akan tertutup untuk melihat kebaikan atau sisi positif dari apapun kondisi yang ada di hadapan...\r\n.\r\nDengan mata lalat kita tidak akan pernah terpuaskan, senantiasa mengeluh dan jauh dari rasa syukur...\r\nSedangkan dengan mata lebah kita akan selalu melihat pada sisi positif yang bisa diambil dalam seburuk-buruknya kondisi...\r\n.\r\nApa hasilnya?\r\nLebah akan menghasilkan madu yang sangat bermanfaat, sedangkan lalat kaya akan kuman dan penyakit.\r\n.\r\nApa yang anda PIKIRKAN akan menghasilkan apa yang Anda LIHAT, dan apa yang Anda lihat akan menghasilkan apa yang anda PEROLEH.\r\n.\r\nKehidupan bergantung pada hati dan pikiran.\r\nKalau hati dan pikiran Anda selalu negatif, maka apa saja yang Anda lihat akan selalu menjadi negatif dan hasil akhirnya adalah sebuah kehidupan negatif yang terus menerus.\r\n.\r\nKalau hati dan pikiran selalu positif, maka apa saja yang anda lihat akan selalu menjadi positif dan hasil akhirnya adalah kehidupan positif yang terus-menerus.', '2017-02-06 15:50:23', 11, NULL, NULL, NULL, NULL),
	(30, 'GIVE BEFORE GET', '2017-02-14', 'https://qx.esq165.co.id/upload/sementara/img/file_58984495de2e8.jpg', 'Jika kita ingin menikmati kebaikan, kita bisa memulai dengan menabur kebaikan kepada orang-orang di sekitar kita.', 'Di satu desa di Osaka, Jepang, ada seorang petani yang menanam jagung unggulan & seringkali memenangkan penghargaan Petani Dengan Jagung Terbaik Sepanjang Musim.\r\n.\r\n.\r\nSuatu hari, seorang wartawan dari koran lokal melakukan wawancara untuk menggali Rahasia Kesuksesan petani tersebut.\r\n.\r\n.\r\nWartawan itu menemukan bahwa ternyata petani itu selalu membagikan benih jagungnya kepada para tetangganya.\r\n.\r\n.\r\n"Bagaimana Anda bisa berbagi benih jagung dengan tetangga Anda, lalu bersaing dengannya dalam kompetisi yang sama setiap tahunnya?"\r\ntanya wartawan, dengan penuh rasa heran & takjub.\r\n.\r\n.\r\n"Tidakkah Anda mengetahui bahwa angin menerbangkan serbuk sari dari jagung yang akan berbuah & membawanya dari satu ladang ke ladang yang lain\r\nJika tetangga saya menanam jagung yang jelek, maka kualitas jagung saya akan menurun ketika terjadi serbuk silang. Jika saya ingin menghasilkan Jagung kualitas unggul, maka saya harus membantu tetangga saya untuk menanam jagung yang bagus pula", jawab si petani itu.\r\n.\r\n.\r\nDalam kehidupan ini,\r\njika kita ingin menikmati kebaikan, kita bisa memulai dengan menabur kebaikan kepada orang-orang di sekitar kita.', '2017-02-06 16:40:43', 11, NULL, NULL, NULL, NULL),
	(31, 'BAGAIMANA MEMANFAATKAN SUARA HATI', '2017-02-15', 'https://qx.esq165.co.id/upload/sementara/img/file_589848a43de18.jpg', 'Pengalaman dan kebiasaanlah yang telah membelenggu hati serta pikiran, yang pada akhirnya merugikan. Memanfaatkan suara hati  fitrahnya mendorong untuk lebih sempurna, merdeka dan lebih maju.', 'Sebelum memimpin sebuah bank tahun 1997, Dony mempunyai pengalaman memimpin beberapa perusahaan. Ia selalu menghadapi permasalahan yang hampir sama. Apabila hendak melakukan suatu perbaikan atau perombakan manajemen, para karyawan selalu menanggapinya dengan pernyataan ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Dari dulu juga begini, tidak apa-apa kok!ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\ninilah belenggu para karyawan Dony.\r\n.\r\nSaat itu Dony memimpin sebuah bank swasta. Para karyawannya terbiasa mengirim surat dengan dua lembar kertas. Pada lembar kedua isinya ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Demikianlah, terima kasihÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â dan sebuah goresan tanda tangan. Kebiasaan tersebut diubahnya sehingga surat itu dipadatkan menjadi satu halaman saja. Penghematan yang dibuatnya ternyata sangat berpengaruh.\r\n.\r\nKatakan saja, seandainya ada satu juta nasabah yang setiap bulan harus dikirimi surat pemberitahuan, artinya satu tahun menjadi 1 juta x 12 = 12 juta lembar! Kalikan saja dengan Rp250 untuk harga per lembar surat. Penghematan 250 x 12 juta lembar = Rp3 milyar. Seandainya hal itu dilakukan sepuluh tahun yang lalu, penghematan yang terjadi adalah Rp30 milyar. Inilah contoh betapa berharganya sebuah dorongan suara hati murni yang telah merdeka.\r\n.\r\nInti permasalahannya, mengapa hal itu tidak mampu ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“dilihatÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â oleh karyawan yang bertanggung jawab terhadap surat-surat tersebut?\r\n.\r\nKembali...pengalaman dan kebiasaanlah yang telah membelenggu hati serta pikiran, yang pada akhirnya merugikan. Terkadang seseorang tidak bisa lagi menilai sesuatu secara obyektif, apalagi jika pengalaman atau budaya tersebut dimiliki secara kolektif, dan telah berubah menjadi suatu paham.\r\n.\r\nKini Dony telah berhasil memanfaatkan suara hati fitrahnya, mendorongnya untuk lebih sempurna, di saat karyawannya masih terbelenggu oleh pengalaman yang acapkali membuat mereka tidak berpikir merdeka dan lebih maju.', '2017-02-06 17:09:38', 11, NULL, NULL, NULL, NULL),
	(32, 'JANGAN BIARKAN EGO MEMBUNUHMU', '2017-02-16', 'https://qx.esq165.co.id/upload/sementara/img/file_58984c293d192.jpg', '" Jangan biarkan EGO membunuh dirimu. lebih baik BUNUHLAH EGOMU! "', 'Siapa yang merasa dirinya EGOIS?\r\n.\r\nAtau ada sebagian dari Anda yang tidak merasa punya sifat egois?\r\n.\r\nBerikut ini Saya akan sharing sebuah kisa tentang sebuah KEEGOISAN. Apakah yang disebabkan oleh KEEGOISAN Anda? Simak cerita berikut ini.\r\n.\r\nAda seorang ilmuwan yang sangat pintar. Setelah dia sudah begitu banyak melakukan usaha dan latihan, dia akhirnya berhasil membuat sebuah formula dan mempelajari cara menciptakan dirinya sendiri. Dia melakukannya dengan begitu sempurna sehingga sangat mustahil untuk menentukan awal dari penciptaan itu.\r\n.\r\nSuatu hari, saat melakukan penelitiannya, dia menyadari bahwa malaikat maut sedang mencarinya. Dengan maksud untuk tetap hidup, dia menciptakan selusin tiruan dirinya dari formula yang telah dia ciptakan. Penciptaan tersebut benar-benar serupa sehingga semuanya benar-benar tampak seperti dirinya.\r\n.\r\nLalu saat malaikat maut datang, dia kebingungan untuk mengetahui siapakah diantara ketigabelas orang itu yang merupakan ilmuwan yang asli, karena kebingungan, akhirnya dia meninggalkan mereka semua dan kembali ke surga.\r\n.\r\nTapi tak berapa lama kemudian, setelah malaikat tersebut menjadi ahli dalam mengenal kebiasaan manusia, malaikat tersebut datang dengan sebuah ide cerdas. Dia mengatakan kepada ketigabelas ilmuwan itu, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Tuan, Anda pasti sangat jenius karena berhasil membuat sebuah reproduksi yang begitu sempurna. Namun, aku telah menemukan sebuah celah pada pekerjaanmu, hanya sebuah celah kecil.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\nIlmuwan yang asli lantas maju dan berteriak, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Mustahil!!Dimana celahnya?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Jawab sang ilmuwan dengan penuh emosi karena merasa terkalahkan.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“DisiniÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Âkata sang malaikat, lalu mengambil si ilmuwan dan mencabut nyawanya\r\nSang ilmuwan tak sadar jika malaikat sedang mengecohkan dirinya. Sang Malaikat berusaha memancing sang ilmuwan agar menjawab pertanyaan yang ia ajukan.\r\n.\r\nDengan jawaban yang penuh sifat egois dan amarah, sang malaikat berhasil membuat sang ilmuwan mengaku dengan cara yang tidak disangka-sangka.\r\n.\r\nSeluruh hasil dan formula reproduksi yang telah dibuat sang ilmuwan akhirnya gagal karena dia tidak bisa mengendalikan kesombongannya, jadi dia kehil.angan hidupnya.\r\n.\r\nJadi, saat ilmu dan kemampuan seorang manusia menjadikan seorang yang berada di posisi atas dan membuatnya sukses, bagaimanapuntiga huruf yang bernama ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“EGOÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â dapat menjatuhkannya dengan cepat pada kecepatan dua kali lipat.\r\n.\r\nApabila seseorang berpegang pada prinsip-prinsip yang salah dan pemikiran yang tidak jernih, akan menimbulkan suatu tindakan yang salah pula\r\n.\r\nKesimpulannya, jangan biarkan ego membunuh dirimu. lebih baik BUNUHLAH EGOMU!', '2017-02-06 17:13:19', 11, NULL, NULL, NULL, NULL),
	(33, 'ELANG DAN AYAM', '2017-02-17', 'https://qx.esq165.co.id/upload/sementara/img/file_5899393a02e33.jpg', 'Saat Anda merasa diri Anda tidak mampu meraih yang Anda impikan, saat itulah Anda memutuskan diri Anda sendiri untuk menjadi orang yang gagal.', 'Di sebuah lereng gunung yang curam, ada sebuah sarang elang yang berisikan empat telur elang berukuran besarÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\n.\r\nSuatu hari, gempa bumi telah menguncang gunung itu menyebabkan salah satu dari telur itu jatuh ke kandang ayam yang berada di lembah di bawah lereng itu.\r\n.\r\nAyam-ayam pun tahu bahwa mereka harus melindungi telur elang itu. Kemudian, telur elang pun menetas dan seekor elang yang cantik pun terlahir.\r\n.\r\nSebagai ayam, ayam-ayam itu pun membesarkan ELANG sebagai seekor AYAMÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\n.\r\nSang elang pun sangat menyukai tempat tinggal dan keuarganya itu, namun sepertinya ia merasa ada semangat untuk berteriak lebih keras dari sekedar jiwa ayam.\r\n.\r\nHingga pada suatu hari, elang itu pun menatap langit dan melihat sekelompok elang-elang hebat terbang tingi melayang-layang.\r\n.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“OhÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â teriak sang elang. ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Andai saja aku bisa terbang tinggi seperti burung-burung itu.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nAyam-ayam itu pun tertawa, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Kau tidak bisa terbang tinggi seperti mereka. Kau adalah seeokor AYAM dan ayam TIDAK BISA terbang.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nElang itu pun terus menatap keluarganya yang sesungguhnya di angkasa sana, bermimpi mengkhayalkan ia bisa seperti merekaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\n.\r\nSetiap kali elang itu membicarakan tentang impian-impiannya, ia selalu diberitahu bahwa ia tidak akan bisa melakukannya.\r\n.\r\nItulah yang dipelajari elang untuk diyakiniÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦ Seiring waktu, elang itu pun berhenti bermimpi dan kembali menjalani hidupnya sebagai ayam.\r\n.\r\nAkhirnya, setelah hidup lama sebagai seekor ayam, elang itu pun mati. Mati sebagai seekor ayam tanpa pernah bisa terbang tinggiÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\n.\r\nApa hikmahnya?\r\n.\r\nMungkin saat ini kita juga merasakan hal yang sama seperti elang tersebut, saat kita ingin meraih sesuatu yang besar ada saja halangan dan perkataan orang di sekitar kita yang mematahkan semangat kita tersebutÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\nKadang kita merasa hanya seperti ayam tersebut yang tidak dapat terbang tinggi, tidak memiliki kemampuan lebih, padahal tanpa disadari di dalam diri kita terdapat potensi yang besar untuk meraih kesuksesan yang kita inginkan.\r\n.\r\nSaat Anda merasa diri Anda tidak mampu meraih yang Anda impikan, saat itulah Anda memutuskan diri Anda sendiri untuk menjadi orang yang gagal.', '2017-02-07 10:04:29', 11, NULL, NULL, NULL, NULL),
	(34, 'BAHAGIA ITU SEDERHANA', '2017-02-18', 'https://qx.esq165.co.id/upload/sementara/img/file_58993bf38b22e.jpg', 'Langkah jitu wujudkan ungkapan "BAHAGIA ITU SEDERHANA" ke dalam kehidupan kita.', '5 Langkah Wujudkan Ungkapan "BAHAGIA ITU SEDERHANA"\r\n.\r\nSering kita mendengar ungkapan "BAHAGIA ITU SEDERHANA." Namun sayangnya, menjalani hidup bahagia itu tak semudah mengucapkannya. Ada yang tak jua merasa bahagia hingga sekarang dan ada juga yang kehilangan rasa bahagia itu.\r\n.\r\nJika ini kegalauan Anda, coba simak ulasan berikut. Ada 5 langkah jitu wujudkan ungkapan "BAHAGIA ITU SEDERHANA" ke dalam kehidupan kita.\r\n.\r\n1. Mulailah dari Hal Kecil\r\nPencapaian dari impian dan harapan yang besar kerap menjadi tolak ukur kebahagiaan. Mungkin ini benar, tapi prosesnya pencapaian itu butuh waktu. Kapan kita akan bahagia jika menunggu itu?\r\nMulai saja kebahagiaan Anda detik ini dari hal-hal kecil yang tampaknya sepele. Misalnya pastikan untuk memiliki tidur berkualitas, tubuh yang sehat, menjaga komunikasi, dan sebagainya.\r\n.\r\n2. Redam dan kendalikan Amarah\r\nAmarah itu adalah rasa alamiah dari manusia. Boleh saja kita marah, asal tidak berlebihan dan berkepanjangan. Jadi, redam dan kendalikan amarah itu.\r\nCaranya? Banyak tips yang bisa dicoba dan salah satunya seperti dengan berpuasa.\r\n.\r\n3. Nikmati Proses\r\nBahkan jika proses itu adalah kegagalan, nikmati dan hadapi saja. Jangan takut menghadapi tantangan dan hal baru, karena di situ ada kunci kebahagiaan.\r\nOtak manusia akan memberikan rasa yang kuat, ketika dirangsang dengan kejutan. Katakan pada diri sendiri, "nikmati dan ambil hikmahnya."\r\n.\r\n4. Stop Memanjakan Kesedihan\r\nSedih berlarut-larut dan mencari alasan berkubang dalam rasa duka sering jadi kebiasaan manusia. Menghambur-hamburkan uang dan membuat waktu untuk memanjakan kesedihan.\r\nHentikan dan lakukan sebaliknya. Keraslah pada rasa sedih itu dan bergeraklah untuk lebih maju.\r\n.\r\n5. Trik Berpura-Pura\r\nPerasaan itu seringnya mengikuti tindakan. Contohnya ketika merasa sedih, berpura-pura dan sengajalah untuk bersikap ceria.\r\nTrik ini cukup ampuh untuk memperbaiki suasana hati dan membuat kita benar-benar merasa lebih bahagia. Cobalah jika tak percaya.', '2017-02-07 10:16:29', 11, NULL, NULL, NULL, NULL),
	(35, 'SEMUA ORANG ADALAH PEMIMPIN', '2017-02-19', 'https://qx.esq165.co.id/upload/sementara/img/file_58993cdddf802.jpg', 'Sering kali kita tak menyadari bahwa sebenarnya Kita adalah Pemimpin bagi diri Kita sendiri.', 'Banyak orang yang berharap menjadi pemimpin, seperti menjadi seorang manajer, direktur, atau mungkin pimpinan di organisasi. Namun, mereka sering kali tak menyadari bahwa sebenarnya mereka adalah pemimpin bagi diri mereka sendiri.\r\n.\r\nSaat seorang anak menjadi ketua kelas, ia adalah pemimpin. Guru SD adalah pemimpin bagi muridnya. Seorang ibu pun pemimpin bagi anak-anaknya. Hampir setiap orang menjadi pemimpin di lingkungan masing-masing, terlepas dari besar kecilnya jumlah orang dalam kelompok tersebut. Meski hanya satu orang saja pengikutnya, ia masih bisa dikatakan sebagai pemimpin.\r\n.\r\nBahkan, setiap manusia adalah pemimpin bagi dirinya sendiri.\r\n.\r\nTidak adanya kesadaran bahwa setiap orang adalah pemimpin, acapkali mengakibatkan orang tidak mau mengembangkan ilmu kepemimpinannya. Jargon-jargon seperti, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Saya ini rakyat kecil,ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â sesungguhnya sangat mengerdilkan jiwa manusia yang mulia. Betapa tidak, seorang tukang becak pun adalah pemimpin bagi keluarganya di rumah. Apalagi, bila ia mampu menghidupkan kebesaran jiwa di kalbu anak-anaknya.', '2017-02-07 10:20:17', 11, NULL, NULL, NULL, NULL),
	(36, 'INTEGRITAS', '2017-02-20', 'https://qx.esq165.co.id/upload/sementara/img/file_58993d3f08bb8.jpg', 'Integritas hanya membutuhkan tepukan halus di bahu dari seorang malaikat.', 'Beberapa tahun yang lalu, Dr.Ary pernah ditawari bekerja sama untuk pemasaran suatu produk terbaru dari rekannya. Saat pertama bertemu, rekannya menunjukkan cash flow perusahaan yang cukup baik. Kemudian, sambil bersantai rekannya bercerita tentang mitra kerja lamanya, yang menurut-nya sangat mengecewakan, dan baru saja putus hubungan dengannya. Ia men-ceritakan panjang lebar tentang keburukan-keburukan eks-mitra kerjanya itu..\r\n.\r\nIa berniat untuk bekerja sama dengan perusahaan Dr.Ary kala itu sebelum mendirikan lembaga Training ESQ. Kemudian, Dr.Ary bertanya kepadanya ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Apakah Anda juga akan menceritakan diri saya kepada orang lain, seperti Anda menceritakan eks-mitra kerja Anda kepada saya?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nDia terdiam, seolah menyadari kekeliruannya. Saat itu juga, tanpa menunggu jawabannya, Dr.Ary langsung memutuskan untuk berkata ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“tidakÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â, bahwa saya belum tertarik pada jenis usaha tersebut, dan mengucapkan terima kasih atas penawarannya.\r\n.\r\n"Sebenarnya, secara bisnis, penawaran itu cukup bagus, namun saya berpikir bahwa ia bukanlah orang yang tepat untuk diajak bekerja sama dalam jangka panjang karena tidak memiliki integritas terhadap orang lain yang tidak ada di depannya." tutur Dr.Ary\r\n.\r\nINTEGRITAS adalah sikap jujur dan dapat dipercaya. Integritas muncul dari kesadaran diri yang bersumber dari hati. Integritas tidak menipu dan tidak berbohong. Integritas tidak memerlukan tepuk tangan orang lain dan sorak-sorai pujian. Integritas hanya membutuhkan tepukan halus di bahu dari seorang malaikat.', '2017-02-07 10:21:53', 11, NULL, NULL, NULL, NULL),
	(37, 'BERPRINSIPLAH PADA SESUATU YANG ABADI', '2017-02-21', 'https://qx.esq165.co.id/upload/sementara/img/file_58993e1e7f00e.jpg', 'ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Prinsip yang benar tidaklah b', 'Agus bekerja di sebuah perusahaan swasta. Teman-temannya rata-rata memiliki mobil mewah. Agus merasa minder dan rendah diri, dari situlah ia berusaha sekuat tenaga untuk bisa mempunyai mobil mewah. Akhirnya, setelah setahun kemudian impian itu tercapai, meski anak dan istrinya masih menyewa sebuah kamar yang sempit. Namun, demi simbol kekayaan, ia akhirnya bisa membeli sebuah mobil mewah.\r\n.\r\nTahun terus berjalan, dan teman-temannya yang umumnya adalah para pengusaha sukses, sudah berganti mobil yang lebih baru dan lebih mewah. Mobil Agus akhirnya ketinggalan zaman, karena ternyata mobil kebanggaannya itu sudah tak bisa dibanggakannya lagi seiring waktu berjalan. Agus kembali kehilangan status.\r\n.\r\nApa pesan yang bisa kita ambil dari kisah Agus?\r\n.\r\nDalam lingkungan kita, mungkin sering kita temukan banyak orang yang lebih dari diri kita dari segi harta-benda, penghormatan, jabatan, dan tingkat sosial. Itu sering kali membuat sebagian orang menjadi rendah diri, bahkan kehilangan kepercayaan diri. Tidak ada sebuah ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“peganganÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â yang memberikan kekuatan diri sejati. Padahal, tidak ada satu pun ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“peganganÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â yang lebih kuat dari keyakinan akan Allah Yang Agung, yang dengannya kita mampu membangun kepercayaan diri yang tinggi. Inilah prinsip tauhid atau spiritual comitment..ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Laa ilaha Illallah.\r\n.\r\nSahabat ESQ, berprinsiplah pada sesuatu yang abadi. Stephen R Covey pun mendukung konsep ini, ia berkata ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“.....prinsip yang benar tidaklah berubah. Kita dapat memegang prinsip tersebut. Prinsip tidak bereaksi terhadap apa pun.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nPrinsip itu kekal, tak peduli apa pun yang terjadi. Dengan berprinsip pada sesuatu yang Kekal, kita tidak akan goyah meski kehilangan harta, orang-orang kesayangan, kawan, dan penghargaan, bahkan mengalami penyiksaan.', '2017-02-07 10:25:33', 11, NULL, NULL, NULL, NULL),
	(38, 'Rencana Kerja Harian', '2017-02-22', 'https://qx.esq165.co.id/upload/sementara/img/file_5899403157a83.jpg', 'Meraih sukses itu seperti meniti sebuah peta yg panjang dan berliku. Anda tidak akan mampu membayangkannya. Akan tetapi, biarlah jari jemari Anda yang mengikuti kemana arah jalan setapak takdir kita.', 'Selamat pagi. \r\nSiapa diantara sahabat ESQ yang ingin hidup sukses dan maju? Ya..Saya percaya bahwa Anda semua ingin sekali maju dan ingin kemajuan yg berarti, kan? \r\nOleh karena itu, Saya percaya bahwa Anda pasti sudah membuat rencana apa yang akan Anda kerjakan pada hari ini di dalam buku Ajaib Agenda Harian Anda masing-masing.\r\n.\r\nSehingga, pagi ini tidak perlu lagi duduk merenung membuat planning. Waktu pagi itu singkat sekali dan Matahari akan segera naik.\r\n.\r\nSahabat ESQ pasti sudah paham betul, meraih sukses itu seperti meniti sebuah peta yg panjang dan berliku. Anda tidak akan mampu membayangkannya dengan pikiran apa yang ada di hadapan. Akan tetapi, biarlah jari jemari Anda yang mengikuti kemana arah jalan setapak takdir kita. Begitulah Anda akan dituntun oleh jemari Anda sendiri , kemana akan melangkah. Hingga hidup menjadi sebuah permainan yg menantang dan mengasyikkan.\r\n.\r\nAnda akan menguasai detik demi detik permainan kehidupan. Dengan cara ini Anda akan memiliki kompas dan stop watch yg bisa Anda atur sendiri. See you on a top of the mountain.', '2017-02-07 10:41:25', 11, NULL, NULL, NULL, NULL),
	(39, 'BELAJAR IKHLAS DARI GULA PASIR', '2017-02-23', 'https://qx.esq165.co.id/upload/sementara/img/file_589942be2f680.jpg', 'Kadang KEBAIKAN yang ditanam tidak pernah disebut orang\r\nTetapi KESALAHAN akan dibesar-besarkanÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒ', 'GULA PASIR memberi RASA MANIS pada KOPI, tapi orang menyebutnya KOPI MANIS... bukan KOPI GULA...\r\nGULA PASIR memberi rasa MANIS pada TEH, tapi orang menyebutnya TEH MANIS...bukan TEH GULA...\r\nGULA PASIR memberi rasa MANIS pada ES JERUK..., tapi orangenyebutnya ES JERUK MANIS...bukan ES JERUK GULA\r\nORANG menyebut ROTI MANIS...bukan ROTI GULA...\r\nORANG menyebut Syrup PANDAN , Syrup APEL, Syrup JAMBU....\r\npadahal bahan dasarnya GULA....\r\n.\r\nTapi GULA tetap IKHLAS LARUT dalam memberi RASA MANIS...\r\n.\r\nAkan tetapi apabila berhubungan dengan penyakit, barulah GULA disebut.. PENYAKIT GULA ..!!!\r\n.\r\nBegitu pun dalam kehidupanÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\nKadang KEBAIKAN yang ditanam tidak pernah disebut orang\r\nTetapi KESALAHAN akan dibesar-besarkanÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦!\r\n.\r\n.\r\nIKHLAS lah seperti GULA...\r\nLARUT lah seperti GULA...\r\nTetap SEMANGAT memberi KEBAIKAN...\r\nTetap SEMANGAT menyebar KEBAIKANÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\nKarena KEBAIKAN tidak UNTUK DISEBUT...\r\nTapi untuk DIRASAKAN..', '2017-02-07 10:45:23', 11, NULL, NULL, NULL, NULL),
	(40, 'FILOSOFI POHON BAMBU', '2017-02-24', 'https://qx.esq165.co.id/upload/sementara/img/file_589943b76945e.jpg', 'Tidak ada kata menyerah untuk terus tumbuh, tidak ada alasan untuk terpendam dalam keterbatasan', 'Tahukah Anda bahwa pohon bambu tidak akan menunjukkan pertumbuhan berarti selama 5 tahun pertama? Walaupun setiap hari disiram dan dipupuk, tumbuhnya hanya beberapa puluh centimeter saja.\r\n.\r\nNamun setelah 5 tahun kemudian, pertumbuhan pohon bambu sangat dahsyat dan ukurannya tidak lagi dalam hitungan centimeter melainkan meterÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦!\r\n.\r\n.\r\nLantas sebetulnya apa yang terjadi pada sebuah pohon bambu???\r\n.\r\nTernyata selama 5 tahun pertama, pohon bambu mengalami pertumbuhan dahsyat pada akar BUKAN pada batang, pada saat itu pohon bambu sedang mempersiapkan pondasi yang sangat kuat, agar bisa menopang ketinggiannya yang berpuluh-puluh meter dikemudian hari.\r\n.\r\n.\r\nApa hikmah yang bisa diambil?\r\n.\r\nJika Anda mengalami suatu hambatan dan kegagalan, bukan berarti Anda tidak mengalami perkembangan, melainkan justru Anda sedang mengalami pertumbuhan yang luar biasa di dalam diri Anda.\r\n.\r\nKetika Anda lelah dan hampir menyerah dalam menghadapi kerasnya kehidupan, jangan pernah terbersit pupus harapan.\r\n\r\nBagian TERBERAT dari sebuah KESUKSESAN adalah perjuangan di saat awal seseorang MEMULAI USAHA, karena segala sesuatu terasa begitu BERAT DAN PENUH TEKANAN.\r\n.\r\nNamun bila ia dapat melewati batas tertentu, sesungguhnya seseorang dapat merasakan segala kemudahan dan kebebasan dari tekanan dan beban.\r\n.\r\nNamun sayang, banyak orang yang MENYERAH di saat tekanan dan beban dirasakan terlalu berat, bagai sebuah roket yang gagal menembus atmosfir.\r\n.\r\n.\r\nBuya Hamka berkata ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Kalau hidup sekedar hidup, babi di hutan juga hidup dan kalau kerja sekedar kerja, kera juga bekerjaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â.\r\n.\r\nKetika pohon bambu ditiup angin kencang, ia akan merunduk, tetapi setelah angin berlalu, ia akan tegak kembali, laksana perjalanan hidup seorang manusia yang tak pernah lepas dari cobaan dan rintangan.\r\n.\r\nFleksibilitas pohon bambu mengajarkan kita sikap hidup yang berpijak pada keteguhan hati dalam menjalani hidup, walaupun badai dan topan menerpa.\r\n.\r\nTidak ada kata menyerah untuk terus tumbuh, tidak ada alasan untuk terpendam dalam keterbatasan, karena bagaimanapun pertumbuhan demi pertumbuhan harus diawali dari kemampuan untuk mempertahankan diri dalam kondisi yang paling sulit sekalipun.\r\n.\r\nMaka jadilah seperti pohon bambu yang MENJULANG TINGGI dan menjadi KEBERKAHAN bagi sesama!!!', '2017-02-07 10:50:27', 11, NULL, NULL, NULL, NULL),
	(41, 'INILAH KEBIASAAN YANG BISA MEMBUAT ANDA KAYA', '2017-02-25', 'https://qx.esq165.co.id/upload/sementara/img/file_589944b611b65.jpg', 'ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Jika kita tidak mau berubah, ', 'Sering kali kita berpikir, bagaimana caranya agar kita bisa kaya secara materil dengan waktu cepat, tapi sedikit di antara kita tidak memikirkan arti kaya yang sebenarnya.\r\n\r\nBagaimana Caranya?\r\n\r\nCaranya...HABIT atau KEBIASAAN kita perlu di setting. Karena ini yang memiliki peranan penting dalam menentukan arah yang akan kita lalui!\r\n.\r\n.\r\n1. Kebiasaan Mengucap Syukur\r\n\r\nIni adalah kebiasaan istimewa yang bisa mengubah hidup selalu menjadi lebih baik. Bahkan agama mendorong kita bersyukur tidak saja untuk hal-hal yang baik, tapi juga dalam kesusahan dan hari-hari yang buruk.\r\n.\r\n.\r\nAda RAHASIA besar di balik ucapan syukur yang sudah terbukti sepanjang sejarah.\r\n\r\nHellen Keller yang buta dan tuli sejak usia dua tahun, telah menjadi orang yang terkenal dan dikagumi di seluruh dunia.\r\nSalah satu ucapannya yang banyak memotivasi orang adalah, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Aku bersyukur atas cacat-cacat ini, aku menemukan diriku, pekerjaanku dan TuhankuÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â.\r\n.\r\n.\r\nMemang sulit untuk bersyukur, namun kita bisa belajar secara bertahap.\r\n\r\nMulailah mensyukuri kehidupan, mensyukuri kesehatan, keluarga, sahabat, dan sebagainya. Lama kelamaan Anda bahkan bisa bersyukur atas kesusahan dan situasi yang buruk.\r\n.\r\n.\r\n2. Kebiasaan Berpikir Positif\r\n\r\nHidup kita dibentuk oleh apa yang paling sering kita pikirkan. Kalau selalu berpikiran positif, kita cenderung menjadi pribadi yang yang positif.\r\n\r\nCiri-ciri dari pikiran yang positif selalu mengarah kepada kebenaran, kebaikan, kasih sayang, dan harapan.\r\n\r\nSering-seringlah memantau apa yang sedang Anda pikirkan. Kalau Anda terbenam dalam pikiran negatif, kendalikanlah segera ke arah yang positif.\r\n\r\nJadikanlah berpikir positif sebagai kebiasaan dan lihatlah betapa banyak hal-hal positif yang akan Anda alami.\r\n.\r\n.\r\n3. Kebiasaan Bertindak\r\n\r\nBila Anda sudah mempunyai pengetahuan, sudah mempunyai tujuan yang hendak dicapai dan sudah mempunyai kesadaran mengenai apa yang harus dilakukan, maka langkah selanjutnya adalah bertindak.\r\n\r\nBiasakan untuk mengahargai waktu, lawanlah rasa malas dengan bersikap aktif. Banyak orang yang gagal dalam hidup karena hanya mempunyai impian dan hanya mempunyai tujuan tapi tak mau melangkah.\r\n.\r\n.\r\nItulah 3 Kebiasaan yang bisa membuat Anda kaya.\r\n\r\nJika Anda berhasil menjalankannya, maka akan lebih mudah bagi Anda untuk menggapai tujuan akhir sebagai seseorang yang KAYA.\r\n\r\nKarena orang KAYA akan selalu dibarengi dengan orang SUKSES yang telah memaknai hidupnya dengan sangat berarti.\r\n.\r\n.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Jika kita tidak mau berubah, maka waktu yang akan merubah kitaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â -Ary Ginanjar Agustian-', '2017-02-07 10:53:33', 11, NULL, NULL, NULL, NULL),
	(42, 'BERSYUKUR dan BERTERIMAKASIH', '2017-02-26', 'https://qx.esq165.co.id/upload/sementara/img/file_58994694f12b6.jpg', 'Saat BERKAH (hal yang menguntungkan) datang, semua sibuk tanpa peduli siapa yang memberi. Namun saat masalah datang, maka semua akan spontan mencari sumber masalahnya.', 'Tentang rasa syukur sebagai sumber kebahagiaan. Ada seorang dermawan yang dari atas gedung menebar uang :\r\n.\r\nRp. 5.000,-\r\nRp. 10.000,-\r\nRp. 20.000,-\r\nRp. 50.000,-\r\nRp. 100.000,-\r\n.\r\nDi bawah gedung berkerumun banyak orang yang sibuk saling berebut memunguti uang yang berserakan "TANPA ADA YANG PEDULI" sumber uang itu dari SIAPA.\r\n.\r\n.\r\nSuatu saat, Sang Dermawan naik lagi keatas gedung tersebut dan kali ini beralih menebar krikil2 kecil kedalam kerumunan orang\r\ndibawah, ada yang terkena di kepala, bahu, tangan, punggung dan anggota tubuh lainnya.\r\n.\r\n.\r\nMereka panik dan marah, menengadah keatas berusaha "MENCARI TAHU" darimana sumber dari krikil dijatuhkan?\r\n.\r\n.\r\nItulah sikap dari kebanyakan manusia, saat BERKAH (hal yang menguntungkan) datang, semua sibuk tanpa peduli siapa yang memberi dan sedikit sekali yang mampu berterima kasih dan mau mengucap syukur.\r\n.\r\n.\r\nNamun saat masalah datang, maka semua akan spontan mencari sumber masalah dan biang keroknya serta marah dan enyalahkan orang lain tanpa mau cari solusi lagi.\r\n.\r\n.\r\n"Apakah kita hanya mau menerima yang baik saja, tetapi tidak mau menerima yang buruk ??" Tanpa mau tahu bahwa hidup ini sudah satu paket, baik & buruk, senang & susah, semuanya satu kesatuan yang tak terpisahkan.\r\n.\r\n.\r\nBila suatu ketika Anda "kena giliran" menjalani hal-hal buruk dan susah, maka jalanilah dengan tetap bersyukur dan sabar karena\r\nhanya itu kuncinya.\r\n.\r\n.\r\nSemoga menjadi renungan untuk kita bersama', '2017-02-07 11:01:29', 11, NULL, NULL, NULL, NULL),
	(43, 'PENTINGNYA SEBUAH INTEGRITAS DAN LOYALITAS', '2017-02-27', 'https://qx.esq165.co.id/upload/sementara/img/file_5899487cc43f0.jpg', 'Integritas tidak memerlukan tepuk tangan orang lain, dan sorak sorai pujian. Integritas hanya bersahabat dengan suara hati, suara Tuhan.', 'Apa pentingnya sebuah integritas dan loyalitas? Simak dan pahami baik-baik apa makna dari cerita yang saya sampaikan berikut ini.\r\n.\r\n.\r\nSaya akan beritahu sebuah contoh singkat tentang integritas dan kepercayaan di Amerika Serikat. Sebuah lembaga bernama Ethnics Officers Association memprakarsai survey terhadap 1.300 pekerja di semua jenjang perusahaan-perusahaan Amerika. Yang mereka temukan dalam survey tersebut ternyata sangat mengejutkan. Apa hasil surveynya???\r\n.\r\nTernyata, sekitar separuh dari mereka mengaku terlibat dalam praktek-praktek bisnis yang tidak etis dan tidak jujur. Mulai dari hal-hal kecil seperti mencuri kertas atau pensil, berbohong kepada atasan, sampai pembajakan hak cipta.\r\n.\r\n.\r\nApa pendapat Anda terhadap orang-orang yang melakukan tindakan kurang terpuji tersebut???\r\n.\r\n.\r\nTernyata, masih banyak orang-orang yang melakukan kejahatan kecil apabila memiliki kesempatan. Dan apa yang mereka lakukan itu tidak dilihat oleh orang lain. Orang-orang yang demikian menganggap bahwa hal tersebut tidak akan diketahui oleh atasan mereka, serta mereka menganggap pelanggaran-pelanggaran etika itu adalah hal biasa dan wajar bila dilakukan.\r\n.\r\n.\r\nPadahal, itu menyangkut sesuatu yang serius. Yaitu integritas dan loyalitas. Integritas merupakan sikap jujur, konsisten, komitmen, berani dan dapat dipercaya. Sedangkan loyalitas adalah kesetiaan pada prinsip yang dianut. Mengapa hal yang tidak baik itu bisa terjadi? Siapa yang bisa menjawab???\r\n.\r\n.\r\nYa....Itu terjadi karena adanya prinsip-prinsip hidup yang dianut, seperti prinsip bekerja semata-mata hanya untuk mencari uang dan hasilnya ingin dinilai baik oleh atasan.\r\n.\r\n.\r\nApa hasilnya? \r\nHasilnya adalah uang menjadi orientasi utama dan akan bekerja baik apabila hanya dilihat oleh orang lain. Akibatnya orang tersebut akan sulit mencapatkan kepercayaan. Lakukanlah hal-hal baik, dimanapun, kapanpun dan kepada siapapun. Integritas tidak memerlukan tepuk tangan orang lain, dan sorak sorai pujian. Integritas hanya bersahabat dengan suara hati, suara Tuhan.', '2017-02-07 11:13:08', 11, NULL, NULL, NULL, NULL),
	(44, 'PRASANGKA BAIK ATAU PRASANGKA BURUK??', '2017-02-28', 'https://qx.esq165.co.id/upload/sementara/img/file_58994ae590ceb.jpg', 'Tindakan seseorang sangat bergantung pada Pikirannya.', 'PRASANGKA BAIK ATAU PRASANGKA BURUK??\r\nBagaimana Anda semua menanggapi headline di atas? Ada yang bisa menjawab??? Kali ini saya akan berbagi sebuah cerita mengenai prasangka.\r\n.\r\n.\r\nPada suatu pagi, ada sebuah rapat antar departemen hasil evaluasi rutin sedang dibicarakan. Tahukah Anda? Seorang karyawan tiba-tiba menguap di tengah rapat yang sedang berlangsung dengan serius. Seisi ruangan pun spontan menoleh ke orang yang menguap tersebut. Sang Bos menatap ke arahnya sambil menggeleng-gelengkan kepala dan berkata ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Saya kecewa sekali dengan Anda. Saya rasa Anda tidak menghormati orang-orang yang hadir dalam rapat iniÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â. Sang karyawan pun langsung tertunduk. Dengan wajah pucatnya, ia berkata dengan lirih ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Maaf, saya ingin menyampaikan, bahwa sebenarnya saya tidak bisa ikut rapat ini. Karena rapat ini cukup penting, saya coba hadir. Saya mengantuk, karena tida bisa tidur. Semalam anak saya kecelakaan dan sekarang sedang dirawat di ICU dalam keadaan tidak sadar. Mohon maaf kalau menguap saya tadi kurang sopan.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\n.\r\nSemua orang yang hadir langsung terperangah. Mereka terjerumus pada prasangka dan belenggu pikiran yang menganggap jika ada orang menguap di tengah rapat penting, maka orang tersebut ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“tidak antusiasÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â. Sebuah prasangka buruk telah terjadi. Semua yang ada di ruang meeting punya asumsi buruk terhadap karyawan yang menguap. Apa yang bisa kita petik dari cerita di atas? Simak dulu cerita lain yang juga ada hubungannya dengan prasangka.\r\n.\r\n.\r\nWisnu bekerja sebagai tenaga professional sebuah perusahaan ternama. Suatu hari ia ditawari untuk membeli taksi oleh seorang supir. Awal mulanya, Wisnu curiga dan berpikir ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Jangan-jangan saya akan ditipu oleh supir iniÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â. Namun, Wisnu mengambil langkah yang sangat mengagumkan. Ia setuju untuk membeli taksi tersebut, sekaligus memberi kesempatan kepada supir tadi untuk menjalankan taksi itu, dengan catatan, sang supir harus membayar uang setoran sebesar Rp 35.000/hari. Wisnu juga memberikan jatah satu hari setiap bulan sebagai hari ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“bebas setorÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â. Begitu bijak langkah yang diambil oleh Wisnu.\r\n.\r\n.\r\nSetelah itu, apa yang terjadi? Satu bulan kemudian, sang supir datang ke rumah Wisnu bersama taksinya, sambil memperlihatkan setumpuk uang sambil berkata ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Pak Wisnu, lihat. Sekarang saya sudah punya tabungan sebegini banyak. Usaha Wisnu dan sang supir berjalan dengan lancar. Sungguh pekerjaan yang betul-betul dilandasi oleh kepercayaan dan prasangka baik akan melahirkan hasil yang baik pula.\r\n.\r\n.\r\nTerlihat perbedaan kan dari cerita di atas. Yang bisa kita petik dari kedua cerita di atas adalah bahwa tindakan seseorang sangat bergantung pada pikirannya. Lingkungan pun juga sangat berperan dalam mempengaruhi cara berpikir seseorang. Prasangka buruk mengalir serta berubah menjadi sikap ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“defensifÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â dan tertutup karena selalu beranggapan buruk kepada orang lain. Sebaliknya, berprasangka baik pada orang lain akan mendorong dan menciptakan kondisi untuk saling percaya, saling mendukung, terbuka dan kooperatif.', '2017-02-07 11:20:15', 11, NULL, NULL, NULL, NULL),
	(45, 'MEMBENCI HIDUP ATAU MENIKMATINYA ?', '2017-03-01', 'https://qx.esq165.co.id/upload/sementara/img/file_58994e222029d.jpg', 'ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Setiap diri telah dikaruniai ', 'Jerry adalah seorang manager restoran di Amerika. Dia selalu punya semangat yang baik dan selalu punya hal positif untuk dikatakan. Jika seseorang bertanya kepadanya tentang apa yang sedang dia kerjakan, dia akan selalu menjawab, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Jika aku dapat yang lebih baik, aku lebih suka menjadi orang kembar!ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Banyak pelayan di restorannya keluar jika Jerry pindah kerja, sehingga mereka dapat tetap mengikuti Jerry dari satu restoran ke restoran yang lain.\r\n.\r\n.\r\nAlasan mengapa para pelayan restoran tersebut keluar mengikuti Jerry adalah karena sikapnya. Jerry adalah seorang motivator alami. Jika ada karyawan yang sedang mengalami sesuatu yang buruk, dia selalu ada di sana , memberitahu karyawan tersebut bagaimana melihat sisi positif dari situasi yang sedang dialami. Melihat gaya tersebut benar-benar membuat satu orang temannya jadi penasaran. Suatu hari, ia temui Jerry dan bertanya padanya, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Aku tidak mengerti! Tidak mungkin seseorang menjadi orang yang berpikiran positif sepanjang waktu. Bagaimana kamu dapat melakukannya?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Jerry menjawab, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Tiap pagi aku bangun dan berkata pada diriku, aku punya dua pilihan hari ini. Aku dapat memilih untuk ada di dalam suasana yang baik atau memilih dalam suasana yang jelek. Dan Aku selalu memilih dalam suasana yang baik.\r\n.\r\n.\r\nTiap kali sesuatu terjadi, aku dapat memilih untuk menjadi korban atau aku belajar dari kejadian itu. Aku selalu memilih belajar dari hal itu. Setiap ada sesorang menyampaikan keluhan, aku dapat memilih untuk menerima keluhan mereka atau aku dapat mengambil sisi positifnya.. Aku selalu memilih sisi positifnya.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Tetapi tidak selalu semudah itu,ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â protesku. ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Ya, memang begitu,ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â kata Jerry, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Hidup adalah sebuah pilihan. Saat kamu membuang seluruh masalah, setiap keadaan adalah sebuah pilihan. Kamu memilih bagaimana bereaksi terhadap semua keadaan. Kamu memilih bagaimana orang-orang di sekelilingmu terpengaruh oleh keadaanmu. Kamu memilih untuk ada dalam keadaan yang baik atau buruk. Itu adalah pilihanmu, bagaimana kamu hidup.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\n.\r\nBeberapa tahun kemudian, Jerry mengalami musibah yang tak pernah terpikirkan terjadi dalam bisnis restoran: membiarkan pintu belakang tidak terkunci pada suatu pagi dan dirampok oleh tiga orang bersenjata. Saat mencoba membuka brankas, tangannya gemetaran karena gugup dan salah memutar nomor kombinasi. Para perampok panik dan menembaknya. Untungnya, Jerry cepat ditemukan dan segera dibawa ke rumah sakit.\r\n.\r\n.\r\nSetelah menjalani operasi selama 18 jam dan seminggu perawatan intensif, Jerry dapat meninggalkan rumah sakit dengan beberapa bagian peluru masih berada di dalam tubuhnya. Teman Jerry, melihat Jerry enam bulan setelah musibah tersebut. Saat ia tanya Jerry bagaimana keadaannya, dia menjawab, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Jika aku dapat yang lebih baik, aku lebih suka menjadi orang kembar. Mau melihat bekas luka-lukaku? ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Temannya menunduk untuk melihat luka-lukanya, tetapi ia masih juga bertanya apa yang dia pikirkan saat terjadinya perampokan. ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Hal pertama yang terlintas dalam pikiranku adalah bahwa aku harus mengunci pintu belakang,ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â jawab Jerry.\r\n.\r\n.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Kemudian setelah mereka menembak dan aku tergeletak di lantai, aku ingat bahwa aku punya dua pilihan: aku dapat memilih untuk hidup atau mati. Aku memilih untuk hidup.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Apakah kamu tidak takut?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Tanya teman Jerry. Jerry melanjutkan, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Para ahli medisnya hebat. Mereka terus berkata bahwa aku akan sembuh.\r\n.\r\n.\r\nTapi saat mereka mendorongku ke ruang gawat darurat dan melihat ekspresi wajah para dokter dan suster aku jadi takut. Mata mereka berkata ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œOrang ini akan matiÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢. Aku tahu aku harus mengambil tindakan.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Apa yang kamu lakukan?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Tanya teman Jerry. ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Disana ada suster gemuk yang bertanya padaku,ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â kata Jerry. ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Dia bertanya apakah aku punya alergi. ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œYaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ jawabku..\r\n.\r\n.\r\nPara dokter dan suster berhenti bekerja dan mereka menunggu jawabanku. Aku menarik nafas dalam-dalam dan berteriak, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œPeluru!ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ Ditengah tertawa mereka aku katakan, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œ Aku memilih untuk hidup. Tolong aku dioperasi sebagai orang hidup, bukan orang matiÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Jerry dapat hidup karena keahlian para dokter, tetapi juga karena sikap hidupnya yang mengagumkan.\r\n.\r\n.\r\nApa yang bisa kita petik dari kisah jerry di atas? Kita bisa belajar dari Jerry bahwa setiap hari Anda dapat memilih apakah Anda akan menikmati hidupmu atau membencinya. Satu hal yang benar-benar memang milik Anda dan tidak bisa dikontrol oleh orang lain adalah sikap hidup Anda, sehingga jika Anda bisa mengendalikannya dan segala hal dalam hidup akan jadi lebih mudah.', '2017-02-07 11:33:57', 11, NULL, NULL, NULL, NULL),
	(46, 'KEBEBASAN TERBANG ANGSA-ANGSA', '2017-03-02', 'https://qx.esq165.co.id/upload/sementara/img/file_589951fbc3fd0.jpg', 'Kebersamaan itu lebih berharga dari Kebebasan Ego..', 'Angsa merupakan salah satu burung air terbesar yang dapat terbang. Angsa umumnya terdapat di daerah beriklim subtropis.\r\n.\r\nUntuk menghindari musim dingin, sekelompok angsa akan bermigrasi pergi ketempat yang lebih hangat. Mereka terbang dalam formasi ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œVÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢.\r\n.\r\nDengan terbang dalam formasi ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œVÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢, keseluruhan kawanan angsa itu meningkatkan efisiensi penerbangan sebesar 71%. Dibandingkan dengan hanya satu angsa terbang sendirian.\r\n.\r\nKetika leader kelelahan terbang, dia berpindah ke ujung formasi ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œVÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢, sementara angsa lain akan mengambil tempatnya.\r\n.\r\nAngsa-angsa terbang dengan formasi ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œVÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢, mereka bersorak untuk menyemangati yang di depan. Dengan cara itu, mereka mempertahankan kecepatan yang seimbang.\r\n.\r\nKetika seekor angsa menjadi SAKIT , terluka atau lelah, maka ia harus meninggalkan formasi. Tetapi akan ditemani oleh angsa lainnya hingga WAFAT atau ia mampu terbang lagi.\r\n.\r\nKemudian mereka kembali mencapai\r\nperkumpulan mereka atau mereka membuat formasi ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œVÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ yang baru.\r\n\r\nPESAN MORAL :\r\nTuhan mengajarkan manusia tentang bagaimana mencapai tujuan dan menjalani kehidupan. Kisah kebersamaan lebah, kisah kebersamaan semut.\r\n\r\nBegitu juga kisah ikan salmon dan berbagai hewan lainnya yang mengajarkan arti pentingnya kebersamaan dalam menjalani misi kehidupan selalu diajarkan. Termasuk juga kisah angsa-angsa ini.\r\n\r\nMelalui team work dan kesamaan tujuan akan tercipta sinergi yang kuat. Inilah yang disebut JAMA\'AH.\r\n\r\nDi jepang anak-anak kecil diminta mematahkan satu ikat anak panah. Hasilnya ? Tentu tidak bisa karena ia satu ikatan yg kuat. Lalu dipisahkan satu anak panah dan patahkan.... Hasilnya : " kraak !" Langsung patah !\r\n.\r\nAkan tetapi kadang kala, godaan sangat kuat atas nama kebebasan dan kemajuan.. Melihat langit yg luas untuk dikuasai.. melihat laut yg luas sumber rezeki yg menarik hati..\r\n\r\nMaka.. mungkin akan ada anggota angsa yang tergoda ingin terbang sendiri.. karena menyangka ia merasa ia akan dapat makan lebih banyak ikan dibawah sana.. ia lupa tujuan mereka ke mana.. merasa ikan layak untuk yang bekerja.. merasa tidak perlu berbagi lagi.. lupa visi misi... tanpa sadar, egoisme dan kesombongan mulai mendominasi.. mulai merasa diri lebih hebat...merasa diri lebih mampu...dan merasa diri bisa terbang lebih jauh ..dan merasa bisa terbang lebih tinggi...\r\nINILAH PENYAKIT MERASA!\r\nYang sesungguhnya.. bukan kebebasan yg mereka dapatkan.. hanyalah sebuah penjara ego..\r\n.\r\nHingga jadi lupa... bahwa dirinya tidak selamanya akan sehat... ketika terkena musibah dan jatuh sakit ..disanalah baru terasa bahwa jama\'ah atau kebersamaan itu lebih berharga dari kebebasan ego..', '2017-02-07 11:51:40', 11, NULL, NULL, NULL, NULL),
	(47, '5 TINGKAT YANG HARUS DILALUI OLEH SEORANG PEMIMPIN BERPENGARUH', '2017-03-03', 'https://qx.esq165.co.id/upload/sementara/img/file_58996ca4216cd.jpg', 'Seorang Pemimpin, bagaimana pun gaya kepemimpinannya, akan berperilaku menurut prinsip yang dianutnya.', 'Di sekitar kita, banyak sekali contoh pemimpin dengan tipikal, gaya, dan prinsip yang berbeda-beda. Seorang pemimpin, bagaimana pun gaya kepemimpinannya, akan berperilaku menurut prinsip yang dianutnya.\r\n.\r\n.\r\nAda pemimpin yang menonjol prestasi kerja dan integritasnya, tetapi tidak dicintai oleh lingkungannya. Contoh: Seorang manajer baru bernama Kusuma dipercaya memegang sebuah posisi penting. Namun, Kusuma kurang disukai bawahannya, meski ia tergolong rajin dan pandai. Ia dianggap kurang mampu membina hubungan baik dengan orang lain, cenderung kaku, kurang ramah, dan tidak peka.\r\n.\r\n.\r\nSebaliknya, ada seorang pemimpin perusahaan yang sangat ramah, peka, baik hati, serta pandai bergaul, tetapi lamban dan kurang disiplin. Akibatnya, para bawahan tidak memiliki semangat juang, meski sang pemimpin tersebut adalah orang yang menyenangkan. Akhirnya, kinerja perusahaan turun.\r\n.\r\n.\r\nAda lagi pemimpin yang berprestasi, kinerjanya menonjol serta pandai bergaul, namun ia sangat sibuk dengan pekerjaannya sendiri. Karena ia tak pernah membimbing bawahannya. Ia pun kurang memberi kepercayaan kepada orang lain. Akibatnya, pada saat pekerjaan telah tinggi menumpuk, ia merasa sangat tertekan karena target waktu yang ditetapkan tak mampu dicapai. Kinerjanya pun akhirnya anjlok.\r\n.\r\n.\r\nBambang adalah seorang pemimpin sebuah perusahaan otomotif. Ia sangat senang mengajari serta membimbing orang lain, khususnya para bawahannya sendiri. Malangnya, jarang ada yang mau mengikuti kata-katanya karena ia sendiri jarang mengerjakan pekerjaannya secara sungguh-sungguh. Bambang lebih senang membimbing tanpa memberi contoh/teladan.\r\n.\r\n.\r\nAda pula Respati, seorang pemimpin usaha properti yang disegani dan dicintai. Kerjanya sungguh-sungguh dan suka membimbing para karyawannya. Namun, tidak punya kader penerus.\r\n.\r\n.\r\nBerdasarkan kondisi-kondisi di atas, Pak Ary telah merangkum dan membuat lima tangga kepemimpinan. Setiap tingkatan pada tangga tersebut harus dilalui dengan benar dan tidak boleh ada satu anak tangga pun yang terlewat, atau diloncati.\r\n.\r\n.\r\nDengan melalui kelima anak tangga tersebut, diharapkan semua permasalahan seperti contoh tadi, mampu diantisipasi. Selain itu, urutan tangga tersebut mampu menghasilkan seorang pemimpin yang tidak hanya dicintai, dipercaya, atau diikuti, namun juga membimbing dan memiliki kader. Ia akan memiliki pengaruh besar yang sangat kuat dalam jangka panjang. Tangga kepemimpinan tersebut dibagi menjadi 5 tingkatan sebagai berikut.\r\n.\r\n.\r\nPemimpin Tingkat 1: Pemimpin yang Dicintai\r\nPemimpin Tingkat 2: Pemimpin yang Dipercaya\r\nPemimpin Tingkat 3: Pembimbing\r\nPemimpin Tingkat 4: Pemimpin yang Berkepribadian\r\nPemimpin Tingkat 5: Pemimpin yang Abadi', '2017-02-07 13:45:27', 11, NULL, NULL, NULL, NULL),
	(48, 'Karena Anak-anak Percaya Apa pun yang Orangtua katakan', '2017-03-04', 'https://qx.esq165.co.id/upload/sementara/img/file_58996f1d26a09.jpg', 'Anak-anak mendengar, menyerap dan percaya apa pun yang dikatakan orangtuanya. Maka berhati-hatilah.', 'Pada hakikatnya, manusia adalah makhluk pembelajar. Lihat bagaimana bayi belajar, selalu penuh antusias. Ia belajar dengan seluruh indranya. Saat ia melihat benda baru, ia pelajari dengan sungguh-sungguh, dipukul-pukul untuk mendengarkan suaranya, ia cium baunya, bahkan ia jilat untuk mengetahui rasanya, tak peduli apa pun jenis benda itu.\r\n.\r\nYang perlu Bunda, Ayah dan para calon orangtua ketahui, proses pendidikan harusnya menjadi hal yang membahagiakan baik bagi setiap anak maupun guru atau orangtua. Karena anak-anak sesungguhnya menyukai belajar.\r\n.\r\nNamun seringkali terjadi, kegiatan belajar mengajar menjadi hal yang tidak menyenangkan bahkabn kadang menjadi pemicu stress. Padahal, riset membuktikan bahwa dalam keadaan stress, otak tidak dapat bekerja optimal.\r\n.\r\nBanyak orang tua yang menganggap tempat belajar adalah di sekolah. Saat bel berbunyi itulah tanda dimulainya pelajaran. Namun, belajar yang sesungguhnya dilakukan selama detik-detik kehidupan anak. Anak belajar dari apa pun yang ia lihat, dengar, raba, cium dan rasakan.\r\n.\r\nAnak-anak belajar terutama dari kedua orangtuanya. Jennifer Day dalam buku Children Believe Everything You Say mengatakan bahwa anak-anak mendengar, menyerap dan percaya apa pun yang dikatakan orangtuanya. Maka berhati-hatilah.\r\n.\r\nKisah-kisah berikut ini memberi inspirasi tentang belajar pentingnya memaknai apapun yang kita ucapkan pada anak-anak, karena lewat kata-kata orangtua, anak-anak membangun pengertian, memahami dirinya dan menambah pengetahuan.', '2017-02-07 13:54:31', 11, NULL, NULL, NULL, NULL),
	(49, 'IQ VS EQ? MANA YANG AKAN MENANG?', '2017-03-05', 'https://qx.esq165.co.id/upload/sementara/img/file_5899774ae57df.jpg', 'Keberadaan EQ memang mutlak diperlukan untuk mencapai prestasi tinggi. Bukan hanya sekedar kemampuan intelektual (IQ).', 'Saya pernah membangun sebuah perusahaan yang bergerak di bidang telekomunikasi dan perdagangan. Khususnya bidang usaha pemasaran radio panggil dan telepon genggam GSM. Saya merekrut orang-orang yang lulus dari tempat saya mengajar. Usia mereka pada saat itu memang relatif muda. Saya pun senang memilih karyawan yang usianya lebih muda. Ya paling tidak maksimal seusia dengan saya. Pasti, Anda begitu juga kan?\r\n.\r\n.\r\nPada saat saya memimpin perusahaan tersebut, saya pun banyak mengalami permasalahan. Anda tahu apa? Kurangnya komitmen, semangat, kreativitas dan konsistensi dari karyawan yang membuat permasalahan terus ada. Bagi saya pribadi, sangat mudah untuk mengajari mereka cara bekerja sesuai dengan jobdesk masing-masing. Tetapi tahukah Anda? Yang paling sulit itu bukan mengajarkan mereka tentang cara kerja, namun mengajarkan bagaimana mereka memiliki kecerdasan emosi (EQ) serta memberi pemahaman kepada mereka bahwa keberadaan EQ amatlah penting bagi kehidupan mereka pada masaa yang akan datang. Bagaimana? Anda setuju dengan statement yang saya berikan mengenai EQ?\r\n.\r\n.\r\nPermasalahan yang saya hadapi tak hanya itu saja. Pada divisi pemasaran, permasalahan lebih terasa. Para karyawan saya yang berada di divisi pemasaran diharapkan mampu mencapai target yang telah ditentukan oleh bagian keuangan agar bisa menutupi biaya, namun kemampuan mereka sangat terbatas. NahhÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦Anda yang bekerja pada divisi marketing sepertinya pernah merasakan fakta yang saya ceritakan barusan kan???\r\n.\r\n.\r\nBanyak karyawan saya yang pada saat itu tidak memiliki kepercayaan diri. Apalagi semangat dalam bekerja. Kepercayaan diri, semangat dan cita-cita seakan mereka kubur dalam-dalam, takut melihat kenyataan di lapangan yang sangat keras. Sulit cari kerja, persaingan dunia usaha dan beratnya persaingan antar pencari kerja menjadi sebuah persepsi yang mereka pikirkan pada saat itu.\r\n.\r\n.\r\nDisitu saya merasa bingung. Ingin ganti tenaga yang lebih professional, tapi terkendala biaya. Lalu saya berpikir ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Kapan mereka bisa diberi kesempatan untuk bekerja?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â. Setelah berpikir ulang, akhirnya saya mengambil tindakan dramatis untuk perusahaan saya. Ingin tahu? Lanjutkan menyimak cerita ini.\r\n.\r\n.\r\nSetiap pagi selama 30 menit sebelum memulai pekerjaan, saya adakan training untuk karyawan. Saya namakan ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“morning briefingÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â. Materi yang saya berikan bukanlah hal teknis, tetapi tentang komitmen, integritas, berpikir merdeka dan kreatif, visi, arti kerja kera dan daya tahan. Apa yang terjadi setelah saya melakukan morning briefing??? Sangat mengejutkan. Perusahaan kami merebut juara satu pemasaran GSM salah satu provider telepon di wilayah Bali selama dua tahun berturut-turut. Perusahaan kami pun mampu mengalahkan banyak perusahaan raksasa yang bermodal kuat.\r\n.\r\n.\r\nBanyak pelajaran yang saya petik selama saya memimpin perusahaan tersebut. Saya menyimpulkan, bahwa keberadaan EQ memang mutlak diperlukan untuk mencapai prestasi tinggi. Bukan hanya sekedar kemampuan intelektual (IQ), namun kecerdasan emosi sangat berperan besar dalam menyukseskan seseorang. Lalu, manakah yang akan menang, IQ atau EQ??? Siapa yang bisa jawab???\r\n.\r\n.\r\nYaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦Anda benar. Tak ada yang menang diantara keduanya. Mereka saling bersinergi satu sama lain membentuk sebuah kolaborasi yang sangat mengagumkan untuk diri Anda.\r\n.\r\n.\r\nNamun, apakah IQ dan EQ saja cukup???\r\nMasih ada 1 poin lagi yang akan menyempurnakann itu semua. Yaitu SQ (Kecerdasan Spiritual).', '2017-02-07 14:29:55', 11, NULL, NULL, NULL, NULL),
	(50, 'SINERGI ANTARA IQ, EQ dan SQ', '2017-03-06', 'https://qx.esq165.co.id/upload/sementara/img/file_589978b11af66.jpg', 'Sebuah penggabungan antara rasionalitas dunia (EQ Dan IQ) serta kepentingan spiritual (SQ).\r\nHasilnya adalah kebahagiaan dan kedamaian pada Jiwa.', 'Erwyn bekerja di sebuah perusahaan otomotif sebagai seorang buruh. Tugasnya memasang dan mengencangkan baut pada jok pengemudi. Itulah tugas rutin yang sudah dikerjakannya selama hamper sepuluh tahun. Karena pendidikannya hanya setingkat SMP, sulit baginya untuk meraih posisi puncak.\r\n.\r\n.\r\nSaya bertanya pada Erwyn, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Bukankah itu suatu pekerjaan yang sangat membosankan?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Ia menjawab dengan tersenyum, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Tidakkah ini pekerjaan mulia, saya telah menyelamatkan ribuan orang yang mengemudikan mobil-mobil ini? Saya mengencang-kuatkan seluruh kursi pengemudi yang mereka duduki sehingga mereka sekeluarga selamatm termasuk kursi mobil yang Anda duduki itu.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\n.\r\nEsok harinya, saya mendatangi Erwryn lagi, kemudian bertanya ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Mengapa Anda bekerja begitu giat, upah Anda tidak besar kan? Mengapa Anda tidak melakukan mogok kerja seperti buruh yang lain untuk menuntut kenaikan upah? Erwyn memandangi 5, sambil tersenyum dan menjawab, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Saya memang senang dengan kenaikan upah seperti teman-teman yang lain, tapi saya pun memahami bahwa keadaan ekonomi memang sedang sulit dan perusahaan pun terkena imbasnya. Saya memahami keadaan pimpinan perusahaan yang juga tentu sedang dalam kesulitan, bahkan terancam pemotongan gaji seperti saya. Jadi, kalau saya mogok kerja, itu hanya akan memperberat masalah mereka, masalah saya juga. Kemudian Erwyn lanjut berkata\r\n\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Saya bekerja, karena prinsip saya adalah memberi, bukan untuk perusahaan, namun lebih kepada pengabdian saya kepada Tuhan.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\n.\r\nErwyn mampu memaknai pekerjaanya sebagai pengabdian kepada Tuhan dan demi kepentingan umat manusia yang dicintainya. Erwyn berpikir secara logis dengan memahami kondisi perusahaan secara keseluruhan, situasi ekonomi dan masalah atasannya. Erwyn berprinsip dari dalam, bukan dari luar, ia tidak terpengaruh oleh lingkungannya.\r\n.\r\n.\r\nBisa Anda liat, sebuah penggabungan atau sinergi antara rasionalitas dunia (EQ Dan IQ) serta kepentingan spiritual (SQ).\r\nHasilnya adalah kebahagiaan dan kedamaian pada jiwa Erwyn, sekaligus etos kerja yang tinggi dan tak terbatas.', '2017-02-07 14:36:56', 11, NULL, NULL, NULL, NULL),
	(51, '"APA PERBEDAAN ANTARA HAMBATAN DAN KESEMPATAN?"', '2017-03-07', 'https://qx.esq165.co.id/upload/sementara/img/file_58997babb4581.jpg', 'Ada kesulitan dalam sebuah kesempatan, dan Selalu ada Kesempatan dalam setiap kesulitan.', 'Perbedaannya terletak pada cara dan sikap kita dalam memandangnya.Akan ada kesulitan dalam sebuah kesempatan, dan Selalu ada Kesempatan dalam setiap kesulitan.\r\n.\r\n.\r\nTergantung bagaimana Anda memandangnya suatu keadaan maka akan menentukan apakah Anda sukses atau tidak.\r\n.\r\n.\r\nSalah satu contohnya adalah, apabila kita jalan-jalan bersama keluarga menuju puncak misalnya, namun Anda melakukan perjalanan di hari libur, maka tentunya di di ujung tol jagorawi terjadi kemacetan, sehingga jalanan padat merayap.\r\n.\r\n.\r\nApabila kita tidak berpikir positif dan menemukan kesempatan didalam kemacetan tersebut, tentu kita akan marah-marah dan merasa jengkel dengan kemacetan tersebut.\r\n.\r\n.\r\nMungkin saja Anda akan marah-marah dan menyalahkan siapapun, mulai menyalahkan pengatur lalu lintas, menyalahkan ruas jalan, menyalahkan jalan tol, malah mungkin menyalahkan pemerintah. namun kita terkadang jarang menginstrospeksi diri sendiri.\r\n.\r\n.\r\nNamun apabila Anda mampu mengambil kesempatan di dalam kemacetan itu, Anda akan mulai membuka pembicaraan yang terbuka dengan keluarga Anda ketika menunggu kemacetan, tertawa bersama dan saling bercerita apa saja yang telah di lakukan selama kegiatan anak-anak kita selama seminggu lalu.\r\n.\r\n.\r\nPengalaman baik apa yang mereka rasakan ketika seminggu lalu, kesan apa yang tertanam yang mereka dapatkan, rencana perbaikan apa yang akan mereka lakukan di minggu mendatang, dan Anda bisa bertanya apa yang bisa Anda lakukan untuk mendukung cita-cita dan harapan anak-anak kita.\r\n.\r\n.\r\nKarena mungkin kesempatan itu tidak Anda dapatkan ketika Anda dirumah, karena kesibukan Anda bekerja dan memberikan nafkah buat keluarga.', '2017-02-07 14:50:53', 11, NULL, NULL, NULL, NULL),
	(52, 'BAGAIMANA MENJUAL SISIR KEPADA ORANG YANG BOTAK', '2017-03-08', 'https://qx.esq165.co.id/upload/sementara/img/file_58997d2e5ad2e.jpg', 'Kita tidak bisa mengatur situasi seperti yang kita kehendaki.\r\nTapi, kita bisa mengerahkan segenap potensi kita untuk mencari solusi.', 'BAGAIMANA MENJUAL SISIR KEPADA ORANG YANG BOTAK\r\n.\r\nSebuah perusahaan membuat tes terhadap tiga calon staf penjual barunya. Tesnya unik, yaitu: Menjual sisir di komplek Biara Shaolin!. Tentu saja, ini cukup unik karena para biksu di sana semuanya gundul dan tak butuh sisir.\r\n.\r\n.\r\nKesulitan ini juga yang membuat calon pertama hanya mampu menjual satu sisir. Itupun karena belas kasihan seorang biksu yang iba melihatnya.\r\n.\r\n.\r\nTapi, tidak dengan calon kedua. Ia berhasil menjual 10 sisir, ia tidak menawarkan kepada para biksu, tetapi kepada para turis yang ada di komplek itu, mengingat angin di sana memang besar sehingga sering membuat rambut jadi awut-awutan.\r\n.\r\n.\r\nLalu bagaimana dengan calon ketiga? Ia berhasil menjual 500 sisir..!!\r\n.\r\n.\r\nCaranya? Ia menemui kepala biara. Ia lalu meyakinkan jika sisir ini bisa jadi souvenir bagus untuk komplek biara tersebut. Kepala biara bisa membubuhkan tanda tangan di atas sisir-sisir tersebut dan menjadikannya souvenir para turis. Sang kepala biara pun setuju.\r\n.\r\n.\r\nSahabatku yang baik\r\nApa yang sering kita anggap sebagai penghambat terbesar dalam usaha atau karier?\r\nBukankah kita sering kali menyalahkan keadaan?\r\nDan inilah yang membuat calon pertama gagal.\r\nSementara calon kedua, sudah berpikir lebih maju.\r\nNamun ia masih terpaku pada fungsi sisir yang hanya sebagai alat merapikan rambut.\r\n.\r\n.\r\nTapi calon ketiga sudah berani berfikir di luar kotak ( THINKING OUT OF THE BOX ), berfikir diluar kelaziman. Dia bukan hanya berani berpikir bahwa sisir bukan hanya alat merapikan rambut, melainkan bisa menjadi souvenir.\r\n.\r\n.\r\nKita tidak bisa mengatur situasi seperti yang kita kehendaki.\r\nTapi, kita bisa mengerahkan segenap potensi kita untuk mencari solusi.\r\n.\r\n.\r\nSegenap potensi bukan hanya terbatas otot atau kerja keras, tapi juga pikiran, ilmu, intuisi dan kerja cerdas.\r\nPendek kata, kreatifitas akal, ketekunan dan kesabaran .\r\nItulah potensi dalam diri kita yang dapat dipergunakan.', '2017-02-07 14:54:36', 11, '2017-03-10 09:52:59', 11, NULL, NULL),
	(53, 'BAGAIMANA MENGAJARKAN ANAK MANDIRI SEJAK DINI??', '2017-03-09', 'https://qx.esq165.co.id/upload/sementara/img/file_589992d4ebf8f.jpg', 'Kemandirian mulai muncul dan berkembang jauh sebelum mencapai tahap dewasa. Namun seringkali, lingkunganlah yang kurang mendukung perkembangan kemandirian anak.', 'BAGAIMANA MENGAJARKAN ANAK MANDIRI SEJAK DINI??\r\n.\r\nAda seorang anak yang sulit untuk belajar saat menjelang ujian, dengan sangat terpaksa sang ibu selalu membacakan buku pelajaran. Sedangkan si anak hanya berbaring dan mendengarkan pelajaran yang dibacakan.\r\n.\r\nKemudian si ibu membuat pertanyaan-pertanyaan dan si anak diminta menjawabnya. Ibu tersebut mengaku melakukan hal tersebut karena ia begitu khawatir dengan hasil ujian anaknya.\r\n.\r\nHal itu terus terulang hingha si anak hampir menyelesaikan pendidikannya di sekolah dasar. Ia tidak belajar jika tidak dibacakan atau diajarkan oleh sang ibu. Nilai si anak memang bagus, namun si ibulah yang harus selalu aktif mendorong dan mengajari anaknya.\r\n.\r\nApakah Bunda dan Ayah juga pernah berada di situasi semacam ini?\r\n.\r\nKasus seperti ini banyak dialami oleh orangtua masa kini. Kekhawatiran akan nilai-nilai akademis anak, namun melupakan pembangunan potensi lainnya, seperti kemandirian yang berada pada ranah afeksi. Kesuksesan anak bukan saj memahami isi materi pelajaran namun juga bagaimana cara anak mendapatkan materi tersebut.\r\n.\r\nKarena itu, ada istilah yang berkaitan dengan kemandirian belajar atau biasa disebut Self-Direction on Learning (SDL). SDL menggambarkan seseorang yang mengarahkan dan memusatkan diri pada keinginannya belajar sendiri serta bertanggung jawab dalam kegiatan dan proses belajarnya.\r\n.\r\nTak hanya dalam hal belajar, dalam kegiatan sehari-hari pun banyak orangtua yang sangat terampil membantu anaknya dalam melakukan banyak hal. Kemandirian sesungguhnya akan mendukung anak dalam belajar dan meraih kesuksesn masa depan.\r\n.\r\nKemandirian sudah mulai muncul dan berkembang jauh sebelum mencapai tahap dewasa. Namun seringkali, lingkunganlah yang kurang mendukung perkembangan kemandirian anak.', '2017-02-07 16:29:06', 11, '2017-03-10 09:52:01', 11, NULL, NULL),
	(54, 'KEMERDEKAAN BERPIKIR', '2017-03-10', 'https://qx.esq165.co.id/upload/sementara/img/file_5899953f62ca2.jpg', 'Kejernihan pikiran akan mampu mengeluarkan kita dari belenggu-belenggu yang senantiasa menghalang kita menuju sukses.', 'KEMERDEKAAN BERPIKIR\r\n.\r\nApa kabar Sahabat ESQ di seluruh penjuru tanah air?\r\nSemoga kita semua selalu dalam keadaan sehat dan bahagia ya.\r\n.\r\nSiapa yang mau sehat dan bahagia????? Tentunya kita semua mau. Apa yang sudah anda lakukan untuk mencapai itu semua? Sudahkan kita bekerja keras untuk itu?\r\n.\r\nSudahkan resolusi yang kita tuliskan di pergantian tahun kemarin dilaksanakan sesuai rencana? Atau resolusi itu masih belum berjalan sesuai dengan rencana yang ada buat?\r\n.\r\nApakah usaha keras kita belum membuahkan hasil? Apakah cara kerja kita untuk mencapai itu semua salah sehingga kita belum juga sampai di tempat tujuan? Apakah kita kurang berpikir lebih luas lagi sehingga mentok dengan cara-cara yang ada?\r\n.\r\nLalu, apa yang harus kita lakukan?\r\n.\r\nSaya akan menceritakan sebuah kisah yag sangat inspiratif. Bagaimana kemerdekaan, keluasan dalam berpikir serta kejernihan hati mampu melahirkan ide-ide cemerlang (out of the box) yang bisa membawanya pada tingkat kesuksesan.\r\n.\r\nBisnis air mineral atau air putih kemasan gelas atau botol plastik merebak dahsyat. Kini, sudah ada ratusan perusahaan yang bergerak di bidang tersebut. Pelopornya adalah merk A, yang diikuti ratusan merek lain dengan berbagai kemasan, dan isinya hanya air. Sebelum merk A diluncurkan, orang tak pernah menyangka bahwa air yang dikemas dalam botol plastik akan menjadi bisnis raksasa. Kala itu, banyak orang tak mampu melihat peluang tersebut.\r\n.\r\nMengapa? Mereka, termasuk juga kita pada saat itu, sudah terbiasa minum air putih dalam gelas, bukan botol. Pikiran kita telah terbelenggu oleh tradisi minum air di gelas beling atau kaca, bukan plastik. Tanpa disadari, pikiran kita tidak lagi merdeka, tetapi terbelenggu tradisi. Walau air putih selalu kita lihat sehari-hari, kita tak mampu melihat peluang bahwa orang sering kali membutuhkan air pelepas dahaga tersebut di tengah perjalanan. Kemerdekaan dan keluasan berpikir dengan menyucikan pikiran, akan selalu menghasilkan sesuatu yang baru, hal-hal yang ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“out of the box".\r\n.\r\nKejernihan pikiran akan mampu mengeluarkan kita dari belenggu-belenggu yang senantiasa menghalang kita menuju sukses. Di sini, kita sama-sama belajar, untuk terus memperbaiki diri. Sampai pada akhirnya, perbaikan diri tersebut bisa menjadi bekal kita menghadap Sang Maha Agung.', '2017-02-07 16:38:33', 11, '2017-03-10 09:48:49', 11, NULL, NULL),
	(55, 'MEMBANGUN VISI', '2017-03-11', 'https://qx.esq165.co.id/upload/sementara/img/file_58999704ccbd8.jpg', 'Impian dapat mengobarkan api semangat yang tak mampu dipadamkan orang lain.', 'Apakah Sahabat ESQ tau?? \r\nThomas A. Edison adalah penjual koran di kereta api. John D. Rockefeller dulu hanya diupah US$ 6/minggu. Julius Caesar menderita penyakit ayan. Napoleon punya orangtua dari kelas bawah, dan menduduki peringkat 46 dari 65 siswa di akademi militer. Beethoven seorang yang tuli. Plato berpunggung bungkuk, dan Stephen Hawkings menderita lumpuh.\r\n.\r\nApa yang memberi orang-orang besar itu kekuatan untuk mengatasi kekurangan mereka dan membuat mereka menjadi orang-orang sukses?\r\n.\r\nCoba tebak kira-kira apa jawabannyaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\nYa!\r\n.\r\nJawabannya adalah IMPIAN. Impian dapat mengobarkan api semangat yang tak mampu dipadamkan orang lain. Napoleon Hillberkata, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Peliharalah visi dan impian Anda karena mereka anakanak jiwa Anda dan rancangan pencapaian terbesar Anda.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Hubert H Humpreypun berkata, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Apa yang Anda visualisasikan adalah apa yang mampu Anda raih.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Konrad Adenauerbenar ketika ia berkata, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Kita semua hidup di bawah langit yang sama, tetapi tidak semua orang punya cakrawala yang sama.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â \r\nRoger Dawson berpendapat, para ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“PERAIH SUKSESÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â mengetahui bahwa untuk mengubah kehidupan mereka, diri merekalah yang harus berubah terlebih dulu. Mereka harus mengembangkan sikap hidup positif, menentukan tujuan yang akan mengarahkan hidup mereka menjadi lebih baik, dan menguatkan keyakinan dalam diri mereka sendiri bahwa mereka akan berhasil.\r\n.\r\nPenetapan misi adalah suatu langkah awal yang terbukti kebenarannya secara ilmiah sebagai langkah pembangunan wawasan tentang tujuan akhir (visi).  Seorang manusia biasa yang pernah hidup di bumi sebagai seseorang yang yakin akan Tuhan dan berbuat banyak kebaikan dengan prestasi yang luar biasa.\r\n.\r\nSelamat berjuang dalam mencapai Visi dan Misi hidup Sahabat ESQ. Semoga senantiasa dipermudah jalan kita menuju kebaikan.', '2017-02-07 16:45:04', 11, NULL, NULL, NULL, NULL),
	(56, 'Belajar Ilmu Ketentraman Hati dari Abdi Dalem Keraton Yogyakarta', '2017-03-12', 'https://qx.esq165.co.id/upload/sementara/img/file_58999911264e8.jpg', '"Sabar dan Ikhlas" \r\nDua landasan mencapai ketentraman', 'Cukup banyak orang yang penasaran kenapa ada yang mau menjadi seorang abdi dalem. Bahkan peminatnya tergolong banyak, terutama warga Yogyakarta itu sendiri. Padahal, gaji abdi dalem itu sangat kecil dan banyak sekali aturan yang harus diikuti.\r\n.\r\n"Yang saya cari itu bukan gaji, melainkan ketentraman hidup," ungkap Joyo, Abdi dalem Keraton yang dilansir dari Merdeka.com.\r\n.\r\nSemua abdi dalem Keraton Yogyakarta akan mengatakan hal serupa, bila ditanya tentang pekerjaannya. Jika tidak percaya, Anda bisa membuktikannya sendiri.\r\n.\r\nApa yang membuat mereka merasakan ketentraman hati?\r\n.\r\nAlasannya ingin mengabdi ke Sultan dan Keraton. Caranya dengan menghayati diri, pekerjaan, dan kehidupan.\r\n.\r\n"Kalau pengabdian itu dihayati, batin menjadi tentram," terangnya.\r\n.\r\nHal serupa juga berlaku dalam kehidupan kita. Jika kita mengabdi kepada Sang Pencipta dan menghayati setiap detailnya, niscaya ketentraman akan diperoleh.\r\n- Masalah berubah jadi tantangan\r\n- Kekurangan jadi kekuatan\r\n- Kesedihan jadi kesempatan\r\n- Hingga menikmati kebahagiaan dan kesuksesan dengan tidak berlebihan.\r\n.\r\n"Hidup itu ada yang mengatur. Jika kita menghayatinya dan mengenal-Nya, maka akan bertemu dengan sabar dan ikhlas. Dua landasan mencapai ketentraman," pungkasnya.', '2017-02-07 16:53:46', 11, '2017-02-08 10:34:05', 11, NULL, NULL),
	(57, 'Kebahagiaan yang HAKIKI itu DISADARI bukan DICARI', '2017-03-13', 'https://qx.esq165.co.id/upload/sementara/img/file_589999f6a1cfc.jpg', 'keindahan itu bukan dicari, tapi disadari.\r\nBegitu juga dengan kebahagiaan yang tak pernah jauh dari Anda dan bersembunyi di dalam diri Anda.', 'Di Mana Kebahagiaan Itu? Sudahkah Anda menemukannya?\r\n.\r\nSeorang anak yang penuh rasa ingin tahu pernah bertanya, "Di manakah tempat terindah di dunia menurut Ayah?"\r\n.\r\n"Hmm, dirumah. Di rumah Ayah bisa bersama orang-orang yang disayangi dan Ayah suka setiap sudut di rumah," jawab Sang Ayah sambil tersenyum.\r\n.\r\n"Bukan Ayah. Maksudku tempat di luar sana selain rumah. Di mana tempat terindah?," tanya si anak lagi.\r\n.\r\nAyah itu mencoba menjelaskan bahwa bukan perkara di mana tempat yang terindah. Tetapi tentang hati yang indah dan penuh syukur. Hati itu adalah sumber yang membuat semua tempat tampak indah.\r\n.\r\nSang Ayah memberi tahu jika keindahan itu bukan dicari, tapi disadari.\r\n.\r\nBegitu juga dengan kebahagiaan yang tak pernah jauh dari Anda dan bersembunyi di dalam diri Anda.\r\n.\r\nAnda tak akan pernah menemukan kebahagiaan jika mencari di luar, karena memang tidak ada. Satu-satunya cara menemukan kebahagiaan ialah dengan menyadari dan fokuslah ke dalam diri sendiri.', '2017-02-07 16:59:46', 11, NULL, NULL, NULL, NULL),
	(58, 'MENINGKATKAN POTENSI DIRI MELALUI SUARA HATI', '2017-03-14', 'https://qx.esq165.co.id/upload/sementara/img/file_58999b1b3c1d0.jpg', 'Suara Hati merupakan representasi berkembangnya Kecerdasan emosi dan spiritual seseorang.', 'Sahabat ESQ, kami yakin Anda sudah mengetahui apa itu suara hati.\r\n.\r\nYa..Suara hati manusia atau yang disebut dengan inner voice. Suara hati letaknya lebih tinggi dibanding emosi. Suara hati manusia yang aktif bisa membantu dalam mengendalikan emosi seseorang.\r\n.\r\nSama halnya dalam berbicara, suara hati bisa membantu kita mensensor kata-kata, agar kita bisa berbicara lebih baik. Saat bekerja pun, suara hati membantu kita untuk selalu mawas diri, patuh peraturan, meningkatkan kualitas dari pekerjaan yang kita lakukan, dan membantu pemecahan masalah serta pengambilan keputusan, agar berjalan dengan lebih matang dan tepat sasaran.\r\n.\r\nDalam keseharian, Suara Hati merupakan representasi berkembangnya Kecerdasan emosi dan spiritual seseorang (ESQ)-nya. Namun, Suara Hati pun bisa rusak oleh berbagai sebab. Diantaranya saat ada belenggu ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œ belenggu yang mengganggu fungsi kerja dan fungsi berpikir kita. Untuk itu, diperlukan pembersihan diri dari 7 belenggu mental tersebut, dan diperlukan aktivasi suara hati fitrah, agar seluruh fungsi yang kita miliki bisa berkembang.\r\n.\r\n.\r\nUntuk mengembangkan kemampuan suara hati yang baik, diperlukan latihan mental.', '2017-02-07 17:04:11', 11, NULL, NULL, NULL, NULL),
	(59, 'KISAH TERBAIK, BAHAGIA ITU SEDERHANA', '2017-03-15', 'https://qx.esq165.co.id/upload/sementara/img/file_58999c0a8c9dd.jpg', 'Bahagia itu bukan saat kita memiliki segalanya, melainkan saat kita bisa mengasihi / memberi apa yang kita miliki untuk orang lain.', 'Seorang Boss pemilik Bank yang kaya raya, suatu kali diajak oleh temannya ke Panti Asuhan.\r\n.\r\nNamun setelah acara selesai, hati sang Bankir bergumam:\r\n"Kamu bohong, katanya kalau aku main kesini, hati pasti bahagia...."\r\n.\r\nDengan langkah lesu, ia pun kembali ke mobilnya, tetapi tiba-tiba ada seorang anak perempuan kecil menghampirinya:\r\n.\r\n"Om mau pulang ya, Om boleh tidak Ana minta sesuatu?"\r\n.\r\nSang Bankir tersenyum, ia seorang kaya, apa yg tidak bisa dibelinya, apalagi hanya untuk anak kecil ini.\r\n.\r\n"Memangnya kamu mau minta apa?"\r\n"Om, Ana pengen manggil Ayah ke Om. Boleh kan?"\r\n.\r\nSang Bankir kaget, tenggorokannya terasa tersumbat, ternyata bukan boneka atau uang yg diminta, hanya sebutan AYAH.\r\n.\r\nTanpa terasa hatinya luluh dan dengan suara bergetar:\r\n.\r\n"Boleh, Ana boleh panggil Ayah ke Om"\r\n.\r\n"Terima kasih Ayah, kapan Ayah datang lagi?\r\nAna boleh minta satu lagi ke Ayah ??"\r\n.\r\n"Nanti Ayah atur waktu lagi.\r\nBoleh Ana mau minta apa?"\r\n.\r\n"Ana minta, kalo Ayah datang lagi kesini, bawa foto Ayah ya, Ana mau simpan di kamar, jadi kalo Ana kangen sama Ayah, Ana bisa liat foto Ayah"\r\n.\r\nDengan berlinang air mata, Bankir itupun segera memeluk Ana,\r\n"Besok Ayah datang lagi bawa foto, dan Ayah akan sering kesini untuk ketemu Ana"\r\n.\r\nHati Sang Bankir sangat bahagia, ya ia bahagia sekarang...\r\n.\r\nTernyata Bahagia itu bukan saat kita memiliki segalanya, melainkan saat kita bisa mengasihi/memberi apa yang kita miliki untuk orang lain, meski itu hanya sebuah ungkapan Kasih sayang.\r\n.\r\nKelebihan bukan digunakan untuk menyakiti orang lain karena kekayaan hanya bersifat sementara, kebahagiaan tidak bisa dibeli dgn uang.\r\n.\r\nApa pun juga yang kita perbuat, perbuatlah dengan segenap hati seperti untuk Tuhan dan bukan untuk manusia.', '2017-02-07 17:06:19', 11, NULL, NULL, NULL, NULL),
	(60, '5 PRINSIP YANG HARUS DI MILIKI OLEH SEORANG PEMIMPIN', '2017-03-16', 'https://qx.esq165.co.id/upload/sementara/img/file_58999d95eefba.jpg', 'Kesalahan yang dibuat seorang karyawan dalam suatu perusahaan adalah seperti seorang penumpang kapal laut yang membocorkan dinding kapal dalam kamar yang disewanya sendiri.', 'Kesalahan yang dibuat seorang karyawan dalam suatu perusahaan adalah seperti seorang penumpang kapal laut yang membocorkan dinding kapal dalam kamar yang disewanya sendiri.\r\n.\r\nSaat membocorkan kapal, ia merasa bahwa itu adalah kesalahan yang hanya akan berdampak pada dirinya sendiri karena ia hanya membocorkan dinding kapal di ruangannya. Akan tetapi, dampak sebenarnya adalah ia akan menenggelamkan seluruh kapal beserta puluhan mungkin ratusan penumpangnya, termasuk juga Anda di dalamnya.\r\n.\r\nLho? Bagaimana bisa?\r\n.\r\nPemahamannya begini... bahwa setiap orang memiliki peran yang sangat besar, dan setiap tindakan memiliki konsekuensi terhadap maju-mundurnya suatu perusahaan atau organisasi.\r\n.\r\nDalam rangka memelihara sistem perusahaan secara keseluruhan, serta untuk menyelamatkan nasib karyawan lainnya, pemimpin tidak perlu ragu-ragu untuk bertindak, dan berani mengatakan ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“tidakÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â dengan tegas dan pasti.\r\n.\r\nDi sinilah saat prinsip pertama (Star Principle) ketika Meyakini sesuatu harus ditegakkan. Apabila hal tersebut tidak didukung pelaksanaan prinsip kedua (Angel Principle), ketegasan ini menjadi kurang efektif. Begitu pula apabila prinsip kepemimpinan (Leadership Principle) tidak menggunakan nilai-nilai di dalam prinsip kedua, ketegasan menjadi tidak efektif lagi.\r\n.\r\nPelaksanaan prinsip keempat, yaitu prinsip pembelajaran (Learning Principle) harus pula diterapkan dengan dukungan prinsip kelima, yaitu visi yang telah ditetapkan dengan jelas sejak awal (Vision Principle). Semua prinsip ini saling terkait dan saling memengaruhi.\r\n.\r\nMisalnya, apabila seseorang hanya bersikap tegas tetapi mengabaikan lima prinsip lainnya, ketegasan itu hanya akan menimbulkan keotoriteran karena tidak dilandasi dengan visi, kepercayaan yang berasal dari kelima prinsip lainnya.\r\n.\r\nNamun ingat..ketegasan juga harus didukung oleh kematangan emosi. Seorang pemimpin tidak hanya perlu tegas, tetapi juga bersikap rahman-rahim, adil, dan bijaksana.\r\n.\r\nPemimpin juga harus mampu melihat dengan menggunakan sisi lain secara menyeluruh, dan berpikir melingkar (komprehensif).\r\n.\r\nSudahkah Anda menjadi pemimpin yang memiliki 5 prinsip ini?', '2017-02-07 17:12:41', 11, NULL, NULL, NULL, NULL),
	(61, 'Apabila seseorang sudah memiliki keyakinan di benaknya, seribu jalan akan tercipta untuk mencapainya', '2017-03-17', 'https://qx.esq165.co.id/upload/sementara/img/file_58999e2188800.jpg', 'If you have a clear vision you will event forget your breakfast', 'Charles Schwab, seorang pebisnis asal Amerika dan juga merupakan founder dari Charles Schwab Corporation pernah diminta memberikan jasa konsultasi pada sebuah pabrik baja yang sedang mengalami krisis. Tiga bulan setelah Schwab memberikan nasihatnya, pabrik baja tersebut tumbuh kembali dengan sehat. Yang dilakukannya ternyata sederhana, ia tidak membiarkan seluruh manajer serta staf disana terjebak dalam rutinitas pekerjaan.\r\n.\r\n.\r\nSetiap manajer, supervisor, dan staf diwajibkan selalu membuat rencana kerja untuk esok hari, dan pada pagi harinya rencana kerja tersebut dikerjakan dengan sebaik-baiknya. Ketika senja tiba, setiap orang mengevaluasi kembali apa yang sudah dan belum dikerjakan. Sore itu juga, mereka membuat rencana kerja untuk keesokan hari, dan begitu seterusnya. Hasilnya, sungguh mengejutkan, pabrik baja tersebut sempat tercatat sebagai salah satu pabrik baja terbesar di Amerika Serikat.\r\n.\r\n.\r\nBisa kita lihat dari contoh diatas, setelah semua terencana dengan jelas dan rinci, barulah operasi dimulai berdasarkan rencana yang telah digodok sebelumnya. Karena umumnya, kegagalan suatu usaha terletak pada tahap perencanaan awal, berupa salah produksi, salah membaca pasar, salah promosi, atau bahkan sering kali karena tidak melalui proses perencanaan yang masak sehingga dalam pelaksanaannya terjadi praktek tambal-sulam di sana-sini.\r\n.\r\n.\r\nBagi yang belum membiasakan untuk membuat perencanaan terlebih dahulu, mari lakukan sekarang, tidak ada kata terlambat. KarenaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦ Apabila seseorang sudah memiliki tujuan akhir dan keyakinan dalam benaknya, seribu jalan akan tercipta untuk mencapainya.', '2017-02-07 17:19:24', 11, NULL, NULL, NULL, NULL),
	(62, 'KARENA HIDUP ADALAH PERMAINAN', '2017-03-18', 'https://qx.esq165.co.id/upload/sementara/img/file_5899a04a03ee1.jpg', 'Hidup hanyalah rangkaian yang akan mengantar kita pada hari akhir yang kekal,mulailah memfocuskan diri pada tujuan.', 'Hidup ibarat permainan yang senantiasa berganti tema. Dari satu waktu ke waktu manusia tidak berhenti memasuki permainan yang baru. Sedih berganti dengan tawa, bahagia berganti dengan duka. Semua adalah keniscayaan yang senantiasa berganti, keniscayaan yang tak pernah kekal. Yang kekal hanyalah permainan itu sendiri.\r\n.\r\n.\r\nLalu sebenarnya apa hakikat kehidupan sesungguhnya? \r\nHakikat hidup sesungguhnya adalah ibadah. Menjalani permainan sesuai dengan ketentuan yang ditetapkan, itulah ibadah yang seharusnya dilakukan oleh manusia.\r\n.\r\n.\r\nKesedihan yang dialami oleh seseorang, seiring berjalannya waktu akan segera terlupakan. Kekayaan yang dimilki, tanpa dinyana dapat sekejap mata habis tak tersisa. Kesempurnaan fisik pun perlahan sirna dijemput usia.\r\n.\r\n.\r\nBegitulah hidup, seluruhnya hanya bagian-bagian yang segera berganti. Karena itulah, hanya orang-orang yang bertaqwa yang dapat melihat esensi yang ada dalam setiap permainan yang sedang dijalaninya.\r\n.\r\n.\r\nHanya orang-orang yang bertaqwalah yang dapat memahami bahwa di balik kesedihan pasti ada kegembiraan yang tengah dipersiapkan oleh Allah, manakala ia berhasil mengambil pelajaran dari kesedihan yang dialaminya. Hanya orang-orang yang menaati ketentuan permainan itulah yang akan menjemput kemenangan, meski ia harus melalui rintangan yang berat. Inilah mengapa Allah mensyaratkan ketaqwaan sebagai kunci untuk mendapatkan negeri akhirat yang penuh dengan kepastian.\r\n.\r\n.\r\nDi dunia ini setiap insan akan di ombang-ambingkan dalam kegelisahan, kecemasan, dan ketidakpastian. Hanya orang-orang dengan hati yang teguh saja, yang mampu melihat kehidupan ini hanyalah fatamorgana belaka.\r\n.\r\n.\r\nDengan demikian, berhentilah untuk terlalu serius menjalani permainan demi permainan yang digelar dalam kehidupan ini. Mulailah memperhatikan isyarat apa yang sesungguhnya diberikan  di balik permainan tersebut. Tangkaplah esensi yang sesungguhnya hendak dianugerahkan. Karena hidup hanyalah rangkaian yang akan mengantar kita pada hari akhir yang kekal, marilah kita mulai memfocuskan diri pada tujuan.', '2017-02-07 17:24:59', 11, NULL, NULL, NULL, NULL),
	(63, 'INILAH CARA MENGHANCURKAN MENTAL BLOCK DI DALAM DIRI!', '2017-03-19', 'https://qx.esq165.co.id/upload/sementara/img/file_5899a297bb376.jpg', 'Dalam upaya untuk mencapai apa yang kita inginkan tidaklah mudah, dibutuhkan kerja keras, niat, dan usaha untuk mencapainya.', 'Seperti yang kita ketahui, bahwa mental adalah syarat mutlak manusia untuk dapat mencapai kesuksesan. Seseorang dengan mental yang kuat tentu akan memiliki keberanian lebih dalam mengambil resiko,pantang menyerah dan terus berjuang untuk mencapai apa yang diinginkannya. Sehingga bisa dikatakan, dengan mental kuat dan percaya diri tinggi, kesuksesan sudah ada di depan mata.\r\n.\r\n.\r\nAkan tetapi, ada juga seseorang yang memiliki mental lemah. Ini yang membuat mereka merasa tidak mampu melakukan segala hal dengan baik, sulit untuk maju atau berpikir positif ke depan. Dan pada umumnya akan menimbulkan rasa kurang percaya diri, gugup, ragu, malas, dan tidak focus. Kondisi inilah yang akhirnya membuat seseorang mengalami ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œMental BlockÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢.\r\n.\r\n.\r\nSiapapun bisa terjebak dalam mental blok, entah itu artis, penulis, maupun orang biasa. Jika kenyataannya tidak sesuai dengan harapan atau Anda tidak mampu melakukannya jangan biarkan pikiran Anda terjebak dalam pemikiran yang negatif.\r\n.\r\n.\r\nBerikut ini 7 TIPS untuk menghilangkan ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œMental BlokÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢\r\n\r\n?Cari Penyebab Mental Block, Lalu Hilangkan\r\n.\r\nKenali penyebab mental block yang membuat Anda merasa tidak nyaman dan buanglah penyebab mental blok tersebut dari diri Anda.\r\n.\r\n?Menjadi Diri Sendiri\r\n.\r\nJangan mencoba untuk meniru orang lain, Anda adalah Anda dan tidak akan pernah mampu untuk menjadi orang lain. Yakinlah pada kemampuan diri Anda sendiri bahwa Anda itu Spesial.\r\n.\r\n?Gali kemampuan yang Anda miliki\r\n.\r\nKetahuilah potensi Anda dan manfaatkan potensi tersebut untuk menjadi senjata dalam meraih kesuksesan.\r\n.\r\n?Latih mental\r\n.\r\nBiasakan untuk melatih mental Anda, terutama dalam berbicara dan berpendapat.\r\n.\r\n?Teruslah Menggali Ilmu Pengetahuan\r\n.\r\nPengetahuan yang luar dengan sendirinya mampu untuk meningkatkan mentalitas pada diri Anda.\r\n.\r\n?Jangan Hanya Berdiam Diri\r\n.\r\nTidak berdiam diri, karena tindakan yang Anda lakukan merupakan solusi yang tepat untuk mengatasi mental blok Anda.\r\n.\r\n?Jangan Takut Untuk Mencoba\r\nSaat pertama kali melakukan akan terasa sulit, namun kesulitan itulah yang akan membuat Anda lebih percaya diri dan memiliki mental yang kuat.\r\n.\r\n?Memvisualisasikan pengalaman-pengalaman bahagia\r\nCobalah untuk memvisualisasikan suatu peristiwa dari masa lalu Anda ketika Anda merasa benar-benar bahagia. Gunakanlah sebagai sumber untuk mengumpulkan energi positif, dan memanfaatkan hal ini untuk bersemangat dan menjadi produktif dalam mengerjakan tugas-tugas Anda.\r\n.\r\n.\r\nItulah bagaimana cara mengatasi dan menghancurkan ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œmental blockÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ dalam diri. Memang benar dalam upaya untuk mencapai apa yang kita inginkan tidaklah mudah, dibutuhkan kerja keras, niat, dan usaha untuk mencapainya.\r\n.\r\n.\r\nHal tersebut juga berlaku dalam mengatasi mental blok, jika Anda sudah melakukan tindakan tersebut dengan baik, maka secara bertahap Anda akan mampu mengatasi dan menghancurkan mental block dalam diri.', '2017-02-07 17:34:02', 11, NULL, NULL, NULL, NULL),
	(70, '*RAS (Reticular Activating System)*', '2017-06-01', 'https://qx.esq165.co.id/upload/sementara/img/file_58a2b1bd5bff1.jpg', '*What you seek is seeking you*', 'RAS berfungsi seperti google search. *What you seek is seeking you*, Apa yang Anda cari, itulah yang akan Anda temukan. RAS akan mencari jalan menuju yang kita inginkan.\r\n\r\nKalau tidak diprogram, RAS tidak akan aktif. Lalu apa yang akan kita dapatkan? Hanya keluhan sehari-hari.. padahal trilyunan kesempatan ada di hadapan.\r\n\r\nTuhan dan Alam semesta di bawah kendaliNya, menjawab apa yang kita cari tersebut. Quantum Field adalah Hard Drive, Pusat dimana otak kita senantiasa dalam keadaan Positif.\r\n\r\nQuantum Field atau Lapangan Quantum inilah yang bersinergi dengan fungsi Manusia sebagai khalifah di Alam Semesta, sehingga bila kita mengaktifkan Quantum Field, dengan ijin dan kekuasaan Tuhan, Alam Semesta akan bergerak untuk membuka jalan menuju apa yang kita tuju.\r\n\r\nContohnya, seorang yang bermental Juara, akan terus menerus menjadi juara. Karena Quantum field nya sudah aktif.', '2017-02-14 14:30:09', 11, NULL, NULL, NULL, NULL),
	(71, 'Makna Ketegaran Dibalik Ungkapan "Sabar Itu Subur"', '2017-03-20', 'https://qx.esq165.co.id/upload/sementara/img/file_58a2b4adf09b3.jpg', 'Ketegaran dan kekuatan orang sabar ini tentu membawa banyak hal positif dalam hidupnya, termasuk lebih tentram dan berbahagia.', 'Percayakah Anda jika ungkapan sabar itu subur benar adanya? Menariknya, makna di balik ungkapan tersebut sungguh sangat menakjubkan.\r\n.\r\nContohnya seperti cerita dari Daniel Goleman, yang pernah meneliti tentang kecerdasan emosi. Dalam penelitian di Taman Kanak-Kanak Stanford, pernah dikumpulkan sejumlah anak berusia 4 tahun. Mereka diminta masuk ke dalam sebuah ruangan dan diberi sepotong marshmallow untuk tiap anak.\r\n.\r\n"Kalian boleh memakan marshmallow itu jika mau. Namun, jika kalian tidak memakannya selama saya pergi dan kembali lagi, maka kalian akan mendapatkan satu lagi marshmallow."\r\n.\r\n14 tahun kemudian, peneliti menemukan hal menarik pada perkembangan anak-anak itu. Anak-anak yang mampu menahan diri dan mau menunggu, memiliki ketahanan mental yang jauh lebih baik, dibanding yang memakannya langsung. Mereka tahan terhadap stres, lebih tenang, dan nilai akademiknya sangat baik.\r\n.\r\nSetelah tumbuh dewasa, anak-anak yang mampu bersabar memiliki kehidupan sosial, karir, dan cinta yang lebih baik. Hasil penelitian ini menunjukkan bahwa benar jika ungkapan orang sabar itu subur.\r\n.\r\nOrang yang sabar akan lebih luwes ketika bergaul hingga menghadapi permasalahan. Orang sabar tahan terhadap tekanan dan pembawaannya lebih tenang. Ketegaran dan kekuatan orang sabar ini tentu membawa banyak hal positif dalam hidupnya, termasuk lebih tentram dan berbahagia.', '2017-02-14 14:42:47', 11, NULL, NULL, NULL, NULL),
	(72, 'CARI PERHATIAN PADA TAMU', '2017-03-21', 'https://qx.esq165.co.id/upload/sementara/img/file_58a2ba6090299.jpg', 'Jika anak merasakan pengalaman berkesan saat kedatangan tamu, maka perilaku mereka akan baik selama kedatangan tamu.', 'Apakah Bunda dan Ayah pernah merasa kalau disaat ada tamu di rumah, anak-anak kita terkadang suka cari perhatian dengan berulah yang terkadang membuat kita menjadi jengkel. Banyak sekali keluhan tersebut muncul dari para orangtua.\r\n.\r\nApa sih yang sebenarnya menjadi penyebab anak melakukan aksinya saat ada tamu di rumah?\r\n.\r\nSalah satunya, mungkin sang anak merasa diabaikan sehingga ia mencari-cari perthatian. Ia tidak suka jika melihat orangtuanya terlalu asik dengan orang lain, apalagi jika tamu menyita waktu lama.\r\n.\r\nKehadiran tamu merupakan hal positif bagi sebuah keluarga. Anak bisa belajar untuk berkenlan dengan orang lain selain anggota keluarganya. Kehadiran tamu pun merupakan kesempatan emas untuk meningkatkan ikatan emosional antara orangtua dengan anak, membangun konsep diri positif dan kepercayaan diri anak.\r\n.\r\nApa yang perlu Bunda dan Ayah lakukan saat kedatangan tamu? Beri informasi pada anak, bahwa di rumah akan kedatangan tamu. Jangan lupa untuk memberikan gambaran tentang sosok tamu yang akan datang. Sampaikan pada anak jika tamu datang, nanti ia akan diperkenalkan. Beri pemahaman pada anak bahwa Bunda dan Ayah akan berbicara sebentar dengan tamu.\r\n.\r\nSaat bertamu harusnya anak mendapatkan perhatian dan sentuhan terlebih dahulu, seperti mengucapkan salam dan mengusap kepala mereka. Dengan demikian, anak merasa bahwa dirinya juga orang yang penting dan layak dihormati. Jika anak merasakan pengalaman berkesan saat kedatangan tamu, maka perilaku mereka akan baik selama kedatangan tamu.', '2017-02-14 15:05:56', 11, NULL, NULL, NULL, NULL),
	(73, 'MEMAKNAI KESUKSESAN DARI BERBAGAI SISI', '2017-03-22', 'https://qx.esq165.co.id/upload/sementara/img/file_58a2bb32eaa4e.jpg', 'Jika sukses adalah sebuah istana megah, Maka elemen dasarnya adalah kepentingan orang lain.', '"Saya sudah bekerja keras dan berusaha sekuat tenaga, namun kesuksesan belum jua diraih."\r\n.\r\nApakah ini yang sedang Anda rasakan saat ini? Jika ya, coba simak kisah inspiratif di bawah ini:\r\n.\r\nSebuah kisah tentang gentong yang rusak. Ada seorang petani yang setiap hari mengambil air dari sumur, yang letaknya cukup jauh dari rumahnya. Dia membawa dua gentong di sisi kiri dan kanan. Namun, gentong sebelah kanan kondisinya sudah retak. Sehingga air yang terbawa ke rumah hanya tersisa setengah.\r\n.\r\n"Kasihan sekali Pak Tani itu. Dia memanggul gentong tapi retak. Air yang dibawa tidak banyak dan tidak bisa sukses 100 persen," tutur seorang pemuda pada pamannya.\r\n.\r\n"Oh jangan risau dan bersedih dulu. Coba lihat bunga-bunga warna-warni yang harum mewangi di sisi kanan jalan ini. Mereka tumbuh subur, karena air rembesan dari gentong kanan Pak Tani yang bocor,"\r\n.\r\nHikmah apa yang bisa diambil dari kisah ini?\r\n.\r\nKita mungkin merasa tidak sukses karena hanya melihat dari satu sisi. Seperti pemuda itu yang hanya melihat separuh air yang terbawa di gentong sebelah kanan. Padahal, jika mau melihat dari sudut lain, banyak orang yang mensyukuri keberadaan Anda. Seperti si gentong kanan yang menyirami bunga-bunga kekeringan.\r\n.\r\nMungkin kita merasa sedih karena tidak bisa sukses. Namun, dari sisi orang lain, mereka bisa melihat dan merasakan langsung manfaat yang ada dari keberadaan kita. Jadi, kalau sukses adalah sebuah istana megah, batu batanya atau elemen dasarnya adalah kepentingan orang lain.', '2017-02-14 15:09:35', 11, NULL, NULL, NULL, NULL),
	(74, 'Solusi yang Bijaksana', '2017-03-23', 'https://qx.esq165.co.id/upload/sementara/img/file_58a2bdc442c16.jpg', 'Dengan berpikir melingkar maka kita akan menemukan solusi yang tepat dan bijaksana.', 'Sahabat ESQ, Anda pasti mengenal Levi Strauss, sebuah pabrik pembuat pakaian jadi raksasa. Ada satu dilema besar sedang yang sedang mereka hadapi. Ternyata, dua perusahaan sub kontraktor jahitnya di Bangladesh menggunakan tenaga kerja anak-anak.\r\n.\r\nApa yang terjadi? Bisa Anda bayangkan, bagaimana bisa sub kontraktor tersebut mempekerjakan anak-anak di bawah umur??? Reaksi keraspun bermunculan dari para Aktivis Hak Asasi Manusia (HAM). Mereka mendesak Levi Strauss untuk tidak mengggunakan pekerja anak-anak dibawah umur. Namun, di sisi lain, Investigator perusahaan menemukan fakta bahwa jika anak-anak tersebut dipecat maka mereka akan jatuh miskin dan berpeluang besar terjerumus ke lembah prostitusi.\r\n.\r\nLalu langkah apa yang mereka ambil menyikapi dilemma di atas?\r\n.\r\nLevi Strauss tidak memberhentikan anak-anak tersebut. Mereka tetap masuk dalam daftar upah karyawan, diperbolehkan sekolah. Ketika usia mereka menginjak 14 tahun, perusahaan akan menarik mereka kembali untuk bekerja.', '2017-02-14 15:21:10', 11, NULL, NULL, NULL, NULL),
	(75, 'Mengeluh, Yes or No ?', '2017-03-24', 'https://qx.esq165.co.id/upload/sementara/img/file_58a2d3579ab08.jpg', 'Kurangi mengeluh dan perbanyaklah bersyukur.', 'Sahabat ESQ, Pagi !!!\r\n\r\nSuatu hari, ada seekor keledai tua yang terperosok ke dalam sumur tua. Dia pun menjerit ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚ÂEo! Eeoo! Eeeooo!ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â untuk memanggil bantuan. Jeritannya pun didengar oleh sang pemilik keledai tersebut.\r\n.\r\nNamun,sangat menyedihkan, sang pemilik ternyata sudah tidak menginginkannya lagi. Sang keledai sudah tua dan tak berguna lagi bpemili pemilik.\r\n.\r\nSi pemilik pun sedari dulu berkeinginan untuk menutup sumur tua itu karena dianggap berbahaya. Karena itu, sang pemilik memutuskan untuk mengubur keledai tua itu hidup-hidup. Ia mengambil sekop dan membuang tanah ke sumur itu. Si keledai terkejut dan ia pun semakin menjerit ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚ÂEo! Eeoo! Eeeoo!ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â.\r\n.\r\nPemilik pun tambah kalap melemparkan tanah ke sumur. Setelah beberapa lama, si keledai pun berhenti menjerit, dia mendapat akal. Sang pemilik mengira keledai itu sudah mati dan makin semangat menyekop tanah tanpa menyadari bahwa setiap sekop tanah yang menimpa punggungnya, dia menggoyang punggungnya hingga tanah itu jatuh, menginjak-injak tanah itu hingga padat, dan dia pun naik satu inci lebih tinggi. Sekop tanah berikutnya, jatuhkan, injak-injak dan seinci lebih tinggi.\r\n.\r\nPemilik keledai itu begitu sibuk menyekop tanah hingga tidak menyadari sepasang telinga mulai tampak di mulut sumur. Ketika pijakannya sudah cukup tinggi, keledai itu melompat dan menendang bokong sang pemilik, lalu melarikan diri.\r\n.\r\nApa yang bisa Sahabat ESQ petik dari kisah seekor keledai tua itu??\r\n.\r\nKeledai tua itu mengajarkan bagaimana cara agar kita tidak terus-terusan mengeluh. Lihat saja, jika sang keledai terus berteriak Eeoo! Eooo! Apa yang akan terjadi? Apakah sang keledai tua bisa keluar dari sumur hanya dengan teriakannya?\r\n.\r\nTidak kan?\r\n.\r\nSang keledai tua memgajarkan kita untuk selalu melihat situasi. Situasi buruk tidak akan berubah hanya karena kita mengeluh setiap hari. Mengeluh tidak akan pernah dapat mengubah keadaan. Mengeluh pun tidak akan pernah membawa Anda ke situasi yang jauh lebih baik. Mengeluh hanya akan membuat Anda semakin terpuruk dan hidup dan kemurungan.\r\n.\r\nUbahlah pola pikir Anda untuk selalu bersyukur. Kurangi mengeluh dan perbanyaklah bersyukur.', '2017-02-14 16:52:43', 11, NULL, NULL, NULL, NULL),
	(76, 'Ternyata Di Sini Letak Kebahagiaan', '2017-03-25', 'https://qx.esq165.co.id/upload/sementara/img/file_58a5184055c29.jpg', '"Ini namanya kebahagiaan. Ini sangat bernilai, dicari, dan diperlukan oleh manusia."', 'Suatu waktu, Tuhan memanggil 3 malaikat sambil memperlihatkan sesuatu...\r\n.\r\n"Ini namanya kebahagiaan. Ini sangat bernilai, dicari, dan diperlukan oleh manusia. Simpanlah di suatu tempat, supaya manusia sendiri yang menemukannya. Jangan di tempat yang terlalu mudah, nanti kebahagiaan ini disia-siakan. Namun, jangan juga di tempat susah, nanti justru ak bisa ditemukan. Yang penting, letakkan kebahagiaan ini di tempat yang bersih," perintah Tuhan.\r\n.\r\nKetiga malaikat pun turun ke bumi dan berdiskusi di mana meletakkan kebahagiaan.\r\n.\r\nMalaikat pertama berkata, "Letakkan saja di gunung yang tinggi." Namun, malaikat lain kurang setuju.\r\nMalaikat kedua berkata, "Bagaimana kalau di dasar samudera saja?" Usul itu pun kurang disepakati.\r\nKemudian, malaikat ketiga membisikkan usulnya. Ketiga malaikat langsung sepakat. Sejak saat itu, kebahagiaan manusia tersimpan rapi di tempat yang dibisikkan malaikat ketiga. Tempat yang sulit, sekaligus mudah.\r\n.\r\nHari ke hari, tahun ke tahun kita terus mencari kebahagiaan itu. Kita ingin menemukannya, merasakannya dan melakukan berbagai cara.\r\nAda yang mencari dengan berwisata\r\nAda yang mencari di keramaian\r\nAda yang mencari di kesunyian\r\nAda yang mencarinya sambil bekerja keras\r\nAda juga yang mencari sambil bermalas-malasan\r\n.\r\nBahkan, ada yang mengklaim kebahagiaan itu dengan mengejar gelar, jabatan, harta, dan sebagainya. Pernikahan pun dihubungkan dengan kebahagiaan, seolah yang belum menikah berarti tidak bahagia. Padahal kita semua tahu, kebahagiaan tidak ada di tempat-tempat itu.\r\n.\r\nLalu di mana para malaikat meletakkan kebahagiaan?\r\n.\r\nPara malaikat tidak meletakkan kebahagiaan di gunung, dasar samudera, kesuksesan, pernikahan, kekayaan, dan sebagainya. Malaikat meletakkan KEBAHAGIAAN manusia di tempat yang sangat dekat, namun jarang terlihat. Tempat yang sesuai dengan perintah Tuhan.\r\n.\r\nYa, ternyata malaikat meletakkan KEBAHAGIAAN manusia di...\r\n.\r\nHATI YANG BERSIH', '2017-02-16 11:01:33', 11, NULL, NULL, NULL, NULL),
	(77, 'Prasangka Baik Baik', '2017-03-27', 'https://qx.esq165.co.id/upload/sementara/img/file_58a52a35e59c9.jpg', 'Prasangka Baik Baik Prasangka Baik Baik Prasangka Baik Baik', 'Prasangka Baik Baik Prasangka Baik Baik', '2017-02-16 11:27:45', 11, NULL, NULL, NULL, NULL),
	(78, 'APA MAKNA SUKSES BAGI ANDA?', '2017-03-26', NULL, 'Sukses adalah sebuah keinginan universal yang diperjuangkan banyak orang untuk diraih dalam kehidupan.', 'Sukses adalah sebuah keinginan universal yang diperjuangkan banyak orang untuk diraih dalam kehidupan. Semua orang pasti ingin mencapai kehidupan yang SUKSES. Semua orang ingin memiliki hubungan yang SUKSES, membesarkan anak-anak yang SUKSES, dan membangun karir yang SUKSES. Inilah yang menjadi alasan kita mendidik diri kita sendiri dan bekerja keras. Itu adalah satu hal yang kita yakini akan mendatangkan kebahagiaan dan arti dalam hidup kita.\r\n.\r\nJadi apa itu sukses? Bagaimana Anda tahu kapan Anda akan sukses?\r\n.\r\nSebenarnya tidak ada definisi khusus atas kesuksesan. Sukses memiliki arti yang berbeda-beda pada orang yang berbeda..\r\n.\r\nBeberapa orang percaya kalau mereka sukses ketika mereka diakui oleh masyarakat atau oleh kelompok mereka. Yang terpenting bukanlah apa yang mereka pikirkan tetapi apa yang orang lain pikirkan tentang diri mereka. Namun ada juga yang merasa sukses selama mereka telah melakukan usaha terbaik mereka, apa pun hasilnya atau apa pun yang orang pikirkan tentang mereka.', '2017-02-16 14:21:30', 11, NULL, NULL, NULL, NULL),
	(79, '5 CARA YANG DAPAT ANDA TIRU DARI ORANG-ORANG SUKSESÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â', '2017-03-28', 'https://qx.esq165.co.id/upload/sementara/img/file_58a555da67373.jpg', 'ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Anda adalah rata-rata dari ke', 'Cara paling efektif untuk sukses dalam segala hal adalah dengan mempelajari orang-orang yang telah mencapai apa yang ingin Anda capai, melakukan dengan persis dan lebih baik apa yang telah mereka lakukan. Dalam proses meniru orang-orang sukses itu, Anda akan mengembangkan perbedaan-perbedaan baru milik Anda sendiri dan menemukan cara yang bahkan lebih baik. \r\nJadi, kuncinya adalah bukan hanya meniru mentah-mentah dan berhenti sampai di situ. Kuncinya dalah meniru hal-hal yang hebat dan membuang hal-hal yang buruk. Dengan melakukan itu, Anda mengembangkan sebuah pendekatan yang bahkan lebih unggul.\r\n.\r\nJadi apa yang sebaiknya dipelajari dan ditiru dari orang-orang sukses? Sedikitnya, ada 5 hal yang dapat Anda pahami dan Anda fokuskan diantaranya : \r\n\r\n1. Paradigma\r\n.\r\nParadigma seseorang adalah sekumpulan keyakinan, nilai-nilai dan sikap-sikapnya. Semuanya menentukan cara mereka melihat dunia, cara mereka berpikir, dan keputusan-keputusan yang mereka buat. Anda dapat mengetahui paradigma seseorang dengan cara mendengarkan apa yang mereka katakan, mengamati cara mereka bereaksi dan membaca tulisan-tulisan mereka.\r\n.\r\nMisalnya, jika orang sukses yang Anda kagumi adalah seorang pembicara yang berbagi bagaimana Ia dapat meraih kesuksesan melalui seminarnya maka Anda dapat hadir dan mempelajari bagaimana Ia bisa sukses. Dengan berkumpul dengan orang-orang yang sukses Anda dapat meniru paradigma mereka.\r\n\r\n2. Pengetahuan dan Keterampilan\r\n.\r\nHal kedua yang perlu Anda tiru adalah kapabilitas orang tersebut ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œ pengetahuan dan keterampilan mereka. Pengetahuan dan keahlian apa yang mereka miliki sehingga dapat melakukan apa yang mereka lakukan?\r\n.\r\nPara pembicara dan pelatih seminar terbaik memiliki pengetahuan yang dalam tentang NLP (Neuro Linguistic Programming), Accelerated Learning Technique, dasar-dasar Hipnotis dan pendidikan Psikologi. Jadi, Anda dapat membaca buku dan menghadiri berbagai pelatihan untuk mendapatkan tingkat kompetensi yang setara dengan mereka.\r\n\r\n3. Keadaan Emosi\r\n.\r\nDalam situasi tertentu, kondisi emosi Anda (apa yang Anda rasakan di dalam diri Anda) memiliki pengaruh besar pada kinerja Anda. Ini khususnya terjadi di bidang olahraga, pembicara publik, penjualan, negosiasi, dan kepemimpinan.\r\n.\r\nUntuk memotivasi orang melakukan apa pun, pertama-tama Anda harus berada dalam kondisi yakin, termotivasi, dan bersemangat. Para penjual, pemimpin, dan komunikator top tahu bagaimana cara membuat diri mereka berada dalam kondisi yang tepat. Anda juga sebaiknya demikian!\r\n\r\n4. Perilaku (Komunikasi)\r\n.\r\nAspek selanjutnya yang perlu diamati dan ditiru adalah perilaku orang-orang yang sukses, khususnya pola bahasa mereka. Kata-kata seperti apa yang mereka gunakan? Bagaimana mereka menyusun kalimat mereka? Bagaimana pendekatan mereka terhadap sesuatu? Bagaimana mereka menggunakan mata, kepala tubuh, gerak tangan, dan nada bicara mereka?\r\n\r\n5. Lingkungan\r\n.\r\nElemen terakhir yang dapat Anda tiru adalah lingkungan orang tersebut. Ini tidak selalu mungkin atau dapat dilakukan. Namun, sesekali hal ini dapat memberi Anda keunggulan juga.\r\n.\r\nBerdasarkan sebuah penelitian, orang-orang yang paling sukses tetap merasa termotivasi dan tertantang dengan berada di antara orang-orang serupa yang sama positifnya dengan mereka. Mereka juga menghabiskan waktu dengan orang-orang yang bisa mereka ajak berdiskusi, mendapatkan umpan balik, dan meningkatkan posisi mereka. Anda dapat melakukan hal yang sama pada diri Anda sendiri. Jika lingkungan Anda tidak mendukung atau tidak mendidik Anda, ubahlah! Mulailah membangun jaringan dengan kelompok-kelompok baru di dalam atau di luar tempat kerja Anda.', '2017-02-16 14:34:59', 11, NULL, NULL, NULL, NULL),
	(80, 'Thursday Quote', '2017-02-16', 'https://qx.esq165.co.id/upload/sementara/img/file_58a5576d9b8a8.jpg', 'PENGALAMAN kehidupan dan lingkungan akan sangat mempengaruhi CARA BERPIKIR seseorang .', NULL, '2017-02-16 14:41:23', 126, NULL, NULL, NULL, NULL),
	(81, 'Friday Quote', '2017-02-17', 'https://qx.esq165.co.id/upload/sementara/img/file_58a68bdf81dfd.jpg', 'Hanya dengan berpegang kepada TUHAN-lah yang dapat menimbulkan rasa TENANG dan AMAN.', NULL, '2017-02-16 14:42:37', 126, '2017-02-17 12:36:40', 126, NULL, NULL),
	(82, 'Monday Quote', '2017-02-20', 'https://qx.esq165.co.id/upload/sementara/img/file_58a9d6f73c9b5.jpg', 'Sebuah KEPERCAYAAN diri dan keberanian tinggi pada akhirnya menimbulkan kepercayaan pada orang lain', NULL, '2017-02-16 14:43:28', 126, '2017-02-20 00:33:48', 126, NULL, NULL),
	(83, 'JANGAN MELANGGAR SUARA HATI', '2017-03-29', 'https://qx.esq165.co.id/upload/sementara/img/file_58a558551eee4.jpg', 'Suara hati murni tidak akan pernah bohong, dan akan jujur .', 'Selama ini, mungkin banyak orang yang sering mendapat rujukan tentang praktek ekonomi yang bersih dari negara seperti Amerika Serikat. Rupanya, praktik yang diidealkan itu beberapa waktu belakangan ini dilanda keruntuhan. Pasar modal yang dulu selalu punya predikat baik dan jujur, kini menjadi sebuah tanda tanya, karena beberapa perusahaan besar yang dulunya terdaftar di Wall Street terbongkar ketidaklayakan praktiknya.\r\n.\r\nJatuhnya Enron, yang disusul dengan jatuhnya perusahaan-perusahaan dot.com di Wall Street 2000-2001, sebagai akibat tindak penyelewengan korporatis lainnya. Kegelisahan semakin terasa di dunia bisnis Amerika saat itu. Ketidakpercayaan kalangan pebisnis pada institusi finansial semakin meruncing. Permasalahan demi permasalahan seakan terkuak kebobrokannya.\r\n.\r\nDorongan berlebihan untuk mencapai sesuatu yang diinginkan, seperti harta, kedudukan, dan kehormatan, dengan mengabaikan keseimbangan hukum ketetapan Tuhan, hanya akan menghasilkan kehancuran. Misalnya, dorongan untuk menjadi yang terbesar tanpa mempedulikan keadilan, dan faktor-faktor lain, akan mengakibatkan seluruh sistem terganggu. Saya yakin, jauh dalam hati kecil mereka di Enron, kehancuran itu sebenarnya telah diketahui.\r\n.\r\nNamun, taukah Anda??? \r\nMereka mengabaikan suara hati yang sebenarnya membisikkan informasi tersebut. Di sini dibutuhkan kecerdasan emosi dan spiritual. Menurut perhitungan di atas kertas, mungkin saja semua tampak baik dan sempurna, namun sering kali hati yang jernih menyuarakan informasi yang berbeda. Otak memang mampu membuat alasan apa saja dengan logis sehingga siapa pun bisa menerima argument tersebut. Namun, suara hati murni tidak akan pernah bohong, dan akan jujur untuk mengilhami yang sebenarnya karena ia adalah cerminan fitrah manusia.\r\n.\r\nJadi kesimpulannya? Apakah Anda sudah mendengarkan suara hati Anda sendiri?', '2017-02-16 14:45:08', 11, NULL, NULL, NULL, NULL),
	(84, 'Tuesday Quote', '2017-02-21', 'https://qx.esq165.co.id/upload/sementara/img/file_58a73fb336df8.jpg', 'Dunia adalah wujud pembuktian KUALITAS DIRI manusia', NULL, '2017-02-16 14:45:27', 126, '2017-02-18 01:23:51', 126, NULL, NULL),
	(85, 'Wednesday Quote', '2017-02-22', 'https://qx.esq165.co.id/upload/sementara/img/file_58a9d9b11c006.jpg', 'Keberhasilan sesungguhnya ditentukan oleh besar-tidaknya KEYAKINAN anda meraih kemenangan.', NULL, '2017-02-16 14:48:32', 126, '2017-02-20 00:45:32', 126, NULL, NULL),
	(86, 'Thursday Quote', '2017-02-23', 'https://qx.esq165.co.id/upload/sementara/img/file_58a73df1b6927.jpg', 'Jangan remehkan diri anda karena anda memiliki MODAL KEMULIAAN.', NULL, '2017-02-16 14:49:24', 126, '2017-02-18 01:16:22', 126, NULL, NULL),
	(87, '5 Faktor yang memengaruhi keberhasilan', '2017-03-30', 'https://qx.esq165.co.id/upload/sementara/img/file_58a55c1e839a1.jpg', 'Semua manusia di muka bumi ini senantiasa menginginkan\r\nkeadilan,kejujuran,keterbukaan,kedamaian, kepercayaan..', 'Pagi! Sahabat ESQ. Saya harap Sahabat ESQ dimanapun berada selalu dalam keadaan sehat dan bahagia.\r\n.\r\n.\r\nApakah Anda tau? Bahwa semua manusia di muka bumi ini senantiasa menginginkan\r\nkeadilan,kejujuran,keterbukaan,kedamaian, kepercayaan, keagungan, kreativitas, kemurahhatian, saling memaafkan, sifat memberi, empati,kebijaksanaan, kelembutan,kesantunan, kemuliaan, keluasan hati, dan kesabaran.\r\n.\r\nCoba saja kita perhatikan satu -  persatu sifat tersebut Saya yakin, Anda menginkan sifat-sifat tersebut kan? Anda pasti berharap bahwa sifat tersebut juga dimiliki oleh bawahan atau atasan Anda di kantor?\r\n.\r\nKebalikannya, pasti Anda tidak suka kalau ada orang di sekitar Anda memiliki sifat bertentangan dengan sifat-sifat di atas.\r\n.\r\nDalam Buku karya Thomas Stanley "The Millionaire Mind" dipaparkan sebuah jajak pendapat yang melibatkan 733 juta super jutawan. Mereka diminta untuk mengurutkan, faktor-faktor apa yang paling berperan dalam keberhasilan seseorang.\r\n.\r\nSahabat ESQ, ada 5 Faktor yang mempengaruhi keberhasilan, yaitu:\r\n\r\n1. Jujur pada semua orang\r\n2. Menerapkan Disiplin\r\n3. Bergaul baik dengan orang\r\n4. Memiliki pasangan yang mendukung\r\n5. Bekerja lebih giat daripada kebanyakan orang', '2017-02-16 15:03:11', 11, NULL, NULL, NULL, NULL),
	(88, 'Apakah memelihara kebencian akan membuat orang yang dibenci menderita?', '2017-03-31', 'https://qx.esq165.co.id/upload/sementara/img/file_58a5664da376d.jpg', '"Setitik kebencian didalam hati, membuahkan penderitaan,namun setitik kasih sayang di hati,akan berbuah kebahagiaan"', 'Setiap orang tentunya memiliki sebuah pengalaman yang membuat dirinya membenci seseorang ataupun membenci sesuatu, namun apakah dengan memelihara kebencian tersebut akan membuat orang yang dibenci akan menderita?\r\n.\r\n.\r\nAnda semua pasti tahu jawabannya, Anda pasti mengatakan "TIDAK"\r\n.\r\n.\r\nMalah yang akan terjadi sebaliknya, seseorang yang membenci orang lain, justru dialah yang akan paling menderita, hidupnya tidak nyaman, serasa di hantui oleh kebencian, dan tidak tenang dalam hidup, mungkin akan mengalami stress yang berkepanjangan, apakah Anda pernah mengalami hal ini ?\r\n.\r\n.\r\nSaatnya ada melepas kebencian tersebut, buanglah kebencian itu dan gantilah kebencian dengan kasih sayang. karena dengan kasih sayang akan menentramkan dan membahagiakan.\r\n.\r\n.\r\nNamun apabila Anda masih sulit melepaskan kebencian terhadap orang lain, yang telah membuat hati Anda sakit hati ataupun rugi dalam hal fisikal dan material. Saatnya anda menghubungi orang yang tepat untuk mengobatinya, salah satunya adalah psikiater.\r\n.\r\n.\r\nSilahkan Anda hitung berapa biaya kepada Psikiater untuk berkonsultasi?\r\n.\r\n.\r\nYa salah satu caranya adalah Anda cari orang yang dapat membantu Anda untuk menghilangkan kebencian itu, karena apabila Anda masih memiliki rasa benci dalam hidup kepada seseorang. Anda akan seperti orang yang ingin berlari, namun anda membawa beban berat di punggung dan kaki Anda, apakah hidup Anda akan cepat maju mencapai kesuksesan dan tujuan hidup? tentunya tidak...', '2017-02-16 15:46:39', 11, NULL, NULL, NULL, NULL),
	(89, '3 CARA MENEMUKAN KUNCI KEBAHAGIAAN', '2017-04-01', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6666ba3e40.jpg', '"Kebahagiaan itu tak bersyarat"', 'KEBAHAGIAAN adalah satu hal yang sangat didambakan oleh setiap manusia di muka bumi. Meski definisinya bisa berbeda-beda, semua orang berbondong-bonding menempuh segala cara untuk meraihnya.\r\n.\r\nManusia bekerja keras untuk itu dan tak jarang mereka terjebak dalam syarat-syarat semu. Misalnya, "bahagia itu jika memiliki banyak uang", "bahagia itu jika menikah tepat waktu", "bahagia itu ..bla..bla..bla..".\r\n.\r\nPadahal, kebahagiaan itu tak bersyarat. Siapa pun dan dalam kondisi apa pun, kita bisa mendapatkannya. Triknya cukup temukan kunci untuk membuka pintu menuju kebahagiaan itu.\r\n.\r\nApa dan di mana kunci itu berada? Temukan jawabannya di bawah ini :\r\n\r\n1. Keikhlasan\r\nBersikap ikhlas membuat segala sesuatu terasa lebih mudah, ringan, dan indah. Rintangan apa pun bisa dihadapi, hati menjadi tenang, dan jiwa lebih ceria, serta penuh bersemangat, bila kita menanamkan sifat dan sikap ikhlas ini. \r\n\r\n2. Kejujuran\r\nKejujuran akan membawa kita pada kebaikan dan kebaikan akan mengantarkan kita pada kebahagiaan, baik di dunia maupun di akhirat. Mulailah bersikap jujur pada diri sendiri, lalu kepada orang lain. Jujur ini adalah pelengkap dari ikhlas, untuk menyempurnakan perasaan damai di dalam hati.\r\n\r\n3. Rasa Syukur\r\nBersyukur merupakan sikap yang akan membuat keikhlasan dan kejujuran menjadi lebih tangguh. Syukur menjauhkan kita dari perbuatan sia-sia yang menguras waktu seperti mengeluh, putus asa, dan sebagainya. Dengan syukur, kita bisa menggenggam kebahagiaan, ketegaran, dan semangat sejati.', '2017-02-17 10:02:33', 11, NULL, NULL, NULL, NULL),
	(90, 'Nasib ditentukan oleh dirinya sendiri', '2017-04-02', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6864d3a03a.jpg', '"Nasib seseorang itu ditentukan oleh dirinya sendiri, Karena Anda akan sesuai dengan apa yang Anda pikirkan tentang diri Anda" ( Ary Ginanjar Agustian )', 'Bacalah kisah ini....\r\n.\r\n.\r\nSeorang pemuda sedang berjalan di tengah kegelapan malam, dilihatkan ada laki-laki lain yang sedang menunduk di bawah lampu dan sepertinya sedang mencari-cari sesuatu, dengan serta merta sang pemuda menghampiri laki-laki yang sedang berada di bawah penerangan lampu jalan dan berkata : "Anda sedang apa?", "saya sedang mencari kunci lemari saya, kunci lemari saya hilang" jawab laki-laki itu sambil terus mencari-cari sesuatu di bawah lampu jalan.\r\n.\r\n.\r\nAkhirnya sang pemuda memutuskan untuk membantu laki-laki malang itu untuk mencari-cari kunci lemarinya yang hilang.\r\n.\r\n.\r\nSetelah sekian lama pemuda itu mencari-cari kunci lemari laki-laki tadi namun tidak kunjung ketemu juga, maka sang pemuda berkata : "Kita sudah mencarinya ke mana-mana dan belum menemukannya, Anda yakin kunci itu hilang disini ?"\r\n.\r\n.\r\nLaki-laki itu menjawab :"Tidak, saya kehilangan kunci itu di rumah, tapi dibawah lampu jalan ini cahayanya lebih terang".\r\n.\r\n.\r\nApa artinya dari kisah tadi....\r\n.\r\n.\r\nBerhentilah mencari jawaban atas kesuksesan Anda di luar diri Anda sendiri, jawaban mengapa Anda belum menciptakan kehidupan dan hasil yang Anda inginkan, karena Anda-lah yang menciptakan kualitas kehidupan yang Anda jalani dan hasil yang Anda ciptakan.', '2017-02-17 12:13:48', 11, NULL, NULL, NULL, NULL),
	(91, 'Sukses ada di tangan Anda sendiri', '2017-04-03', 'https://qx.esq165.co.id/upload/sementara/img/file_58a686ba52a32.jpg', '"Satu-satunya yang harus kita takuti adalah rasa takut itu sendiri" \r\n( Franklin Delano Roosevelt )', '"Bagaimanapun kondisi Anda saat ini, Anda masih memiliki sesuatu yang berharga, yaitu keputusan untuk Memilih" ( Ary Ginanjar Agustian )\r\n.\r\n.\r\nDi Washington, seorang pria duduk di kursi roda sambil memegang sebuah cangkir kaleng dan beberapa buah pensil, untuk memperoleh nafkah hidup yang sangat kecil dengan mengemis. Alasan untuk tindakannya itu adalah bahwa kedua kakinya sudah tidak berfungsi lagi, karena pengakit polia. Otaknya tidak rusak. Sebaliknya, ia kuat dan sehat. Namun, pilihannya membuatnya menerima jalan ketakutan ketika penyakit yang mengerikan itu menyerangnya, dan kemampuannya menyusut karena tidak digunakan.\r\n.\r\n.\r\nDi bagian lain kota yang sama terdapat seorang pria lain yang juga cacat. kedua kakinya juga sudah tidak berfungsi, tetapi reaksinya terhadap peristiwa itu jauh berbeda. Ketika ia tiba pada persimpangan jalan di mana ia terpaksa membuat pilihan, ia menempuh suatu keyakinan diri yang tinggi dan berpikir positif. Dengan demikian hal itu membawanya ke Gedung Putih dan posisi\r\ntertinggi yang di berikan oleh rakyat Amerika.\r\n.\r\n.\r\nPria itu adalah Franklin Delano Roosevelt adalah Presiden Amerika Serikat ke-32 dan merupakan satu-satunya Presiden Amerika yang terpilih empat kali dalam masa jabatan dari tahun 1933 hingga 1945.\r\n.\r\n.\r\nInilah Quote yang paling terkenal dari Frank :\r\n.\r\n.\r\n"Satu-satunya yang harus kita takuti adalah rasa takut itu sendiri" ( Franklin Delano Roosevelt )\r\n.\r\n.\r\nSukses dan tidaknya hidup Anda saat ini ada di tangan Anda sendiri.', '2017-02-17 12:16:50', 11, NULL, NULL, NULL, NULL),
	(92, '"KEKURANGAN BUKAN ALASAN UNTUK TIDAK SUKSES"', '2017-04-04', 'https://qx.esq165.co.id/upload/sementara/img/file_58a687ff54cbf.jpg', 'ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Kita semua hidup di bawah lan', 'Thomas A. Edison adalah penjual koran di kereta api. John D. Rockefeller dulu hanya diupah US$ 6/minggu. Sedangkan Julius Caesar menderita penyakit ayan. Napoleon punya orangtua dari kelas bawah, dan menduduki peringkat 46 dari 65 siswa di akademi militer. Beethoven seorang yang tuli. Plato berpunggung bungkuk, dan Stephen Hawkings lumpuh.\r\n.\r\n.\r\nApa yang memberi orang-orang besar itu kekuatan untuk mengatasi kekurangan mereka dan menjadi orang-orang sukses?\r\n.\r\n.\r\n\r\nJawabannya adalah IMPIAN. IMPIAN dapat mengobarkan api semangat yang tak mampu dipadamkan orang lain. Napoleon Hill berkata, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Peliharalah visi dan impian Anda karena mereka anak-anak jiwa Anda dan rancangan pencapaian terbesar Anda.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Hubert H Humprey pun berkata,\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Apa yang Anda visualisasikan adalah apa yang mampu Anda raih.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\n.\r\nKonrad Adenauer benar ketika ia berkata, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Kita semua hidup di bawah langit yang sama, tetapi tidak semua orang punya cakrawala yang sama.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Roger Dawson berpendapat, para ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Peraih SuksesÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â mengetahui bahwa untuk mengubah kehidupan mereka, diri merekalah yang harus berubah terlebih dulu. Mereka harus mengembangkan sikap hidup positif, menentukan tujuan yang akan mengarahkan hidup mereka menjadi lebih baik, dan menguatkan keyakinan dalam diri mereka sendiri bahwa mereka akan berhasil.', '2017-02-17 12:21:06', 11, NULL, NULL, NULL, NULL),
	(93, 'MUSUH YANG PALING BERAT UNTUK DITAKLUKAN', '2017-04-05', 'https://qx.esq165.co.id/upload/sementara/img/file_58a688712d85b.jpg', 'Apabila seseorang telah mampu memimpin diri, berarti dia telah berhasil menjelajahi dirinya sendiri, mengenal secara mendalam siapa dirinya. Sebelum memimpin ke luar, ia telah mampu memimpin ke dalam.', 'Frederick Agung, raja Prusia yang terkenal itu, suatu hari berjalan-jalan di pinggiran kota Berlin. Ia bertemu dengan seorang laki-laki tua yang sedang berjalan ke arahnya, kemudian ia bertanya :\r\n.\r\n.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Kau siapaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â tanya Frederick.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Saya raja,ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â jawab sang laki-laki tua.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Raja?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Frederick tertawa. ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Atas kerajaan mana kau memerintah?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Atas diri saya sendiri,ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â jawab laki-laki tua itu dengan bangga.\r\n.\r\n.\r\nPotongan perbincangan diatas mengingatkan kita bahwa seorang pemimpin harus mampu memimpin dirinya sendiri sebelum memimpin orang lain. Apabila seseorang telah mampu memimpin diri, berarti dia telah berhasil menjelajahi dirinya sendiri, mengenal secara mendalam siapa dirinya. Sebelum memimpin ke luar, ia telah mampu memimpin ke dalam.\r\n.\r\n.\r\nBegitu pula dengan yang terjadi pada Nelson Mandela menceritakan bahwa selama penderitaan 27 tahun dalam penjara pemerintah Apartheid, justru melahirkan perubahan dalam dirinya. Ia mengalami perubahan karakter, dan memeperoleh kedamaian dalam dirinya. Sehingga, ia menjadi manusia yang mampu mengendalikan diri, memaafkan orang yang memusuhinya. Karena itulah, Mandela kemudian diakui sebagai pemimpin sejati.\r\n.\r\n.\r\nMempimpin diri adalah pekerjaan yang berat. Tak mudah bagi seseorang untuk selalu mampu memimpin diri sendiri melawan penjajahan hawa nafsu. Hal ini berkaitan dengan kedisiplinan diri, yaitu mencapai apa yang sungguh-sungguh diharapkan, dan melakukan yang tidak diinginkan. Musuh yang paling berat untuk ditaklukan adalah diri sendiri.\r\n.\r\n.\r\nSeperti yang dikatakan oleh penulis buku terkenal, Kenneth Blanchard, bahwa kepemimpinan dimulai dari dalam hati, dan keluar untuk melayani mereka yang dipimpinnya. Kepemimpinan adalah transformasi internal dalam diri seseorang. Sesuatu yang tumbuh dan berkembang dari dalam diri seseorang. Kepemimpinan lahir dari proses internal (leadership from the inside out)', '2017-02-17 12:23:46', 11, NULL, NULL, NULL, NULL),
	(94, '"Akhir Pertandingan Kura-kura dan Kelinci"', '2017-04-06', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6894ccb9c6.jpg', 'Kerja team adalah tentang kepemimpinan situasional', 'Akhirnya Kelinci dan kura-kura menjadi sahabat baik, dan mereka memikirkan beberapa hal bersama-sama, keduanya menyadari bahwa mereka dapat mengadakan sebuah kompetisi yang saling menguntungkan.\r\n.\r\n.\r\nMereka memutuskan untuk bertanding lagi, tetapi kalli ini sebagai team yang solid. Mereka meninggalkan garis start. Kelinci berlari sambil menggendong kura-kura hingga tepi sungai. Kemudian, kura-kura mengambil alih dan berenang sambil membawa kelinci di punggungnya.\r\n.\r\n.\r\nDi seberang, kelinci kembali menggendong kura-kura dan mereka mencapai garis finish bersama-sama dengan memperbaiki rekor kecepatan mereka sebelumnya, akhirnya mereka berdua merasakan kebahagiaan, kepuasaan dan kesuksesan yang lebih besar lagi.\r\n.\r\n.\r\nHikhmahnya adalah :\r\nMemiliki keahlian dan motivasi individu memang bagus. Akan tetapi, Anda dapat bekerja dalam team dan memanfaatkan keahlian masing-masing, karena akan selalu ada situasi di mana Anda bekerja dengan buruk dan orang lain bekerja dengan baik.\r\n.\r\n.\r\nKerja team adalah tentang kepemimpinan situasional, yaitu membiarkan orang dengan keahlian yang relevan mengambil alih kepemimpinan dalam sebuah situasi.\r\nKelinci dan kura-kura memperoleh satu pelajaran penting.\r\n.\r\n.\r\nYaitu jika kita berhenti berkompetisi dengan seorang rival, dan justru berkompetisi melawan situasi, kita akan menampilkan hasil yang jauh lebih baik.', '2017-02-17 12:26:22', 11, NULL, NULL, NULL, NULL),
	(95, 'BUKAN AKU TAK CINTA', '2017-04-07', 'https://qx.esq165.co.id/upload/sementara/img/file_58a68a1b521f6.jpg', 'Dalam kesunyian, mereka berpikir dengan hati terluka: Mengapa anakku memilih sendiri.', 'ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“APAKAH ANDA PERNAH MENGALAMINYA?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Bukan Aku Tak CintaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nPutri menunggu dengan antusias. Kaki kecilnya bolak balik melangkah dari ruang tamu ke pintu depan. Diliriknya jalan raya depan rumah. Kosong, Belum ada. Putri masuk lagi. Keluar lagi. Belum ada. Masuk lagi. Keluar lagi. Begitu terus selama hampir satu jam. Suara si Mbok yang menyuruhnya berulang-ulang makan duluan tidak digubrisnya. Pukul 18.30. Ada suara klakson mobil! Putri kecil melompat girang! Dengan senangnya Putri berseru : ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“HoreÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦Mama Pulang! Papa Pulang!ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â.\r\n.\r\nDilihatnya dua orang yang sangat dicintainya itu masuk ke rumah. Yang satu langsung masuk ke kamar mandi. Yang satu menghempaskan diri di sofa sambil mengurut-ngurut kepala. Wajah-wajah yang letih selepas bekerja seharian, mencari nafkah untuk keluarga. Bagi si kecil Putri situasi itu begitu tidak dipahaminya. Di otaknya yang kecil, Putri Cuma tahu, ia kangen Mama dan Papa, ia girang Mama dan Papa pulang.\r\n.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“MamaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦MamaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦.MamaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦..MamaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nPutri menggerak-gerakkan tangan Mama. Mama diam saja. Dengan cemas Putri bertanya, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Mama sakit, ya ? Mananya yang sakit? Mam, mana yang sakit?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nMama tidak menjawab. Hanya mengernyitkan alis sambil memejamkan mata, Putri makin gencar bertanya, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“MamaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦MamaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦mana yang sakit? Putri ambilin obat ya? ya? ya?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nTiba-tibaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\n.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Putri kepala Mama lagi pusing! Kamu jangan berisikÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nMama membentak dengan suara keras yang tinggi dengan sorot mata yang tajam menatap Putri.\r\nKaget, Putri mundur perlahan, matanya menyipit dan berkaca-kata, dengan bibir menahan tangis, kaki kecilnya gemetar. Bingung, Putri salah apa? Putri sayang MamaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦ Putri salah apa MaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦? Dengan takut-takut Putri menyingkir ke sudut ruangan. Melihat Mama dari jauh, yang kembali mengurut-ngurut kepalanya. Otak kecil Putri terus bertanya-tanya : Mama, Putri salah apa? Mama tidak suka dekat-dekat dengan Putri? Putri mengganggu Mama? Putri tidak boleh sayang Mama?\r\n.\r\nBerbagai peristiwa sejenis kembali terulang dan terulang lagi. Dan otak kecil Putri merekam semuanya.\r\n.\r\nMaka tahun demi tahun berlalu, Putri tidak lagi kecil seperti dulu, Putri bertambah tinggi, Putri remaja yang cantik, Putri mulai beranjak menuju dewasa.\r\n.\r\nAda suara klakson mobil! Mama Pulang! Papa Pulang!\r\n.\r\nPutri menurunkan kakinya dari atas meja. Melihat ke jendela. Mematikan TV. Buru-buru naik ke atas, ke kamarnya, dan mengunci pintu. Menghilang dari pandangan.\r\n.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Putri Mana?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Sudah makan duluan, Tuan, NyonyaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nMalam itu mereka kembali hanya makan berdua. Dalam kesunyian, mereka berpikir dengan hati terluka: Mengapa anakku memilih sendiri, anak yang kubesarkan dengan susah payah, dengan kerja keras, tampaknya tidak suka menghabiskan waktu bersama-sama denganku? Apa salahku? Apa dosaku? Ah, anak zaman sekarang memang tidah tahu hormat sama orangtua! Tidak seperti zaman dulu.\r\n.\r\nDi atas, Putri mengamati dan menatap kedua orang tuanya yang paling dia cintai selama ini dalam diam. Dari jauh. Dari tempat ia tidak akan terluka. ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Mama, Papa katakan pada Putri, bagaimana caranya Putri memeluk Mama dan PapaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚ÂÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\n.\r\n---------------------------------------------------\r\nApa yang Anda rasakan membaca kisah ini?\r\n.\r\nApa yang akan Anda lakukan untuk Putra Putri Anda tercinta?', '2017-02-17 12:31:51', 11, NULL, NULL, NULL, NULL),
	(96, '"BAGAIMANA MENDOBRAK KETERBATASAN DIRI"', '2017-04-08', 'https://qx.esq165.co.id/upload/sementara/img/file_58a69f8872a39.jpg', 'Banyak keterbatasan-keterbatasan pada diri kita adalah karena perbuatan kita sendiri', 'Banyak keterbatasan-keterbatasan pada diri kita adalah karena perbuatan kita sendiri. kita percaya bahwa hal-hal tertentu adalah mustahil karena kita meyakinkan diri kita sendiri bahwa itu memang mustahil.\r\n.\r\n.\r\nDidalam sebuah percobaan, seorang ilmuan meletakkan kaca pemisah antara dua ekor ikan, yang satu adalah pemangsa yang lainnya. Pada awalnya, ikan pemangsa membentur kaca pemisah berkali-kali, tetapi akhirnya ia menyerah. Setelah beberapa saat, peneliti itu mengambil kaca pemisah, akan tetapi ikan pemangsa itu terus berenang dengan tenang di tempatnya dan sudah tidak mau lagi untuk memakan mangsanya karena takut terbentur dinding kaca.\r\n.\r\n.\r\nKita manusia yang memiliki kebiasaan. maka ujilah keterbatasan Anda sekali waktu. Ketika usia Anda bertambah dan pikiran Anda semakin berkembang serta menjadi lebih mahir dengan apa yang Anda kerjakan, Anda akan menemukan bahwa batasan-batasan yang pernah membatasi Anda dahulu, sekarang sudah tidak ada lagi.\r\n.\r\n.\r\nPergunakan waktu Anda untuk merenung yang produktif, pertimbangkanlah apa yang ingin Anda lakukan jika Anda akan menemukan bahwa mungkin mimpi Anda akan terwujud bila Anda menetapkan tujuan yang pasti, terukur dan Anda punya rencana untuk mencapainya.', '2017-02-17 14:02:04', 11, NULL, NULL, NULL, NULL),
	(97, 'KENAPA HARUS TAKUT?', '2017-04-09', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6a0ac17c4b.jpg', '"Jangan biarkan ketakutan menghentikan apa yang ingin anda lakukan"\r\nAry Ginanjar Agustian-', 'Setiap orang tentunya memiliki rasa takut, apakah anda anak-anak, remaja atau sudah dewasa tentunya memiliki satu ketakutan. misalnya takut berbicara di hadapan orang banyak, takut terhadap binatang tertentu, takut dengan gelap, atau takut menghadapi masa depan.\r\n.\r\n.\r\nBermacam-macam orang mengalami ketakutan, namun sesungguhnya wajar saja apabila ada orang yang mengalami rasa takut dengan catatan selama ketakutan itu tidak menghantui kita dan mengambil alih energi kita.\r\n.\r\n.\r\nSebenarnya ketakutan itu bisa kita manfaatkan juga untuk kemajuan kita, contohnya\r\n1. Takut miskin maka kita berusaha untuk produktif dalam bekerja dan berkarya.\r\n.\r\n2. Takut tidak lulus ujian maka kita belajar dengan sungguh-sungguh.\r\n\r\n3. Takut jomblo maka kita berusaha mencari pasangan yang baik dan memperbaiki diri untuk menjadi pasangan yang baik pula.\r\n\r\n4. Takut neraka maka berusaha beribadah dengan sungguh-sungguh. \r\n.\r\n.\r\nDan tentunya masih banyak lagi ketakutan yang bisa kita ubah menjadi sesuatu yang positif dan memberdayakan.\r\n.\r\n.\r\nApakah Anda juga pernah mengalami ketakutan namun Anda mengubahnya menjadi positif ?', '2017-02-17 14:07:08', 11, NULL, NULL, NULL, NULL),
	(98, 'KEKUATAN PIKIRAN', '2017-04-10', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6a18c86dc4.jpg', 'Pemikiran Positif akan memberikan dampak yang positif, Pemikiran Negatif akan memberikan dampak negatif pula.', 'Karl Wallenda, lahir di Jerman, mempunyai hobby unik, yakni berjalan di atas tali.\r\n.\r\nSejak usia 6 tahun dia sudah menunjukkan kebolehannya.\r\nSeiring menanjaknya usia,\r\nkeahliannya ini semakin berkembang.\r\nJarak terpanjang pernah ia lalui adalah 550 meter.\r\n.\r\nDemikian juga jarak ketinggian dari permukaan tanah semakin tinggi,\r\nsetinggi dua gedung pencakar langit yg ada.\r\n.\r\nKehebatannya berjalan di atas tali telah membumbungkan namanya.\r\nDia telah memecahkan beberapa rekor dunia atas bidangnya ini.\r\n.\r\nMaka tidaklah mengherankan setiap pertunjukkannya di umumkan,\r\npuluhan ribu orang pasti datang menyaksikan pertunjukan yg penuh bahaya itu.\r\n.\r\nNamun sungguh naas baginya.\r\nSaat melakukan aksi di atas dua gedung pencakar langit di kota San Juan, Puerto Rico, dia tergelincir, jatuh bebas ke bawah, & tewas seketika.\r\n.\r\nBanyak spekulasi beredar akan tragedi itu, bahwa kegagalannya pasti karena tiupan angin kencang menghempasnya jatuh.\r\n.\r\nAda juga yg katakan cuaca dingin saat itu, telah melicinkan bentangan tali yg dilaluinya.\r\n.\r\nNamun istrinya Karl, membantah semua spekulasi itu.\r\nDia berkata,\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Selama 3 bulan ini, suami saya selalu berpikir tentang jatuh, berbeda dengan sebelumnya.\r\nPadahal ketakutan akan jatuh tidak pernah terlintas dalam pikirannya sebelumnya.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\nSesungguhnya Karl telah menakdirkan dirinya sendiri untuk gagal, dia terus menerus mengatakan pada dirinya bahwa dia akan jatuh.\r\n.\r\nIni kembali membuktikan pada kita untuk kesekian kalinya,\r\nbahwa PEMIKIRAN NEGATIF AKAN SELALU MEMBERI DAMPAK YG NEGATIF PULA.\r\n.', '2017-02-17 14:11:37', 11, NULL, NULL, NULL, NULL),
	(99, 'BERTANGGUNG JAWAB 100% ATAS DIRI SENDIRI', '2017-04-11', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6a3d224fbe.jpg', 'Ambilah resiko itu, karena sesungguhnya ketika Anda tidak mengambil resiko itu, justru Anda sedang beresiko.', 'Pernah Curhat?\r\nPasti pernah dong..\r\n.\r\nNamun mari kita coba evaluasi semua curhatan kita, berapa banyak di antara kita curhat pada orang yang tidak tepat, curhat kepada orang yang tidak mampu menyelesaikan persoalan yang kita alami.\r\n.\r\n.\r\nCurhat tentang persoalan yang ada di kantor dengan istri/suami, begitupula sebaliknya curhat tentang istri/suami kepada teman di kantor??? ( sepertinya yang sedang baca senyum-senyum nih....hehehe..)\r\n.\r\n.\r\nPadahal istri/suami tidak punya kuasa apapun terhadap persoalan di kantor, begitupula sebaliknya teman kantor tidak punya kuasa sama sekali dengan kondisi di rumah tangga.\r\n.\r\n.\r\nTetapi mengapa kita tetap curhat kepada orang yang tidak punya kuasa apapun untuk menolong permasalahan kita???\r\n.\r\n.\r\nJawabannya adalah karena kita lebih mudah menyalahkan kondisi, tidak beresiko apapun terhadap persoalan, dan tetap bisa bersembunyi dibalik zona nyaman.\r\n.\r\n.\r\nPertanyaan berikutnya?\r\n.\r\nApakah ada perubahan dengan seperti itu?\r\nYaa jelas saja tidak akan ada perubahan.\r\n.\r\nKarena bila kita tidak berani bertanggung jawab 100% terhadap kondisi yang terjadi di dalam hidup kita.\r\nMaka sebaiknya terima saja apapun yang kita dapatkan dalam hidup ini.\r\n.\r\n.\r\nMenyalahkan pasangan hidup dengan kondisi yang terjadi di rumah tangga anda, atau menyalahkan atasan, rekan kerja, sistem, juga team anda di kantor.\r\nMaka berarti anda tidak bertanggung jawab 100% atas hidup anda!!!\r\n.\r\n.\r\nBila anda pulang kerja dan merasa terlalu letih, hingga tidak ada tenaga untuk bermain dengan anak anda, ngobrol dengan istri/suami, hanya tidur2an sambil nonton TV, atau bahkan main HP, dan terus seperti itu dari hari-kehari dan beberapa waktu setelah itu keluarga anda tidak pernah menganggap anda ada, apakah itu salah mereka? Salah pekerjaan anda?\r\n.\r\n.\r\nTIDAK!!!...\r\n.\r\nMULAILAH SAAT INI JUGA UNTUK BERTANGGUNG JAWAB 100% ATAS DIRI ANDA!!!\r\n.\r\n.\r\nRubahlah respon anda, perilaku anda, maka dunia anda akan berubah.\r\n.\r\n.\r\nBila anda memiliki persoalan di kantor dengan atasan, rekan kerja atau team anda, maka bicaralah dengan mereka untuk menyelesaikan persoalannya!!!,\r\n.\r\nBegitu pula bila anda memiliki persoalan dengan istri/suami anda bicaralah dengannya, untuk mencari solusinya.\r\n.\r\n.\r\nAmbilah resiko itu, karena sesungguhnya ketika Anda tidak mengambil resiko itu, justru Anda sedang beresiko.\r\nSolusinya adalah keluarlah dari zona nyaman dan mulailah berhenti mengeluh serta menyalahkan orang lain, menyalahkan sistem atau kondisi.\r\n.\r\nBila Anda tidak suka dengan yang anda dapatkan saat ini, maka rubahlah respon Anda!!', '2017-02-17 14:21:42', 11, NULL, NULL, NULL, NULL),
	(100, 'TIGA LANGKAH BERPIKIR KREATIF', '2017-04-12', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6a5317f2b1.jpg', 'Cara berpikir tradisional adalah musuh nomor satu untuk orang yang tertarik pada program keberhasilan pribadi yang kreatif.', 'Cara berpikir tradisional adalah musuh nomor satu untuk orang yang tertarik pada program keberhasilan pribadi yang kreatif. Cara berpikir Tradisional membekukan pikiran Anda, menghambat kemajuan Anda, mencegah Anda mengembangkan kekuatan kreatif. Berikut ini adalah tiga cara untuk memeranginya:\r\n.\r\n1. Jadilah Orang Yang Bersedia Menerima Gagasan.\r\nsambut baik gagasan. hancurkan pikiran penghalang seperti:\r\n"Tidak akan berhasil"\r\n"Tidak dapat dikerjakan"\r\n"ini tidak ada gunanya" dan\r\n"Ini tindakan bodoh"\r\n.\r\n2. Jadilah Orang Yang Suka Bereksperimen.\r\nDobraklah rutinitas tetap harian Anda. Pergilah ke restoran baru, beli buku baru, kunjungi museum, dapatkan teman baru; ambil rute yang berbeda ke tempat kerja pada hari tertentu, ambil jenis liburan yang berbeda tahun ini, sesuatu yang baru dan berbeda akhir pekan ini.\r\n\r\nJika Anda bekerja di bagian distribusi, kembangkan minat akan produksi, akunting, keuangan, dan unsur lain dalam bisnis. Ini memberi Anda keleluasaan pengetahuan dan menyiapkan Anda untuk tanggung jawab yang lebih besar\r\n.\r\n3. Jadilah Progresif, Bukan Regresif.\r\nbukan "Itulah cara kami biasa mengerjakannya maka kami harus mengerjakannya dengan cara itu", melainkan rubahlah dengan kalimat "Bagaimana kami dapat mengerjakannya lebih baik daripada dengan cara yang biasa saya lakukan?".\r\n\r\nBukan cara berpikir mundur, regresif, melainkan cara berpikir maju, progresif. Sewaktu masih Anak-anak Anda terbiasa bangun jam 4 pagi untuk mengisi bak mandi untuk keperluan sehari-hari, Anda meminta Anak Anda melakukan hal yang sama.', '2017-02-17 14:29:48', 11, NULL, NULL, NULL, NULL),
	(101, 'Ada yang menarik dari AMYGDALA dan RAS', '2017-04-13', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6aa7084be0.jpg', 'Aktifkan RAS Anda untuk mewujudkan KESUKSESAN.', 'Menurut Howard Gardner (1983) terdapat lima pokok utama dari kecerdasan emosional seseorang, yakni mampu menyadari dan mengelola emosi diri sendiri, memiliki kepekaan terhadap emosi orang lain, mampu merespon dan bernegosiasi dengan orang lain secara emosional, serta dapat menggunakan emosi sebagai alat untuk memotivasi diri.\r\n.\r\n.\r\nTaukah Anda, Apa itu Amygdala?\r\n.\r\nAmygdala adalah sekelompok saraf yang berbentuk kacang almond. Amigdala dipercayai merupakan bagian otak yang berperan dalam melakukan pengolahan dan ingatan terhadap reaksi emosi.\r\n.\r\n.\r\nBagian otak yang menyimpan berbagai memori sub-conscious yang negatif, buruk, dan berbahaya untuk kesuksesan.\r\n.\r\n.\r\nBagaimana cara mengendalikan Amygdala?\r\n.\r\nCaranya.. dengan mengaktifkan RAS (Reticular Activating System).\r\n.\r\nSistem aktivasi retikular di otak ini, berfungsi secara aktif untuk mencari berbagai kesempatan menuju cita-cita, posibilitas menuju terwujudnya impian.\r\n.\r\n.\r\nSayangnya, banyak yang tidak menyadari, manusia memiliki RAS yang mampu dimaksimalkan dengan baik. Akibatnya banyak impian Anda yang terkubur dan tidak terwujudkan.\r\n.\r\n.\r\nPadahal RAS lah yang secara aktif mencari dan mengumpulkan berbagai informasi mengenai apa yang kita inginkan, dan menggerakkan diri kita untuk mewujudkannya.\r\n.\r\n.\r\nMari aktifkan RAS Anda untuk mewujudkan KESUKSESAN.', '2017-02-17 15:01:59', 11, NULL, NULL, NULL, NULL),
	(102, 'MEMPERKENALKAN AMYGDALA', '2017-04-14', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6ae3189cf1.jpg', 'Untuk mencapai kesuksesan kita perlu tau cara untuk mengontrol Amygdala.', 'Sekarang Anda sudah tau kan apa itu Amygdala?\r\n.\r\n.\r\nAmygdala-lah yang akan melakukan pengolahan terhadap reaksi emosi. Seperti saat Anda merasa sedih Anda akan menangis, bukan? Sebab Amygdala Anda memacu jaringan otak dan juga struktur sarafnya untuk mengeluarkan air mata.\r\n.\r\n.\r\nSaat dalam situasi ketakutan Amygdala mengirim pesan ke batang otak sehingga memunculkan ekspresi takut juga meningkatkan laju detak jantung yang meninggikan tekanan darah dan membuat nafas menjadi lebih cepat.\r\n.\r\n.\r\nUntuk mengetahui fungsi Amygdala pada otak manusia, peneliti menguji dua orang perempuan dengan kondisi genetik langka yang disebut dengan penyakit Urbach-Wiethe.\r\n.\r\n.\r\nPenyakit itu menyebabkan kerusakan bagian Amygdala dan membuat seseorang tidak bisa mengontrol rasa takut atau emosi lainnya.\r\n.\r\n.\r\nPeneliti membandingkan kedua perempuan itu dengan 12 partisipan lainnya yang tidak memiliki penyakit tersebut.\r\n.\r\n.\r\nPara partisipan diminta untuk melakukan judi dimana akan ada dua kemungkinan yang dihasilkan. Kemungkinan pertama adalah partisipan akan memenangkan US$ 20 atau kehilangan US$ 5. Kemungkinan kedua adalah partisipan akan memenangkan atau kehilangan US$ 20.\r\n.\r\n.\r\nHasilnya menunjukkan kedua perempuan yang punya penyakit Urbach-Wiethe ternyata memilih pilihan kedua, yaitu mengambil risiko kehilangan yang lebih besar.\r\n.\r\n.\r\nHal ini membuktikan bahwa bagian Amygdala pada otaknya memang tidak berfungsi sehingga ia cenderung tidak takut kehilangan uang.\r\n.\r\n.\r\nKarena menurut Ralph Adolphs (Professor of Psychology and Neuroscience ) "Seseorang dengan Amygdala normal harusnya bisa lebih berhati-hati dalam bertindak dan punya rasa takut akan kehilangan," kata Ralph Adolphs.', '2017-02-17 15:05:51', 11, NULL, NULL, NULL, NULL),
	(103, 'MEMAHAMI SUBCONSCIOUS MIND!', '2017-04-15', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6af253f150.jpg', 'Hidup Anda akan berubah, penyakit Anda bisa tersembuhkan, kesuksesan dan kebahagiaan bisa Anda raih dengan mudah.', 'Subconscious Mind atau biasa juga disebut dengan alam bawah sadar merupakan bagian pikiran manusia yang tidak disadari keberadaannya namun memiliki pengaruh yang sangat besar.\r\n.\r\n.\r\nPasti Anda sering mendengar bahwa manusia umumnya hanya menggunakan 10 % dari seluruh kekuatannya, 90% lainnya tertidur di alam bawah sadar.\r\n.\r\n.\r\nOleh karena itu, sangat penting bagi siapapun juga untuk memahami Potensi Pikiran Bawah Sadar. Selain itu, apabila Anda mengerti cara menggunakan kekuatan bawah sadar Anda, maka Anda bisa menjadi apapun yang Anda inginkan.\r\n.\r\n.\r\nHidup Anda akan berubah, penyakit Anda bisa tersembuhkan, kesuksesan dan kebahagiaan bisa Anda raih dengan mudah.\r\n.\r\n.\r\nKita bisa mempelajari profil tokoh dunia, yang berhasil dengan pemrograman Otak dan manajemen alam bawah sadar ini. Contohnya Rudy Hartono dan Muhammad Ali.\r\n.\r\n.\r\nMereka selalu percaya dirinya adalah JUARA. Hingga akhirnya mereka benar-benar menjadi orang ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œ orang yang tidak terkalahkan.\r\n.\r\n.\r\nJadi, pilihannya sekarang adalah Anda membiarkan pikiran terbatas atau menjadi tidak terbatas?', '2017-02-17 15:08:21', 11, NULL, NULL, NULL, NULL),
	(104, 'KECERDASAN EMOSIONAL YANG SALAH', '2017-04-16', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6afb0e1c3f.jpg', 'Senyuman adalah senjata yang luar biasa untuk membangun hubungan dengan orang.', 'Pernahkah Anda mendengar cerita tentang Willy Sutton.\r\n\r\nWilly Sutton adalah salah seorang perampok bank di Amerika Serikat yang sangat terkenal. Yang hebatnya, ia sering kali merampok bank dengan tidak menimbulkan kehebohan. Ia bisa keluar masuk bank yang dirampoknya dengan leluasa. Setelah tertangkap dan dipenjara, Willy Sutton pernah ditanya rahasia melakukan perampokan selama bertahun-tahun. Dalam wawancara itu, Willy Sutton berkata, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Dalam hidupku, aku akhirnya belajar bahwa kamu lebih gampang memaksa orang melakukan apa yang kamu inginkan dengan senjata plus senyuman dari pada hanya menggunakan senjata saja.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â\r\n.\r\n.\r\nWalaupun Willy menodongkan senjata ke orang-orang yang menjadi korban, ia tak pernah lupa untuk menebarkan senyum kepada para korbannya. Jelas sekali, Willy Sutton adalah seorang penjahat yang paham tentang bagian dari kecerdasan emosional. Sayangnya, pemahaman ini dipakai untuk tujuan yang salah, yaitu merampok bank. Kalau saja kepandaian ini dipakai untuk berdagang atau pekerjaan halal lainnya, bisa jadi ia juga akan sama hasilnya. Betul juga yang dikatakan oleh Willy Sutton, SENYUMAN ADALAH SENJATA YANG LUAR BIASA UNTUK MEMBANGUN HUBUNGAN DENGAN ORANG.\r\n.\r\n.\r\nKadang-kadang lebih mudah meminta orang melakukan sesuatu (plus orang itu melakukannya dengan suka cita) dari pada memaksanya dengan omelan atau gertakan. Bukankah ada pepatah, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“LEBIH MUDAH MENANGKAP LALAT DENGAN MADU DARI PADA DENGAN MENGGUNAKAN CUKA.ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Hukum ini berlaku dalam hubungan dan interaksi kita. Jika mau menciptakan hubungan yang nyaman dan orang-orang lebih bersedia melakukan apa yang kita inginkan, lakukan dengan senyuman. Lebih banyaklah belajar tersenyum dan berikanlah senyum yang tulus.', '2017-02-17 15:12:09', 11, NULL, NULL, NULL, NULL),
	(105, 'APA MANFAAT MUSIK UNTUK KEHIDUPAN???', '2017-04-17', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6b0853831f.jpg', 'Usahakan untuk mendengarkan musik-musik dengan lirik yang baik dan membangun.', 'Siapa diantara Anda yang suka musik??? Pasti semua akan menjawab ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“YA, SAYA SUKA MUSIKÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â. Saat ini, musik sudah menjadi kebutuhan primer untuk banyak orang. Ibarat seperti makan sayur tanpa garam, hidup akan terasa hambar jika taka da musik. YaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦bagi sebagian orang, music mempunyai arti yang sangat penting.\r\n.\r\n.\r\nDi kala senang maupun sedih, terkadang musik menjadi peneman setia. Musik bisa membangun mood jadi lebih baik. Mendengarkan music mempunyai banyak manfaat. Apa manfaatnya? Mari kita simak\r\n\r\n1. Kesehatan\r\nMenurut seorang psikolog dari London, Dr Victoria, mendengarkan musik dapat mempengaruhi reaksi yang sangat luas dalam tubuh dan pikiran, dan juga beberapa bagian otak dapat diaktifkan oleh sebuah musik. Sebuah penelitian dari Kanada juga menunjukan bahwa ada hubungan kausal yang nyata antara musik dan system reward, yaitu bagian inti dari otak yang bereaksi terhadap rangsangan yang positif bagi kita. Seperti makan, minum dsb dan memperkuat perilaku ini agar kita melakukannya lagi.\r\n\r\n2. Menghilangkan Stress\r\nMusik dapat menghilangkan rasa cemas dan takut. Apalagi ketika kamu akan memulai sebuah pekerjaan. Putarlah musik favoritmu dan tanpa sadar kamu akan merasakan bahwa pikiran dan tubuhmu akan lebih rileks. Para peneliti di MCGill University di Montreal menunjukkan bahwa mendengarkan musik favoritmu akan memicu pelepasan hormon bahagia yaitu dopamine.\r\n\r\n3. Fokus\r\nSeperti yang dikatakan oleh seorang psikolog klinis yang berbasis di Paris Brigitte Forgeot, musik dapat membantu otak untuk menghasilkan gelombang tertentu, maka kita dapat menginduksi bagian yang berbeda dari kewaspadaan, tergantung pada tujuan apa yang akan kita lakukan.\r\n.\r\n.\r\nNah, bagaimana??? Cukup penting kan manfaat musik bagi kehidupan. Usahakan untuk mendengarkan musik-musik dengan lirik yang baik dan membangun ya. Agar membawa mood kita ke arah positif.', '2017-02-17 15:15:12', 11, NULL, NULL, NULL, NULL),
	(106, 'TAHUKAH ANDA BETAPA PENTINGNYA MENGENDALIKAN AMARAH?', '2017-04-18', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6b1b469eb1.jpg', 'ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Berbicaralah saat kau marah d', 'Kisah berikut ini akan mengantarkan anda pada sebuah kenyataan yang sering kita temui sehari-hari\r\n\r\n.\r\n.\r\nRomy adalah seorang pemuda yang cepat marah, tak jarang dia membentak istrinya. Setiap kali ada perkara yang menurutnya tidak benar, meskipun hanya sebuah masalah kecil, dia akan langsung menyalahkan sang istri dan memarahinya.\r\n.\r\n.\r\nSuatu hari, ayahnya mendapati keributan yang terjadi dalam rumah tangga Romy dan istrinya. Ayahnya pun memanggil Romy dan mengajaknya ke suatu tempat. Ternyata, mereka tiba di sebuah pohon besar di pinggir danau. Sang Ayah menyerahkan sebilah pisau dan menyuruhnya melemparkan pisau tersebut ke batang pohon di hadapan mereka.\r\n.\r\n.\r\n"Untuk apa?" tanya Romy.\r\n.\r\n.\r\n"Lakukan saja!" perintah ayahnya keras.\r\n.\r\n.\r\nDengan malas Romy melaksanakannya. Dia melemparkan pisau dengan asal ke arah pohon tersebut. Namun ternyata hanya membentur batang pohon dan terjatuh ke tanah.\r\n.\r\n.\r\n"Ayah, jika Ayah mengharapkan aku mampu melempar pisau hingga menembus kulit pohon itu, Ayah sama saja dengan bermimpi. Seandainya pun aku ahli dalam melempar pisau, tapi tidak bisakah Ayah lihat betapa tebalnya kulit pohon itu? Mustahil aku melakukan itu, Ayah."\r\n.\r\n.\r\nTidak mengindahkan ucapan putranya, ayahnya kembali menyuruh Romy mengulangi melempar pisau. Berulangkali dia mencobanya, pada awalnya ia kembali gagal, gagal, dan gagal. Tetapi sekali, dua kali ia akhirnya berhasil menancapkan pisau di batang pohon yang besar tersebut meskipun tidak begitu dalam.\r\n.\r\n.\r\nNamun sang Ayah belum puas, dia terus meminta Romy untuk melanjutkannya. Sementara Romy yang mulai kehilangan kesabaran akhirnya tidak tahan lagi.\r\n.\r\n.\r\n"Pak Tua! Aku tidak peduli apabila dirimu adalah ayahku. Tapi aku sama sekali tidak mengerti dengan keinginanmu, apa pentingnya pisau dan pohon ini hingga aku harus menghabiskan waktuku di tempat ini?"\r\n.\r\n.\r\n"Dasar anak muda jaman sekarang, melakukan hal sekecil ini saja tak becus. Berhentilah menjadi sok jagoan jika melempar pisau saja kau tak mampu!" tegur ayahnya dengan suara lantang sembari mencabut pisau yang masih tertancap.\r\n.\r\n.\r\nRomy benar-benar tidak bisa lagi mengontrol emosinya.\r\n.\r\n.\r\n"Berikan pisau itu, akan aku buktikan betapa hebatnya aku. Tak ada hal yang tak bisa aku lakukan!" sentaknya marah dan kemudian dengan penuh amarah di lemparkannya kembali pisau tersebut. Kali ini tidak diragukan lagi pisau itu menghujam batang pohon begitu dalam. "Kau lihat itu!" serunya menatap lelaki tua di hadapannya dengan tatapan menantang. "Aku bisa melakukannya!".\r\n.\r\n\r\n.\r\nAyahnya hanya tersenyum, sembari berjalan mendekati pohon itu ia berujar pelan, "Kau benar, anakku, kau bisa melakukannya." Dengan mengeluarkan tenaga yang lumayan besar dicabutnya pisau dari pohon yang ternyata benar-benar tertancap kuat, "Dengan luapan emosi seperti itu apapun bisa kau hancurkan, anakku...", "Kemari dan lihatlah ini..." panggilnya.\r\n.\r\n.\r\nRomy yang mulai bisa mengatur emosinya kini hanya terdiam bingung sembari mendekati ayahnya.\r\n.\r\n.\r\n"Apakah kau dapat melihat lubang yang ditinggalkan oleh pisau ini? Dapatkah kau melihat dalamnya kerusakan yang diakibatkan oleh lemparan pisau di kala kau sedang marah? Apakah menurutmu pohon ini akan kembali seperti sedia kala?", "Kurang lebih seperti itulah bekas yang akan kau tinggalkan setiap kali engkau mengambil sebuah tindakan untuk melampiaskan amarahmu. Tidak akan menjadi masalah jika engkau melampiaskannya pada masalah-masalah yang mengakibatkan amarahmu muncul, bila untuk mencari jalan keluar dalam mengatasinya. Namun pernahkah kau berpikir luka seperti apa yang akan kau berikan apabila kau melampiaskan setiap amarahmu kepada seseorang? Seseorang yang mempunyai hati dan perasaan."\r\n.\r\n.\r\n"\'Maaf\' mungkin bisa menyembuhkannya, tapi takkan pernah bisa menghapus bekas luka yang telah ditimbulkannya..."\r\n.\r\n.\r\nMendengar perkataan Ayahnya, Romy lalu terdiam. Tak bisa berbicara sepatah katapun.\r\n.\r\n.\r\nPelajaran apa yang bisa Anda ambil dari kisah Romy di atas? Bahwasanya amarah itu bukan jalan keluar untuk menyelesaikan masalah. Selesaikan masalah dengan hati dan pikiran yang tenang. Seseorang yang baik bukanlah seseorang yang bisa mengambil keputusan cepat dengan rasa amarah yang tinggi, namun ketika seseorang itu mampu mengendalikan marahnya dan tetap tenang dalam kondisi terburuk sekalipun.\r\n.\r\n.\r\nSeperti kata seorang jurnalis Ambrose Bierce yang mengatakan ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Berbicaralah saat kau marah dan kau akan mengatakan perkataan yang akan kau sesali selamanyaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â', '2017-02-17 15:20:40', 11, NULL, NULL, NULL, NULL),
	(107, 'MENGAMBIL HIKMAH DARI GENGHIS KHAN, SEORANG PENGUASA MONGOLIA YANG DITAKUTI DAN DISEGANI', '2017-04-19', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6b3590d77a.jpg', 'IQ saja tidak cukup Anda miliki sebagai bekal hidup. Kolaborasikan kecerdasan IQ, EQ, dan SQ', 'Siapa yang tak kenal Genghis Khan? Seorang penguasa Mongolia yang ditakuti dan disegani. Dibawah kepemimpinannya, ia berhasil membuat Kekaisaran Mongolia menjadi kerajaan terbesar di dunia. Khan terkenal sebagai pemimpin yang bengis dan kejam.\r\n.\r\n.\r\nDibalik sikap bengis dan kejamnya seorang Genghis Khan, ia adalah sosok pemimpin yang sangat mempunyai rasa toleransi yang tinggi. Pada masa kekuasaannya, ia memberlakukan kode etik yang diberi nama "Yassa". Isinya melarang hal-hal yang dianggap tabu seperti berzina, mencuri dan berbohong. Tidak seperti kerajaan lain. Khan adalah seorang pemimpin yang dapat mentoleransi perbedaan. Khan memberikan kebebasan kepada penduduknya untuk menganut agama mereka sendiri dan menjalankan ibadah sesuai agamanya masing-masing.\r\n.\r\n.\r\nPada suatu hari, usai mengikuti pertempuran yang hebat, Genghis Khan beristirahat sejenak melepas lelah di tepi air terjun kecil ditemani burung rajawali yang selalu mengikutinya. Sengaja ia mencari tempat yang agak sepi dan jauh dari serdadunya agar ia dapat beristirahat dengan tenang tanpa diganggu. Beberapa saat kemudian ia mulai merasa haus dan segera membawa wadah yang terbuat dari tanah liat untuk menampung air dari air terjun dekat tempatnya berteduh. Ketika ia hendak menampung air dengan mangkuknya itu tiba-tiba saja burung rajawali peliharaannya itu menyambar mangkuk tersebut hingga jatuh. Kaget ia dibuatnya, karen hal ini tak pernah dilakukan sebelumnya oleh rajawalinya yang setia.\r\n.\r\n.\r\nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Sepertinya burung itu hanya mengajak bercanda disaat aku sedang lelah,ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â pikirnya dalam hati seraya kembali mengambil mangkuk yang terjatuh itu dan mencoba kembali menampung air dengannya. Kemudian, untuk kedua kalinya sang burung rajawali peliharaannya menjatuhkan mangkuk yang dipegang sang panglima perang tersebut. Kali ini sang rajawali menghentaknya dengan sangat keras sehingga mangkuk tersebut terpental cukup jauh. Ia menjadi jengkel. Kalau sekali mungkin ini bisa dianggap bercanda, namun untuk yang kedua kalinya maka ini seperti pelecehan baginya. Dengan murka dirinya mengancam akan menyembelih burung rajawalinya jika hal itu dilakukannya lagi.\r\n.\r\n.\r\nLalu Khan memungut kembali mangkuk yang terbuat dari tanah liat itu untuk kembali mencoba menampung air dengannya. Baru saja ditengadahkan mangkuknya di bawah kucuran air terjun, sang rajawali tanpa terduga kembali menyambar mangkuknya dengan sangat keras hingga terpental jauh dan terpecah. Tak lagi menahan kesabarannya, diayunkan pedang perangnya ke arah burung rajawalinya hingga putuslah leher sang rajawali dan terlepaslah jiwa dari raganya. Setelah puas melampiaskan kemarahannya, Khan mencoba menaiki ujung tebing yang merupakan tempat sumber mata air itu berada untuk meminumnya dan sekaligus melihat-lihat keadaan sekitar.\r\n.\r\n.\r\nBegitu ia sampai di atas, betapa kagetnya ia melihat ada bangkai binatang yang membusuk tergenang tepat di sumber mata air tersebut. Seketika ia menyadari bahwa sang rajawali sejak tadi sebenarnya hendak memberitahukan kepadanya bahwa air yang ingin diminumnya sudahlah tercemar bangkai yang membusuk dan bukan tak mungkin akan bisa membunuhnya. Dengan sedih ia menatap ke arah mayat burung rajawali yang baru saja ditebasnya. Betapa sedih dan menyesalnya ia atas perbuatannya. Dihampirinya jasad sang rajawali, dilepasnya baju perang yang dipakainya untuk digunakan membungkus jasad sang rajawali dan kemudia dimakamkan dengan terhormat menggunakan upacara kemiliteran.\r\n.\r\n.\r\nSebagai panglima perang, Genghis Khan begitu hebat nan perkasa mengalahkan musuh-musuhnya, namanya tersohor di seluruh dunia. Bahkan hingga kini sejarah kehebatannya dan lekang dimakan usia. Namun kehebatannya menaklukkan dan menguasai orang lain bukanlah jaminan baginya untuk dapat mengalahkan dan menguasai dirinya. Ia menyadari bahwa sangatlah penting baginya dan seluruh pasukannya untuk dapat menguasai dirinya sebelum menguasai orang lain.\r\n.\r\n.\r\nApa hikmah yang dapat kita simpulkan darin kisah Genghis Khan di atas? YaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦Hikmahnya adalah bagaimana cara kita untuk mengontrol diri dari berbagai emosi. Memang, fitrah manusia sejak lahir adalah bisa merasakan berbagai macam emosi seperti sedih, senang, marah takut, kecewa dan berbagai macam emosi lainnya. Munculnya emosi adalah sesuatu hal yang wajar.\r\n.\r\n.\r\nNamun, emosi menjadi sangat tidak wajar ketika emosi sudah mengendalikan diri kita. Pada akhirnya, bukan kita yang mengendalikan emosi, namun emosi lah yang akan menguasi diri kita. Banyaknya orang dengan emosi dan perilaku tidak baik, karena tidak seimbangnya IQ, EQ dan SQ. Sosok besar seperti Genghis Khan mempunyai IQ yang sangat tinggi, namun??? Perangainya yang kurang baik disebabkan karena ia tidak memiliki kecerdasan EQ dan SQ.', '2017-02-17 15:26:21', 11, NULL, NULL, NULL, NULL),
	(108, 'INILAH RAHASIA KEKUATAN SUARA HATI SEJATI MANUSIA', '2017-04-20', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6b3cf394a9.jpg', 'Suara hati adalah sesuatu yang bersifat built in, ada pada semua manusia.', 'Tahukah Anda bahwa suara hati bisa meningkatkan berbagai potensi yang Anda miliki?\r\n.\r\nSuara hati (inner voice atau inner speech) merupakan bahan penelitian bagi para ahli dari berbagai disiplin ilmu, diantaranya psikologi, filsafat, psikiatri, spiritualitas, religiusitas, neuro-kognitif, dan sosio-ekologi. Morin (2007) memaparkan bahwa Suara hati membantu kita dalam mengevaluasi apa yang sudah kita lakukan, membantu mengingat pengalaman masa lalu, dan memonitor ingatan tersebut, dengan berada dalam tahap kesadaran yang lebih tinggi (higher order consciousness), dan membantu kita menghadapi masa yang tengah dijalani dengan lebih awas dan dalam potensi yang lebih berkembang. Sehingga kita tidak sekedar ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œhadirÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ di masa kini tanpa kesadaran.\r\n.\r\n.\r\nSuara hati aktif saat kita sendiri, maupun saat bersama orang lain. Yang terpenting adalah, kita harus berada dalam keadaan sadar. Saat seseorang mabuk karena minuman keras, misalnya, suara hati menjadi tidak aktif. Inilah mengapa banyak orang yang mabuk kemudian melakukan berbagai hal yang tidak ia inginkan. Sebabnya adalah, tidak adanya kewaspadaan diri yang seharusnya hadir dengan aktivasi suara hati ini.\r\n.\r\n.\r\nHeery (1989) dalam Jurnal Psikologi Transpersonal, mengungkapkan bahwa ada 3 jenis suara hati, yaitu :\r\n1. Suara hati dialami sebagai fragmen dari diri sendiri\r\n2. Suara hati dialami sebagai dialog yang menyediakan bimbingan untuk pertumbuhan individu\r\n3. Suara hati dialami sebagai channel yang terbuka antara diri kita dengan tuntunan dari Tuhan\r\n.\r\n.\r\nMorin (2013) dalam bukunya, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Suara Hati ; Jendela Kesadaran DiriÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â, membagi fungsi-fungsi Suara Hati yaitu dalam :\r\n- Merencanakan Tugas\r\n- Mengorganisir Ingatan\r\n- Memotivasi Diri\r\n- Memecahkan Masalah\r\n- Mengatur Waktu\r\n- Membantu Proses Berpikir\r\n- Melatih Bahan Pembicaraan\r\n- Aktif memandu kita saat Membaca, Menulis dan Berhitung\r\n- Membantu Proses Belajar\r\n- Mengendalikan Emosi\r\n- Menentukan apa yang akan dilakukan\r\n- Mensensor Diri\r\n- Mengulang Pembicaraan yang telah lewat\r\n.\r\n.\r\nPada fungsi diatas kita bisa mencermati bahwa Suara hati letaknya lebih tinggi dibanding emosi, karena suara hati yang aktif, bisa membantu kita dalam mengendalikan emosi. Pun dalam berbicara, suara hati bisa membantu kita mensensor kata-kata, agar kita bisa berbicara dengan lebih baik. Saat bekerja pun, suara hati membantu kita untuk selalu mawas diri, patuh peraturan, patuh pada jadwal kerja, memenuhi tenggat waktu, dan meningkatkan kualitas dari pekerjaan yang kita lakukan, dan membantu pemecahan masalah serta pengambilang keputusan, agar berjalan dengan lebih matang dan tepat sasaran.\r\n.\r\n.\r\nSuara hati adalah sesuatu yang bersifat built in, ada pada semua manusia. Morin (2007) menjelaskan bahwa Bagian otak yang mengaktifkan Suara Hati (inner speech) adalah bagian hemisfer kiri, tepatnya di Inferior Frontal Gyrus. Hemisfer kiri ini terbukti lebih aktif pada partisipan saat diminta untuk mengucapkan kalimat atau kata di dalam hati. Penelitian pada pasien dengan kerusakan otak mendukung pendapat ini : Kerusakan pada area Broca (pusat Bahasa di otak) di hemisfer kiri, atau kerusakan di posterior kiri dan daerah anterior frontalis mengganggu proses ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Suara HatiÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â ini.', '2017-02-17 15:29:57', 11, NULL, NULL, NULL, NULL),
	(109, 'SEBUAH WAY OF LIFE', '2017-04-21', 'https://qx.esq165.co.id/upload/sementara/img/file_58a6b6fb3c984.jpg', 'Hidup harus punya prinsip, pegangan atau way of life.', 'Hidup harus punya prinsip, pegangan atau way of life. \r\nAda sebuah konsep untuk membangun mindset kehidupan, yang jika dibaca setiap hari, maka kita akan terasa energinya.\r\n.\r\n.\r\nEnergi tersebut akan mengalir dalam diri kita, membuat kita senantiasa berpikir luas, penuh cinta kasih dan melayani,\r\n.\r\n.\r\nBerpikir visioner, memiliki integritas : yang membuat kita mengambil jalan yang lebih mulia, dan membuat kita senantiasa mencoba menghindar saat tersesat untuk kembali.', '2017-02-17 15:41:18', 11, NULL, NULL, NULL, NULL),
	(110, 'Refleksi Kehidupan dari Kisah 4 Lilin', '2017-04-23', 'https://qx.esq165.co.id/upload/sementara/img/file_58aa64993c52d.jpg', 'Hal yang akan selalu hidup adalah HARAPAN yang bersemayam di dalam hati kita', 'Pernahkah Anda mendengar tentang kisah 4 lilin? Kisah menarik yang bisa menjadi refleksi kita memandang kehidupan ini. Serta mengajarkan satu pelajaran menarik yang bisa memotivasi kita menjalani hari-hari. Apa itu?\r\n.\r\nDi malam yang gelap dalam sebuah kamar, ada 4 lilin menyala dengan batangnya yang terus meleleh dan sedikit demi sedikit habis. Suasana begitu sunyi, sehingga terdengarlah percakapan mereka.\r\n.\r\nLilin pertama berkata,"Aku adalah Perubahan."\r\n"Namun manusia tak mampu berubah jadi lebih baik. Mereka terlena dengan kesenangan mereka dan lebih suka bermalas-malasan. Jadi lebih baik aku memadamkan diri saja," katanya. Sedikit demi sedikit, lilin pertama pun padam.\r\n.\r\nGiliran lilin kedua berkata, "Aku adalah Iman."\r\n"Sayang aku tak berguna lagi. Manusia tak mau mengenalku. Beribadah pun tak mau tepat waktu, hanya karena mementingkan hobi dan urusan dunia mereka. Jadi, tak ada gunanya aku tetap menyala," tuturnya. Begitu selesai bicara, tiupan angin memadamkannya.\r\n.\r\nDengan sedih lilin ketiga berkata, "Aku adalah Cinta."\r\n"Tak mampu lagi aku tuk tetap menyala. Manusia hanya mencintai kekasih dan dirinya saja. Terkadang mereka mengabaikan orang-orang yang jelas menyayanginya, yakni orang tua dan keluarga," terangnya. Tak menunggu waktu lama, padamlah lilin ketiga.\r\n.\r\nTanpa diduga, seorang anak masuk ke dalam kamar. Dia melihat melihat ketiga lilin telah padam. Karena takut kegelapan, anak itu berkata:\r\n"Apa yang terjadi? Kalian harus tetap menyala!!! Aku takut akan kegelapan."\r\nDia pun menangis tersedu-sedu.\r\n.\r\nLalu, dengan terharu lilin keempat berkata, "Jangan takut. Jangan menangis. Selama aku masih ada dan menyala, kita bisa menyalakan ketiga lilin lainnya. Akulah... HARAPAN."\r\n.\r\nDengan keteguhan hati dan mata bersinar, sang anak mengambil lilin harapan itu. Lalu dia menyalakan kembali ketiga lilin lainnya.\r\n.\r\nYa, hal yang akan selalu hidup adalah HARAPAN yang bersemayam di dalam hati kita. Semoga masing-masing dari kita bisa jadi seperti anak itu. Dalam situasi apapun bisa menghidupkan kembali IMAN, CINTA, dan PERUBAHAN dengan kekuatan HARAPAN-nya.\r\n.\r\nJaga selalu HARAPAN Anda...', '2017-02-20 10:41:33', 11, NULL, NULL, NULL, NULL),
	(111, 'SUDAHKAH ANDA MERDEKA?', '2017-04-24', 'https://qx.esq165.co.id/upload/sementara/img/file_58aa6611a2d90.jpg', 'Saatnya menjadi manusia yang merdeka. Memerdekakan diri dari penghambaan terhadap sesama manusia.', 'Pagi !!!\r\n.\r\n.\r\nAda seorang laki-laki asing yang berjalan melewati pasar. Tiba-tiba, seorang pengemis tua menadahkan tangannya pada lelaki tersebut. Pengemis tua itu telah bertahun-tahun mengemis di pasar dan selama itu pula ia menduduki sebuah peti kayu. Lelaki asing tersebut kemudian menghentikan langkahnya dan sejenak memperhatikan si pengemis tua tersebut. Lelaki asing itu kemudian bertanya ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Pak Tua, pernahkan engkau membuka peti yang engkau duduki?ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â Si Pak Tua itu pun menunjukkan ekspresi yang kaget. Ia memang tak pernah membuka peti kayu tersebut selama bertahun-tahun. Dengan penuh penasaran, dia kemudian membuka peti yang didudukinya dan betapa terkejutnya, ketika ia melihat sebongkah emas ada di dalam peti tersebut.\r\n.\r\nKisah singkat di atas memberikan pelajaran pada kita, bahwa selama ini kita sering melupakan sesuatu yang sudah kita miliki, dan selalu pula membandingkan dengan apa yang tidak kita miliki. Betul apa betul Sahabat ESQ? Siapa yang sering begitu???\r\n.\r\nKita seringkali merasa bahwa diri kita tidak beruntung dan selalu melihat apa yang dipunya oleh orang lain. Padahal Sahabat ESQ, rejeki yang kita miliki dan potensi yang kita punya itu sama dengan apa yang dipunya oleh orang lain.\r\n.\r\nKita lebih sering menadahkan tangan pada orang lain dibandingkan menadahkan tangan pada Tuhan, Sang Pemilik segalanya. Kita cenderung lebih sering meyakini kekuatan yang terpampang di depan mata, dibandingkan dengan kekuatan Tuhan yang tak bisa ditandingi oleh kekuatan manapun.\r\n.\r\nDiri atau hati kita terkungkung akibat dari ulah diri kita sendiri. Hati merasa terpenjara karena merasa tak pernah puas dengan apa yang sudah di dapat. Merasa diri tidak merdeka karena selalu melihat apa yang dipunya oleh orang lain. Akibatnya apa? Timbul rasa iri yang menyebakan Anda berpotensi kena penyakit hati.\r\n.\r\nAdakah Sahabat ESQ yang merasa tidak diri merdeka karena sikap iri dan dengki pada orang lain?\r\n.\r\nBagaimana cara memerdekakan dan mengendalikan dari belenggu-belenggu negative yang dapat mengotori hati dan pikiran? Sesungguhnya, kendali untuk menghadapi apapun ada di dalam diri kita sendiri. Kendali itu adalah keberadaan Allah di dalam hati kita.\r\n.\r\nSemestinya, dengan keyakinan bahwa Tuhan ada di hati kita, pantang bagi kita untuk merasa bahwa diri kita lebih rendah dari orang lain. Kita sejajar dengan orang lain, bahkan dengan pejabat tinggi sekalipun. Buktikan kemampuan Anda dengan potensi yang diberikan.\r\n.\r\nSaatnya menjadi manusia yang merdeka. Memerdekakan diri dari penghambaan terhadap sesama manusia. Beridiri sama tinggi dan saing berkompetisi untuk membuktikan kompetensi terbaik. Inilah saatnya Anda terbebas dari belenggu pengemis, terbebas dari penindasan atas sesama manusia. Inilah saatnya berdiri sama tinggi.', '2017-02-20 10:49:09', 11, NULL, NULL, NULL, NULL),
	(112, 'JANGAN MEMBUAT DIRI ANDA GAGAL', '2017-04-22', 'https://qx.esq165.co.id/upload/sementara/img/file_58aaaf2de497c.jpg', 'Dalam benak para pemenang, mereka hanya dapat SUKSES atau mereka hanya dapat BELAJAR sesuatu.', 'JANGAN MEMBUAT DIRI ANDA GAGAL\r\n.\r\nDalam benak para pemenang, mereka hanya dapat SUKSES atau mereka hanya dapat BELAJAR sesuatu. Keyakinan itulah yang memberi mereka keberanian untuk bermimpi dan mengejar impian itu meski harus menghadapi tantangan-tantangan dan halangan-halangan.\r\n.\r\nDalam benak para pemenang, kegagalan hanya terjadi saat mereka berhenti atau jika mereka sama sekali tidak berusaha. Selama Anda memberikan usaha terbaik dan menolak untuk berhenti, Anda tidak mungkin GAGAL!\r\n.\r\nSeperti orang lain, para pemenang juga takut akan kegagalan. Namun, karena cara mereka mendefinisikan kegagalan, mereka tidak pernah mengizinkan diri mereka untuk berhenti atau tidak melakukan usaha terbaik untuk mendapatkan impian mereka.\r\n.\r\nJadi, apakah Anda akan mengizinkan diri Anda untuk menjadi orang yang GAGAL?', '2017-02-20 16:00:02', 11, '2017-03-10 09:50:30', 11, NULL, NULL),
	(113, 'FILOSOFI "TRUK SAMPAH"', '2017-04-25', 'https://qx.esq165.co.id/upload/sementara/img/file_58aab05924b1a.jpg', 'Orang sukses tidak akan pernah membiarkan truk sampah mempengaruhi fokus mereka.', 'Sudah berapa banyak tips sukses yang Anda baca? Dari sekian tips tersebut, pasti Anda akan menemukan kata "fokus". Ya, kita harus fokus pada target yang dituju untuk berhasil, hingga akhirnya sukses.\r\n.\r\nNamun, fokus dalam perjalanan meraih sukses itu banyak rintangan. Ada saja hal-hal yang membuat kita hilang konsentrasi, mudah emosi, dan sebagainya.\r\n.\r\nContohnya, apa yang Anda lakukan dan rasakan jika bertemu dengan supir angkutan yang ugal-ugalan, pelayan resto yang kurang sopan, atasan yang marah-marah, atau teman yang semaunya sendiri?\r\n.\r\nMarah, kesal, bad mood di saat seperti itu adalah hal WAJAR. Namun, Anda perlu tahu bahwa ciri orang sukses ialah seberapa cepat dirinya kembali FOKUS pada apa yang PENTING, ketika hambatan itu terjadi.\r\n.\r\nJika terasa sulit, mari belajar sejenak dari filosofi \'truk sampah\' yang diajarkan oleh seorang supir taksi berikut ini.\r\n.\r\nSuatu hari saya naik taksi dengan supir yang mengemudi sangat baik. Dia mengemudi di jalurnya dan sesuai aturan. Namun tiba-tiba sebuah mobil melesat dan berhenti mendadak di hadapan kami. Supir taksi langsung menginjak rem dan hampir saja menabrak mobil tersebut.\r\n.\r\nLalu, pengemudi mobil tersebut mengeluarkan kepalanya dan berteriak-teriak dengan kata-kata kasar kepada kami. Supir taksi hanya tersenyum sambil melambaikan tangan pada orang itu.\r\n.\r\nSaya heran dan bertanya, "kenapa Anda tersenyum? Orang tadi yang salah dan hampir merusak mobilmu. Kita juga hampir celaka gara-gara dia."\r\n.\r\nSambil tersenyum sopir taksi menjawab, "Banyak orang tampak seperti TRUK SAMPAH. Mereka berjalan dengan banyak sampah seperti frustasi, amarah, dan kekecewaan. Ketika sampah-sampah itu sudah tak muat lagi, mereka perlu tempat untuk membuangnya. Tanpa Anda sadari, mereka juga membanjiri sampah-sampah itu pada Anda. Jadi, bila seseorang ingin membuang sampah, jangan diambil hati. Tersenyum saja dan beroda semoga dia cepat pulih. Dengan begitu Anda bisa melanjutkan pekerjaan Anda dan hati terasa senang."\r\n.\r\nSebuah pelajaran berharga yang menunjukkan bahwa orang sukses tidak akan pernah membiarkan truk sampah mempengaruhi fokus mereka. Kita punya pilihan untuk fokus pada tujuan atau terpengaruh oleh lingkungan yang kurang baik.\r\n.\r\nDalam ilmu Quantum Excellence, pelajaran dari supir taksi tersebut disebut FLIP SWITCHING. Ilmu yang menunjukkan bahwa satu pikiran positif jauh lebih powerful, dibanding 1000 pikiran negatif. Ilmu yang bisa melatih disiplin diri meraih kesuksesan hidup.\r\n.\r\nSemoga kita senantiasa diberi kekuatan, kesehatan, dan pikiran yang jernih untuk bisa menggapai impian.', '2017-02-20 16:03:42', 11, NULL, NULL, NULL, NULL),
	(114, 'USAHA YANG TAK PERNAH MENGKHIANATI HASIL .', '2017-04-26', 'https://qx.esq165.co.id/upload/sementara/img/file_58abb1e7dba71.jpg', 'Usaha adalah sesuatu yang kita kerahkan untuk mendapatkan sesuatu yang menjadi tujuan hidup kita.', 'Sahabat ESQ, Pagi!!\r\n.\r\nPerjalanan panjang saya selama kurang lebih 16 tahun bersama ESQ merupakan salah satu pengalaman sekaligus pelajaran terbaik bagi saya. Melalui ESQ, saya bisa mengenal banyak orang-orang hebat yang tumbuh dengan karakter unggul di dalamnya.\r\n.\r\nTentunya, untuk menjadi manusia unggul dan berkarakter dilalui dengan proses yang tak mudah. Membangun mimpi untuk menjadi ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“orangÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â tak semulus yang orang kira. Berawal menjadi staff pengajar di sebuah perguruan tinggi negeri di Bali. Selama 5 tahun menjadi pengajar itulah, saya banyak mengamati bahwa kebanyakan mata kuliah tidak mengajarkan pentingnya kecerdasan emosi yang mampu melahirkan sikap-sikap yang penuh integritas dan kemandirian. Betapa rendahnya kesadaran dan apresiasi tentang kecerdasan emosi dalam pendidikan pada saat itu.\r\n.\r\nDari situ, saya beralih untuk membangun perusahaan yang bergerak di bidang telekomunikasi. Permasalahan yang saya hadapi saat itu adalah kurangnya komitmen, integritas dan semangat dari para karyawan. Hal tersulit saat membangun perusahaan adalah bukan ketika bagaimana memberitahu karyawan apa yang harus mereka kerjakan, namun bagaimana mengajarkan mereka untuk punya kercerdasan emosi (EQ) dan kecerdasan spiritual )SQ), dan memberi pemahaman bahwa keberadaan EQ dan SQ sangat penting bagi kehidupan mereka kelak\r\n.\r\nMengambil hikmah dari semua pengalaman yang sudah saya rasakan, saya berkesimpulan bahwa EQ dan SQ memang mutlak diperlukan untuk mencapai prestasi tertinggi dalam hidup.\r\n.\r\nKeberhasilan Anda saat ini, tak lepas dari usaha keras Anda untuk menuju ke ke arah yang gemilang. Usaha adalah sesuatu yang kita kerahkan untuk mendapatkan sesuatu yang menjadi tujuan hidup kita. Usaha yang dekat dengan keberhasilan tidak harus melulu disangkutpautkan dengan sesuatu yang besar. Usaha yang kecil, namun jika dilakukan secara terus menerus akan menghasilkan sebuah keberhasilan yang luar biasa. Keberhasilan bukan sesuatu yang saya dapatkan secara instan, namun merupakan keberhasilan yang memerlukan usaha yang melalui proses panjang.\r\n.\r\nSelain usaha yang tak ada hentinya, doa juga tak kalah penting bagi perjalanan hidup dan karier saya. Doa merupakan salah satu faktor yang mempengaruhi keberhasilan saya membangun ESQ. Doa memang sesuatu yang mungkin akan sulit untuk dibuktikan.', '2017-02-21 10:22:39', 11, NULL, NULL, NULL, NULL),
	(115, 'KUNCI SUKSES WANITA ADA PADA KEBAHAGIAAN', '2017-04-28', 'https://qx.esq165.co.id/upload/sementara/img/file_58abb425e36d7.jpg', 'Jika bahagia, sukses pun akan menghampiri dengan sendirinya.', 'Apakah saat ini Bunda dan Sista merasa tidak bahagia? Jika jawabannya ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“YAÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â, Bunda dan Sista harus simak kisah ini sampai habis. Jika jawaban Anda ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“TIDAKÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â, Bunda dan Sista pun juga disarankan untuk menyimak kisah ini sampai habis agar Bunda dan Sista lebih bersyukur.\r\n.\r\nSeorang wanita bernama Lizzie, berusia 24 tahun yang di diagnose oleh dokter mengidap sebuah sindrom langka bernama neonatal progeroid atau istilah lainnya sindrom percepatan penuaan dini. Sindrom ini yang menyebabkan tubuh\r\nLizzie tidak bisa menyimpan lemak.\r\n.\r\nHal itu menyebabkan Lizzie memiliki berat badan yang sangat kurus.Yang lebih menyedihkan lagi, kini Lizzie juga kehilangan salah satu penglihatannya. Namun apa yang terjadi? Lizzie tak pernah sekalipun putus asa. Bahkan, ia pun tak percaya ketika ia dinobatkan menjadi salah satu sumber inspirasi tentang arti kecantikan.\r\n.\r\nSejak kecil, Lizzie sudah menjadi korban bullying. Lizzie mengaku sempat depresi, namun rasa bersyukurnya akan hidup mampu mengalahi rasa depresinya itu. Di titik rendah dalam hidupnya, dia memutuskan untuk bangkit dan tidak boleh menyerah karena ejekan dan cemoohan yang tiap hari ia terima.\r\n.\r\nDengan dukungan sahabat dan keluarganya, Lizzie akhirnya menemukan kekuatan untuk bangkit dan terus memotivasi dirinya sendiri. Dia pun melakukan berbagai macam riset berbagai hal tentang penyakit yang derita oleh nya. Pada akhirnya, Lizzie pun menemukan kepercayaan dirinya sendiri.\r\n.\r\nLizzie berhasil keluar dari zona yang membelenggu dirinya untuk menuju kesuksesan. Keinginan Lizzie untuk bahagia lebih besar dari belenggu yang selama ini mengurung dirinya.\r\n.\r\nKunci sukses wanita bermula dari KEBAHAGIAAN. Jika bahagia, sukses pun akan menghampiri dengan sendirinya.\r\n.\r\nApakah Bunda dan Sista ingin sukses dan bahagia seperti Lizzie?', '2017-02-21 10:30:37', 11, NULL, NULL, NULL, NULL),
	(116, 'BELAJAR JADI ORANGTUA', '2017-04-29', 'https://qx.esq165.co.id/upload/sementara/img/file_58abb51d61bcc.jpg', 'ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“Didiklah anakmu sesuai dengan', 'Pagi..Bunda, Ayah dan para calon orang tua dimanapun Anda berada. Bagaimana kesibukan di hari ini? Semoga Allah selalu limpahkan rahmat dan berkahnya untuk kita semua.\r\n.\r\nOh iya, bagaimana kabar anak-anak di rumah? Semoga mereka juga tumbuh menjadi anak-anak sholeh dan sholeha ya. Pastinya setiap orangtua menginginkan anak-anaknya tumbuh menjadi orang yang sukses tanpa melupakan kaidah-kaidah agama kan? Lalu, upaya apa yang sudah Bunda dan Ayah lakukan untuk mencapai tahap tersebut?\r\n.\r\nSeperti Bunda dan Ayah ketahui, untuk menjadi pilot yang pandai menerbangkan pesawat, seseorang harus belajar menjadi pilot yang baik di sekolah pilot. Untuk menjadi, penembak yang jitu, seseorang juga harus berlatih menembak setiap hari agar tembakan tidak salah sasaran. Begitu juga untuk menjadi orangtua. Untuk menjadi orangtua pun harus dilalui dengan tahap belajar. Tapi, apakah menjadi orangtua ada sekolahnya? Tentu saja tidak.\r\n.\r\nMenjadi orangtua sekaligus membesarkan anak-anak tumbuh menjadi orang yang yang berakhlak mulia bukan pekerjaan yang mudah. Segala sesuatunya harus dipersiapkan dengan matang, agar anak-anak kita bisa tumbuh sesuai dengan apa yang kita harapkan. Bukan hanya sekedar status sebagai orangtua, namun yang lebih penting adalah orangtua harus memahami pola asuh yang seperti apa yang akan diterapkan ke anak-anak. Peran orangtua di keluarga adalah sebagai pendukung untuk mendorong perkembangan anak. Tentu saja, ini bukan hanya tugas Bunda semata. Namun juga peran Ayah dalam keluarga sangat penting, yaitu sebagai sosok yang dihormati sebagai pemimpin di dalam keluarga.\r\n.\r\nPengetahun untuk menjadi orangtua bukanlah sebuah pelajaran yang bisa kita dapatkan di bangku sekolah. Faktanya, dunia yang terus berubah dan ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“memaksaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â kita untuk selalu memperbaharui ilmu dan kemampuan menjadi orang tua. \r\n.\r\nKita tidak perlu menjadi orang lain untuk menjadi orang tua yang baik. Mulailah dari hal yang kita bisa dan biasa kita lakukan. Kita adalah orang tua yang hebat kalau kita bisa membagikan apapun yang kita miliki untuk dipelajari anak kita. Untuk hal-hal yang kita tidak bisa, kita bisa minta bantuan orang lain.\r\n.\r\nIntinya, belajar menjadi orangtua yang baik adalah kewajiban setiap orangtua. Untuk menghasilkan anak-anak baik dan berkarakter unggul, tentu saja dimulai dengan menjadi orangutan yang baik dan punya karakter unggul juga.\r\n.\r\nUntuk menjadi karakter yang unggul, apa langkah yang sudah Bunda dan Ayah tempuh?', '2017-02-21 10:36:13', 11, NULL, NULL, NULL, NULL),
	(117, 'TAKE ACTION', '2017-04-30', 'https://qx.esq165.co.id/upload/sementara/img/file_58ad48af1fc1c.jpg', 'Jangan menunggu SEMPURNA untuk bisa SUKSES mulailah Kesuksesan Anda dari Sekarang !', 'Percayakah Anda bahwa Tuhan sanggup mengubah KEKURANGAN kita menjadi sebuah KEKUATAN yang mungkin tidak kita duga? Asal kita tidak menyerah dan harus jeli melihat peluang di sekitar kita, menambah wawasan dan menggali potensi kita.\r\n.\r\nInilah contoh nyatanyaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\nSuze Orman adalah seorang Pengusaha, Penulis Buku dan Motivator Wanita kelas dunia. Namun siapa sangka, pembicara kelas dunia ini sebenarnya adalah orang yang selalu mengalami kesulitan bicara sejak kecil hingga beranjak dewasa. Suze mengalami kesulitan membaca huruf "R, S, & T", bahkan sampai sekarang Suze masih kesulitan mengucapkan kata "Fear", "Fair", "Bear" & "Beer".\r\n.\r\nAwalnya Suze sangat malu dengan kekurangannya itu, namun dia sadar jika tidak segera di perbaiki, maka dia akan kesulitan untuk belajar dan sekolah. Suze pun sering mendapat nilai paling rendah dalam pelajaran bahasa. Ketika dia hendak masuk kuliah Kedokteran, pihak kampus menolaknya dan menyarankan dia masuk jurusan Kerja Sosial, Suze menyetujuinya dan dia berhasil menyelesaikan studinya dengan gelar Bachelor of Art di Bidang Kerja Sosial.\r\n.\r\nSambil bekerja dan mengelola restorannya, ia ikut pelatihan jadi Akuntan di Merrill Lynch, Perusahaan Sekuritas, kemudian dia pindah dan diangkat sebagai Wakil Presiden di Bidang Investasi di Prudential Bache Securities tahun 1983.\r\n.\r\nTahun 1987 ia keluar dari Prudential dan mendirikan Suze Orman Financial Group, di Emeryville, California. Sejak itu ia mulai rajin menulis buku keuangan dan bukunya selalu memecahkan rekor sebagai buku dengan penjualan terbanyak. Dari sana ia di undang untuk menjadi pembicara di berbagai kesempatan.\r\n.\r\nApa hikmahnya?\r\n.\r\nSetiap orang pasti punya KEKURANGAN, namun kita tidak perlu menunggu SEMPURNA agar kita bisa SUKSES. Kita belajar dari kekurangan kita dan TAKE ACTION sekarang juga, jangan biarkan kekurangan kita membatasi kita untuk melakukan hal-hal yang besar dalam hidup kita...!', '2017-02-22 15:17:06', 11, NULL, NULL, NULL, NULL),
	(118, 'KESUKSESAN MILIK SIAPA SAJA, TAK TERKECUALI HABIBIE AFSYAH', '2017-04-27', 'https://qx.esq165.co.id/upload/sementara/img/file_58ad49f94ee32.jpg', '\'Kelemahanku adalah kekuatanku untuk sukses\'', 'Adakah Bapak Ibu yang mengenal Habibie Afsyah? Seorang internet marketer muda sukses di Indonesia. Sosok hebat yang membuktikan bahwa sukses itu milik siapa saja, tak terkecuali dirinya yang memiliki keterbatasan.\r\n.\r\nYa, anak bungsu dari 8 bersaudara ini memiliki penyakit bawaan bernama muscular dytrophy. Penyakit yang merenggut fungsi motorik tubuh, hingga tak bisa digerakkan.\r\n.\r\nNamun Habibie tak pernah berkecil hati dan mendapatkan dukungan luar biasa dari Ibundanya. Awal kesuksesannya pun bermula dari dorongan ibundanya untuk ikut kursus internet marketing. Bahkan Orangtuanya rela menjual kendaraan, untuk membantu kelancaran belajar Habibie.\r\n.\r\nHabibie sempat enggan menjalankan kursus internet marketing tersebut. Namun, semangatnya terpicu saat akhirnya berhasil menjual produknya di situs Amazon.\r\n.\r\nDengan telaten Habibie menjalankan usahanya dan terus belajar, hingga akhirnya kini sukses besar. Habibie menunjukkan pada kita bahwa keterbatasan dan usia yang masih belia, bukan kendala meraih cita-cita.\r\n.\r\n"kalau saya yang punya keterbatasan seperti ini saja bisa, Anda pun pasti juga bisa. Kemandirian dan kesuksesan adalah kodrat Anda," terang Habibie.\r\n.\r\nKini, Habibie pun aktif sebagai penulis, pembicara, dan pebisnis yang memotivasi siapa pun untuk bisa merengkuh kesuksesan. \'Kelemahanku adalah kekuatanku untuk sukses\' jadi salah satu slogan hidupnya. Jika Habibie saja bisa, bagaimana dengan Anda?\r\n.\r\nYuk pacu lagi semangat dan susun lagi strategi untuk meraih impian. Semoga kita mampu melewati segala rintangan dan akhirnya sukses seperti Habibie.', '2017-02-22 15:22:16', 11, NULL, NULL, NULL, NULL),
	(119, 'Ingin menjadi teman curhat anak? Simak tips berikut ini', '2017-05-01', 'https://qx.esq165.co.id/upload/sementara/img/file_58ad583ea9814.jpg', 'Tunjukkan keingintahuan dan rasa penasaran kita pada apa yang mereka ceritakan', 'Bunda dan Ayah, seringkali kita merasa frustrasi menghadapi anak tingkah laku anak yang terkadang tidak kita mengerti, Sikap anak yang tidak jelas tersebut biasanya dipicu oleh permasalahan yang sedang mereka hadapi. Namun, yang menjadi kendalanya adalah mereka tidak mau menceritakan apa yang mereka alami kepada kita sebagai orangtua.\r\n.\r\nHal ini seringkali terjadi pada anak-anak remaja. Betul kan? Agar anak kita tidak bercerita kepada orang yang tidak tepat, alangkah baiknya bila kita bisa menjadi teman cerita bagi anak kita. Bagaimana cara yang tepat untuk membujuk anak agar mau berbicara tentang masalahnya kepada kita?\r\n.\r\nBunda dan Ayah bisa simak tips berikut :\r\n.\r\n1. Selalu ada buat mereka\r\n\r\nAnak-anak cukup peka terhadap aktivitas kita sebagai orangtua. Bila orangtua terlihat sibuk, mereka pasti akan berusaha tahu diri. Namun, bila mereka melihat kita punya waktu untuk kita sendiri dan untuk mereka, mereka akan merasa nyaman berbicara dengan kita.\r\n.\r\n2. Dilarang memaksa\r\n\r\nSemakin kuat Bunda dan Ayah memaksa mereka untuk berbicara atau bercerita, mereka akan semakin menutup diri. Bila kita bersikap lebih santai, mereka mungkin akan lebih mudah bercerita.\r\n.\r\n3. Tidak bersikap menghakimi\r\n\r\nBila Bunda dan Ayah bersikap menghakimi pada saat mereka berbicara, di lain waktu mereka pasti tidak berminat untuk menceritakan masalahnya.\r\nTunjukkan keingintahuan dan rasa penasaran kita pada apa yang mereka ceritakan dan usahakan untuk tidak menjudge "benar" atau "salah".\r\n.\r\n4. Bersikap Layaknya Sahabat\r\n\r\nBila kita bisa bersikap sebagai orangtua dan sahabat pada saat mereka bercerita, ini dapat memelihara keinginan anak untuk selalu berbagi cerita dengan Anda. Ingat, jangan bersikap kaku dan sok jaga wibawa ya.\r\n\r\nSemoga bisa membantu Bunda dan ayah di luar sana yang sedang kebingungan bagaimana caranya menjadi sahabat terbaik bagi anak.', '2017-02-22 16:24:46', 11, NULL, NULL, NULL, NULL),
	(120, 'MENUMBUHKAN SIKAP SOPAN SANTUN KEPADA ANAK', '2017-05-02', 'https://qx.esq165.co.id/upload/sementara/img/file_58ae928999c07.jpg', 'Dibalik seorang anak yang selalu bersikap hormat, berbudi pekerti yang baik dan menghargai orang lain, ada sosok orangtua yang memiliki keteladanan dalam sikap mulia tersebut.', 'Selamat sore Bunda dan AyahÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦\r\nSemoga selalu semangat ya dalam menjalani aktivitas.\r\n.\r\nMembicarakan tentang keluarga, apalagi anak seakan tak ada habisnya untuk dibahas. Ada saja sesuatu hal yang menarik untuk dipahami lebih dalam. Salah satunya adalah sikap anak-anak.\r\n.\r\nSebagai orangtua, pastinya kita sudah mengajarkan norma-norma yang baik pada anak kita, agar mereka pun tumbuh menjadi anak-anak yang penuh sopan santun terhadap orang lain.\r\n.\r\nDi Zaman modern seperti ini, sikap sopan santun seakan menjadi hal langka. Betul kan?\r\n.\r\nSikap hormat dan santun anak-anak tidak akan tumbuh begitu sja. Tidak akan tumbuh juga melalui ceramah atau dengan paksaan. Untuk menumbuhkan budi pekerti yang baik atau akhlak yang baik itu hanya dengan keteladanan atau contoh dari orang-orang sekitarnya. Sehingga, anak-anak akan tumbuh menjadi pribadi yang bermurah hati dan penuh rasa hormat. Orangtua yang menghormati anak akan berdampak baik terhadap tumbuh kembang anak saat dewasa. Menghormati anak mempunyai arti yang luas, bukan saja dalam berbicara maupun berperilaku tapi juga menghormati hak-haknya dan privasinya.\r\n.\r\nIstilah menghormati anak mungkin terkesan janggal karena biasanya anak dituntut harus menghormati orang dewasa. Namun, bagaimana anak-akan bersikap sopan santun, jika merekapun tidak pernah merasakan penghormatan terhadap dirinya.\r\n.\r\nDibalik seorang anak yang selalu bersikap hormat, berbudi pekerti yang baik dan menghargai orang lain, ada sosok orangtua yang memiliki keteladanan dalam sikap mulia tersebut.\r\n.\r\nSetuju???', '2017-02-23 14:45:36', 11, NULL, NULL, NULL, NULL),
	(121, 'Kisah 3 Tukang Bangunan Memandang Kehidupan', '2017-05-03', 'https://qx.esq165.co.id/upload/sementara/img/file_58ae935e8f8e7.jpg', 'Perbedaan yang menunjukkan kualitas seorang manusia di muka bumi.', 'kisah 3 tukang bangunan memandang pekerjaan dalam kehidupannya di bawah ini.\r\n.\r\n.\r\nDi sebuah desa, ada seorang anak kecil yang sedang berjalan-jalan. Anak itu melintasi sebuah lokasi yang akan dibangun gedung sekolah. Anak itu melihat ada 3 orang tukang bangunan sedang bekerja.\r\n.\r\n"Pak, sedang mengerjakan apa?" tanya anak itu menghampiri tukang pertama.\r\n.\r\n"Kamu bisa lihat sendiri. Saya ini seorang tukang bangunan. Saya sedang mengerjakan pekerjaan saya sebagai tukang bangunan," jawabnya.\r\n.\r\nLalu anak itu menghampiri orang kedua dan bertanya hal yang sama, "Pak sedang mengerjakan apa?"\r\n.\r\n"Saya sedang membantu sekolah ini membuat gedung sekolah yang layak," jawab tukang kedua.\r\n.\r\nMasih ingin tahu, anak itu juga menghampiri tukang ketiga, "Pak sedang mengerjakan apa?"\r\n.\r\nDengan tersenyum tukang ketiga menjawab, "Saya sedang membangun mimpi anak-anak di desa ini, supaya mereka berani bermimpi lebih tinggi dan meraih cita-citanya. Sehingga nantinya mereka membawa manfaat di masyarakat."\r\n.\r\n.\r\nApa pendapat sahabat ESQ membaca kisah tersebut? Manakah dari ketiga tukang tersebut yang memiliki jawaban paling berkesan dan menggetarkan jiwa menurut Anda?\r\n.\r\nKita mungkin melakukan pekerjaan yang sama persis dengan rekan kerja kita. Kita mungkin melakukan bisnis serupa dengan pesaing kita.\r\nNamun visi dan misi melakukannya pasti berbeda-beda.\r\n.\r\nPerbedaan yang menunjukkan kualitas seorang manusia di muka bumi.\r\nKualitas yang berisi keseimbangan antara IQ, EQ dan SQ.\r\nKeseimbangan yang akan mengantar manusia pada kebahagiaan, ketentraman, dan semangat sejati.\r\n.\r\nJadi, bukan pada apa jenis pekerjaan Anda. Namun lebih pada bagaimana Anda memandang pekerjaan itu dan memiliki visi-misi atasnya.\r\n.\r\nLalu, apa pekerjaan Sahabat ESQ dan untuk apa Anda bekerja?', '2017-02-23 14:49:07', 11, NULL, NULL, NULL, NULL),
	(122, 'PESAN DIBALIK BAJU BEKAS AYAH', '2017-05-04', 'https://qx.esq165.co.id/upload/sementara/img/file_58afa91dd9db0.jpg', '"Sama sama manusia, entah hitam putih tinggi pendek, namun yang membedakan nilainya adalah kesungguhannya dalam berusaha."', 'Dari setiap apa yang dikerjakan, pernahkah sahabat ESQ berpikir tentang pelajaran yang diperoleh? Pelajaran yang pasti ada, namun hanya sebagian kecil orang yang mampu menelaahnya.\r\n.\r\nContohnya seperti kisah seorang bapak dan anak berikut ini.\r\n.\r\nAda seorang anak berkulit hitam dan lahir di daerah kumuh yang ada di New York. Dia melewati hidupnya di lingkungan yang miskin dan penuh diskriminasi. Namun satu pelajaran hidup dari ayahnya, membuat si anak survive dan sukses. Apa itu?\r\n.\r\nSuatu hari, sang ayah memberikan sehelai pakaian bekas kepada anak itu dan bertanya, "Menurutmu berapa nilai pakaian ini?"\r\n.\r\n"Mungkin 1 dolar ayah," jawabnya.\r\n.\r\n"Bisakah kamu menjualnya seharga 2 dollar? Jika berhasil berarti kamu telah membantu orang tuamu," kata ayah.\r\n.\r\n"Aku akan mencobanya."\r\n.\r\nLalu anak itu membawa pakaian itu ke stasiun kereta bawah tanah dan menjual pakaian bekasnya selama lebih dari 6 jam. Akhirnya, ia berhasil menjual pakaian itu seharga 2 dollar dan berlari dengan riang untuk pulang ke rumah.\r\n.\r\nKemudian, ayah kembali menyerahkan sepotong pakaian bekas kepada anaknya. "Nak, coba kamu jual pakaian bekas ini seharga 20 dollar," kata ayah.\r\n.\r\n"Bagaimana mungkin ayah? Pakaian ini mungkin hanya laku 2 dollar," ucap anak itu.\r\n.\r\nAyah tersenyum dan berkata, "kenapa tak kamu coba dulu."\r\n.\r\nAnak itu berpikir sejenak dan akhirnya mendapatkan ide. Dia meminta bantuan sepupunya untuk menggambar seekor Donald Duck yang lucu dan seekor Mickey Mouse yang nakal di pakaian bekasnya. Lalu, anak itu menjualnya ke sekolah anak orang kaya dan laku 25 dollar.\r\n.\r\nBelum selesai, ayah kembali memberikan sepotong pakaian bekas dan berkata, "Apakah kamu mampu menjual pakaian ini seharga 200 dollar?"\r\n.\r\nKali ini anak itu menerima tugas tanpa keraguan dan dengan senang hati. Kebetulan aktris film populer "Charlie Angels", Farrah Fawcett sedang berada di New York. Setelah konferensi pers, anak itu menerobos penjagaan keamanan dan meminta tanda tangan sang artis di baju bekasnya. Akhirnya baju itu bisa terjual seharga 200 dollar.\r\n.\r\nMalam harinya ayah bertanya, "Anakku, dari pengalaman menjual tiga helai baju bekas itu, apa yang kamu pahami?"\r\n.\r\n"Selama kita berpikir pasti ada caranya," kata anak itu dengan riang dan penuh semangat.\r\n.\r\nAyah menggelengkan kepala, "kamu tidak salah.. Tapi bukan itu maksud ayah. Ayah hanya ingin memberitahumu bahwa sehelai pakaian bernilai 1 dollar bisa ditingkatkan nilainya, apabila kita mau berusaha dan mencoba. Apalagi kita sendiri yang seorang manusia. Mungkin kita ini berkulit gelap dan miskin, tapi apa bedanya?"\r\n.\r\nSejak saat itu, si anak belajar dengan giat dan menjalani latihan lebih keras.\r\n.\r\n"Sama sama manusia, entah hitam putih tinggi pendek, namun yang membedakan nilainya adalah kesungguhannya dalam berusaha."\r\n.\r\n20 tahun kemudian, namanya terkenal di seluruh dunia.\r\n.\r\nAnak itu adalah MICHAEL JORDAN.', '2017-02-24 10:35:56', 11, NULL, NULL, NULL, NULL),
	(123, 'MENGHARGAI PROSES BELAJAR ANAK', '2017-05-05', 'https://qx.esq165.co.id/upload/sementara/img/file_58afaa617832b.jpg', '"Pentingnya menilai pekerjaan anak dari prosesnya, Bukan hasilnya"', 'Untuk bisa berjalan, seorang anak butuh belajar. Untuk bisa memakai pakaiannya sendiri, seorang anak pun butuh belajar. Tak mungkin kan soerang anak bayi yang baru lahir bisa berjalan dan memakai pakaiannya sendiri. Itu semua butuh proses. Tahap anak mengenal, memahamai dan melakukan.\r\n.\r\nBegitu pula di dunia pendidikan. Untuk seorang murid memahami suatu pelajaran, butuh proses belajar. Tak semua anak mempunyai daya tangkap dan daya cerna yang sama. Ada yang satu kali diajarkan satu mata pelajaran langsung mengerti, namun ada juga anak yang baru mengerti ketika diajarkan berkali-kali. Yang perlu Bunda dan Ayah ingat, setiap anak dilahirkan dengan karakter dan kemampuan yang berbeda-beda.\r\n.\r\nDi dunia pendidikan misalnya, guru memberi PR kepada siswa. Ketika PR dikumpulkan dan jawaban sang siswa tidak sama dengan guru, seringkali guru menyalahkan. Banyak terjadi ketika hasil jawaban sama namun cara yang dipakai oleh siswa berbeda, juga masih disalahkan oleh guru.\r\n.\r\nItu merupakan sebuah moment dimana guru tidak menghargai proses belajar siswa. Padahal, inilah pentingnya menilai pekerjaan anak dari prosesnya. Bukan hasilnya. Dengan menghargai proses, Bunda dan Ayah bisa belajar untuk menjalani setiap tahapan-tahapannya. Tak ada orang yang jadi dokter hebat dalam waktu lima hari. Untuk menjadi seorang dokter harus kuliah selama bertahun-tahun hingga akhirnya diakui sebagai dokter\r\n.\r\nProses adalah suatu hal yang harus dihargai. Menghargai dengan menikmati dan menjalani setiap detik proses tersebut.', '2017-02-24 10:39:12', 11, NULL, NULL, NULL, NULL),
	(124, 'PERNAHKAH ANDA RAGU AKAN HIDUP ?', '2017-05-06', 'https://qx.esq165.co.id/upload/sementara/img/file_58afad7670f77.jpg', 'Yakinlah dalam melakukan sesuatu maka lakukanlah. Jangan ada kata ragu sedikitpun.', 'Sahabat ESQ, pernahkah Anda ragu akan hidup Anda? Ragu akan takdir yang sudah ditentukan Tuhan untuk kita? YaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦pasti kita semua pernah mengalaminya kan?\r\n.\r\nBanyak orang yang bertanya-tanya ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“apakah yang saya lakukan sudah benarÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â, ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“atau bagaimana ya kehidupan saya ke depannyaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â, atau ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“gimana nasib hidup saya setelah ini. Seringkali keraguan akan hidup membuat orang menjadi gelap mata.\r\n.\r\nYang perlu Sahabat ESQ ketahui, keraguan itu melumpuhkan. Keraguan akan menghentikan Anda dalam memutuskan suatu hal yang positif. Pada akhirnya, apa yang terjadi? Hidup Anda berakhir dengan sia-sia karena keraguan Anda sendiri.\r\n.\r\nHidup dengan penuh keraguan tidak akan membawa kita pada titik ketenangan, titik kesuksesan. Hidup penuh ragu hanya membuat kita lelah, lelah fisik dan lelah pikiran. Sahabat ESQ pasti sering kita ragu dalam menentukan sesuatu, betul apa betul? Bahkan terkadang rasa ragu itu membuat kita jadi serba salah akan hidup. Ragu itu muncul karena kita ada ketakutan akan sesuatu hal.\r\n.\r\nSahabat ESQ, ketakutan yang kita alami selama ini bersumber dari keraguan dan ketidakyakinan akan hal yang kita takuti. Rasa ragu tersebut perlahan menjadi sebuah ketakutan, sehingga menjadikan kita sulit menentukan pilihan mana yang lebih baik dan buruk.\r\n.\r\nKalau Sahabat ESQ masih merasa punya keyakinan untuk melakukan sesuatu maka lakukanlah. Jangan ada kata ragu sedikitpun. Sebaliknya,kalau yang sahabat ESQ rasakan hanya keraguan, maka jangan pernah lakukan hal tersebut. Karena tidak akan datang kebaikan dari sesuatu yang Sahabat ESQ mulai dengan penuh keraguan.', '2017-02-24 10:50:23', 11, NULL, NULL, NULL, NULL),
	(125, 'I LOVE MONDAY! TIPS JADIKAN HARI SENIN LEBIH MENYENANGKAN', '2017-05-07', 'https://qx.esq165.co.id/upload/sementara/img/file_58b3a9c0b360b.jpg', 'Ubahlah \'I hate Monday\' menjadi \'I Love Monday\'', 'I LOVE MONDAY! TIPS JADIKAN HARI SENIN LEBIH MENYENANGKAN\r\n.\r\n.\r\nKebanyakan orang mengaku tidak suka, takut, hingga benci dengan hari Senin. Hari yang membuat suasana hati menjadi buruk dan merasa malas. Bahkan sebagian orang sering berucap "I HATE MONDAY".\r\n.\r\nApakah Anda memiliki pemikiran serupa dengan fenomena di atas? Jika ya, yuk diubah! Perubahan yang jadi langkah perbaikan untuk meraih kesuksesan.\r\n.\r\nKita sudah sering mendengar, membaca, dan melihat bahwa orang sukses dan berhasil meraih impian dengan mencintai semua hari. Mereka fokus pada tujuan untuk mencapai target yang diinginkan.\r\n.\r\nNah, untuk bisa survive seperti mereka, mari lakukan perbaikan diri dari hal-hal kecil. Contohnya memperbaiki pandangan \'I hate Monday\' menjadi \'I Love Monday\'.\r\n.\r\nCaranya, ikuti tips berikut ini:\r\n.\r\n1 Nikmati Pemandangan Pagi Hari\r\n.\r\nSebelum mulai bekerja, usahakan menikmati pemandangan di pagi hari. Misalnya mengamati lingkungan sekitar, menghirup udara sejuk, melewati rute berbeda, menyapa orang yang ditemui, dan lain-lain.\r\n.\r\nAnda juga bisa mengambil waktu sejenak untuk sekedar minum kopi, duduk dengan rekan kerja, atau berbicara tentang hal lucu dan menarik yang dialami. Awali hari dengan pemikiran dan suasana hati yang positif, untuk dapatkan semangat dan rasa senang.\r\n.\r\n2 Rencanakan Jadwal Kerja Anda\r\n.\r\nPerencanaan sangat bermanfaat untuk menghindari hari senin yang suram. Buatlah daftar pekerjaan yang akan dikerjakan dan Anda tinggal memilih hendak memulai dari pekerjaan yang mana.\r\n.\r\nRencana kerja membuat Anda lebih terarah dan bisa lebih enjoy menikmati hari senin. Anda juga bisa menyusun rencana lain yang lebih menyenangkan seperti, menonton film favorit usai menyelesaikan semua pekerjaan, dan sebagainya.\r\n.\r\n3 Kenakan Pakaian Terbaik Anda\r\n.\r\nMemakai pakaian favorit dan terbaik bisa membantu menaikkan mood melalui sebuah hari. Anda bisa lebih percaya diri, merasa senang, dan optimis melalui hari senin.\r\n.\r\nKenakan pakaian terbaik Anda, namun jangan berlebihan. Maksudnya, pastikan Anda nyaman menggunakan pakaian tersebut dan tidak mengganggu aktivitas yang sedang dikerjakan.\r\n.\r\n4 Mulai Pekerjaan dari Apa yang Disukai\r\n.\r\nSangat dianjurkan untuk memulai pekerjaan dari apa yang disukai. Cara ini sukses membuat mood lebih positif, perasaan senang, dan terasa ringan. Pekerjaan juga jadi lebih efektif, karena lebih cepat selesai.\r\n.\r\nAnda pun bisa memulai dengan membersihkan meja kerja atau email terlebih dahulu. Jadikan tempat Anda kerja senyaman mungkin, untuk bisa happy dan produktif.\r\n.\r\n5 Tersenyumlah\r\n.\r\nTips terakhir, tersenyumlah. Cara paling simple dan mudah yang memberikan dampak positif pada pikiran dan perasaan. Sekalipun ada masalah, tetapla tersenyum.\r\n.\r\nSenyum juga salah satu perwakilan dari rasa syukur. Rasa yang akan mengantarkan Anda pada rasa damai dan bersemangat menjalankan aktivitas seharian penuh.\r\n.\r\n"I love Monday"\r\n.\r\nSemoga kita diberi kemudahan dan kelancaran untuk selalu memperbaiki diri. Mari berbagi dalam kebaikan untuk mewujudkan kesuksesan bersama.', '2017-02-27 11:35:53', 11, NULL, NULL, NULL, NULL),
	(126, 'TENTANG ILMU YAKIN', '2017-05-08', 'https://qx.esq165.co.id/upload/sementara/img/file_58b3ad8b99796.jpg', 'Kita kerap merasa ragu melangkah, takut memulai, dan bingung dengan perasaan sendiri. Kenapa? Karena kita tidak memiliki "ILMU YAKIN"', 'Ingin sukses.. Punya mimpi besar.. Tapi merasa tidak yakin mampu meraihnya?\r\n.\r\nJangan berkecil hati, karena bukan Anda saja yang mengalami dilema ini. Sebagian besar orang di dunia terbelenggu dalam rasa takut dan keraguannya sendiri, sehingga menghambat perjalanan meraih kesuksesan, serta impian.\r\n.\r\nUntuk mengatasi dilema ini, yuk belajar bersama dari kisah inspiratif berikut. Kisah yang mengajarkan kita tentang Ilmu Yakin dalam Kehidupan.\r\n.\r\n.\r\nSekitar seminggu lalu saya dapat rejeki dan saat dalam perjalanan pulang, saya melihat seorang kakek sedang berjalan di pinggir jalan yang ramai. Dia memakai kain jarik dan membawa tongkat bambu. saya lalu berhenti di depan kakek itu.\r\n.\r\n"Mau ke mana kek? Yuk bareng sama saya saja," kata saya mengajak kakek boncengan.\r\n.\r\n"Ke sana (sambil menunjuk jalan. Iya Mas, makasih," kata si kakek dan langsung naik ke boncengan.\r\n.\r\nSaat itulah saya mendapatkan ilmu baru dari si kakek. Namanya Kakek Muji dan pekerjaannya jualan toge di sebuah pasar di Jogja. Setiap pagi Kakek Muji di antar cucunya naik motor sejauh 6 kilometer. Cucunya kemudian bekerja sampai sore, sehingga tidak bisa menjemput si kakek di pasar. Sementara pasar tutup jam 11 siang. Jadi, Kakek Muji harus pulang naik bus, lalu turun di perempatan. Dari sana masih harus berjalan kaki sejauh 3 kilometer, untuk sampai rumah.\r\n.\r\n"Kakek dulu naik sepeda mas tiap ke pasar. Tapi sudah 5 tahun ini kakek diantar, pulangnya naik bus. Enggak kuat naik sepeda lagi," tuturnya.\r\n.\r\n"Lho, tapi jalan kaki 3 kilometer itu masih jauh kek. Enggak malah capek?" tanya saya penasaran.\r\n.\r\n"Bukan begitu mas. Selama 5 tahun ini hanya 3-4 kali saja kakek benar-benar jalan kaki sampai rumah. Selalu setiap hari ada saja yang membonceng kakek sampai rumah. Gonta-ganti orangnya dan kakek enggak kenal," katanya.\r\n.\r\nSetiap hari kakek selalu yakin pasti Allah akan memilihkan satu dari ratusan orang yang lewat untuk mengantarnya sampai rumah. "Biar jadi pahala mereka semua, karena kakek tidak bisa membalasnya," tambahnya.\r\n.\r\nSontak saat itu saya berpikir, Wow! ILMU YAKIN Kakek Muji mengalahkan teknologi Gojek, yang harus pakai gadget untuk memanggil.\r\n.\r\nHari ini saya yang ditunjuk dan besok pasti ada orang lain lagi yang mengantarkan kakek. Dengan perbandingan 5 tahun hanya 3-4 kali berjalan kaki, si kakek membuktikan bahwa Tuhan hadir setiap hari.\r\n.\r\nLalu bagaimana dengan kita?\r\n.\r\nKita kerap merasa ragu melangkah, takut memulai, dan bingung dengan perasaan sendiri. Kenapa? Karena kita tidak memiliki "ILMU YAKIN" yang sumbernya dari dan untuk Allah..\r\n.\r\nSi kakek sudah sampai di depan rumahnya dan saya pamit melanjutkan perjalanan. Saat bersalaman, kakek memanjatkan doa yang membuat saya merinding mendengarnya.\r\n.\r\nJogja siang yang begitu panas ini pun jadi terasa sejuk di hati.\r\n.\r\n.\r\nBagaimana menurut sahabat ESQ? Apa kesan dan pelajaran berharga yang bisa diambil dari kisah di atas?', '2017-02-27 11:47:55', 11, NULL, NULL, NULL, NULL),
	(127, 'CARA WANITA MOVE ON', '2017-05-09', 'https://qx.esq165.co.id/upload/sementara/img/file_58b3b016352e9.jpg', 'Keep smile and always be happy !!!', 'BUNDA DAN SISTA INGIN MOVE ON?\r\n.\r\n\r\nApa yang sekilas Bunda dan Sista pikirkan ketika mendengar kata move on?\r\n.\r\nMantan kekasih kah? Mantan sahabat? Pengalaman masa lalu? Bunda dan Sista tidak salah, karena memang move on berkaitan erat dengan melepaskan masa lalu (termasuk mantan) dan memulai hari ke masa depan (menggapai impian).\r\n.\r\nKata atau ungkapan move on juga identik dengan kaum hawa. Kaum yang dikenal memiliki hati lembut dan penuh kasih sayang. Mereka cenderung susah melupakan dan tenggelam dalam kegalauan. Setuju?\r\n.\r\nNamun mau sampai kapan Bunda dan Sista seperti itu? Hidup ini telalu singkat untuk dibuat bersedih dan mengurung diri. Hidup ini juga terlalu indah dan sayang dilewatkan begitu saja.\r\n.\r\nUntuk itu, yuk belajar MOVE ON. Mantapkan kaki melangkah ke depan dan jadikan masa lalu sebagai pelajaran. Bagaimana caranya?\r\n.\r\nUntuk mengetahuinya, yuk simak sejenak ulasan berikut. Ada beberapa langkah yang bisa Bunda dan Sista coba untuk bisa move on dengan mudah. Di antaranya:\r\n.\r\n\r\n#1 Manajemen Waktu\r\n\r\n#2 Kerjakan Hal Bermanfaat yang Membuat Anda Bahagia\r\n\r\n#3 Mengubah Sudut Pandang\r\n\r\n#4 Bersosialisasi\r\n\r\n#5 Tersenyumlah dan Waktunya Move On\r\n\r\nBagaimana? Mudah kan?\r\n.\r\nJadi...\r\n.\r\nApalagi alasan Bunda dan Sista untuk tidak move on?\r\n.\r\nKeep smile and always be happy !!!', '2017-02-27 11:52:24', 11, NULL, NULL, NULL, NULL),
	(128, 'BERKENALAN DENGAN LINGKUNGAN ANAK', '2017-05-10', 'https://qx.esq165.co.id/upload/sementara/img/file_58b3e9329b76b.jpg', 'Pendidikan yang harus dijalankan orang tua adalah pendidikan bagi perkembangan akal dan rohani anak.', 'Bunda dan Ayah, kita sebagai orangtua tentunya memiliki tugas dan peran yang sangat penting dalam keluarga. Disamping itu, kita juga harus mampu mengembangkan potensi yang ada pada diri anak, memberi teladan dan mampu mengembangkan pertumbuhan pribadi dengan penuh tanggung jawab dan penuh kasih sayang.\r\n.\r\nSeperti yang Bunda dan Ayah tau, anak-anak akan tumbuh dengan berbagai bakat dan sikap yang berbeda-beda. Mereka adalah karunia yang sangat berharga bagi kita sebagai orangtua.\r\n.\r\nBunda dan Ayah sebagai orang tua pasti menginginkan sang buah hati menjadi orang yang berkembang secara sempurna. Kita menginginkan anak kelak menjadi orang yang sehat dan kuat, berketrampilan, cerdas, pandai dan beriman.\r\n.\r\nDalam keluarga, Bunda dan Ayah merupakan pendidik yang menjadi contoh dan acuan bagi anak-anak. Pendidikan yang harus dijalankan orang tua adalah pendidikan bagi perkembangan akal dan rohani anak, pendidikan ini mengacu pada aspek-aspek kepribadian secara dalam garis besar.\r\n.\r\nMenggenai pendidikan akal yang dilakukan orang tua adalah menyekolahkan anak karena sekolah merupakan lembaga paling baik dalam mengembangkan akal dan interaksi sosial. Selain pendidikan, hal yang penting yang harus diperhatikan adalah lingkungan bermain anak-anak.\r\n.\r\nTerkadang lingkungan menjadi faktor terbesar dalam perkembangan anak-anak. Sebagai orang tua kita harus selalu mengawasi anak dengan cara yang asyik tanpa anak merasa selalu dibuntuti oleh orang tua.\r\n.\r\nUntuk menyiasatinya, kita bisa berkenalan dengan teman anak yag bisa bermain, ajak bicara dan bertanya sesuatu layaknya teman dengan sopan. Atau kita bisa mengajak jalan-jalan bersama agar anak-anak tidak merasa orangtuanya ikut campur dalam lingkungan mainnya.\r\n.\r\nBisa juga kita mengajak makan bersama di rumah sambil ngobrol-ngobrol santai dengan mereka. Untuk bisa berkenalan dengan lingkungan anak, kita harus bisa membawa diri layaknya berbicara seperti teman dengan mereka.', '2017-02-27 15:57:16', 11, NULL, NULL, NULL, NULL),
	(129, 'MEMENANGKAN PERSAINGAN DENGAN PENGORBANAN', '2017-05-11', 'https://qx.esq165.co.id/upload/sementara/img/file_58b3ea779e976.jpg', 'Berkorbanlah lebih untuk mendapatkan sesuatu yang lebih besar.', 'MEMENANGKAN PERSAINGAN DENGAN PENGORBANAN\r\n.\r\nSemangat pagi! Bagaimana kabar Anda hari ini?\r\n.\r\n.\r\nNah, untuk menambah semangat, ada satu kisah inspiratif yang akan jadi ilmu yang berharga untuk kita. Bapak Ibu mungkin pernah mendengar atau baru pertama kali tahu tentang kisah seseorang yang sedang mengikuti tes lamaran kerja di bawah ini:\r\n.\r\nAda sebuah perusahaan di Indonesia yang sedang melakukan rekrutmen karyawan baru secara besar-besaran. Dari beberapa jabatan yang hendak diisi, ada satu posisi yang dilamar lebih dari 2000 pelamar.\r\n.\r\nNamun hanya ada 1 orang yang akhirnya diterima.\r\n.\r\nIni sebuah kisah nyata dan bagaimana 1 orang itu bisa mengalahkan 2000 pelamar lainnya?\r\n.\r\nTernyata ini RAHASIA-nya!!!\r\n.\r\nDalam proses rekrutmen tersebut ada sebuah tes tulis dengan beberapa pertanyaan yang harus dipecahkan pelamar. Dari beberapa pertanyaan itu, ada satu kasus yang mudah sekaligus rumit untuk dipecahkan.\r\n.\r\nKasus itu adalah:\r\nAnda sedang mengendarai motor di tengah malam yang sedang hujan. Di tengah jalan, Anda melihat 3 orang sedang menunggu kedatangan angkot, yang tak kunjung terlihat.\r\n1. Seorang nenek tua yang sangat lapar\r\n2. Seorang dokter yang pernah menyelamatkan hidup Anda sebelumnya\r\n3. Seseorang yang spesial yang selama ini jadi idaman hati\r\nJika hanya satu orang yang yang bisa Anda bonceng, siapakah yang akan Anda ajak?\r\nDan jelaskan, megapa Anda melakukan itu!\r\n.\r\nNah, jika itu terjadi pada Anda, apakah jawaban Anda?\r\n.\r\nCoba jawab dulu dalam hati atau sebuah kertas. Lalu cek bagaimana jawaban 1 orang yang mengalahkan 2000 pelamar tersebut di bawah ini.\r\n.\r\nDari 2000 pelamar, hanya ada 1 orang yang memiliki jawaban brilian dan langsung diterima. Dia tidak memberikan penjelasan atas jawabannya, namun hanya menjabarkan tindakan yang akan dilakukannya dengan singkat.\r\n.\r\n"Saya akan memberikan kunci motor saya kepada sang dokter dan meminta kepadanya untuk membawa nenek tua tersebut, agar segera ditolong. Sedangkan saya sendiri akan tetap tinggal di sana dengan sang idaman hati untuk menunggu angkot."\r\n.\r\nJawaban pelamar itu langsung mendapatkan kualifikasi smart & briliant employee dari perusahaan.\r\n.\r\nPelamar itu melakukan efisiensi pekerjaan dengan cara yang menyenangkan. Dia mau berkorban lebih untuk mendapatkan sesuatu yang lebih besar. Dia memecahkan masalah dengan jalan dan cara yang jarang dilihat seseorang.\r\n.\r\nApa hikmah yang bisa Anda tangkap dari kisah ini?\r\n.\r\n.\r\nSalam 165', '2017-02-27 16:05:12', 11, '2017-03-10 09:47:41', 11, NULL, NULL),
	(130, 'Jadilah Wanita yang Berbahagia', '2017-05-12', 'https://qx.esq165.co.id/upload/sementara/img/file_58b3ebed10913.jpg', '" Para wanita pun ingin berbahagia "', 'Kita para wanita pun juga ingin berbahagia. Menjalani hari-hari degan suka cita dan melangkah mantap menuju masa depan. Apakah Bunda & Sista setuju?\r\n.\r\nKaum Adam kerap menilai kebahagiaan wanita itu rumit dan ribet. Padahal, faktanya tak selalu demikian. Kebahagiaan wanita itu sederhana dan tak melulu tentang uang, derajat, atau cinta.\r\n.\r\nBila tak percaya, coba lakukan 5 hal berikut dan jadilah wanita yang berbahagia.\r\n.\r\n#1 Syukuri Hal-Hal Kecil\r\nBanyak hal sepele di sekitar kita yang sering terlewat atau terlupakan begitu saja. Padahal, mensyukuri hal-hal kecil merupakan sebuah cara untuk melembutkan hati dan membuatnya lebih lapang.\r\n.\r\n#2 Merawat Diri\r\nSaat menatap cermin dan merasa nyaman dengan wajah sendiri, pasti ada rasa bahagia. Apalagi bila Anda memiliki tubuh sehat karena rajin merawat diri. Kesehatan ini bukan hanya tampak dari luar, tapi juga kesehatan pikiran dan mental.\r\n.\r\n#3 Tinggalkan Kebiasaan Buruk\r\nHal ini tidak mudah, namun pasti bisa bila mau berusaha. Misalnya, coba Bunda & Sista membiasakan diri bangun di pagi hari. Kebahagiaan bisa muncul pada seseorang yang selalu dapat menerapkan kebiasaan baik dalam hidupnya.\r\n.\r\n#4 Bahagiakan Orang Lain\r\nSudakah Bunda & Sista membahagiakan kedua orang tua? Atau membahagiakan orang di sekitar? Para wanita perlu tahu bahwa rahasia bahagia salah satunya ialah saat mau dan berani menciptaan bahagia untuk orang lain. Ini juga karena rasa bahagia itu mudah menular.\r\n.\r\n#5 Kerjakan yang Anda Sukai\r\nMelakukan hal yang tidak disukai cenderung membawa kita pada rasa hampa. Daripada membuang waktu sia-sia, yuk buat hidup lebih berwarna dengan mengerjakan dan melakukan yang disukai.\r\nHidup akan jadi lebih bermaknda dan indah ketika kita bisa merengkuh kebahagiaan. Hidup ini singkat dan hanya sekali Ladies. Untuk itu mari manfaatkan dengan pikiran dan tindakan yang membawa kita menjadi wanita yang berbahagia.', '2017-02-27 16:06:53', 11, NULL, NULL, NULL, NULL),
	(131, '4 Tipe Manusia dalam Menjalani Hidup', '2017-05-13', 'https://qx.esq165.co.id/upload/sementara/img/file_58b51e9003174.jpg', 'kebahagiaan dan kesuksesan.', 'Tahukah Anda, bahwa ada 4 tipe mayoritas manusia di dunia dalam menjalani hidup. Tipe-tipe yang diklasifikasikan dalam dua kategori yakni kebahagiaan dan kesuksesan.\r\n.\r\n==========\r\n????Tipe Pertama\r\n==========\r\n- Belum sukses\r\n- Belum bahagia\r\nBerusaha mewujudkan keduanya dengan berbagai cara, langkah, dan usaha. Karena belum bisa mendapatkan keduanya, mereka cenderung murung hingga hilang semangat.\r\n.\r\n==========\r\n????Tipe Kedua\r\n==========\r\n- Sukses\r\n- Belum bahagia\r\nMeraih sukses, tapi belum juga mendapatkan kebahagiaan. Banyak yang berada di fase ini dan mencoba mencari cara untuk bahagia.\r\n.\r\n==========\r\n????Tipe Ketiga\r\n==========\r\n- Belum sukses\r\n- Bahagia\r\nRasa bahagia itu ada, namun ada yang kurang dan merasa tak bergairah menjalani hidup. Rasa yang mengusik, sehingga bahagia itu terasa monoton.\r\n.\r\n==========\r\n????Tipe Keempat\r\n==========\r\n- Sukses\r\n- Bahagia\r\nIni adalah harapan, impian, dan tujuan semua manusia di muka bumi.\r\n.\r\n.\r\nManakah yang menggambarkan diri Anda?\r\n.\r\n.\r\nDari keempat tipe tersebut, tentu kita semua mendambakan masuk dalam tipe keempat. Sudah sukses, bahagia pula. Siapa yang mau?\r\n.\r\n.\r\nYa, kita semua berharap dan bermimpi mampu meraih kesuksesan sekaligus merengkuh kebahagiaan. Namun sayangnya, baru segelintir orang yang tergolong tipe keempat ini.\r\n.\r\n.\r\nOrang-orang yang tahu bagaimana cara membuka kunci mendapatkan SUKSES sekaligus BAHAGIA. Cara yang terbukti efektif dan kerap diungkapkan oleh mereka yang sukses dan bahagia. Namun, kita sering melewatkannya, melupakannya, atau bahkan menyepelekannya.\r\n.\r\n.\r\nCara yang bagaimana itu?\r\n.\r\n.\r\nSumbernya ialah TRUE POWER yang ada di dalam diri kita. Tengok sejenak True Power itu dan Anda akan paham tentang banyak hal yang perlu dilakukan untuk bahagia dan sukses.', '2017-02-28 14:02:20', 11, NULL, NULL, NULL, NULL),
	(132, 'BAHAGIA LALU SUKSES atau SUKSES LALU BAHAGIA', '2017-05-14', 'https://qx.esq165.co.id/upload/sementara/img/file_58b53c1288dc9.jpg', '"Untuk bahagia tidak harus sukses."', 'Pernahkah Anda bertanya-tanya tentang mana yang lebih dulu diraih. Bahagia lalu sukses atau sukses lalu bahagia?\r\n.\r\nPertanyaan yang umumnya diajukan, ketika seseorang berada di fase belum meraih bahagia dan sukses dikehidupannya. Seseorang yang berusaha mewujudkan impian itu dengan berbagai cara yang ada.\r\n.\r\nDalam proses meraih bahagia dan sukses itu, ada yang merasa :\r\n- Lelah\r\n- Hilang semangat\r\n- Bingung\r\n- Galau\r\n- Hingga merasa sakit\r\n.\r\nApakah sahabat ESQ sedang merasakan hal ini? Jika ya coba simak ulasan berikut:\r\n.\r\nDalam sebuah review Psychological Bulletin yang dirangkum dari 225 studi tentang bahagia dan sukses, ditemukan bahwa untuk bahagia tidak harus sukses.\r\n.\r\nMaksudnya rasa bahagialah yang akan mengantarkan kita pada kesuksesan. Dengan kata lain lebih baik bahagia dulu baru sukses. Kok bisa? Kenapa?\r\n.\r\nAlasannya karena kebahagiaan mengarahkan kita ke keberhasilan yang lebih besar dalam hidup. Orang bahagia sering memiliki suasana hati positif. Suasana yang akan mendorong diri untuk menjadi lebih baik dan mengoptimalkan usaha dalam berbagai aktivitas atau pekerjaan.\r\n.\r\nKetika orang merasa senang, mereka cenderung merasa yakin, optimis, energik dan lain-lain. Orang bahagia tentu juga akan berjumpa dengan kendala, rintangan, kegagalan, atau rasa sakit. Namun, energi positif dari kebahagiaan itu bisa jadi kekuatan dan benteng untuk bisa bergerak maju dan menemukan solusi.\r\n.\r\nLalu, bagaimana caranya menjadi orang yang bahagia? Cukup temukan "True Power" yang ada di dalam diri kita masing-masing. Kekuatan sejati yang akan membimbing kita manusia mampu merengkuh KEBAHAGIAAN dan mewujudkan KESUKSESAN.', '2017-02-28 16:03:19', 11, NULL, NULL, NULL, NULL),
	(133, 'KETIKA IQ, EQ, DAN SQ BERSINERGI', '2017-05-15', 'https://qx.esq165.co.id/upload/sementara/img/img_17022205_10154919028826893_4641047315076474468_n.jpg', 'ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“saya tidak pernah gagal, teta', 'Sahabat ESQ, siapa yang tak kenal tokoh dunia yang satu ini. YaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¦Thomas Alva Edison, merupakan salah satu tokoh yang berkontribusi bagi kemajuan di dunia ini. Ia banyak mengembangkan peralatan yang dibutuhkan orang. Salah satu benda yang ia temukan adalah bola lampu.\r\n.\r\nPenemuannya tersebut merupakan benda terpenting yang pernah ditemukan dalam sejarah umat manusia. Coba Sahabat ESQ bayangkan, berkat penemuannya itu, kita dapat beraktivitas pada malam hari selayaknya beraktivtas pada siang hari.\r\n.\r\nDengan kemampuan intelegensinya yang tinggi, ia mampu menciptakan sesuatu hal yang mustahil dilakukan orang pada saat itu, namun ia mampu menciptakannya. Pada masa remaja, Edison sangat senang mempelajari sesuatu dan membaca buku-buku. Dari semua buku-buku yang dipelajarinya, ia menerapkan pelajaran tersebut dengan cara bereksperimen di laboratorium kecilnya. Ia tinggal di laboratorium dan hanya tidur 4 jam sehari. Edison melakukan eksperimen terus menerus hingga penemuan-penemuannya berhasil.\r\n.\r\nYang perlu Sahabat ESQ ketahui, sebelum Edison bisa menyalakan lampu pertamanya, ia melakukan 5.000 eksperimen yang selalu berakhir dengan kegagalan. Namun, gagal ribuan kali pun tak membuat Edison patah semangat. Edison merupakan seseorang yang sangat positif dan tahan banting. Ini yang membawanya kepada kreativitas tingkat tinggi. Edison merupakan seorang yang pantang menyerah, bahkan ketika ia ditanya berapa kali dia gagal sebelum memperoleh keberhasilannya dalam menghidupkan lampu, ia menjawab ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“saya tidak pernah gagal, tetapi saya belajar tentang benda yang dapat digunakan untuk membuat lampu dan benda yang tidak dapat digunakan untuk membuat lampuÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â.\r\n.\r\nPositif thinking, pantang menyerah, kreatif dan terus berusaha, itulah hal yang wajib kita contoh dari sifat Thomas Alva Edison untuk meraih kesuksesan. Sinergi antara kecerdasan IQ, EQ, dan SQ bisa membawa Edison pada titik sukses yang mampu membawanya menjadi seorang penemu hebat kelas dunia.\r\n.\r\nBegitu juga dengan Sahabat ESQ. Anda membutuhkan ke tiganya (IQ, EQ, dan SQ) untuk bisa meraih kesuksesan. Seorang pemimpin pun harus mempunyai kemempuan-kemampuan khusus dalam menjalankan tugasnya. Kemampuan-kemampuan khusus tersebut meliputi kecerdasan intelegensia (IQ), kecerdasan emosional (EQ), dan kecerdasan spiritual (SQ). Kemampuan-kemampun tersebutlah yang membuat menusia menjadi seorang pemimpin yang efektif.\r\n.\r\nSudahkah IQ, EQ dan SQ Anda bersinergi dengan baik dan terarah?', '2017-03-02 10:01:18', 11, NULL, NULL, NULL, NULL),
	(134, 'Terapi Patah Hati untuk Sembuhkan Luka di Hati', '2017-05-16', 'https://qx.esq165.co.id/upload/sementara/img/img_16998822_683671755148581_2530264044939251288_n.jpg', 'Berpikirlah dengan bijak dan perluas jaringan sosial Anda', 'Patah hati itu sakit dan membuat segala hal yang berjalan tampak tak menyenangkan. Mau makan tak enak, tidur tak nyaman, dan semuanya serba salah. Setuju?\r\n.\r\nNamun, sista perlu tahu bahwa patah hati adalah salah satu risiko dalam menjalin sebuah hubungan. Risiko yang harus ditanggung dan dihadapi, karena hidup terus berlanjut apa pun yang terjadi.\r\n.\r\nRintangannya ialah, anggapan umum yang mengatakan bahwa menyembuhkan hati dan move on itu susah. Hal ini benar, jika Anda membiarkan diri terus terpuruk dengan keadaan.\r\n.\r\nNamun, bila mampu mengendalikan diri, maka rasa sakit akibat patah hati akan mudah sembuh. Sista cukup tahu apa saja yang perlu dilakukan untuk mengobati hati yang terluka. Contohnya, seperti 5 resep patah hati berikut ini:\r\n\r\n#1 Menangislah Sepuasnya\r\nAnda boleh menangis sista. Ini cara paling alami melampiaskan kekesalan, rasa tertekan, dan penat. Menangis bisa menurunkan kadar emosi seseorang dan membuat kita merasa jauh lebih baik setelahnya. Air mata yang dikeluarkan saat menangis bisa mengeluarkan hormon stres akibat patah hati, yakni endorphin leucine-enkaphalin.\r\n\r\n#2 Menuliskan Kekecewaan\r\nCoba sista tuliskan hal apa saja yang membuat sakit hati dan kecewa. Ini akan membantu menyembuhkan diri, karena menulis bisa jadi media penyaluran emosi. Setelah menulis, biasanya akan muncul rasa tenang dan lebih baik.\r\n\r\n#3 Sibukkan Dirimu\r\nTemukan kesibukan baru untuk mengisi waktu dengan hal-hal bermanfaat. Misalnya ikut kursus memasak, menjahit, dan sebagainya. Kesibukan juga bisa membuat kita lupa dengan perasaan sakit hati, karena energi sudah habis untuk kesibukan itu.\r\n\r\n#4 Nikmati Waktu Me Time\r\nAda keinginan untuk jalan-jalan? Coba manfaatkan momen patah hatimu dengan menikmati me time. Ini bisa jadi waktu yang tepat untuk bersenang-senang dan melakukan apa saja yang sista inginkan. Sendiri tak selalu menyedihkan. Nikmati saja waktu Anda dan hargai diri Anda dengan memanjakan diri sendiri.\r\n\r\n#5 Jatuh Cinta Lagi\r\nObat terbaik untuk patah hati ialah jatuh cinta lagi. Coba sista kembali membuka diri dan menerima kehadiran orang lain. Patah hati bisa membuat wanita merasa trauma untuk menjalin hubungan lagi. Tapi cara ini justru membuat Anda susah move on.\r\n.\r\nJadi, berpikirlah dengan bijak dan perluas jaringan sosial Anda. Namun, jangan lupa untuk menjadi diri sendiri ya sista. Semoga berhasil!!!', '2017-03-02 10:03:10', 11, NULL, NULL, NULL, NULL),
	(135, 'Belajar Hidup Seekor dari Lobster', '2017-05-17', 'https://qx.esq165.co.id/upload/sementara/img/img-17156347-686272304888526-137955513724477781-n.jpg', 'Ketidaknyamanan dalam hidup bisa diatasi dengan kemauan, keputusan, dan usaha menciptak', 'Bunda & Sista, Yuk! Belajar Hidup dari Seekor Lobster\r\n.\r\nBunda dan Sista mungkin bertanya-tanya kenapa belajar dari lobster.\r\n.\r\nAda satu sisi di mana wanita dan lobster memiliki kesamaan. Lobster dikenal sebagai hewan yang lembut dan lunak, sedangkan wanita identik dengan manusia berhati lembut dan manis.\r\n.\r\nDengan sifat tersebut, lobster ditakdirkan untuk tinggal di dalam cangkang yang kaku dan keras seumur hidupnya. Sama dengan perjalanan hidup manusia yang kerap bertemu tantangan dan rintangan.\r\n.\r\nBedanya...\r\n.\r\nPara lobster itu mampu survive dan mengatasi stres yang dialaminya dengan baik. Sedangkan kita para wanita, cenderung terbelenggu dengan kesakitan, ketakutan, kesedihan, dan sebagainya.\r\n.\r\nUntuk itu Bunda dan Sista, yuk belajar mengatasi stres dari lobster!\r\n.\r\nCangkang lobster yang keras tidak bisa bertambah besar. Untuk itu, mereka akan merasa terhimpit, tertekan dan tidak nyaman ketika tumbuh besar.\r\n.\r\nDi masa ini lobster harus keluar dari cangkangnya yang membuatnya tidak nyaman dan membuat cangkang baru. Bagaimana lobster membuat cangkang baru?\r\n.\r\nSemua berawal dari stimulasi RASA TIDAK NYAMAN. Ketika cangkang lama terasa begitu sesak, ia akan meninggalkannya dan membuat rumah baru.\r\n.\r\nBegitu juga dengan kita manusia. Saat kita merasa STRES, TIDAK NYAMAN, dan TERTEKAN karena hal tertentu, itulah saatnya untuk TUMBUH dan BERKEMBANG.\r\n.\r\nKetidaknyamanan dalam hidup bisa diatasi dengan kemauan, keputusan, dan usaha menciptakan perubahan. Serta menghilangkan kebiasaan-kebiasaan lama yang kurang baik.\r\n.\r\nJadi, Bunda dan Sista tetaplah berpositif thinking dan melangkah maju ketika di situasi tidak nyaman. Jika Anda mampu melakukannya, rasa bahagia hingga sukses mampu didapatkan.\r\n.\r\nYuk terus berbenah!', '2017-03-08 11:39:52', 11, '2017-03-10 09:47:16', 11, NULL, NULL),
	(136, 'KERJA KERAS BOLEH, NAMUN INGAT UNTUK MEMBUAT JEDA', '2017-05-22', 'https://qx.esq165.co.id/upload/sementara/img/img-17103545-1607029675988937-4430824878838394169-n.jpg', 'Bekerja keras boleh saja, akan tetapi Anda harus tetap memperhatikan hal-hal utama dalam hidup', 'KERJA KERAS BOLEH, NAMUN INGAT UNTUK MEMBUAT JEDA\r\n.\r\n.\r\nPak Norman adalah penebang pohon handal yang akhirnya diterima kerja di sebuah pabrik besar di bidang pengolahan kayu. Dia senang karena upah yang diterima juga sesuai harapan dan berjanji untuk bekerja dengan baik.\r\n.\r\nSang pimpinan memberi Pak Norman kapak baru dan menunjukkan pohon mana saja yang boleh ditebang. Di hari pertama bekerja, Pak Norman mampu menebang 18 pohon.\r\n.\r\n"Wow, selamat! Pertahankan itu," kata Sang Pimpinan.\r\n.\r\nMotivasi dari Sang Pimpinan memberikan energi baru dan membuat Pak Norman lebih bersemangat. Namun, di hari berikutnya, ia hanya bisa menebang 15 pohon. Hari ketiga Pak Norman bekerja lebih keras, tapi hanya 13 pohon yang ditebang.\r\n.\r\nPak Norman terus bekerja keras, tapi hari demi hari semakin sedikit pohon yang ia tebang. "Aku sepertinya kehilangan kekuatanku," pikirnya.\r\n.\r\nDia akhirnya menemui Sang Pimpinan dan minta maaf atas kinerja buruknya. Saat itu Sang Pimpinan bertanya, "Kapan terakhir kali kamu mengasah kapakmu?," tanyanya.\r\n.\r\n"Mengasah kapak? Saya tidak punya waktu untuk itu karena sibuk menebang pohon," jawab Pak Norman.\r\n.\r\nYa, seperti itulah kehidupan kita. Terkadang kita begitu sibuk dan tak punya banyak waktu untuk mengasah "kapak" kita. Setiap orang berlomba-lomba menjadi lebih sibuk dari sebelumnya, hingga lupa membuat jeda. Memberikan jeda untuk berbahagia, istirahat, dan mengasah ilmu.\r\n.\r\nBekerja keras boleh saja, akan tetapi Anda harus tetap memperhatikan hal-hal utama dalam hidup. Misalnya waktu beribadah, waktu untuk keluarga, waktu untuk membaca, dan sebagainya.\r\n.\r\nDalam aktivitas yang padat, kita perlu jeda untuk berpikir dan berkontemplasi. Jika tidak, hasilnya akan seperti Pak Norman dengan pekerjaan dan kapaknya.\r\n.\r\nBack to reality.. today is Monday????????\r\n.\r\nSemoga cerita ini bermanfaat .\r\n\r\nSalam 165', '2017-03-13 11:58:20', 11, NULL, NULL, NULL, NULL),
	(137, 'BAGAIMANA MEMBENTUK PRIBADI ANAK AGAR MANDIRI?', '2017-05-18', 'https://qx.esq165.co.id/upload/sementara/img/img-17155924-688005771381846-1201820024513673287-n.jpg', 'Menjadi orangtua yang baik merupakan satu bentuk tugas juga tanggung jawab yang sangat mulia', 'BAGAIMANA MEMBENTUK PRIBADI ANAK AGAR MANDIRI?\r\n.\r\n.\r\nBunda dan Ayah, semua orangtua menginginkan anaknya selalu bisa dekat dengan dirinya. Namun, tidak selamanya itu baik buat masa depan si anak. Terkadang, perlindungan anak yang ekstrem membuat anak kehilangan kemampuan belajar dan berpikir secara independen dan mandiri.\r\n.\r\n.\r\nKemandirian perlu orangtua ajarkan sejak dini pada anak-anak. Banyak orang tua yang melewati cara mengajarkan anak mandiri dikarenakan banyak alasan, salah satunya adalah kesibukan orang tua dengan pekerjaan diluar rumah atau bahkan ketergantungan antara anak dan orang tua hingga sulit untuk Bunda dan Ayah melepaskan anak-anak.\r\n.\r\n.\r\nBanyak diantaranya orangtua yang akan melakukan segala cara untuk membahagiakan anak-anaknya, dan mereka cenderung akan memberikan apa saja yang diinginkan oleh anak-anaknya, namun ternyata hal ini tidak selalu baik dalam cara mendidik anak yang baik. Ada anak yang memang dibiasakan untuk hidup dengan nyaman dan mereka cenderung tidak pernah terbiasa untuk menghadapi kesulitan dalam hidupnya, dan itu akan membuatnya si anak akan menjadi pribadi yang manja serta tidak mau untuk hidup mandiri.\r\n.\r\n.\r\nMenjadi orangtua yang baik merupakan satu bentuk tugas juga tanggung jawab yang sangat mulia. Maka Bunda dan Ayah harus bisa menjadi orangtua yang bisa di banggakan oleh anak-anak Bunda dan Ayah. Bunda dan Ayah harus bisa mendidik anak dengan cara yang baik, Insya Allah anak Bunda dan Ayah akan memberikan kegembiraan bagi Bunda, Ayah dan juga untuk keluarga.', '2017-03-13 12:07:14', 11, NULL, NULL, NULL, NULL),
	(138, 'THE KING\'S SPEECH KISAH SI RAJA GAGAP', '2017-05-19', 'https://qx.esq165.co.id/upload/sementara/all/screenshot-2016-03-15-00-37-26-1.png', 'Sebagai seorang rakyat tentunya menginginkan sosok pemimpin yang mempunyai wibawa, pandai, dan paling tidak pintar berbicara.', 'THE KING\'S SPEECH KISAH SI RAJA GAGAP\r\n\r\nSebagai seorang rakyat tentunya menginginkan sosok pemimpin yang mempunyai wibawa, pandai, dan paling tidak pintar berbicara. Namun coba bayangkan, bagaimana jadinya jika seorang pemimpin suatu negara tidak bisa berpidato di depan rakyatnya?\r\n.\r\nInilah yang melatari kisah film Inggris, The King\'s Speech. Di mana Raja Inggris George VI justru mempunyai kesulitan berbicara di depan umum.\r\n.\r\nSingkat cerita, Raja George VI selalu gagap saat berbicara padahal dirinya baru saja dilantik menjadi Raja dan harus menyampaikan pidato ke seluruh warga Inggris saat itu.\r\n.\r\nSetelah coba menemui banyak dokter dan terapis terbaik di Inggris namun tetap gagal, sang Raja frustasi. Ia menganggap bahwa dirinya bukanlah seseorang yang pantas memimpin negara besar seperti Inggris.\r\n.\r\nNamun dengan bantuan sang isteri, Elizabeth, yang sabar memberi dukungan, akhirnya Ia menemukan seorang therapis bernama Lionel Logue, yang bisa menyembuhkan penyakit gagap sang Raja.\r\n.\r\nLionel menyuruh sang Raja membaca sebuah buku dengan sekencang-kencangnya sambil mendengarkan lagu Mozart \'The Marriage of Figaro\' dengan keras hingga ia tidak bisa mendengar suaranya sendiri. Lionel pun merekamnya dengan sebuah piringan hitam.\r\n.\r\nMerasa tidak berhasil, sang raja pulang dengan putus asa dan harapan hampa.\r\n.\r\nNamun betapa kaget dirinya ketika mendengar rekaman piringan hitam tersebut bahwa ternyata dirinya tidak gagap sedikitpun saat membaca.\r\n.\r\nAkhirnya Raja kembali untuk melakukan terapi bersama Lionel dan hasil yang didambakan tercapai.\r\n.\r\n.\r\nRaja mampu menyelesaikan pidatonya dengan baik, serta sukses menyentuh hati seluruh masyarakat Inggris. Bahkan jeda-jeda dalam setiap kalimat yang Raja sampaikan memiliki kekuatan tersendiri dibanding pidato Raja atau Presiden sebelumnya.\r\n.\r\nHIKMAH YANG BISA DIPETIK DARI FILM KING\'S SPEECH\r\n.\r\n1. Pentingnya Pemimpin Untuk Bisa Tampil Di Depan Orang\r\n.\r\nMasalah yang dihadapi Raja George adalah berkomunikasi, padahal dirinya merupakan orang nomor satu di negeri Ratu Elizabeth. Di awal kisah, sang Raja tampil gugup di depan banyak orang dan membuat rakyatnya menjadi resah dan ragu oleh kepemimpinannya.\r\n.\r\n.\r\n2. Buang Rasa Malu dan Minder\r\n.\r\nSang terapis, Lionel menemukan petunjuk bahwa gangguaan berbicara yang dialami Raja bukan karena gangguan otak namun juga pengaruh lingkungan. Ketakutan dan malu menghantui Raja sehinga sulit untuk mengucapkan satu patah kata pun. Oleh karena itu buang rasa minder jika ingin terus melangkah maju.\r\n.\r\n.\r\n3. Kerja Keras dan Pantang Menyerah\r\n.\r\nRaja hampir saja frustasi, sudah kelilling keseluruh penjuru Inggris untuk mengunjungi dokter namun kegagapannya belum juga bisa disembuhkan. Akan tetapi dengan keinginan kuat dan dorongan dari sang isteri, akhirnya Ia mampu tampil cemerlang di depan publik.\r\n.\r\n.\r\nBagaimana menurut Anda?', '2017-03-31 11:29:13', 11, '2017-07-11 12:28:41', 11, NULL, NULL),
	(139, 'Modal Kemuliaan', '2017-04-03', 'https://qx.esq165.co.id/upload/sementara/img/img-esq-book1.jpg', 'Jangan remehkan diri anda karena anda memiliki MODAL KEMULIAAN.', NULL, '2017-04-03 17:24:39', 126, NULL, NULL, NULL, NULL),
	(140, 'Daily Inspiration Today', '2017-07-10', 'https://qx.esq165.co.id/upload/sementara/all/esq-165-quotes-ary-ginanjar-agustian-esq-leadership-center-pelatihan-esq-training-esq.jpg', 'Daily Inspiration Today', NULL, '2017-07-10 13:45:22', 11, NULL, NULL, NULL, NULL),
	(142, 'Pikiran yang terbuka dan mulut yang tertutup merupakan suatu kombinasi kebahagiaan.', '2017-07-11', 'https://qx.esq165.co.id/upload/sementara/all/1200px-aryginanjaragustian.jpg', 'Semakin banyak Anda berbicara tentang diri sendiri, semakin banyak pula kemungkinan untuk Anda berbohong.', 'content', '2017-07-11 12:21:17', 11, '2017-07-11 13:46:51', 11, NULL, NULL);
/*!40000 ALTER TABLE `minspirasi` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.minspirasi_log
DROP TABLE IF EXISTS `minspirasi_log`;
CREATE TABLE IF NOT EXISTS `minspirasi_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idInspirasi` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `isView` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='rule table ini:\r\n1. jika ada di table ini, berarti pilihan isView = 1/0\r\n--> isView = 1=di lihat | 0=di hapus/reject/swipe buang\r\n\r\n2. jika tidak ada di table ini, berarti inspirasi muncul tapi dicuekin sama user. nggak di hapus, nggak di lihat juga.';

-- Dumping data for table k_auto2000.minspirasi_log: ~122 rows (approximately)
DELETE FROM `minspirasi_log`;
/*!40000 ALTER TABLE `minspirasi_log` DISABLE KEYS */;
INSERT INTO `minspirasi_log` (`id`, `idInspirasi`, `idUser`, `isView`) VALUES
	(1, 0, 0, 0),
	(2, 0, 0, 0),
	(3, 0, 0, 0),
	(4, 0, 0, 0),
	(5, 0, 0, 0),
	(10, 0, 0, 0),
	(11, 0, 0, 0),
	(20, 0, 0, 0),
	(21, 0, 0, 0),
	(22, 0, 0, 0),
	(23, 0, 0, 0),
	(24, 0, 0, 0),
	(25, 0, 0, 0),
	(26, 0, 0, 0),
	(27, 0, 0, 0),
	(29, 0, 0, 0),
	(30, 0, 0, 0),
	(31, 0, 0, 0),
	(32, 0, 0, 0),
	(33, 0, 0, 0),
	(34, 0, 0, 0),
	(35, 0, 0, 0),
	(36, 0, 0, 0),
	(37, 0, 0, 0),
	(38, 0, 0, 0),
	(39, 0, 0, 0),
	(40, 0, 0, 0),
	(41, 0, 0, 0),
	(42, 0, 0, 0),
	(43, 0, 0, 0),
	(44, 0, 0, 0),
	(45, 0, 0, 0),
	(46, 0, 0, 0),
	(47, 0, 0, 0),
	(48, 0, 0, 0),
	(49, 0, 0, 0),
	(50, 0, 0, 0),
	(51, 0, 0, 0),
	(52, 0, 0, 0),
	(53, 0, 0, 0),
	(54, 0, 0, 0),
	(55, 0, 0, 0),
	(56, 0, 0, 0),
	(57, 0, 0, 0),
	(58, 0, 0, 0),
	(59, 0, 0, 0),
	(60, 0, 0, 0),
	(61, 0, 0, 0),
	(62, 0, 0, 0),
	(63, 0, 0, 0),
	(70, 0, 0, 0),
	(71, 0, 0, 0),
	(72, 0, 0, 0),
	(73, 0, 0, 0),
	(74, 0, 0, 0),
	(75, 0, 0, 0),
	(76, 0, 0, 0),
	(77, 0, 0, 0),
	(78, 0, 0, 0),
	(79, 0, 0, 0),
	(80, 0, 0, 0),
	(81, 0, 0, 0),
	(82, 0, 0, 0),
	(83, 0, 0, 0),
	(84, 0, 0, 0),
	(85, 0, 0, 0),
	(86, 0, 0, 0),
	(87, 0, 0, 0),
	(88, 0, 0, 0),
	(89, 0, 0, 0),
	(90, 0, 0, 0),
	(91, 0, 0, 0),
	(92, 0, 0, 0),
	(93, 0, 0, 0),
	(94, 0, 0, 0),
	(95, 0, 0, 0),
	(96, 0, 0, 0),
	(97, 0, 0, 0),
	(98, 0, 0, 0),
	(99, 0, 0, 0),
	(100, 0, 0, 0),
	(101, 0, 0, 0),
	(102, 0, 0, 0),
	(103, 0, 0, 0),
	(104, 0, 0, 0),
	(105, 0, 0, 0),
	(106, 0, 0, 0),
	(107, 0, 0, 0),
	(108, 0, 0, 0),
	(109, 0, 0, 0),
	(110, 0, 0, 0),
	(111, 0, 0, 0),
	(112, 0, 0, 0),
	(113, 0, 0, 0),
	(114, 0, 0, 0),
	(115, 0, 0, 0),
	(116, 0, 0, 0),
	(117, 0, 0, 0),
	(118, 0, 0, 0),
	(119, 0, 0, 0),
	(120, 0, 0, 0),
	(121, 0, 0, 0),
	(122, 0, 0, 0),
	(123, 0, 0, 0),
	(124, 0, 0, 0),
	(125, 0, 0, 0),
	(126, 0, 0, 0),
	(127, 0, 0, 0),
	(128, 0, 0, 0),
	(129, 0, 0, 0),
	(130, 0, 0, 0),
	(131, 0, 0, 0),
	(132, 0, 0, 0),
	(133, 0, 0, 0),
	(134, 0, 0, 0),
	(135, 0, 0, 0),
	(136, 0, 0, 0),
	(137, 0, 0, 0),
	(138, 0, 0, 0),
	(139, 0, 0, 0),
	(140, 0, 0, 0),
	(142, 0, 0, 0);
/*!40000 ALTER TABLE `minspirasi_log` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mjabatan
DROP TABLE IF EXISTS `mjabatan`;
CREATE TABLE IF NOT EXISTS `mjabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `icon` varchar(200) DEFAULT 'http://olahdana.com/qx/aset/img/etc/icon.png',
  `urut` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mjabatan: ~0 rows (approximately)
DELETE FROM `mjabatan`;
/*!40000 ALTER TABLE `mjabatan` DISABLE KEYS */;
/*!40000 ALTER TABLE `mjabatan` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mjenis_resource
DROP TABLE IF EXISTS `mjenis_resource`;
CREATE TABLE IF NOT EXISTS `mjenis_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `urut` int(1) DEFAULT '1000',
  `pilihan` varchar(200) DEFAULT NULL,
  `isFile` int(11) DEFAULT '1' COMMENT 'kalo 1=perlu upload, 0 gak perlu upload',
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mjenis_resource: ~9 rows (approximately)
DELETE FROM `mjenis_resource`;
/*!40000 ALTER TABLE `mjenis_resource` DISABLE KEYS */;
INSERT INTO `mjenis_resource` (`id`, `nama`, `info`, `urut`, `pilihan`, `isFile`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 'Images', NULL, 1, 'img', 1, NULL, NULL, '2016-05-20 17:56:20', 11, NULL, NULL),
	(2, 'Video', NULL, 2, 'video', 1, NULL, NULL, '2016-05-20 17:56:25', 11, NULL, NULL),
	(3, 'Audio', NULL, 6, 'audio', 1, NULL, NULL, '2016-05-23 14:02:15', 11, NULL, NULL),
	(4, 'Video - Youtube', NULL, 2, 'youtube', 0, NULL, NULL, '2016-05-20 17:57:12', 11, NULL, NULL),
	(5, 'Artikel - File Offices', 'Catatan untuk Artikel Office', 3, 'office', 1, NULL, NULL, '2016-05-26 14:23:11', 11, NULL, NULL),
	(7, 'Artikel', NULL, 3, 'textarea', 0, NULL, NULL, '2016-05-26 13:01:10', 11, NULL, NULL),
	(8, 'Webpage / URL', NULL, 7, 'url', 0, NULL, NULL, '2016-05-20 17:56:53', 11, NULL, NULL),
	(9, 'File - PDF', NULL, 1000, 'pdf', 1, '2016-05-20 17:59:54', 11, '2016-05-23 13:55:34', 11, NULL, NULL),
	(10, 'Trailler', NULL, 2, 'trailler', 1, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `mjenis_resource` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mkunci_jawaban
DROP TABLE IF EXISTS `mkunci_jawaban`;
CREATE TABLE IF NOT EXISTS `mkunci_jawaban` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mkunci_jawaban: ~5 rows (approximately)
DELETE FROM `mkunci_jawaban`;
/*!40000 ALTER TABLE `mkunci_jawaban` DISABLE KEYS */;
INSERT INTO `mkunci_jawaban` (`id`, `nama`) VALUES
	(1, 'A'),
	(2, 'B'),
	(3, 'C'),
	(4, 'D'),
	(5, 'E');
/*!40000 ALTER TABLE `mkunci_jawaban` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mmember_exercise
DROP TABLE IF EXISTS `mmember_exercise`;
CREATE TABLE IF NOT EXISTS `mmember_exercise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idExercise` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `jam` time NOT NULL,
  `isBerhasil` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idUser_idExercise_tgl` (`idUser`,`idExercise`,`tgl`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='ini untuk mencatat setiap kali member melakukan exercise\r\n\r\nnote: baik berhasil ataupun belum, simpan di sini.';

-- Dumping data for table k_auto2000.mmember_exercise: ~26 rows (approximately)
DELETE FROM `mmember_exercise`;
/*!40000 ALTER TABLE `mmember_exercise` DISABLE KEYS */;
INSERT INTO `mmember_exercise` (`id`, `idUser`, `idExercise`, `tgl`, `jam`, `isBerhasil`) VALUES
	(1, 11, 1, '2017-09-01', '12:08:20', 1),
	(5, 11, 1, '2017-09-02', '14:09:21', 1),
	(6, 11, 1, '2017-09-05', '14:04:21', 1),
	(7, 11, 1, '2017-09-28', '13:54:21', 1),
	(8, 11, 1, '2017-09-29', '13:51:21', 1),
	(9, 11, 1, '2017-09-30', '13:50:21', 1),
	(10, 11, 1, '2017-10-01', '13:40:21', 1),
	(11, 11, 1, '2017-10-02', '13:34:21', 1),
	(12, 11, 3, '2017-09-01', '13:28:21', 1),
	(13, 11, 3, '2017-09-02', '13:24:21', 1),
	(14, 11, 3, '2017-09-05', '13:21:21', 1),
	(15, 11, 3, '2017-09-28', '13:17:21', 1),
	(16, 11, 3, '2017-09-29', '13:13:21', 1),
	(17, 11, 3, '2017-09-30', '13:17:19', 1),
	(18, 11, 3, '2017-10-01', '12:03:21', 1),
	(19, 11, 3, '2017-10-02', '17:16:21', 1),
	(27, 11, 1, '2017-09-27', '13:58:21', 1),
	(28, 11, 1, '2017-09-26', '12:07:09', 1),
	(29, 11, 1, '2017-09-03', '14:10:21', 0),
	(30, 11, 1, '2017-09-04', '14:08:21', 0),
	(31, 11, 1, '2017-09-15', '12:14:09', 0),
	(32, 11, 1, '2017-09-16', '14:06:21', 0),
	(33, 11, 1, '2017-09-13', '12:12:00', 1),
	(34, 11, 1, '2017-09-06', '12:25:00', 1),
	(36, 11, 1, '2017-09-07', '12:25:00', 1),
	(39, 11, 1, '2017-09-08', '12:45:00', 1);
/*!40000 ALTER TABLE `mmember_exercise` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mmember_exercise_berhasil
DROP TABLE IF EXISTS `mmember_exercise_berhasil`;
CREATE TABLE IF NOT EXISTS `mmember_exercise_berhasil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idExercise` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `streakBulanIni` int(11) NOT NULL,
  `streakAllTime` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idUser_idExercise_tgl` (`idUser`,`idExercise`,`tgl`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='ini untuk mencatat setiap kali member berhasil melakukan exercise\r\n\r\nnote: yang berhasil only yang disimpan di sini\r\n';

-- Dumping data for table k_auto2000.mmember_exercise_berhasil: ~24 rows (approximately)
DELETE FROM `mmember_exercise_berhasil`;
/*!40000 ALTER TABLE `mmember_exercise_berhasil` DISABLE KEYS */;
INSERT INTO `mmember_exercise_berhasil` (`id`, `idUser`, `idExercise`, `tgl`, `streakBulanIni`, `streakAllTime`) VALUES
	(7, 11, 1, '2017-09-01', 1, 1),
	(10, 11, 1, '2017-09-02', 2, 2),
	(11, 11, 1, '2017-09-05', 1, 1),
	(12, 11, 1, '2017-09-28', 3, 3),
	(13, 11, 1, '2017-09-29', 4, 4),
	(14, 11, 1, '2017-09-30', 5, 5),
	(15, 11, 1, '2017-10-01', 1, 6),
	(16, 11, 1, '2017-10-02', 2, 7),
	(17, 11, 3, '2017-09-01', 1, 1),
	(18, 11, 3, '2017-09-02', 2, 2),
	(19, 11, 3, '2017-09-05', 1, 1),
	(20, 11, 3, '2017-09-28', 1, 1),
	(21, 11, 3, '2017-09-29', 2, 2),
	(22, 11, 3, '2017-09-30', 3, 3),
	(23, 11, 3, '2017-10-01', 1, 4),
	(24, 11, 3, '2017-10-02', 2, 5),
	(32, 11, 1, '2017-09-27', 2, 2),
	(33, 11, 1, '2017-09-26', 1, 1),
	(34, 11, 1, '2017-09-03', 3, 3),
	(35, 11, 3, '2017-09-03', 3, 3),
	(36, 11, 1, '2017-09-13', 1, 1),
	(37, 11, 1, '2017-09-06', 2, 2),
	(38, 11, 1, '2017-09-07', 3, 3),
	(39, 11, 1, '2017-09-08', 4, 4);
/*!40000 ALTER TABLE `mmember_exercise_berhasil` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mmember_exercise_pilihan
DROP TABLE IF EXISTS `mmember_exercise_pilihan`;
CREATE TABLE IF NOT EXISTS `mmember_exercise_pilihan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idH` int(11) NOT NULL,
  `idExercisePilihan` int(11) NOT NULL,
  `isBerhasil` int(1) DEFAULT NULL,
  `info` varchar(200) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idMemberLog_idExercisePilihan` (`idH`,`idExercisePilihan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='ini digunakan khusus hanya jika mmember_log.idEcercise== to do list\r\ndi sini isinya adalah pilihan-pilihan dari to_do_list itu,\r\nlalu ada juga hasil evaluasi dari to_do_list tersebut\r\n\r\nidH = mmember_exercise.id \r\n';

-- Dumping data for table k_auto2000.mmember_exercise_pilihan: ~0 rows (approximately)
DELETE FROM `mmember_exercise_pilihan`;
/*!40000 ALTER TABLE `mmember_exercise_pilihan` DISABLE KEYS */;
/*!40000 ALTER TABLE `mmember_exercise_pilihan` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mmodule
DROP TABLE IF EXISTS `mmodule`;
CREATE TABLE IF NOT EXISTS `mmodule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `urut` int(11) DEFAULT '100',
  `icon` varchar(200) DEFAULT 'http://olahdana.com/qx/aset/img/etc/icon.png',
  `info` text,
  `isDeletable` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mmodule: ~3 rows (approximately)
DELETE FROM `mmodule`;
/*!40000 ALTER TABLE `mmodule` DISABLE KEYS */;
INSERT INTO `mmodule` (`id`, `nama`, `urut`, `icon`, `info`, `isDeletable`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 'Module 1', 100, 'http://localhost/juke/cmsauto2000/upload/icon/module/file.jpg', NULL, NULL, '2017-09-20 12:55:17', NULL, '2017-09-20 14:03:44', 11, NULL, NULL),
	(3, 'Module 3', 101, NULL, NULL, NULL, '2017-09-20 13:48:43', NULL, NULL, NULL, NULL, NULL),
	(4, 'Module 4', 102, NULL, NULL, NULL, '2017-09-20 13:48:55', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `mmodule` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mmodule_resource
DROP TABLE IF EXISTS `mmodule_resource`;
CREATE TABLE IF NOT EXISTS `mmodule_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idModule` int(11) DEFAULT NULL COMMENT '1',
  `idResource` int(11) DEFAULT NULL COMMENT 'm',
  `info` varchar(200) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='1 record bisa memiliki beberapa resource.\r\nsaat membuat resource, langsung dipilih ini mau buat siapa? mau buat action? buat motivasi? buat inspirasi? buat tip?\r\n\r\ntapi dari master action / motivasi / inspirasi / tip bisa juga menambahkan resource dari daftar resource.';

-- Dumping data for table k_auto2000.mmodule_resource: ~0 rows (approximately)
DELETE FROM `mmodule_resource`;
/*!40000 ALTER TABLE `mmodule_resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `mmodule_resource` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mmotivasi
DROP TABLE IF EXISTS `mmotivasi`;
CREATE TABLE IF NOT EXISTS `mmotivasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isi` text,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mmotivasi: ~9 rows (approximately)
DELETE FROM `mmotivasi`;
/*!40000 ALTER TABLE `mmotivasi` DISABLE KEYS */;
INSERT INTO `mmotivasi` (`id`, `isi`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(4, '<p><strong style="margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">&nbsp;adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat-kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.</span><br></p>', '2017-09-27 16:05:21', NULL, NULL, NULL, NULL, NULL),
	(5, '<blockquote style="padding: 0px; margin-bottom: 0px; font-size: 17.5px; border: 0px; quotes: none; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif;"><p style="margin-top: 5px; border: 0px; padding: 0px; line-height: 1.6; text-align: justify;">“Kita hidup untuk saat ini, bermimpi untuk masa depan dan kita belajar untuk kebenaran abadi”</p></blockquote><p style="margin-top: 5px; margin-bottom: 15px; border: 0px; padding: 0px; line-height: 1.6; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px;">Di sini Cheng Kai Shek ingin mengajarkan hal seperti hidup Anda sangatlah panjang, Anda masih memiliki banyak tugas dan juga keinginan. Dimana Anda harus bisa bermimpi dan menggapai hal baik selama hidup Anda. Dengan begitu kehidupan Anda menjadi lebih berarti.</p>', '2017-09-27 16:24:57', NULL, '2017-09-28 17:27:16', 11, NULL, NULL),
	(6, '<blockquote style="padding: 0px; margin-bottom: 0px; font-size: 17.5px; border: 0px; quotes: none; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif;"><p style="margin-top: 5px; border: 0px; padding: 0px; line-height: 1.6;">“esensi menjadi manusia adalah ketika seseorang tidak mencari kesempurnaan”</p></blockquote><p style="margin-top: 5px; margin-bottom: 15px; border: 0px; padding: 0px; line-height: 1.6; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px;">Ketika Anda terpuruk akan hal yang selalu Anda buat gagal dan juga tidak pernah sempurna. Jangan menyerah, karena esensi atau sifat alami manusia memang jauh dari kesempurnaan. Sehingga jangan putus asa dan terus berusaha menggapai apa yang Anda targetkan.</p>', '2017-09-27 16:29:57', NULL, '2017-09-28 17:27:38', 11, NULL, NULL),
	(7, '<blockquote style="padding: 0px; margin-bottom: 0px; font-size: 17.5px; border: 0px; quotes: none; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif;"><p style="margin-top: 5px; border: 0px; padding: 0px; line-height: 1.6;">“Pertama mereka mengabaikan Anda. Kemudian mereka tertawa pada Anda. Berikutnya mereka melawan Anda. Lalu Anda menang.”</p></blockquote><p style="margin-top: 5px; margin-bottom: 15px; border: 0px; padding: 0px; line-height: 1.6; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px;">Mahatma Gandhi terkenal akan tokoh agama yang memiliki pemikiran yang tenang, damai dan juga bijaksana dalam bersikap dan berbicara. Hal ini juga ia terapkan ketika menghadapi kehidupan, baik lawan, kawan maupun tantangan hidup. Tanpa menyerah ia bisa membuktikan kebenaran. Maka lihatlah keberhasilan dan “<em style="border: 0px; margin: 0px; padding: 0px;">survive</em>” alias karakter tidak pernah menyerah dan pesimis milik Mahatma Gandhi.</p>', '2017-09-27 16:30:08', NULL, '2017-09-28 17:28:29', 11, NULL, NULL),
	(8, '<blockquote style="padding: 0px; margin-bottom: 0px; font-size: 17.5px; border: 0px; quotes: none; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif;"><p style="margin-top: 5px; border: 0px; padding: 0px; line-height: 1.6;">“ Aku tidak suka mengulang kesuksesan yang ada, aku lebih suka mencari yang lain”</p></blockquote><p style="margin-top: 5px; margin-bottom: 15px; border: 0px; padding: 0px; line-height: 1.6; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px;">Seperti kata Walt Disney, di mana Anda jangan pantang menyerah dan hanya bisa menjadi&nbsp;<em style="border: 0px; margin: 0px; padding: 0px;">follower</em>&nbsp;alias pengikut orang lain saja. Semua orang tentu tahu kisah Disney yang dianggap tidak bisa menggambar dan dipecat, namun ia tak pantang menyerah dan mencari kesuksesan dengan tidak terpaku dan bergantung pada orang lain.</p>', '2017-09-27 16:30:17', NULL, '2017-09-28 17:28:05', 11, NULL, NULL),
	(9, '<blockquote style="padding: 0px; margin-bottom: 0px; font-size: 17.5px; border: 0px; quotes: none; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif;"><p style="margin-top: 5px; border: 0px; padding: 0px; line-height: 1.6;">“ Ada sebuah cara untuk melakukan lebih baik- temukanlah!”</p></blockquote><p style="margin-top: 5px; margin-bottom: 15px; border: 0px; padding: 0px; line-height: 1.6; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px;">Thomas Alfa Edison merupakan seorang ahli dan penemu yang memiliki otak cemerlang. Namun ia juga terkenal akan sifat pantang menyerahnya. Di mana ia mengalami kegagalan hingga ratusan kali, namun&nbsp;<a title="tidak patah semanga" href="https://www.cermati.com/artikel/11-kata-kata-motivasi-kerja-yang-akan-membuatmu-sukses#!" target="_blank" style="color: rgb(0, 123, 193); border: 0px; margin: 0px; padding: 0px;">tidak patah semanga</a>t dan akhirnya mendapatkan hasil yang bisa dinikmati hingga &nbsp;saat ini oleh masyarakat. Sama halnya dengan Alfa Edison dalam menemukan lampu, jangan menyerah akan masalah di kehidupan Anda.</p>', '2017-09-27 16:30:25', NULL, '2017-09-28 17:28:17', 11, NULL, NULL),
	(10, '<blockquote style="padding: 0px; margin-bottom: 0px; font-size: 17.5px; border: 0px; quotes: none; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif;"><p style="margin-top: 5px; border: 0px; padding: 0px; line-height: 1.6;">“Hari depan dunia lebih banyak ditentukan moralitas keputusan kita sekarang”</p></blockquote><p style="margin-top: 5px; margin-bottom: 15px; border: 0px; padding: 0px; line-height: 1.6; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px;">Ketika Anda memutuskan untuk menyerah sekarang, maka tidak akan ada keberhasilan di masa depan. Seodjatmoko ingin menyampaikan bahwa keputusan apa yang Anda ambil sekarang akan berbuah di masa selanjutnya. Untuk itu, cobalah bersikap optimis, positif dan memandang kedepan agar masa selanjutnya Anda mendapatkan hasil yang sesuai.</p>', '2017-09-27 16:30:33', NULL, '2017-09-28 17:27:52', 11, NULL, NULL),
	(11, '<blockquote style="padding: 0px; margin-bottom: 0px; font-size: 17.5px; border: 0px; quotes: none; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif;"><p style="margin-top: 5px; border: 0px; padding: 0px; line-height: 1.6;">“Satu-satunya yang harus kita takuti adalah ketakutan itu Sendiri”</p></blockquote><p style="margin-top: 5px; margin-bottom: 15px; border: 0px; padding: 0px; line-height: 1.6; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px;">Untuk Anda yang merasa khawatir akan semangat hidup dan memiliki ketakutan yang tidak bisa ditaklukan maka motivasi dari Roosevelt sangatlah cocok. Di mana ia mengatakan bahwa ketakutan terbesar tentu saja ketakutan itu sendiri. Jika Anda sudah bisa menaklukan ketakutan tersebut. Maka Anda sudah bisa menata masa depan dengan baik.</p>', '2017-09-27 16:30:43', NULL, '2017-09-28 17:28:40', 11, NULL, NULL),
	(12, '<h2 style="font-family: Roboto, Helvetica, Arial, sans-serif; line-height: 1.2; color: rgb(51, 51, 51); margin-top: 0.312em; margin-bottom: 0.624em; font-size: 1.802em; border: 0px;">Kegagalan Bukan Hasil Akhir</h2><p style="margin-top: 5px; margin-bottom: 15px; border: 0px; padding: 0px; line-height: 1.6; color: rgb(51, 51, 51); font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px;">Dalam hidup kegagalan merupakan proses normal yang akan dihadapi semua orang. Tidak ada keputusan yang tidak disertai risiko. Jangan hanya dengan mengalami kegagalan Anda berhenti berusaha. Jadikan kegagalan yang menghampiri Anda menjadi&nbsp;batu loncatan agar Anda lebih berusaha dan tidak pantang menyerah.</p>', '2017-09-27 16:30:52', NULL, '2017-09-28 17:28:54', 11, NULL, NULL);
/*!40000 ALTER TABLE `mmotivasi` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mposisi
DROP TABLE IF EXISTS `mposisi`;
CREATE TABLE IF NOT EXISTS `mposisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `icon` varchar(200) DEFAULT 'http://olahdana.com/qx/aset/img/etc/icon.png',
  `urut` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mposisi: ~0 rows (approximately)
DELETE FROM `mposisi`;
/*!40000 ALTER TABLE `mposisi` DISABLE KEYS */;
/*!40000 ALTER TABLE `mposisi` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mresource
DROP TABLE IF EXISTS `mresource`;
CREATE TABLE IF NOT EXISTS `mresource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `icon` varchar(200) DEFAULT NULL COMMENT 'url icon',
  `info` text,
  `value` varchar(200) DEFAULT NULL,
  `isi` text,
  `idJenisResource` int(11) NOT NULL,
  `idResourceGroup` int(11) DEFAULT NULL,
  `idH` int(11) DEFAULT NULL,
  `qtyPlayed` int(11) NOT NULL DEFAULT '0',
  `urut` int(11) NOT NULL,
  `idApprovalstatus` int(1) NOT NULL,
  `durasi` time NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COMMENT='resource ini digunakan oleh module apa saja yang membutuhkan.\r\n\r\nm_resourcegroup_id= adalah kode module yang membutuhkan.\r\n\r\ncontoh misalnya resource ini digunakan oleh SliderHome1, maka \r\n1. buat new record SliderHome1 di table m_resourcegroup \r\n2. id tersebut yang digunakan untuk m_resourcegroup_id \r\n';

-- Dumping data for table k_auto2000.mresource: ~6 rows (approximately)
DELETE FROM `mresource`;
/*!40000 ALTER TABLE `mresource` DISABLE KEYS */;
INSERT INTO `mresource` (`id`, `nama`, `icon`, `info`, `value`, `isi`, `idJenisResource`, `idResourceGroup`, `idH`, `qtyPlayed`, `urut`, `idApprovalstatus`, `durasi`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 'video A module 1b', 'http://localhost/juke/cmsauto2000/upload/all/file_1.jpg', NULL, 'http://localhost/juke/cmsauto2000/upload/video/file.mp4', NULL, 2, 2, 1, 0, 0, 1, '00:00:00', '2017-09-20 14:25:20', NULL, '2017-09-22 14:09:21', 11, NULL, NULL),
	(2, 'video B module 2', NULL, NULL, 'https://premium.esq165.co.id/upload/sementara/video/1.mkv', NULL, 2, 2, 1, 0, 0, 1, '00:00:00', '2017-09-20 15:09:16', NULL, '2017-09-20 15:32:10', 11, NULL, NULL),
	(4, 'video C untuk module 4', NULL, NULL, 'https://premium.esq165.co.id/upload/sementara/video/1.mkv', NULL, 2, 2, 4, 0, 0, 1, '00:00:00', '2017-09-20 15:31:51', NULL, '2017-09-20 15:32:16', 11, NULL, NULL),
	(5, 'Video C module 1', NULL, NULL, 'https://premium.esq165.co.id/upload/sementara/video/1.mkv', NULL, 2, 2, 1, 0, 0, 1, '00:00:00', '2017-09-20 15:33:22', NULL, NULL, NULL, NULL, NULL),
	(6, 'Slider 1e', NULL, NULL, 'http://esqx.esq165.co.id/upload/sementara/video/file_587f28fd90475.mp4', NULL, 2, 1, NULL, 0, 0, 1, '00:00:00', '2017-09-20 16:58:40', NULL, '2017-09-20 17:37:06', 11, NULL, NULL),
	(7, 'Slider 2', NULL, NULL, 'http://esqx.esq165.co.id/upload/sementara/video/file_587f28fd90475.mp4', NULL, 2, 1, NULL, 0, 0, 1, '00:00:00', '2017-09-20 17:06:10', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `mresource` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mresource_group
DROP TABLE IF EXISTS `mresource_group`;
CREATE TABLE IF NOT EXISTS `mresource_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='cara menggunakan table ini:\r\ntipe Extends:\r\n1. buat controller di dalam module resource\r\n2. extends controller resource\r\n3. tambahkan hidden field m_resourcegroup_id, dengan field_value_default=id record di table ini \r\n\r\ntipe Children (header-detail):\r\n1. buat controller header\r\n2. buat link-ajax detail yang href=resource\r\n3. set m_resourcegroup_id= id di table ini\r\n4. set h_id = id milik table yang bersangkutan\r\n\r\ncontoh tipe Children:\r\n1. controller module\r\n2. memiliki banyak resource, maka ada link detail resource seperti berikut:\r\nresource?m_resourcegroup_id=2&h_id=100\r\nm_resourcegroup_id = 2 = adalah isi di table ini, 2 = module\r\nh_id=100 = berarti m_module_id=100';

-- Dumping data for table k_auto2000.mresource_group: ~0 rows (approximately)
DELETE FROM `mresource_group`;
/*!40000 ALTER TABLE `mresource_group` DISABLE KEYS */;
INSERT INTO `mresource_group` (`id`, `nama`) VALUES
	(1, 'Slider Home 1');
/*!40000 ALTER TABLE `mresource_group` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mresource_user_view
DROP TABLE IF EXISTS `mresource_user_view`;
CREATE TABLE IF NOT EXISTS `mresource_user_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idResource` int(11) NOT NULL,
  `isSudahPlayVideo` int(1) NOT NULL DEFAULT '0',
  `isSudahLulusTestAssesment` int(1) NOT NULL DEFAULT '0',
  `nilaiTestAssesmentLulus` int(2) NOT NULL DEFAULT '0',
  `nilaiTestAssesmentTerbaik` int(2) NOT NULL DEFAULT '0',
  `nilaiTestAssesmentTerakhir` int(2) NOT NULL DEFAULT '0',
  `idUser` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='untuk mencatat apakah user \r\n1. sudah melihat?\r\n2. sudah test soal assesment?\r\n3. sudah lulus soal test assesment?\r\n';

-- Dumping data for table k_auto2000.mresource_user_view: ~0 rows (approximately)
DELETE FROM `mresource_user_view`;
/*!40000 ALTER TABLE `mresource_user_view` DISABLE KEYS */;
/*!40000 ALTER TABLE `mresource_user_view` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.msoal_assesment
DROP TABLE IF EXISTS `msoal_assesment`;
CREATE TABLE IF NOT EXISTS `msoal_assesment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idResource` int(11) NOT NULL,
  `soal` text NOT NULL,
  `iconsoal` varchar(200) DEFAULT NULL,
  `opsia` text,
  `icona` varchar(200) DEFAULT NULL,
  `opsib` text,
  `iconb` varchar(200) DEFAULT NULL,
  `opsic` text,
  `iconc` varchar(200) DEFAULT NULL,
  `opsid` text,
  `icond` varchar(200) DEFAULT NULL,
  `opsie` text,
  `icone` varchar(200) DEFAULT NULL,
  `kunciJawaban` enum('A','B','C','D','E') NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='ini adalah soal assesment setelah user menonton video di dalam module.\r\njadi relasinya \r\nmodule (one) ---> (many) resource \r\nresource (one) ----> (many) soal assesment  \r\n';

-- Dumping data for table k_auto2000.msoal_assesment: ~2 rows (approximately)
DELETE FROM `msoal_assesment`;
/*!40000 ALTER TABLE `msoal_assesment` DISABLE KEYS */;
INSERT INTO `msoal_assesment` (`id`, `idResource`, `soal`, `iconsoal`, `opsia`, `icona`, `opsib`, `iconb`, `opsic`, `iconc`, `opsid`, `icond`, `opsie`, `icone`, `kunciJawaban`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 1, 'soal 1 untuk video A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A', '2017-09-20 15:45:44', NULL, NULL, NULL, NULL, NULL),
	(2, 1, 'soal 2 untuk video A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B', '2017-09-20 15:53:47', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `msoal_assesment` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.msoal_assesment_test_d
DROP TABLE IF EXISTS `msoal_assesment_test_d`;
CREATE TABLE IF NOT EXISTS `msoal_assesment_test_d` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idH` int(11) NOT NULL,
  `idSoalAssesment` int(11) NOT NULL,
  `jawaban` varchar(1) NOT NULL,
  `kunciJawaban` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `h_id_m_soalassesment_id` (`idH`,`idSoalAssesment`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.msoal_assesment_test_d: ~24 rows (approximately)
DELETE FROM `msoal_assesment_test_d`;
/*!40000 ALTER TABLE `msoal_assesment_test_d` DISABLE KEYS */;
INSERT INTO `msoal_assesment_test_d` (`id`, `idH`, `idSoalAssesment`, `jawaban`, `kunciJawaban`) VALUES
	(13, 3, 1, 'D', 'E'),
	(14, 3, 2, 'E', 'E'),
	(15, 3, 3, 'A', 'E'),
	(16, 3, 5, 'C', 'E'),
	(17, 3, 4, 'B', 'E'),
	(18, 3, 6, 'D', 'E'),
	(19, 4, 1, 'A', 'E'),
	(20, 4, 2, 'B', 'E'),
	(21, 4, 3, 'C', 'E'),
	(22, 4, 5, 'D', 'E'),
	(23, 4, 4, 'E', 'E'),
	(24, 4, 6, '', 'E'),
	(25, 5, 1, 'A', 'E'),
	(26, 5, 2, 'A', 'E'),
	(27, 5, 3, 'A', 'E'),
	(28, 5, 5, 'A', 'E'),
	(29, 5, 4, '', 'E'),
	(30, 5, 6, '', 'E'),
	(31, 6, 1, 'B', 'E'),
	(32, 6, 2, 'B', 'E'),
	(33, 6, 3, '', 'E'),
	(34, 6, 5, 'B', 'E'),
	(35, 6, 4, '', 'E'),
	(36, 6, 6, '', 'E');
/*!40000 ALTER TABLE `msoal_assesment_test_d` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.msoal_assesment_test_h
DROP TABLE IF EXISTS `msoal_assesment_test_h`;
CREATE TABLE IF NOT EXISTS `msoal_assesment_test_h` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `idResource` int(11) NOT NULL,
  `jumlahSoal` int(11) NOT NULL,
  `jumlahBenar` int(11) NOT NULL,
  `jumlahSalah` int(11) NOT NULL,
  `jumlahTidakDiJawab` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.msoal_assesment_test_h: ~4 rows (approximately)
DELETE FROM `msoal_assesment_test_h`;
/*!40000 ALTER TABLE `msoal_assesment_test_h` DISABLE KEYS */;
INSERT INTO `msoal_assesment_test_h` (`id`, `idUser`, `idResource`, `jumlahSoal`, `jumlahBenar`, `jumlahSalah`, `jumlahTidakDiJawab`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(3, 11, 1, 6, 1, 5, 0, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 11, 1, 6, 1, 4, 1, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 11, 1, 6, 0, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 11, 1, 6, 0, 3, 3, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `msoal_assesment_test_h` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mtip
DROP TABLE IF EXISTS `mtip`;
CREATE TABLE IF NOT EXISTS `mtip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isi` text,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mtip: ~9 rows (approximately)
DELETE FROM `mtip`;
/*!40000 ALTER TABLE `mtip` DISABLE KEYS */;
INSERT INTO `mtip` (`id`, `isi`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(4, '<p><strong style="margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">&nbsp;adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat-kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.</span><br></p>', '2017-09-27 16:05:21', NULL, NULL, NULL, NULL, NULL),
	(5, '<p><img src="http://localhost/juke/cmsauto2000/upload/all/1.jpg" style="width: 183px;"><br></p><p><br></p>', '2017-09-27 16:24:57', NULL, '2017-09-27 16:27:37', 11, NULL, NULL),
	(6, '<p><span style="color: rgb(71, 71, 71); font-family: Verdana, Arial, &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;">Akan tiba saatnya kita akan berhenti mencintai seseorang… bukan karena seseorang itu berhenti mencintai kita melainkan… kita menyadari bahwa orang itu akan lebih berbahagia apabila kita melepaskannya.</span><br></p>', '2017-09-27 16:29:57', NULL, NULL, NULL, NULL, NULL),
	(7, '<p><span style="color: rgb(71, 71, 71); font-family: Verdana, Arial, &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;">Perbanyaklah menggunakan telingga melebihi mulutmu. Karena kamu diberi dua tangan dan satu mulut supaya kamu lebih banyak mendengar daripada berbicara.</span><br></p>', '2017-09-27 16:30:08', NULL, NULL, NULL, NULL, NULL),
	(8, '<p><span style="color: rgb(71, 71, 71); font-family: Verdana, Arial, &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;">Kau bisa bersembunyi dari kesalahanmu, tapi tidak dari penyesalan. Kau bisa bermain dengan dramamu, tapi tidak dengan karmamu.</span><br></p>', '2017-09-27 16:30:17', NULL, NULL, NULL, NULL, NULL),
	(9, '<p><span style="color: rgb(71, 71, 71); font-family: Verdana, Arial, &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;">Melepaskan seseorang yang kita cintai memang sungguh menyakitkan namun tak semua yang dicintai harus dimiliki.</span><br></p>', '2017-09-27 16:30:25', NULL, NULL, NULL, NULL, NULL),
	(10, '<p><span style="color: rgb(71, 71, 71); font-family: Verdana, Arial, &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;">Jangan pernah berputus ada jika menghadapi kesulitan, karena setiap tetes air hujan yang jernih berasal daripada awan yang gelap.</span><br></p>', '2017-09-27 16:30:33', NULL, NULL, NULL, NULL, NULL),
	(11, '<p><span style="color: rgb(71, 71, 71); font-family: Verdana, Arial, &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;">Seberat apapun beban masalah yang kamu hadapi saat ini, percayalah bahwa semua itu tak pernah melebihi batas kemampuan kamu.</span><br></p>', '2017-09-27 16:30:43', NULL, NULL, NULL, NULL, NULL),
	(12, '<p><span style="color: rgb(71, 71, 71); font-family: Verdana, Arial, &quot;Open Sans&quot;, sans-serif; font-size: 13px; text-align: justify;">Tegas akan diri sendiri, buang pikiran negatif dan lakukan yang baik. Kegelisahan hanya milik mereka yang putus asa.</span><br></p>', '2017-09-27 16:30:52', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `mtip` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mvision_board
DROP TABLE IF EXISTS `mvision_board`;
CREATE TABLE IF NOT EXISTS `mvision_board` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `info` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idUser_nama` (`idUser`,`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.mvision_board: ~4 rows (approximately)
DELETE FROM `mvision_board`;
/*!40000 ALTER TABLE `mvision_board` DISABLE KEYS */;
INSERT INTO `mvision_board` (`id`, `idUser`, `nama`, `foto`, `info`) VALUES
	(1, 11, 'vision board 1', 'http://localhost/juke/cmsauto2000/upload/vision_board/foto/exercise-vision-board/file.png', 'isi info satu'),
	(2, 11, 'vision board 2', 'http://localhost/juke/cmsauto2000/upload/vision_board/foto/exercise-vision-board/file.png', 'isi info dua'),
	(3, 11, 'vision board 3', 'http://localhost/juke/cmsauto2000/upload/vision-board/vision-board-4.jpg', 'isi info tiga'),
	(4, 11, 'vision board 4', 'http://localhost/juke/cmsauto2000/upload/vision-board/vision-board-5.jpg', 'isi info empat');
/*!40000 ALTER TABLE `mvision_board` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.mvoucher
DROP TABLE IF EXISTS `mvoucher`;
CREATE TABLE IF NOT EXISTS `mvoucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `info` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.mvoucher: ~3 rows (approximately)
DELETE FROM `mvoucher`;
/*!40000 ALTER TABLE `mvoucher` DISABLE KEYS */;
INSERT INTO `mvoucher` (`id`, `kode`, `info`) VALUES
	(1, 'PGX8N', NULL),
	(2, 'G97EY', NULL),
	(3, 'ZJVF2', NULL);
/*!40000 ALTER TABLE `mvoucher` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.x365_hari
DROP TABLE IF EXISTS `x365_hari`;
CREATE TABLE IF NOT EXISTS `x365_hari` (
  `tgl` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.x365_hari: ~365 rows (approximately)
DELETE FROM `x365_hari`;
/*!40000 ALTER TABLE `x365_hari` DISABLE KEYS */;
INSERT INTO `x365_hari` (`tgl`) VALUES
	('2017-01-01'),
	('2017-01-02'),
	('2017-01-03'),
	('2017-01-04'),
	('2017-01-05'),
	('2017-01-06'),
	('2017-01-07'),
	('2017-01-08'),
	('2017-01-09'),
	('2017-01-10'),
	('2017-01-11'),
	('2017-01-12'),
	('2017-01-13'),
	('2017-01-14'),
	('2017-01-15'),
	('2017-01-16'),
	('2017-01-17'),
	('2017-01-18'),
	('2017-01-19'),
	('2017-01-20'),
	('2017-01-21'),
	('2017-01-22'),
	('2017-01-23'),
	('2017-01-24'),
	('2017-01-25'),
	('2017-01-26'),
	('2017-01-27'),
	('2017-01-28'),
	('2017-01-29'),
	('2017-01-30'),
	('2017-01-31'),
	('2017-02-01'),
	('2017-02-02'),
	('2017-02-03'),
	('2017-02-04'),
	('2017-02-05'),
	('2017-02-06'),
	('2017-02-07'),
	('2017-02-08'),
	('2017-02-09'),
	('2017-02-10'),
	('2017-02-11'),
	('2017-02-12'),
	('2017-02-13'),
	('2017-02-14'),
	('2017-02-15'),
	('2017-02-16'),
	('2017-02-17'),
	('2017-02-18'),
	('2017-02-19'),
	('2017-02-20'),
	('2017-02-21'),
	('2017-02-22'),
	('2017-02-23'),
	('2017-02-24'),
	('2017-02-25'),
	('2017-02-26'),
	('2017-02-27'),
	('2017-02-28'),
	('2017-03-01'),
	('2017-03-02'),
	('2017-03-03'),
	('2017-03-04'),
	('2017-03-05'),
	('2017-03-06'),
	('2017-03-07'),
	('2017-03-08'),
	('2017-03-09'),
	('2017-03-10'),
	('2017-03-11'),
	('2017-03-12'),
	('2017-03-13'),
	('2017-03-14'),
	('2017-03-15'),
	('2017-03-16'),
	('2017-03-17'),
	('2017-03-18'),
	('2017-03-19'),
	('2017-03-20'),
	('2017-03-21'),
	('2017-03-22'),
	('2017-03-23'),
	('2017-03-24'),
	('2017-03-25'),
	('2017-03-26'),
	('2017-03-27'),
	('2017-03-28'),
	('2017-03-29'),
	('2017-03-30'),
	('2017-03-31'),
	('2017-04-01'),
	('2017-04-02'),
	('2017-04-03'),
	('2017-04-04'),
	('2017-04-05'),
	('2017-04-06'),
	('2017-04-07'),
	('2017-04-08'),
	('2017-04-09'),
	('2017-04-10'),
	('2017-04-11'),
	('2017-04-12'),
	('2017-04-13'),
	('2017-04-14'),
	('2017-04-15'),
	('2017-04-16'),
	('2017-04-17'),
	('2017-04-18'),
	('2017-04-19'),
	('2017-04-20'),
	('2017-04-21'),
	('2017-04-22'),
	('2017-04-23'),
	('2017-04-24'),
	('2017-04-25'),
	('2017-04-26'),
	('2017-04-27'),
	('2017-04-28'),
	('2017-04-29'),
	('2017-04-30'),
	('2017-05-01'),
	('2017-05-02'),
	('2017-05-03'),
	('2017-05-04'),
	('2017-05-05'),
	('2017-05-06'),
	('2017-05-07'),
	('2017-05-08'),
	('2017-05-09'),
	('2017-05-10'),
	('2017-05-11'),
	('2017-05-12'),
	('2017-05-13'),
	('2017-05-14'),
	('2017-05-15'),
	('2017-05-16'),
	('2017-05-17'),
	('2017-05-18'),
	('2017-05-19'),
	('2017-05-20'),
	('2017-05-21'),
	('2017-05-22'),
	('2017-05-23'),
	('2017-05-24'),
	('2017-05-25'),
	('2017-05-26'),
	('2017-05-27'),
	('2017-05-28'),
	('2017-05-29'),
	('2017-05-30'),
	('2017-05-31'),
	('2017-06-01'),
	('2017-06-02'),
	('2017-06-03'),
	('2017-06-04'),
	('2017-06-05'),
	('2017-06-06'),
	('2017-06-07'),
	('2017-06-08'),
	('2017-06-09'),
	('2017-06-10'),
	('2017-06-11'),
	('2017-06-12'),
	('2017-06-13'),
	('2017-06-14'),
	('2017-06-15'),
	('2017-06-16'),
	('2017-06-17'),
	('2017-06-18'),
	('2017-06-19'),
	('2017-06-20'),
	('2017-06-21'),
	('2017-06-22'),
	('2017-06-23'),
	('2017-06-24'),
	('2017-06-25'),
	('2017-06-26'),
	('2017-06-27'),
	('2017-06-28'),
	('2017-06-29'),
	('2017-06-30'),
	('2017-07-01'),
	('2017-07-02'),
	('2017-07-03'),
	('2017-07-04'),
	('2017-07-05'),
	('2017-07-06'),
	('2017-07-07'),
	('2017-07-08'),
	('2017-07-09'),
	('2017-07-10'),
	('2017-07-11'),
	('2017-07-12'),
	('2017-07-13'),
	('2017-07-14'),
	('2017-07-15'),
	('2017-07-16'),
	('2017-07-17'),
	('2017-07-18'),
	('2017-07-19'),
	('2017-07-20'),
	('2017-07-21'),
	('2017-07-22'),
	('2017-07-23'),
	('2017-07-24'),
	('2017-07-25'),
	('2017-07-26'),
	('2017-07-27'),
	('2017-07-28'),
	('2017-07-29'),
	('2017-07-30'),
	('2017-07-31'),
	('2017-08-01'),
	('2017-08-02'),
	('2017-08-03'),
	('2017-08-04'),
	('2017-08-05'),
	('2017-08-06'),
	('2017-08-07'),
	('2017-08-08'),
	('2017-08-09'),
	('2017-08-10'),
	('2017-08-11'),
	('2017-08-12'),
	('2017-08-13'),
	('2017-08-14'),
	('2017-08-15'),
	('2017-08-16'),
	('2017-08-17'),
	('2017-08-18'),
	('2017-08-19'),
	('2017-08-20'),
	('2017-08-21'),
	('2017-08-22'),
	('2017-08-23'),
	('2017-08-24'),
	('2017-08-25'),
	('2017-08-26'),
	('2017-08-27'),
	('2017-08-28'),
	('2017-08-29'),
	('2017-08-30'),
	('2017-08-31'),
	('2017-09-01'),
	('2017-09-02'),
	('2017-09-03'),
	('2017-09-04'),
	('2017-09-05'),
	('2017-09-06'),
	('2017-09-07'),
	('2017-09-08'),
	('2017-09-09'),
	('2017-09-10'),
	('2017-09-11'),
	('2017-09-12'),
	('2017-09-13'),
	('2017-09-14'),
	('2017-09-15'),
	('2017-09-16'),
	('2017-09-17'),
	('2017-09-18'),
	('2017-09-19'),
	('2017-09-20'),
	('2017-09-21'),
	('2017-09-22'),
	('2017-09-23'),
	('2017-09-24'),
	('2017-09-25'),
	('2017-09-26'),
	('2017-09-27'),
	('2017-09-28'),
	('2017-09-29'),
	('2017-09-30'),
	('2017-10-01'),
	('2017-10-02'),
	('2017-10-03'),
	('2017-10-04'),
	('2017-10-05'),
	('2017-10-06'),
	('2017-10-07'),
	('2017-10-08'),
	('2017-10-09'),
	('2017-10-10'),
	('2017-10-11'),
	('2017-10-12'),
	('2017-10-13'),
	('2017-10-14'),
	('2017-10-15'),
	('2017-10-16'),
	('2017-10-17'),
	('2017-10-18'),
	('2017-10-19'),
	('2017-10-20'),
	('2017-10-21'),
	('2017-10-22'),
	('2017-10-23'),
	('2017-10-24'),
	('2017-10-25'),
	('2017-10-26'),
	('2017-10-27'),
	('2017-10-28'),
	('2017-10-29'),
	('2017-10-30'),
	('2017-10-31'),
	('2017-11-01'),
	('2017-11-02'),
	('2017-11-03'),
	('2017-11-04'),
	('2017-11-05'),
	('2017-11-06'),
	('2017-11-07'),
	('2017-11-08'),
	('2017-11-09'),
	('2017-11-10'),
	('2017-11-11'),
	('2017-11-12'),
	('2017-11-13'),
	('2017-11-14'),
	('2017-11-15'),
	('2017-11-16'),
	('2017-11-17'),
	('2017-11-18'),
	('2017-11-19'),
	('2017-11-20'),
	('2017-11-21'),
	('2017-11-22'),
	('2017-11-23'),
	('2017-11-24'),
	('2017-11-25'),
	('2017-11-26'),
	('2017-11-27'),
	('2017-11-28'),
	('2017-11-29'),
	('2017-11-30'),
	('2017-12-01'),
	('2017-12-02'),
	('2017-12-03'),
	('2017-12-04'),
	('2017-12-05'),
	('2017-12-06'),
	('2017-12-07'),
	('2017-12-08'),
	('2017-12-09'),
	('2017-12-10'),
	('2017-12-11'),
	('2017-12-12'),
	('2017-12-13'),
	('2017-12-14'),
	('2017-12-15'),
	('2017-12-16'),
	('2017-12-17'),
	('2017-12-18'),
	('2017-12-19'),
	('2017-12-20'),
	('2017-12-21'),
	('2017-12-22'),
	('2017-12-23'),
	('2017-12-24'),
	('2017-12-25'),
	('2017-12-26'),
	('2017-12-27'),
	('2017-12-28'),
	('2017-12-29'),
	('2017-12-30'),
	('2017-12-31');
/*!40000 ALTER TABLE `x365_hari` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xbulan
DROP TABLE IF EXISTS `xbulan`;
CREATE TABLE IF NOT EXISTS `xbulan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xbulan: ~0 rows (approximately)
DELETE FROM `xbulan`;
/*!40000 ALTER TABLE `xbulan` DISABLE KEYS */;
/*!40000 ALTER TABLE `xbulan` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xemail_queue
DROP TABLE IF EXISTS `xemail_queue`;
CREATE TABLE IF NOT EXISTS `xemail_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromName` varchar(50) DEFAULT NULL,
  `fromEmail` varchar(50) DEFAULT NULL,
  `toName` varchar(50) DEFAULT NULL,
  `toEmail` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `body` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xemail_queue: ~0 rows (approximately)
DELETE FROM `xemail_queue`;
/*!40000 ALTER TABLE `xemail_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `xemail_queue` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xgcm
DROP TABLE IF EXISTS `xgcm`;
CREATE TABLE IF NOT EXISTS `xgcm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gcm` varchar(200) NOT NULL,
  `json` text NOT NULL,
  `isSending` int(1) DEFAULT '0',
  `sentAt` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `fcmAtauGcm` varchar(3) DEFAULT 'GCM',
  `googleApiKey` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xgcm: ~0 rows (approximately)
DELETE FROM `xgcm`;
/*!40000 ALTER TABLE `xgcm` DISABLE KEYS */;
/*!40000 ALTER TABLE `xgcm` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xgroup_hak
DROP TABLE IF EXISTS `xgroup_hak`;
CREATE TABLE IF NOT EXISTS `xgroup_hak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idGroupUser` int(11) NOT NULL,
  `idModule` int(11) NOT NULL,
  `idModuleAction` int(11) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xgroup_hak: ~0 rows (approximately)
DELETE FROM `xgroup_hak`;
/*!40000 ALTER TABLE `xgroup_hak` DISABLE KEYS */;
/*!40000 ALTER TABLE `xgroup_hak` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xgroup_module
DROP TABLE IF EXISTS `xgroup_module`;
CREATE TABLE IF NOT EXISTS `xgroup_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `info` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xgroup_module: ~0 rows (approximately)
DELETE FROM `xgroup_module`;
/*!40000 ALTER TABLE `xgroup_module` DISABLE KEYS */;
/*!40000 ALTER TABLE `xgroup_module` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xgroup_user
DROP TABLE IF EXISTS `xgroup_user`;
CREATE TABLE IF NOT EXISTS `xgroup_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `urut` int(11) DEFAULT NULL,
  `powerlevel` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xgroup_user: ~2 rows (approximately)
DELETE FROM `xgroup_user`;
/*!40000 ALTER TABLE `xgroup_user` DISABLE KEYS */;
INSERT INTO `xgroup_user` (`id`, `nama`, `urut`, `powerlevel`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(10, 'member', 1, 2, NULL, NULL, NULL, NULL, NULL, NULL),
	(30, 'admin', 2, 3, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `xgroup_user` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xhari
DROP TABLE IF EXISTS `xhari`;
CREATE TABLE IF NOT EXISTS `xhari` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xhari: ~0 rows (approximately)
DELETE FROM `xhari`;
/*!40000 ALTER TABLE `xhari` DISABLE KEYS */;
/*!40000 ALTER TABLE `xhari` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xjenis_kelamin
DROP TABLE IF EXISTS `xjenis_kelamin`;
CREATE TABLE IF NOT EXISTS `xjenis_kelamin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `urut` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xjenis_kelamin: ~0 rows (approximately)
DELETE FROM `xjenis_kelamin`;
/*!40000 ALTER TABLE `xjenis_kelamin` DISABLE KEYS */;
/*!40000 ALTER TABLE `xjenis_kelamin` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xkota
DROP TABLE IF EXISTS `xkota`;
CREATE TABLE IF NOT EXISTS `xkota` (
  `id` int(11) NOT NULL,
  `idPropinsi` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_propinsi_id_nama` (`idPropinsi`,`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table k_auto2000.xkota: ~0 rows (approximately)
DELETE FROM `xkota`;
/*!40000 ALTER TABLE `xkota` DISABLE KEYS */;
/*!40000 ALTER TABLE `xkota` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xlog
DROP TABLE IF EXISTS `xlog`;
CREATE TABLE IF NOT EXISTS `xlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caller` varchar(200) DEFAULT NULL,
  `isi` text NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xlog: ~0 rows (approximately)
DELETE FROM `xlog`;
/*!40000 ALTER TABLE `xlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `xlog` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xmodule
DROP TABLE IF EXISTS `xmodule`;
CREATE TABLE IF NOT EXISTS `xmodule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idGroupModule` int(11) DEFAULT '0',
  `class` varchar(200) NOT NULL DEFAULT '',
  `nama` varchar(200) NOT NULL DEFAULT '',
  `isAdaIndex` int(1) DEFAULT NULL,
  `isAdaAddnew` int(1) DEFAULT NULL,
  `isAdaEdit` int(1) DEFAULT NULL,
  `isAdaDelete` int(1) DEFAULT NULL,
  `isEditable` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xmodule: ~23 rows (approximately)
DELETE FROM `xmodule`;
/*!40000 ALTER TABLE `xmodule` DISABLE KEYS */;
INSERT INTO `xmodule` (`id`, `idGroupModule`, `class`, `nama`, `isAdaIndex`, `isAdaAddnew`, `isAdaEdit`, `isAdaDelete`, `isEditable`) VALUES
	(1, 0, 'xxuser', 'Xxuser', 1, 1, 1, 1, NULL),
	(2, 0, 'awal', 'Awal', 1, 1, 1, 1, NULL),
	(3, 0, 'module', 'Module', 1, 1, 1, 1, NULL),
	(4, 0, 'resource', 'Resource', 1, 1, 1, 1, NULL),
	(5, 0, 'vids', 'Vids', 1, 1, 1, 1, NULL),
	(6, 0, 'jenis_resource', 'Jenis_resource', 1, 1, 1, 1, NULL),
	(7, 0, 'soal_assesment', 'Soal_assesment', 1, 1, 1, 1, NULL),
	(8, 0, 'slider_home1', 'Slider_home1', 1, 1, 1, 1, NULL),
	(9, 0, 'pilihan', 'Pilihan', 1, 1, 1, 1, NULL),
	(10, 0, 'tipe', 'Tipe', 1, 1, 1, 1, NULL),
	(11, 0, 'exercise', 'Exercise', 1, 1, 1, 1, NULL),
	(12, 0, 'read_only_text', 'Read_only_text', 1, 1, 1, 1, NULL),
	(13, 0, 'vision_board', 'Vision_board', 1, 1, 1, 1, NULL),
	(14, 0, 'test_assesment', 'Test_assesment', 1, 1, 1, 1, NULL),
	(15, 0, 'cabang', 'Cabang', 1, 1, 1, 1, NULL),
	(16, 0, 'member', 'Member', 1, 1, 1, 1, NULL),
	(17, 0, 'admin', 'Admin', 1, 1, 1, 1, NULL),
	(18, 0, 'member_exercise', 'Member_exercise', 1, 1, 1, 1, NULL),
	(19, 0, 'detail', 'Detail', 1, 1, 1, 1, NULL),
	(20, 0, 'tip', 'Tip', 1, 1, 1, 1, NULL),
	(21, 0, 'motivasi', 'Motivasi', 1, 1, 1, 1, NULL),
	(22, 0, 'inspirasi', 'Inspirasi', 1, 1, 1, 1, NULL),
	(23, 0, 'info', 'Info', 1, 1, 1, 1, NULL);
/*!40000 ALTER TABLE `xmodule` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xmodule_action
DROP TABLE IF EXISTS `xmodule_action`;
CREATE TABLE IF NOT EXISTS `xmodule_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `urut` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xmodule_action: ~0 rows (approximately)
DELETE FROM `xmodule_action`;
/*!40000 ALTER TABLE `xmodule_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `xmodule_action` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xmodule_group_user
DROP TABLE IF EXISTS `xmodule_group_user`;
CREATE TABLE IF NOT EXISTS `xmodule_group_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idModule` int(11) NOT NULL,
  `idGroupUser` int(11) NOT NULL,
  `isBolehIndex` int(1) DEFAULT NULL,
  `isBolehAddnew` int(1) DEFAULT NULL,
  `isBolehEdit` int(1) DEFAULT NULL,
  `isBolehDelete` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xmodule_group_user: ~23 rows (approximately)
DELETE FROM `xmodule_group_user`;
/*!40000 ALTER TABLE `xmodule_group_user` DISABLE KEYS */;
INSERT INTO `xmodule_group_user` (`id`, `idModule`, `idGroupUser`, `isBolehIndex`, `isBolehAddnew`, `isBolehEdit`, `isBolehDelete`) VALUES
	(1, 1, 30, 1, 1, 1, 1),
	(2, 2, 30, 1, 1, 1, 1),
	(3, 3, 30, 1, 1, 1, 1),
	(4, 4, 30, 1, 1, 1, 1),
	(5, 5, 30, 1, 1, 1, 1),
	(6, 6, 30, 1, 1, 1, 1),
	(7, 7, 30, 1, 1, 1, 1),
	(8, 8, 30, 1, 1, 1, 1),
	(9, 9, 30, 1, 1, 1, 1),
	(10, 10, 30, 1, 1, 1, 1),
	(11, 11, 30, 1, 1, 1, 1),
	(12, 12, 30, 1, 1, 1, 1),
	(13, 13, 30, 1, 1, 1, 1),
	(14, 14, 30, 1, 1, 1, 1),
	(15, 15, 30, 1, 1, 1, 1),
	(16, 16, 30, 1, 1, 1, 1),
	(17, 17, 30, 1, 1, 1, 1),
	(18, 18, 30, 1, 1, 1, 1),
	(19, 19, 30, 1, 1, 1, 1),
	(20, 20, 30, 1, 1, 1, 1),
	(21, 21, 30, 1, 1, 1, 1),
	(22, 22, 30, 1, 1, 1, 1),
	(23, 23, 30, 1, 1, 1, 1);
/*!40000 ALTER TABLE `xmodule_group_user` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xperson
DROP TABLE IF EXISTS `xperson`;
CREATE TABLE IF NOT EXISTS `xperson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL DEFAULT '0',
  `nama` varchar(100) NOT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `hp2` varchar(100) DEFAULT NULL,
  `ktp` varchar(100) DEFAULT NULL,
  `ttlTempat` varchar(100) DEFAULT NULL,
  `ttlTgl` date DEFAULT NULL,
  `idJenisKelamin` int(11) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `kodepos` varchar(10) DEFAULT NULL,
  `idKota` int(11) DEFAULT NULL,
  `info` varchar(200) DEFAULT NULL,
  `idJabatan` int(11) DEFAULT NULL,
  `idForum` int(11) DEFAULT NULL,
  `isNonAktif` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COMMENT='\r\n';

-- Dumping data for table k_auto2000.xperson: ~6 rows (approximately)
DELETE FROM `xperson`;
/*!40000 ALTER TABLE `xperson` DISABLE KEYS */;
INSERT INTO `xperson` (`id`, `idUser`, `nama`, `foto`, `hp2`, `ktp`, `ttlTempat`, `ttlTgl`, `idJenisKelamin`, `alamat`, `kodepos`, `idKota`, `info`, `idJabatan`, `idForum`, `isNonAktif`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`) VALUES
	(1, 1, 'auto', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 2, 'sistem', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 3, 'ngasal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 11, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-25 18:51:39', 11, NULL, NULL),
	(12, 12, 'Member 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-25 19:01:52', NULL, NULL, NULL, NULL, NULL),
	(13, 13, 'Member 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-25 19:04:36', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `xperson` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xperson_blokir
DROP TABLE IF EXISTS `xperson_blokir`;
CREATE TABLE IF NOT EXISTS `xperson_blokir` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPersonBy` int(11) NOT NULL,
  `idPersonBlokir` int(11) NOT NULL,
  `info` varchar(200) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_person_id_by_u_person_id_blokir` (`idPersonBy`,`idPersonBlokir`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='yang diblokir disini, nggak akan muncul lagi di pencarian/request anak dll';

-- Dumping data for table k_auto2000.xperson_blokir: ~3 rows (approximately)
DELETE FROM `xperson_blokir`;
/*!40000 ALTER TABLE `xperson_blokir` DISABLE KEYS */;
INSERT INTO `xperson_blokir` (`id`, `idPersonBy`, `idPersonBlokir`, `info`, `createdAt`) VALUES
	(1, 1, 2, NULL, '0000-00-00 00:00:00'),
	(2, 1, 3, NULL, '0000-00-00 00:00:00'),
	(3, 2, 3, NULL, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `xperson_blokir` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xpropinsi
DROP TABLE IF EXISTS `xpropinsi`;
CREATE TABLE IF NOT EXISTS `xpropinsi` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xpropinsi: ~0 rows (approximately)
DELETE FROM `xpropinsi`;
/*!40000 ALTER TABLE `xpropinsi` DISABLE KEYS */;
/*!40000 ALTER TABLE `xpropinsi` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser
DROP TABLE IF EXISTS `xuser`;
CREATE TABLE IF NOT EXISTS `xuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `hp` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `gcm` varchar(200) DEFAULT NULL,
  `imei` varchar(200) DEFAULT NULL,
  `facebookId` varchar(100) DEFAULT NULL,
  `facebookAccessToken` varchar(200) DEFAULT NULL,
  `idGroupUser` int(11) DEFAULT NULL,
  `isNonAktif` int(1) DEFAULT NULL,
  `refCodeBy` varchar(50) DEFAULT NULL,
  `refCode` varchar(50) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  `lastSeenTime` datetime DEFAULT NULL,
  `lastSeenUrl` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `hp_unik` (`hp`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xuser: ~5 rows (approximately)
DELETE FROM `xuser`;
/*!40000 ALTER TABLE `xuser` DISABLE KEYS */;
INSERT INTO `xuser` (`id`, `email`, `username`, `hp`, `password`, `gcm`, `imei`, `facebookId`, `facebookAccessToken`, `idGroupUser`, `isNonAktif`, `refCodeBy`, `refCode`, `createdAt`, `createdBy`, `updatedAt`, `updatedBy`, `deletedAt`, `deletedBy`, `lastSeenTime`, `lastSeenUrl`) VALUES
	(1, 'auto@gmail.com', 'auto', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'sistem@gmail.com', 'sistem', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 'admin@gmail.com', 'admin', '11', '$P$B5i5MklbTxUcDX2HrPTRYU9DvbdYxK1', NULL, NULL, NULL, NULL, 30, NULL, NULL, NULL, NULL, NULL, '2017-09-25 18:51:39', 11, NULL, NULL, '2017-09-29 09:41:00', 'http://localhost/juke/cmsauto2000/inspirasi'),
	(12, 'member1@gmail.com', 'member1', 'm1', '$P$BlnkXrubmFlif0nmRIV2KqOKMOzgbf.', NULL, NULL, NULL, NULL, 10, NULL, NULL, 'RC000012', '2017-09-25 19:01:52', 11, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 'member2@gmail.com', 'member2', 'm2', '$P$Bi5MEN5TUWoCMcUWdAQ5daxWLn4496/', NULL, NULL, NULL, NULL, 10, NULL, NULL, 'RC000013', '2017-09-25 19:04:36', 11, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `xuser` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser_log
DROP TABLE IF EXISTS `xuser_log`;
CREATE TABLE IF NOT EXISTS `xuser_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `loginTime` datetime NOT NULL,
  `logoutTime` datetime DEFAULT NULL,
  `lastSeen` datetime NOT NULL,
  `url` varchar(200) NOT NULL,
  `isLast` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table k_auto2000.xuser_log: ~0 rows (approximately)
DELETE FROM `xuser_log`;
/*!40000 ALTER TABLE `xuser_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `xuser_log` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser_periode
DROP TABLE IF EXISTS `xuser_periode`;
CREATE TABLE IF NOT EXISTS `xuser_periode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` int(11) NOT NULL,
  `periode` int(11) NOT NULL,
  `lamaHari` int(11) NOT NULL,
  `tglStart` date NOT NULL,
  `tglEnd` date NOT NULL,
  `isPeriodeBerjalan` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table k_auto2000.xuser_periode: ~0 rows (approximately)
DELETE FROM `xuser_periode`;
/*!40000 ALTER TABLE `xuser_periode` DISABLE KEYS */;
/*!40000 ALTER TABLE `xuser_periode` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser_reset_password
DROP TABLE IF EXISTS `xuser_reset_password`;
CREATE TABLE IF NOT EXISTS `xuser_reset_password` (
  `idUser` int(11) DEFAULT NULL,
  `k` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='app baca table ini. jika ada member_id nya, maka: hapus record table ini yang member id dia, lalu log out.\r\n';

-- Dumping data for table k_auto2000.xuser_reset_password: ~0 rows (approximately)
DELETE FROM `xuser_reset_password`;
/*!40000 ALTER TABLE `xuser_reset_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `xuser_reset_password` ENABLE KEYS */;

-- Dumping structure for table k_auto2000.xuser_unconfirmed
DROP TABLE IF EXISTS `xuser_unconfirmed`;
CREATE TABLE IF NOT EXISTS `xuser_unconfirmed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idGroupUser` int(11) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `hp` varchar(50) DEFAULT NULL,
  `apiCode` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `gcm` varchar(200) DEFAULT NULL,
  `imei` varchar(200) DEFAULT NULL,
  `kodeAktivasi` varchar(200) DEFAULT NULL,
  `refCodeBy` varchar(50) DEFAULT NULL,
  `tglDikirimEmail` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `hp` (`hp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table k_auto2000.xuser_unconfirmed: ~0 rows (approximately)
DELETE FROM `xuser_unconfirmed`;
/*!40000 ALTER TABLE `xuser_unconfirmed` DISABLE KEYS */;
/*!40000 ALTER TABLE `xuser_unconfirmed` ENABLE KEYS */;

-- Dumping structure for procedure k_auto2000.xxgenerate_365_hari
DROP PROCEDURE IF EXISTS `xxgenerate_365_hari`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `xxgenerate_365_hari`(
	IN `tahun` INT





)
BEGIN
	DECLARE crt_date DATE;
	declare start_date date;
	declare end_date date;

create table if not exists x365_hari (tgl date);
delete from x365_hari where date_format(tgl, '%Y') = tahun ;

	set start_date = makedate(tahun, 1);
	set end_date = concat(tahun, '-12-31'); 
 
	SET crt_date=start_date;
	WHILE crt_date <= end_date DO
		INSERT INTO x365_hari VALUES(crt_date);
		SET crt_date = ADDDATE(crt_date, INTERVAL 1 DAY);
	END WHILE;
END//
DELIMITER ;

-- Dumping structure for function k_auto2000.xxrandom_number
DROP FUNCTION IF EXISTS `xxrandom_number`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `xxrandom_number`(
	`vmin` int,
	`vmax` int




) RETURNS int(11)
    DETERMINISTIC
BEGIN 
  DECLARE hasil int;
  SET hasil = floor(vmin+ (rand() * (vmax-vmin)));
  RETURN hasil;
END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
