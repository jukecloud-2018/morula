<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Religion extends Model
{
    protected $table = "master_religion";
    protected $hidden = [
        'created_by', 'created_at', 'updated_at'
    ];

}