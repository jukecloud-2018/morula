<?php
namespace App;



use Illuminate\Database\Eloquent\Model;



class Doctor extends Model

{

    protected $table = "doctor";
	
	protected $hidden = [
        'created_by', 'created_at', 'updated_at', 'share_fee'
    ];

}