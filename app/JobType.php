<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class JobType extends Model
{

    protected $table = "master_job_type";
	
	protected $hidden = [
        'created_by', 'created_at', 'updated_at'
    ];

}