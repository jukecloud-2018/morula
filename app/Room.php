<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = "master_room";
    protected $hidden = [
        'created_by', 'created_at', 'updated_at'
    ];

}