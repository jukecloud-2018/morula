<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    protected $table = "master_day";
}