<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = "master_city";

    public function province(){
        return $this->belongsTo("App\Province"); //ambil id province dr tbl province
    }
}