<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientEmergency extends Model
{

    protected $table = "patient_emergency";
	
	public function patient()
    {
        return $this->belongsTo('App\Patient'); // links this->course_id to courses.id
    }

}