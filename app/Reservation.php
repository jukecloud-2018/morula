<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{

    protected $table = "reservation";
	
	public function patient(){
		return $this->belongsTo('App\Patient', 'morula_id');
	}


}