<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/user';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
	
	public function username()
	{
		return 'username';
	}
	
	/*protected function redirectTo()
	{
		
		return '/admin/user';
	}*/
	
	
	protected function authenticate(Request $request)
	{
		//print_r($user);exit;
		if ( $request->user()->hasRole('Admin') ) {// do your margic here
			return redirect('admin');
		}

		return redirect('/admin');
	}
	
	/*protected function setUserSession($user)
	{
		session(
			[
				'roles' => $user->settings->roles
			]
		);
	}*/
	
	
}
