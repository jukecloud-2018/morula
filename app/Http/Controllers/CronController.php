<?php

namespace App\Http\Controllers;

use App\Province;
use App\City;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CronController extends Controller
{
	public function city(Request $request)
    {
		$data = City::get();
		foreach($data as $item){
			$name = ucwords(strtolower($item->name));
			echo $name;echo "<br>";
			$update = City::find($item->id);
			$update->name = $name;
			$update->save();
		}
    }
	
	public function province()
	{
		$data = Province::get();
		foreach($data as $item){
			$name = ucwords(strtolower($item->name));
			echo $name;echo "<br>";
			$update = Province::find($item->id);
			$update->name = $name;
			$update->save();
		}
	}
	
	public function backupdb()
	{
		echo "ok";
	}
}