<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;
use Illuminate\Http\UploadedFile;

class ProfileController extends Controller
{
    /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */
    public function index()
    {
        return view('profile/index', ['user' => User::findOrFail(Auth::id())]);
    }
	
	public function edit()
    {
        
		return view('profile/index', ['user' => User::findOrFail(Auth::id())]);
    }
	
	public function update(Request $request)
    {
		$validate = \Validator::make($request->all(), [
			'name' => 'required',
			'email' => 'email',
			'phone' => 'numeric'
		],
		
		$after_save = [
			'alert' => 'danger',
			'title' => 'Oh wait!',
			'text-1' => 'Ada kesalahan',
			'text-2' => 'Silakan coba lagi.'
		]);
		
		if($validate->fails()){
            return redirect()->back()->with('after_save', $after_save);
        }
		
		$after_save = [
            'alert' => 'success',
            'title' => 'God Job!',
            'text-1' => 'Ubah lagi',
            'text-2' => 'atau kembali.'
        ];
		
		$update = User::where('id', '=', Auth::id())->first();
		
		$update->name = $request->name;
		$update->email = $request->email;
		$update->phone = $request->phone;
		$update->description = $request->description;

		if($request->hasFile('photo')) {
			//echo "OK";exit;
			if ($request->file('photo')->isValid()) {
				//echo $request->photo->getClientOriginalName();exit;
				$upload_foto_filename = time().'_'.$request->photo->getClientOriginalName();
				$request->photo->move(public_path('uploads/user'), $upload_foto_filename);
				$update->photo = $upload_foto_filename;
			}
		}
		
		if($request->passw != '')
			$update->password = Hash::make($request->passw);
		
		$update->save();
		
		return redirect('profile')->with('after_save', $after_save);
    }
	
}