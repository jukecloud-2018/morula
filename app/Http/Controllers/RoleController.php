<?php

namespace App\Http\Controllers;

use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Datatables;
use Hash;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{
    /**
     * Show a list of all of the application's roles.
     *
     * @return Response
     */
    public function index(Request $request)
    {
		return view('admin/role/index');
    }
	
	public function ajaxdt()
	{
		$data = Role::get();
		return Datatables::of($data)->make(true);
	}
	
	public function show($id)
    {
        return view('admin/role/edit', ['role' => Role::findOrFail($id)]);
    }
	
	public function create()
    {
		return view('admin/role/create');
    }
	
	public function edit($id)
    {
        return view('admin/role/edit', ['role' => Role::findOrFail($id)]);
    }
	
	public function editajax(Request $req)
    {
		return response()->json([
			'status' => '200',
			'success' => true,
			'data' => Role::findOrFail($req->id)
		]);
    }
	
	public function destroy($id)
    {
		$del = Role::find($id);
		
		$del->delete();
		
		return response()->json([
			'status' => '200',
			'success' => true
		]);
    }
	
	public function store(Request $request)
    {
		
		$validate = \Validator::make($request->all(), [
			'name' => 'required|max:30',
		],
		
		$after_save = [
			'alert' => 'error',
			'title' => 'Failed!',
			'text-1' => 'Error exists,',
			'text-2' => 'please try again.'
		]);
		
		if($validate->fails()){
            return redirect('admin/role/create')
				->withErrors($validate, 'add')
				->withInput()
				->with('after_save', $after_save);
        }
		
		$after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Add new role',
            'text-2' => 'success.'
        ];
		
		$data = new Role;
		$data->name = $request->name;
		$data->description = $request->description;
		$data->created_by = Auth::id();
		$data->save();
		
		return redirect('admin/role')->with('after_save', $after_save);
    }
	
	public function update(Request $request, $id)
    {
		$validate = \Validator::make($request->all(), [
			
			'name' => 'required|max:30',
			
		],
		
		$after_save = [
			'alert' => 'error',
			'title' => 'Failed!',
			'text-1' => 'Error exists,',
			'text-2' => 'please try again.'
		]);
		
		if($validate->fails()){
            return redirect("admin/role/$id/edit")
				->withErrors($validate, 'update')
				->withInput()
				->with('after_save', $after_save);
        }
		
		$data = Role::find($id);
		$data->name = $request->name;
		$data->description = $request->description;
		$data->save();
		
		$after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Update role data',
            'text-2' => 'success.'
        ];
		
		return redirect('admin/role')->with('after_save', $after_save);
    }
	
}