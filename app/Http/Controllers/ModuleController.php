<?php

namespace App\Http\Controllers;

use App\Module;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Datatables;
use Hash;
use Illuminate\Validation\Rule;

class ModuleController extends Controller
{
    /**
     * Show a list of all of the application's province.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin/module/module');
    }

    public function ajaxdt()
    {
        $data = Module::with('user')->get();
        return Datatables::of($data)->make(true);
    }

    public function editajax(Request $req)
    {
        return response()->json([
            'status' => '200',
            'success' => true,
            'data' => Module::findOrFail($req->id)
        ]);
    }

    public function destroy($id)
    {
        $del = Module::find($id);
        $del->delete();
        return response()->json([
            'status' => '200',
            'success' => true
        ]);
    }

    public function storeModule(Request $request)
    {
        $validate = \Validator::make($request->all(), [
            'name' => 'required|unique:master_module|max:50',
        ],
            $after_save = [
                'alert' => 'error',
                'title' => 'Failed!',
                'text-1' => 'Error exists,',
                'text-2' => 'please try again.'
            ]);

        if($validate->fails()){
            return response()->json([
                'code' => '500',
                'alert' => 'error',
                'title' => 'Failed!',
                'description' => $validate->messages(),
                'success' => false
            ]);
        }

        $data = new Module;
        $data->name = $request->name;
//        $data->spervisor_id = $request->user;
        $data->created_by = Auth::id();
        $data->save();

        $after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Add new module',
            'text-2' => 'success.'
        ];

        return response()->json([
            'status' => '200',
            'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Create new module success.',
            'success' => true
        ]);
    }

    public function update(Request $request, $id)
    {
        $validate = \Validator::make($request->all(), [
            'name' => 'required|unique:master_module|max:50',
        ],

            $after_save = [
                'alert' => 'error',
                'title' => 'Failed!',
                'text-1' => 'Error exists,',
                'text-2' => 'please try again.'
            ]);

        if($validate->fails()){
            return response()->json([
                'code' => '500',
                'alert' => 'error',
                'title' => 'Failed!',
                'description' => $validate->messages(),
                'success' => false
            ]);
        }

        $data = Module::find($id);
        $data->name = $request->name;
//        $data->spervisor_id = $request->user;
        $data->save();

        $after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Update module data',
            'text-2' => 'success.'
        ];

        return response()->json([
            'status' => '200',
            'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Update data module success.',
            'success' => true
        ]);
    }

    function ShowAll(){
        return response()->json([
            'status' => '200',
            'success' => true,
            'data' => Module::all()
        ]);
    }

}