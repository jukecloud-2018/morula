<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Patient;
use App\PatientTemporary;
use App\Visit;
use App\Doctor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables;
use Auth;

class AdmissionController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function dataajax()
    {
        $data = Reservation::get();
        return Datatables::of($data)->make(true);
    }
	
	public function reservationdetail(Request $req)
	{
		$data = Reservation::with('patient')->find($req->id);
		
		if(count($data) > 0){
			return response()->json([
				'status' => '200',
				'alert' => 'success',
				'title' => 'Sukses!',
				'description' => 'Reservation data found.',
				'success' => true,
				'data' => $data
			]);
		
		} else {
			return response()->json([
				'status' => '300',
				'alert' => 'failed',
				'title' => 'Failed!',
				'description' => 'Reservation data not found.',
				'success' => false,
				'data' => null,
			]);
		}
	}
	
	
	public function reservationshowall(Request $req)
    {
        $validate = \Validator::make($req->all(), [
			'company_code' => 'required',
			'doctor_code' => 'required',
			'reservation_date' => 'required|date',
		]);
		
		if($validate->fails()){
			return response()->json([
				'code' => '500',
				'alert' => 'failed',
				'title' => 'Failed!',
				'description' => $validate->messages(),
				'success' => false
			]);
		}
		
		$reservation_date = $req->reservation_date;
		$hour_start = strtotime($reservation_date." 09:00");
		$hour_end = strtotime($reservation_date." 16:00");
		$duration = 30;
		$timeslots = array();
		
		while ($hour_start < $hour_end) {
		   $timeslots[] = date('H:i:s', $hour_start);
		   $hour_start = strtotime("+$duration minute", $hour_start);
		}
		
		$n = 0;
		foreach ( $timeslots as $slot ) {
		   $data[$n]['reservation_id'] = '';
		   $data[$n]['reservation_date'] = $reservation_date;
		   $data[$n]['session'] = $slot;
		   $data[$n]['morula_id'] = '';
		   $data[$n]['first_name'] = '';
		   $data[$n]['last_name'] = '';
		   $data[$n]['birth_date'] = '';
		   $data[$n]['sex'] = '';
		   $data[$n]['mobile'] = '';
		   $data[$n]['visit'] = '';
		   $data[$n]['doctor_code'] = '';
		   $data[$n]['description'] = '';
		   $data[$n]['operator'] = '';
		   $n++;
		}

		$reservation = Reservation::where('reservation_date', $reservation_date)->get();

		foreach ( $reservation as $reserv ) {
			$key = array_search($reserv->session, $timeslots);

			if(is_int($key)){
			    if(isset($reserv->morula_id))
			        $patient = Patient::find($reserv->morula_id);
			    else
                    $patient = PatientTemporary::find($reserv->temp_id);

			    if(count($patient) > 0){
                    $data[$key]['first_name'] = $patient->first_name;
                    $data[$key]['last_name'] = $patient->last_name;
                    $data[$key]['birth_date'] = $patient->birth_date;
                    $data[$key]['sex'] = $patient->sex;
                    $data[$key]['mobile'] = $patient->mobile;
                }

			    $visit = Visit::find($reserv->visit_code);
				$data[$key]['reservation_id'] = $reserv->id;
				$data[$key]['morula_id'] = $reserv->morula_id;
				$data[$key]['visit'] = $visit->name_visit;
				$data[$key]['doctor_code'] = $reserv->doctor_code;
				$data[$key]['description'] = $reserv->description;
				$data[$key]['operator'] = $reserv->created_by;
			}

		}
		
		return Datatables::of($data)->make(true);
		
    }

	public function reservationstore(Request $request)
	{
        $reservation = Reservation::where('reservation_date', $request->reservation_date)
            ->where('session', $request->session)
            ->first();

        if(count($reservation) > 0){
            return response()->json([
                'code' => '500',
                'alert' => 'failed',
                'title' => 'Gagal!',
                'description' => 'Reservation exists.',
                'success' => false
            ]);
        }

        $dataR = new Reservation;

		if(isset($request->morula_id)){
			
			$patient = Patient::where('id', $request->morula_id)->first();
			
			if(count($patient) == 0){
				return response()->json([
					'code' => '500',
					'alert' => 'failed',
					'title' => 'Gagal!',
					'description' => 'Patient not found',
					'success' => false
				]);
			}
			
			$validate = \Validator::make($request->all(), [
				'reservation_date' => 'required|date',
				'session' => 'required|date_format:H:i:s',
				'visit_code' => 'required|numeric',
				'morula_id' => 'required'
			]);
			
			if($validate->fails()){
				return response()->json([
					'code' => '500',
					'alert' => 'failed',
					'title' => 'Gagal!',
					'description' => $validate->messages(),
					'success' => false
				]);
			}

            $dataR->morula_id = $request->morula_id;
		
		} else {
			$validate = \Validator::make($request->all(), [
				'reservation_date' => 'required|date',
				'session' => 'required',
				'visit_code' => 'required|numeric',
				//master_patient
				'first_name' => 'required',
				'last_name' => 'required',
				'sex' => 'required',
				'init_doctor' => 'required|numeric',
				'birth_date' => 'required|date',
				'mobile' => 'required|numeric',
				'mobile_2' => 'required|numeric',
				'id_number' => 'required|unique:master_patient|unique:master_patient_temp|numeric',
				'id_type' => 'required',
			]);
			
			if($validate->fails()){
				return response()->json([
					'code' => '500',
					'alert' => 'failed',
					'title' => 'Gagal!',
					'description' => $validate->messages(),
					'success' => false
				]);
			}
			
			$data = new PatientTemporary;
			$data->first_name = $request->first_name;
			$data->last_name = $request->last_name;
			$data->sex = $request->sex;
			$data->init_doctor = $request->init_doctor;
			$data->birth_date = $request->birth_date;
			$data->mobile = $request->mobile;
			$data->mobile_2 = $request->mobile_2;
            $data->id_number = $request->id_number;
            $data->id_type = $request->id_type;
            $data->status = 1;
			$data->created_by = Auth::id();
			$data->save();

            $dataR->temp_id = $data->id;
		}

		$dataR->reservation_date = $request->reservation_date;
		$dataR->session = $request->session;
		$dataR->visit_code = $request->visit_code;
		$dataR->doctor_code = $request->doctor_code;
		$dataR->description = $request->description;
		$dataR->created_by = Auth::id();
		$dataR->save();
		
		return response()->json([
			'status' => '200',
			'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Create new reservation success.',
			'success' => true
		]);
		
	}
	
	public function visitcodeshowall()
	{
		$data = Visit::get();
		
		if(count($data) > 0){
			return response()->json([
				'status' => '200',
				'alert' => 'success',
				'title' => 'Sukses!',
				'description' => 'Visit code data found.',
				'success' => true,
				'data' => $data
			]);
		
		} else {
			return response()->json([
				'status' => '300',
				'alert' => 'failed',
				'title' => 'Failed!',
				'description' => 'Visit code data not found.',
				'success' => false,
				'data' => null,
			]);
		}
	}
	
	public function doctorsearchbycode(Request $req)
	{
		$data = Doctor::where('doctor_code', $req->code)->first();
		
		if(count($data) > 0){
			return response()->json([
				'status' => '200',
				'alert' => 'success',
				'title' => 'Sukses!',
				'description' => 'Doctor data found.',
				'success' => true,
				'data' => $data->name
			]);
		
		} else {
			return response()->json([
				'status' => '300',
				'alert' => 'failed',
				'title' => 'Failed!',
				'description' => 'Doctor data not found.',
				'success' => false,
				'data' => null,
			]);
		}
	}

	public function doctorshowall()
	{
		$data = Doctor::get();
		
		if(count($data) > 0){
			return response()->json([
				'status' => '200',
				'alert' => 'success',
				'title' => 'Sukses!',
				'description' => 'Doctor data found.',
				'success' => true,
				'data' => $data
			]);
		
		} else {
			return response()->json([
				'status' => '300',
				'alert' => 'failed',
				'title' => 'Failed!',
				'description' => 'Doctor data not found.',
				'success' => false,
				'data' => null,
			]);
		}
	}
	
	public function doctorlist()
	{
		$data = Doctor::get();
		$data_doctor = [];
		foreach($data as $dt){
			$doctor['id'] = $dt->id;
			$doctor['text'] = ($dt->doctor_code == "" ? "" : $dt->doctor_code." - ").$dt->name;
			$data_doctor[] = $doctor;
		}
		
		if(count($data) > 0){
			return response()->json($data_doctor);
		
		} else {
			return response()->json(null);
		}
	}

	public function initpatient(Request $req)
    {

    }
	
}
