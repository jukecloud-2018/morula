<?php

namespace App\Http\Controllers;

use App\User;
use App\UserRole;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Datatables;
use Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */
	
	 
    public function index(Request $request)
    {
		return view('admin/user/index', ['roles' => Role::get()->sortByDesc("id"), 
									'max' => Role::orderBy('id', 'desc')->get(),
								]);
    }
	
	public function ajaxdt()
	{
		$data = User::where('id', '!=', Auth::id())
			->orWhereNull('id')->get();
		return Datatables::of($data)->make(true);
	}
	
	public function show($id)
    {
        return view('admin/user/edit', ['user' => User::findOrFail($id)]);
    }
	
	public function create()
    {
		$roles = Role::get()->sortByDesc("id");
		return view('admin/user/create', ['roles' => $roles, 'max' => Role::orderBy('id', 'desc')->get()]);
    }
	
	public function edit($id)
    {
        return view('admin/user/edit', ['user' => User::findOrFail($id)]);
    }
	
	public function editajax(Request $req)
    {
		return response()->json([
			'status' => '200',
			'success' => true,
			'data' => User::findOrFail($req->id)
		]);
    }
	
	public function destroy($id)
    {
		$del = User::find($id);
		
		$del->delete();
		
		return response()->json([
			'status' => '200',
			'success' => true
		]);
    }
	
	public function store(Request $request)
    {
		
		$validate = \Validator::make($request->all(), [
			'username' => 'required|unique:users|max:20',
			'password' => 'required',
			'name' => 'required|max:30',
			'email' => 'email|max:50',
			'phone' => 'numeric'
		],
		
		$after_save = [
			'alert' => 'error',
			'title' => 'Failed!',
			'text-1' => 'Error exists,',
			'text-2' => 'please try again.'
		]);
		
		if($validate->fails()){
            return redirect('admin/user/create')
				->withErrors($validate, 'add')
				->withInput()
				->with('after_save', $after_save);
        }
		
		$after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Add new user',
            'text-2' => 'success.'
        ];
		
		$data = new User;
		$data->username = $request->username;
		$data->password = Hash::make($request->password);
		$data->name = $request->name;
		$data->email = $request->email;
		$data->phone = $request->phone;
		$data->description = $request->description;
		
		if(count($request->roles) > 0){
			$roles = implode(",", $request->roles);
			$data->roles = $roles;
		}
		
		$data->save();
		return redirect('admin/user')->with('after_save', $after_save);
    }
	
	public function storeajax(Request $request)
    {
		
		$validate = \Validator::make($request->all(), [
			'username' => 'required|unique:users|max:20',
			'password' => 'required',
			'name' => 'required|max:30',
			'email' => 'required|email|max:50',
			'phone' => 'numeric'
		], 
			$after_save = [
				'alert' => 'error',
				'title' => 'Failed!',
				'text-1' => 'Error exists,',
				'text-2' => 'please try again.'
		]);
		
		/*if($validate->fails()){
            return redirect('admin/user/create')
				->withErrors($validate, 'add')
				->withInput()
				->with('after_save', $after_save);
        }*/
		
		if($validate->fails()){
			return response()->json([
				'code' => '500',
				'alert' => 'failed',
				'title' => 'Gagal!',
				'description' => $validate->messages(),
				'success' => false
			]);
		}
		
		$after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Add new user',
            'text-2' => 'success.'
        ];
		
		$data = new User;
		$data->username = $request->username;
		$data->password = Hash::make($request->password);
		$data->name = $request->name;
		$data->email = $request->email;
		$data->phone = $request->phone;
		$data->description = $request->description;
		
		if(count($request->roles) > 0){
			$roles = implode(",", $request->roles);
			$data->roles = $roles;
		}
		
		$data->save();
		//return redirect('admin/user')->with('after_save', $after_save);
		
		return response()->json([
			'status' => '200',
			'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Create new reservation success.',
			'success' => true
		]);
    }
	
	public function update(Request $request, $id)
    {
		$validate = \Validator::make($request->all(), [
			'username' => [
				'required',
				'max:20',
				Rule::unique('users')->ignore($id),
			],
			'name' => 'required|max:30',
			'email' => 'email',
			'phone' => 'numeric'
		],
		
		$after_save = [
            'alert' => 'error',
			'title' => 'Failed!',
			'text-1' => 'Error exists,',
			'text-2' => 'please try again.'
		]);
		
		if($validate->fails()){
            return redirect("admin/user/$id/edit")
				->withErrors($validate, 'update')
				->withInput()
				->with('after_save', $after_save);
        }
		
		$data = User::find($id);
		$data->username = $request->username;
		$data->name = $request->name;
		$data->email = $request->email;
		$data->phone = $request->phone;
		$data->description = $request->description;
		
		if($request->password != '')
			$data->password = Hash::make($request->password);
		
		if(count($request->roles) > 0){
			$roles = implode(",", $request->roles);
			$data->roles = $roles;
		}
		
		$data->save();
		
		$after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Update user data',
            'text-2' => 'success.'
        ];
		
		return redirect('admin/user')->with('after_save', $after_save);
    }
	
}