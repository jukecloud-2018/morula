<?php

namespace App\Http\Controllers;

use App\Doctor;
use App\DoctorSpecialization;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Datatables;
use Hash;
use Illuminate\Validation\Rule;
use Illuminate\Http\UploadedFile;

class DoctorController extends Controller
{
    /**
     * Show a list of all of the application's doctors.
     *
     * @return Response
     */
    public function index(Request $request)
    {
		return view('admin/doctor/index', ['specialization' => DoctorSpecialization::all()]);
    }
	
	public function ajaxdt()
	{
		$data = Doctor::get();
		return Datatables::of($data)->make(true);
	}
	
	public function show($id)
    {
        return view('admin/doctor-show', ['doctor' => Doctor::findOrFail($id)]);
    }
	
	public function create()
    {
		
		return view('admin/doctor-create');
    }
	
	public function edit($id)
    {
        return view('admin/doctor-edit', ['doctor' => Doctor::findOrFail($id)]);
    }
	
	public function editajax(Request $req)
    {
		return response()->json([
			'status' => '200',
			'success' => true,
			'data' => Doctor::findOrFail($req->id)
		]);
    }
	
	public function destroy($id)
    {
		$del = Doctor::find($id);
		
		$del->delete();
		
		return response()->json([
			'status' => '200',
			'success' => true
		]);
    }
	
	public function store(Request $request)
    {
		$validate = \Validator::make($request->all(), [
			'name' => 'required',
			'specialization' => 'required',
			//'doctor_code' => 'required|unique:master_doctor|size:3',
			//'nip' => 'required|unique:master_doctor|numeric',
			'shift' => 'required',
			'status' => 'required'
		], 
			$after_save = [
				'alert' => 'error',
				'title' => 'Failed!',
				'text-1' => 'Error exists,',
				'text-2' => 'please try again.'
		]);
		
		if($validate->fails()){
			return response()->json([
				'code' => '500',
				'alert' => 'error',
				'title' => 'Failed!',
				'description' => $validate->messages(),
				'success' => false
			]);
		}
		
		$data = new Doctor;
		$data->name = $request->name;
		$data->specialization = $request->specialization;
		$data->doctor_code = strtoupper($request->doctor_code);
		$data->status = $request->status;
		$data->nip = $request->nip;
		$data->share_fee = $request->share_fee;
		$data->shift = $request->shift;
		
		if($request->hasFile('photo')) {
			if ($request->file('photo')->isValid()) {
				$upload_foto_filename = time().'_'.$request->photo->getClientOriginalName();
				$request->photo->move(public_path('uploads/doctor'), $upload_foto_filename);
				$data->photo = $upload_foto_filename;
			}
		}
		
		$data->created_by = Auth::id();
		$data->save();
		
		$after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Add new doctor',
            'text-2' => 'success.'
        ];
		
		return response()->json([
			'status' => '200',
			'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Create new doctor success.',
			'success' => true
		]);
    }
	
	public function update(Request $request, $id)
    {
		$validate = \Validator::make($request->all(), [
			'name' => 'required',
			'specialization' => 'required',
			/*'doctor_code' => ['required',
				'size:3',
				Rule::unique('master_doctor')->ignore($id)
			],
			'nip' => ['required',
				'numeric',
				Rule::unique('master_doctor')->ignore($id)
			],*/
			'shift' => 'required',
			'status' => 'required'
		],
		
		$after_save = [
			'alert' => 'error',
			'title' => 'Failed!',
			'text-1' => 'Error exists,',
			'text-2' => 'please try again.'
		]);
		
		if($validate->fails()){
			return response()->json([
				'code' => '500',
				'alert' => 'error',
				'title' => 'Failed!',
				'description' => $validate->messages(),
				'success' => false
			]);
		}
		
		$data = Doctor::find($id);
		$data->name = $request->name;
		$data->specialization = $request->specialization;
		$data->doctor_code = strtoupper($request->doctor_code);
		$data->status = $request->status;
		$data->nip = $request->nip;
		$data->share_fee = $request->share_fee;
		$data->shift = $request->shift;
		
		if($request->hasFile('photo')) {
			if ($request->file('photo')->isValid()) {
				$upload_foto_filename = time().'_'.$request->photo->getClientOriginalName();
				$request->photo->move(public_path('uploads/doctor'), $upload_foto_filename);
				$data->photo = $upload_foto_filename;
			}
		}
		
		$data->updated_by = Auth::id();
		$data->save();
		
		$after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Update doctor data',
            'text-2' => 'success.'
        ];
		
		return response()->json([
			'status' => '200',
			'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Update data doctor success.',
			'success' => true
		]);
    }
	
}