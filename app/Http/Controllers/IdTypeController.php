<?php

namespace App\Http\Controllers;

use App\IdType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Datatables;
use Hash;
use Illuminate\Validation\Rule;

class IdTypeController extends Controller
{
    /**
     * Show a list of all of the application's province.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        return view('admin/id_type/id_type');
    }

    public function ajaxdt()
    {
        $data = IdType::get();
        return Datatables::of($data)->make(true);
    }

    public function editajax(Request $req)
    {
        return response()->json([
            'status' => '200',
            'success' => true,
            'data' => IdType::findOrFail($req->id)
        ]);
    }

    public function destroy($id)
    {
        $del = IdType::find($id);
        $del->delete();
        return response()->json([
            'status' => '200',
            'success' => true
        ]);
    }

    public function storeid(Request $request)
    {
        $validate = \Validator::make($request->all(), [
            'name' => 'required|unique:master_id_type|max:255', //name dari inputan
        ],
            $after_save = [
                'alert' => 'error',
                'title' => 'Failed!',
                'text-1' => 'Error exists,',
                'text-2' => 'please try again.'
            ]);

        if($validate->fails()){
            return response()->json([
                'code' => '500',
                'alert' => 'error',
                'title' => 'Failed!',
                'description' => $validate->messages(),
                'success' => false
            ]);
        }

        $data = new IdType();
        $data->name = $request->name; //$data->name (nama di tabel) , name lainnya nama inputan
        $data->created_by = Auth::id();
        $data->save();

        $after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Add new ID type',
            'text-2' => 'success.'
        ];

        return response()->json([
            'status' => '200',
            'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Create new ID type success.',
            'success' => true
        ]);
    }

    public function update(Request $request, $id)
    {
        $validate = \Validator::make($request->all(), [
            'name' => 'required|unique:master_id_type|max:255', //name inoutan
        ],

            $after_save = [
                'alert' => 'error',
                'title' => 'Failed!',
                'text-1' => 'Error exists,',
                'text-2' => 'please try again.'
            ]);

        if($validate->fails()){
            return response()->json([
                'code' => '500',
                'alert' => 'error',
                'title' => 'Failed!',
                'description' => $validate->messages(),
                'success' => false
            ]);
        }

        $data = IdType::find($id);
        $data->name = $request->name;
        $data->save();

        $after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Update ID type data',
            'text-2' => 'success.'
        ];

        return response()->json([
            'status' => '200',
            'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Update ID type success.',
            'success' => true
        ]);
    }
}