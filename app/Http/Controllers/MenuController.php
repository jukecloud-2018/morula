<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Menu;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Datatables;
use Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class MenuController extends Controller
{
    /**
     * Show a list of all of the application's province.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        return view('admin/menu/menu', ['roles' => Role::get()->sortByDesc("id"),
									'max' => Role::orderBy('id', 'desc')->get()
								]);
    }

    public function ajaxdt()
    {
        $data = Menu::get();
        return Datatables::of($data)->make(true);
    }

//    public function show($id)
//    {
//        return view('admin/menu/menu-edit', ['menu' => Menu::findOrFail($id)]);
//    }

//    public function create()
//    {
////        $roles = Role::get()->sortByDesc('id');
//        $data["roles"] = Role::get()->sortByDesc("id") ;
//        $data['max'] = Role::orderBy('id', 'desc')->get();
//        return view('admin/menu/menu-create', $data);
////        return view('admin/menu-create', ['role' => $roles, 'max' => Role::orderBy('id', 'desc')->get()]);
//    }

//    public function edit($id)
//    {
//        return view('admin/menu/menu-edit', ['menu' => Menu::findOrFail($id)]);
//    }

    public function editajax(Request $req)
    {
        return response()->json([
            'status' => '200',
            'success' => true,
            'data' => Menu::findOrFail($req->id)
        ]);
    }

    public function destroy($id)
    {
        $del = Menu::find($id);
        $del->delete();
        return response()->json([
            'status' => '200',
            'success' => true
        ]);
    }

    public function store(Request $request)
    {
        $validate = \Validator::make($request->all(), [
            //validasi
            'name' => 'required|unique:master_menu|max:30',
            'sort' => 'required',
            'parent_id' => 'required',
            'url' => 'required',
            'active_classes' => 'required',
        ],

            $after_save = [
                'alert' => 'error',
                'title' => 'Failed!',
                'text-1' => 'Error exists,',
                'text-2' => 'please try again.'
            ]);

        if($validate->fails()){
            return response()->json([
                'code' => '500',
                'alert' => 'error',
                'title' => 'Failed!',
                'description' => $validate->messages(),
                'success' => false
            ]);
        }

        $data = new Menu;
        $data->name = $request->name;
        $data->sort = $request->sort;
        $data->parent_id = $request->parent_id;
        $data->url = $request->url;
        $data->active_classes = $request->active_classes;
        $data->icon = $request->icon;
        $data->created_by = Auth::id();

        if(count($request->roles) > 0) {
            $roles = implode(",", $request->roles);
            $data->roles = $roles;
        }
        $data->save();

        $after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Add new menu',
            'text-2' => 'success.'
        ];

        return response()->json([
            'status' => '200',
            'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Create new menu success.',
            'success' => true
        ]);
    }

    public function update(Request $request, $id)
    {
        $validate = \Validator::make($request->all(), [
            //validasi
            'name' => 'required|unique:master_menu|max:30',
            'sort' => 'required',
            'parent_id' => 'required',
            'roles' => 'required',
            'url' => 'required',
            'active_classes' => 'required',
        ],

            $after_save = [
                'alert' => 'error',
                'title' => 'Failed!',
                'text-1' => 'Error exists,',
                'text-2' => 'please try again.'
            ]);

        if($validate->fails()){
            return response()->json([
                'code' => '500',
                'alert' => 'error',
                'title' => 'Failed!',
                'description' => $validate->messages(),
                'success' => false
            ]);
        }

        $data = Menu::find($id);
        $data->name = $request->name;
        $data->sort = $request->sort;
        $data->parent_id = $request->parent_id;
        $data->url = $request->url;
        $data->active_classes = $request->active_classes;
        $data->icon = $request->icon;
        $data->created_by = Auth::id();

        if(count($request->roles) > 0) {
            $roles = implode(",", $request->roles);
            $data->roles = $roles;
        }
        $data->save();

        $after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Update menu data',
            'text-2' => 'success.'
        ];

        return response()->json([
            'status' => '200',
            'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Update data menu success.',
            'success' => true
        ]);
    }

}