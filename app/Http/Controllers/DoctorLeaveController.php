<?php

namespace App\Http\Controllers;

use App\DoctorLeave;
use App\Doctor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Datatables;
use Hash;
use Illuminate\Validation\Rule;

class DoctorLeaveController extends Controller
{
    /**
     * Show a list of all of the application's province.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        return view('admission/doctor-leave', ['doctor' => Doctor::all()]);
    }

    public function ajaxdt()
    {
//        $data = Doctor_request_session_leave::with('doctor')->get();
        $data = DoctorLate::all();
        return Datatables::of($data)->make(true);
    }

//    public function show($id)
//    {
//        return view('admin/doctor-schedule', ['city' => Doctor_request_session_leave::findOrFail($id)]);
//    }
//
//    public function create()
//    {
//        return view('admin/doctor-schedule');
//    }
//
//    public function edit($id)
//    {
//        return view('admin/doctor-schedule', ['city' => Doctor_request_session_leave::findOrFail($id)]);
//    }

    public function editajax(Request $req)
    {
        return response()->json([
            'status' => '200',
            'success' => true,
            'data' => DoctorLeave::findOrFail($req->id)
        ]);
    }

    public function destroy($id)
    {
        $del = DoctorLeave::find($id);
        $del->delete();

        return response()->json([
            'status' => '200',
            'success' => true
        ]);
    }

    public function storeRequestLeave(Request $request)
    {
        $validate = \Validator::make($request->all(), [
            'start' => 'required',
            'end' => 'required',
            'doctor' => 'required',
            'type_leave' => 'required',
        ],
            $after_save = [
                'alert' => 'error',
                'title' => 'Failed!',
                'text-1' => 'Error exists,',
                'text-2' => 'please try again.'
            ]);

        if($validate->fails()){
            return response()->json([
                'code' => '500',
                'alert' => 'error',
                'title' => 'Failed!',
                'description' => $validate->messages(),
                'success' => false
            ]);
        }

        $data = new DoctorLeave();
        $data->doctor_id = $request->doctor;
        $data->start_date = $request->start;
        $data->end_date = $request->end;
        $data->leave_type = $request->type_leave;
        $data->cause= $request->cause;
        $data->created_by = Auth::id();
        $data->save();
//        if(count($request->leave_type) > 0) {
//            $leave_type = implode(",", $request->leave_type);
//            $data->leave_type = $leave_type;
//        }

        $after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Add new city',
            'text-2' => 'success.'
        ];

        return response()->json([
            'status' => '200',
            'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Create New Doctor Late.',
            'success' => true
        ]);
    }

//    public function update(Request $request, $id)
//    {
//        $validate = \Validator::make($request->all(), [
//            'name' => 'required|max:255',
////            'province_id' => ['required|',
////                Province::unique('master_doctor')->ignore($id)]
//            ],
//
//            $after_save = [
//                'alert' => 'error',
//                'title' => 'Failed!',
//                'text-1' => 'Error exists,',
//                'text-2' => 'please try again.'
//            ]);
//
//        if($validate->fails()){
//            return response()->json([
//                'code' => '500',
//                'alert' => 'error',
//                'title' => 'Failed!',
//                'description' => $validate->messages(),
//                'success' => false
//            ]);
//        }
//
//        $data = City::find($id);
//        $data->name = $request->name;
//        $data->province_id = $request->province;
//        $data->save();
//
//        $after_save = [
//            'alert' => 'success',
//            'title' => 'Saved!',
//            'text-1' => 'Update city data',
//            'text-2' => 'success.'
//        ];
//
//        return response()->json([
//            'status' => '200',
//            'alert' => 'success',
//            'title' => 'Sukses!',
//            'description' => 'Update data city success.',
//            'success' => true
//        ]);
//    }

}