<?php

namespace App\Http\Controllers;

use App\Country;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Datatables;
use Hash;
use Illuminate\Validation\Rule;

class CountryController extends Controller
{
    /**
     * Show a list of all of the application's province.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        return view('admin/setting/province');
    }

    public function ajaxdt()
    {
        $data = Country::get();
        return Datatables::of($data)->make(true);
    }

    public function editajax(Request $req)
    {
        return response()->json([
            'status' => '200',
            'success' => true,
            'data' => Country::findOrFail($req->id)
        ]);
    }

    public function destroy($id)
    {
        $del = Country::find($id);
        $del->delete();
        return response()->json([
            'status' => '200',
            'success' => true
        ]);
    }

    public function storeCountry(Request $request)
    {
        $validate = \Validator::make($request->all(), [
            'name' => 'required|unique:master_country|max:255',
        ],
            $after_save = [
                'alert' => 'error',
                'title' => 'Failed!',
                'text-1' => 'Error exists,',
                'text-2' => 'please try again.'
            ]);

        if($validate->fails()){
            return response()->json([
            'code' => '500',
				'alert' => 'error',
				'title' => 'Failed!',
				'description' => $validate->messages(),
				'success' => false
            ]);
        }

        $data = new Country;
        $data->name = $request->name;
        $data->created_by = Auth::id();
        $data->save();

        $after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Add new country',
            'text-2' => 'success.'
        ];

        return  response()->json([
            'status' => '200',
            'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Create new country success.',
            'success' => true
        ]);
    }

    public function update(Request $request, $id)
    {
        $validate = \Validator::make($request->all(), [
            'name' => 'required|unique:master_country|max:255',
        ],

            $after_save = [
                'alert' => 'error',
                'title' => 'Failed!',
                'text-1' => 'Error exists,',
                'text-2' => 'please try again.'
            ]);

        if($validate->fails()){
            return response()->json([
                'code' => '500',
                'alert' => 'error',
                'title' => 'Failed!',
                'description' => $validate->messages(),
                'success' => false
            ]);
        }

        $data = Country::find($id);
        $data->name = $request->name;
        $data->save();

        $after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Update country data',
            'text-2' => 'success.'
        ];

        return response()->json([
            'status' => '200',
            'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Update data country success.',
            'success' => true
        ]);
    }
}