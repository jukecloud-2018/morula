<?php

namespace App\Http\Controllers;

use App\DoctorStatus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Datatables;
use Hash;
use Illuminate\Validation\Rule;

class DoctorStatusController extends Controller
{
    /**
     * Show a list of all of the application's province.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        return view('admin/doctor_status');
    }

    public function ajaxdt()
    {
        $data = DoctorStatus::get();
        return Datatables::of($data)->make(true);
    }

    public function editajax(Request $req)
    {
        return response()->json([
            'status' => '200',
            'success' => true,
            'data' => DoctorStatus::findOrFail($req->id)
        ]);
    }

    public function destroy($id)
    {
        $del = DoctorStatus::find($id);
        $del->delete();
        return response()->json([
            'status' => '200',
            'success' => true
        ]);
    }

    public function storeDoctorStatus(Request $request)
    {
        $validate = \Validator::make($request->all(), [
            'name' => 'required|unique:doctor_status|max:50', //name dari inputan
        ],
            $after_save = [
                'alert' => 'error',
                'title' => 'Failed!',
                'text-1' => 'Error exists,',
                'text-2' => 'please try again.'
            ]);

        if($validate->fails()){
            return response()->json([
                'code' => '500',
                'alert' => 'error',
                'title' => 'Failed!',
                'description' => $validate->messages(),
                'success' => false
            ]);
        }

        $data = new DoctorStatus();
        $data->name = $request->name; //$data->name (nama di tabel) , name lainnya nama inputan
        $data->created_by = Auth::id();
        $data->save();

        $after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Add new status doctor',
            'text-2' => 'success.'
        ];

        return response()->json([
            'status' => '200',
            'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Create new status doctor success.',
            'success' => true
        ]);
    }

    public function update(Request $request, $id)
    {
        $validate = \Validator::make($request->all(), [
            'name' => 'required|unique:doctor_status|max:50', //name inoutan
        ],

            $after_save = [
                'alert' => 'error',
                'title' => 'Failed!',
                'text-1' => 'Error exists,',
                'text-2' => 'please try again.'
            ]);

        if($validate->fails()){
            return response()->json([
                'code' => '500',
                'alert' => 'error',
                'title' => 'Failed!',
                'description' => $validate->messages(),
                'success' => false
            ]);
        }

        $data = DoctorStatus::find($id);
        $data->name = $request->name;
        $data->save();

        $after_save = [
            'alert' => 'success',
            'title' => 'Saved!',
            'text-1' => 'Update status doctor data',
            'text-2' => 'success.'
        ];

        return response()->json([
            'status' => '200',
            'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Update status doctor success.',
            'success' => true
        ]);
    }
}