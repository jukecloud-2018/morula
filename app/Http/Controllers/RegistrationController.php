<?php

namespace App\Http\Controllers;

use App\Patient;
use App\Doctor;
use App\PatientEmergency;
use Illuminate\Support\Facades\Config;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Datatables;
use Auth;
use Illuminate\Http\UploadedFile;

class RegistrationController extends Controller
{
    /*public function __construct()
	{
		$this->middleware('auth');
	}*/
	public function index()
	{
		return view('admission/registration/index');
	}
	
	public function indexajax()
    {
        $data = Patient::get();
		if(count($data) > 0){
			return response()->json([
				'status' => '200',
				'alert' => 'success',
				'title' => 'Sukses!',
				'description' => 'All Patients data found.',
				'success' => true,
				'data' => $data
			]);
		
		} else {
			return response()->json([
				'status' => '300',
				'alert' => 'failed',
				'title' => 'Failed!',
				'description' => 'All Patients data not found.',
				'success' => false,
				'data' => null,
			]);
		}
    }
	
	public function ajaxdt()
    {
        $data = Patient::get();
        return Datatables::of($data)->make(true);
    }
	
	public function show($id)
	{
		$data = Patient::findOrFail($id);
		$data_doctor = Doctor::find($data->init_doctor);
		return view('admission/patient/show', ['data' => $data, 'data_doctor' => $data_doctor]);
	}
	
	public function detail(Request $req)
	{
		$data = Patient::findOrFail($req->id);
		
		if(count($data) > 0){
			return response()->json([
				'status' => '200',
				'alert' => 'success',
				'title' => 'Sukses!',
				'description' => 'Patient data found.',
				'success' => true,
				'data' => $data
			]);
		
		} else {
			return response()->json([
				'status' => '300',
				'alert' => 'failed',
				'title' => 'Failed!',
				'description' => 'Patient data not found.',
				'success' => false,
				'data' => null,
			]);
		}
	}

    public function reservationdetail(Request $req)
    {
        $data = Reservation::with('patient')->find($req->id);

        if(count($data) > 0){
            return response()->json([
                'status' => '200',
                'alert' => 'success',
                'title' => 'Sukses!',
                'description' => 'Reservation data found.',
                'success' => true,
                'data' => $data
            ]);

        } else {
            return response()->json([
                'status' => '300',
                'alert' => 'failed',
                'title' => 'Failed!',
                'description' => 'Reservation data not found.',
                'success' => false,
                'data' => null,
            ]);
        }
    }

    public function reservationshowall(Request $req)
    {
        $validate = \Validator::make($req->all(), [
            'company_code' => 'required',
            'doctor_code' => 'required',
            'reservation_date' => 'required|date',
        ]);

        if($validate->fails()){
            return response()->json([
                'code' => '500',
                'alert' => 'failed',
                'title' => 'Failed!',
                'description' => $validate->messages(),
                'success' => false
            ]);
        }

        $reservation_date = $req->reservation_date;
        $hour_start = strtotime($reservation_date." 09:00");
        $hour_end = strtotime($reservation_date." 16:00");
        $duration = 30;
        $timeslots = array();

        while ($hour_start < $hour_end) {
            $timeslots[] = date('H:i:s', $hour_start);
            $hour_start = strtotime("+$duration minute", $hour_start);
        }

        $n = 0;
        foreach ( $timeslots as $slot ) {
            $data[$n]['reservation_id'] = '';
            $data[$n]['reservation_date'] = $reservation_date;
            $data[$n]['session'] = $slot;
            $data[$n]['morula_id'] = '';
            $data[$n]['first_name'] = '';
            $data[$n]['last_name'] = '';
            $data[$n]['birth_date'] = '';
            $data[$n]['sex'] = '';
            $data[$n]['mobile'] = '';
            $data[$n]['visit'] = '';
            $data[$n]['doctor_code'] = '';
            $data[$n]['description'] = '';
            $data[$n]['operator'] = '';
            $n++;
        }

        $reservation = Reservation::where('reservation_date', $reservation_date)->get();

        foreach ( $reservation as $reserv ) {
            $key = array_search($reserv->session, $timeslots);

            if(is_int($key)){
                if(isset($reserv->morula_id))
                    $patient = Patient::find($reserv->morula_id);
                else
                    $patient = Patienttemp::find($reserv->temp_id);

                if(count($patient) > 0){
                    $data[$key]['first_name'] = $patient->first_name;
                    $data[$key]['last_name'] = $patient->last_name;
                    $data[$key]['birth_date'] = $patient->birth_date;
                    $data[$key]['sex'] = $patient->sex;
                    $data[$key]['mobile'] = $patient->mobile;
                }

                $visit = Visit::find($reserv->visit_code);
                $data[$key]['reservation_id'] = $reserv->id;
                $data[$key]['morula_id'] = $reserv->morula_id;
                $data[$key]['visit'] = $visit->name_visit;
                $data[$key]['doctor_code'] = $reserv->doctor_code;
                $data[$key]['description'] = $reserv->description;
                $data[$key]['operator'] = $reserv->created_by;
            }

        }

        return Datatables::of($data)->make(true);

    }
	
	public function searchbyname(Request $req)
    {
        $validate = \Validator::make($req->all(), [
			'name' => 'required',
			'birth_date' => 'required|date',
		]);
		
		if($validate->fails()){
			return response()->json([
				'code' => '500',
				'alert' => 'failed',
				'title' => 'Gagal!',
				'description' => $validate->messages(),
				'success' => false
			]);
		}
		
		$data = Patient::where('birth_date', $req->birth_date)
							->whereRaw('concat(first_name," ",last_name) like ?', "%{$req->name}%")
							->get();
							
		if(count($data) > 0){
			return response()->json([
				'status' => '200',
				'alert' => 'success',
				'title' => 'Sukses!',
				'description' => 'Patient data found.',
				'success' => true,
				'data' => $data
			]);
		
		} else {
			return response()->json([
				'status' => '300',
				'alert' => 'failed',
				'title' => 'Failed!',
				'description' => 'Patient data not found.',
				'success' => false,
				'data' => null,
			]);
		}
		
    }
	
	public function searchbynamedt(Request $req)
    {
		$data = Patient::where('birth_date', $req->birth_date)
							->whereRaw('concat(first_name," ",last_name) like ?', "%{$req->name}%")
							->get();
							
		return Datatables::of($data)->make(true);
		
    }

	/*public function store(Request $request)
	{
		
		$validate = \Validator::make($request->all(), [
			'first_name' => 'required',
			'last_name' => 'required',
			'sex' => 'required',
			'init_doctor' => 'required',
			'date_birth' => 'required',
			'mobile' => 'required|number'
			
		]);
		
		if($validate->fails()){
			return response()->json([
				'code' => '500',
				'alert' => 'failed',
				'title' => 'Gagal!',
				'description' => $validate->messages(),
				'success' => false
			]);
		}
		
		$data = new Patient;
		$data->first_name = $request->first_name;
		$data->last_name = $request->last_name;
		$data->sex = $request->sex;
		$data->init_doctor = $request->init_doctor;
		$data->place_birth = $request->place_birth;
		$data->date_birth = $request->date_birth;
		$data->email = $request->email;
		$data->office_phone = $request->office_phone;
		$data->body_height = $request->body_height;
		$data->blood = $request->blood;
		$data->address = $request->address;
		$data->city = $request->city;
		$data->postal_code = $request->postal_code;
		$data->phone = $request->phone;
		$data->citizenship = $request->citizenship;
		$data->id_number = $request->id_number;
		$data->id_type = $request->id_type;
		$data->religion = $request->religion;
		$data->marital_status = $request->marital_status;
		$data->company = $request->company;
		$data->active = 1;
		$data->created_by = Auth::id();
		$data->save();
		
		$upd = Patient::find($data->id);
		$morula_id = '1400'.str_pad($upd->id, 6, "0", STR_PAD_LEFT);
		$upd->morula_id = $morula_id;
		$upd->save();
		
		return response()->json([
			'code' => '200',
			'alert' => 'success',
            'title' => 'Sukses!',
            'description' => 'Create new patient success.',
			'success' => true
		]);
		
	}*/
	
	public function update(Request $request, $id)
	{
		
		$validate = \Validator::make($request->all(), [
			'first_name' => 'required',
			'last_name' => 'required',
			'init_doctor' => 'required|numeric',
			
			'id_number' => [
				'required',
				Rule::unique('master_patient')->ignore($id),
				'numeric'
			],
			'id_type' => 'required',
			'birth_place' => 'required',
			'birth_date' => 'required|date',
			'sex' => 'required',
			'body_height' => 'required',
			'blood' => 'required',
			'marital_status' => 'required',
			'married_date' => 'required',
			'religion' => 'required',
			'nationality' => 'required',
			
			'address' => 'required',
			'city' => 'required',
			'postal_code' => 'numeric',
			'phone' => 'numeric',
			'mobile' => 'required|numeric',
			'mobile_2' => 'numeric',
			
			'office_name' => 'required',
			'office_phone' => 'numeric',
			'job_type' => 'required',
			'email' => 'email',
			
			'emergency_name' => 'required',
			'emergency_relationship' => 'required',
			'emergency_address' => 'required',
			'emergency_city' => 'required',
			'emergency_postcode' => 'numeric',
			'emergency_phone' => 'required',
			'emergency_email' => 'email',
		],
		
		$after_save = [
			'alert' => 'error',
			'title' => 'Failed!',
			'text-1' => 'Ada kesalahan',
			'text-2' => 'Silakan coba lagi.'
		]);
		
		if($validate->fails()){
            return redirect()
				->back()
				->withErrors($validate, 'update')
				->withInput()
				->with('after_save', $after_save);
        }
		
		
		
		//$data = new Patient;
		$data = Patient::find($id);
		//$data->company_id = '1400';
		$data->first_name = $request->first_name;
		$data->last_name = $request->last_name;
		$data->init_doctor = $request->init_doctor;
		
		$upload_foto_filename = '';
		if ($request->hasFile('foto')) {
			if ($request->file('foto')->isValid()) {
				$upload_foto_filename = time().'_'.$request->foto->getClientOriginalName();
				$request->foto->move(public_path('uploads/patient'), $upload_foto_filename);
				$data->foto = $upload_foto_filename;
			}
		}
		
		$data->id_number = $request->id_number;
		$data->id_type = $request->id_type;
		$data->birth_place = $request->birth_place;
		$data->birth_date = $request->birth_date;
		$data->sex = $request->sex;
		$data->body_height = $request->body_height;
		$data->blood = $request->blood;
		$data->marital_status = $request->marital_status;
		$data->married_date = $request->married_date;
		$data->religion = $request->religion;
		$data->nationality = $request->nationality;
		
		$data->address = $request->address;
		$data->city = $request->city;
		$data->postal_code = $request->postal_code;
		$data->phone = $request->phone;
		$data->mobile = $request->mobile;
		$data->mobile_2 = $request->mobile_2;
		
		$data->office_name = $request->office_name;
		$data->office_phone = $request->office_phone;
		$data->job_type = $request->job_type;
		$data->email = $request->email;
		
		$data->active = 1;
		$data->created_by = Auth::id();
		$data->save();
		
		$patiente = PatientEmergency::where('patient_id', $id)->first();
		
		//print_r($patiente);exit;
		
		if(count($patiente) == 0){
			$datae = new PatientEmergency;
			$datae->patient_id = $id;
		} else
			$datae = PatientEmergency::find($patiente->id);
		
		$datae->name = $request->emergency_name;
		$datae->relationship = $request->emergency_relationship;
		$datae->address = $request->emergency_address;
		$datae->city = $request->emergency_city;
		$datae->postal_code = $request->emergency_postcode;
		$datae->phone = $request->emergency_phone;
		$datae->email = $request->emergency_email;
		$datae->created_by = Auth::id();
		$datae->save();
		
		$after_save = [
            'alert' => 'success',
            'title' => 'Sukses!',
            'text-1' => 'Update data',
            'text-2' => 'berhasil.'
        ];
		
		return redirect('/admission/patient')->with('after_save', $after_save);
		
	}
}
