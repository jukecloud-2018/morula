<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitCode extends Model
{

    protected $table = "master_visit_code";
	
	protected $hidden = [
        'created_by', 'created_at', 'updated_at'
    ];

}