<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class IdType extends Model
{

    protected $table = "master_id_type";
	
	protected $hidden = [
        'created_by', 'created_at', 'updated_at'
    ];

}