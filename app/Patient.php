<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{

    protected $table = "patient";
	
	public function emergency()
    {
        return $this->hasOne('App\PatientEmergency'); // links this->id to events.course_id
    }

}

