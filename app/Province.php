<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = "master_province";

    public function city(){
        return $this->belongsTo('App\City', 'province_id');
    }

}