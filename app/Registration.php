<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{

    protected $table = "clinic_registration";
	
	public function patient(){
		return $this->belongsTo('App\Patient', 'patient_id');
	}

	public function doctor(){
	    return $this->belongsTo('App\Doctor');
    }


}