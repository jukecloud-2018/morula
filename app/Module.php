<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = "master_module";
    protected $hidden = [
        'created_by', 'created_at', 'updated_at'
    ];
    public function user(){
        return $this->belongsTo("App\User");
    }
}