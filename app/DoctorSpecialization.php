<?php
namespace App;



use Illuminate\Database\Eloquent\Model;



class DoctorSpecialization extends Model

{

    protected $table = "doctor_specialization";
	
	protected $hidden = [
        'created_by', 'created_at', 'updated_at'
    ];

}