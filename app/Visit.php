<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $table = "master_visit";
	protected $hidden = [
        'created_by', 'created_at', 'updated_at'
    ];
}