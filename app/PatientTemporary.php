<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientTemporary extends Model
{

    protected $table = "patient_temporary";

}