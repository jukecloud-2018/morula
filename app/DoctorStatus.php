<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorStatus extends Model
{
    protected $table = "doctor_status";
    protected $hidden = [
        'created_by', 'created_at', 'updated_at'
    ];
}