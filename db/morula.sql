USE [master]
GO
/****** Object:  Database [morula]    Script Date: 31/10/2017 10.58.37 ******/
CREATE DATABASE [morula]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'moruladev', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\moruladev.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'moruladev_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\moruladev_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [morula] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [morula].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [morula] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [morula] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [morula] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [morula] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [morula] SET ARITHABORT OFF 
GO
ALTER DATABASE [morula] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [morula] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [morula] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [morula] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [morula] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [morula] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [morula] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [morula] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [morula] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [morula] SET  DISABLE_BROKER 
GO
ALTER DATABASE [morula] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [morula] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [morula] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [morula] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [morula] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [morula] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [morula] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [morula] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [morula] SET  MULTI_USER 
GO
ALTER DATABASE [morula] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [morula] SET DB_CHAINING OFF 
GO
ALTER DATABASE [morula] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [morula] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [morula] SET DELAYED_DURABILITY = DISABLED 
GO
USE [morula]
GO
/****** Object:  Table [dbo].[clinic_family_link]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[clinic_family_link](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[patient_id] [int] NOT NULL,
	[partner_id] [int] NOT NULL,
	[date_of_marriage] [date] NULL,
	[place_of_marriage] [varchar](50) NULL,
	[certificate_number] [varchar](100) NULL,
	[certificate_image_url] [varchar](255) NULL,
	[valid] [bit] NULL,
	[created_at] [datetime] NULL,
	[created_by] [int] NULL,
	[updated_at] [datetime] NULL,
	[updated_by] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[clinic_registration]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[clinic_registration](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[holding_id] [int] NULL,
	[company_id] [int] NULL,
	[date] [date] NOT NULL,
	[doctor_id] [tinyint] NOT NULL,
	[replacement_doctor_id] [tinyint] NULL,
	[consultation_start] [time](7) NOT NULL,
	[consultation_end] [time](7) NOT NULL,
	[queue_id] [int] NOT NULL,
	[registration_date] [datetime] NOT NULL,
	[patient_id] [int] NOT NULL,
	[patient_first_name] [varchar](100) NOT NULL,
	[patient_last_name] [varchar](100) NULL,
	[patient_birthdate] [date] NOT NULL,
	[patient_sex] [int] NOT NULL,
	[patient_mobile_phone_number] [varchar](100) NULL,
	[patient_is_attend] [bit] NOT NULL,
	[patient_attendance_time] [time](7) NULL,
	[patient_consultation_start] [time](7) NULL,
	[patient_consultation_end] [time](7) NULL,
	[created_at] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_at] [datetime] NULL,
	[updated_by] [int] NULL,
 CONSTRAINT [PK__clinic_r__3213E83FA6563B3F] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[clinic_registration_info]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[clinic_registration_info](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date] [int] NOT NULL,
	[doctor_id] [tinyint] NOT NULL,
	[created_at] [int] NOT NULL,
	[created_by] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[doctor]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[doctor](
	[id] [tinyint] IDENTITY(22,1) NOT NULL,
	[holding_id] [int] NULL,
	[company_id] [int] NULL,
	[code] [varchar](10) NULL CONSTRAINT [DF__master_do__docto__3D5E1FD2]  DEFAULT (NULL),
	[name] [varchar](50) NOT NULL,
	[status_id] [int] NOT NULL,
	[specialization_id] [tinyint] NOT NULL,
	[employee_id] [varchar](100) NULL CONSTRAINT [DF__master_doct__nip__3E52440B]  DEFAULT (NULL),
	[share_fee] [money] NULL CONSTRAINT [DF__master_do__share__3F466844]  DEFAULT (NULL),
	[remark] [varchar](255) NULL,
	[profile_image_url] [varchar](255) NULL CONSTRAINT [DF__master_do__photo__403A8C7D]  DEFAULT (NULL),
	[created_at] [datetime] NOT NULL,
	[created_by] [int] NOT NULL CONSTRAINT [DF__master_do__creat__412EB0B6]  DEFAULT (NULL),
	[updated_at] [datetime] NULL CONSTRAINT [DF__master_do__updat__4316F928]  DEFAULT (NULL),
	[updated_by] [int] NULL CONSTRAINT [DF__master_do__updat__4222D4EF]  DEFAULT (NULL),
 CONSTRAINT [PK_master_doctor_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[doctor_late]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[doctor_late](
	[id] [int] IDENTITY(4,1) NOT NULL,
	[doctor_id] [int] NULL DEFAULT (NULL),
	[date] [date] NOT NULL,
	[session] [time](7) NULL DEFAULT (NULL),
	[start_time] [time](7) NULL DEFAULT (NULL),
	[cause] [nvarchar](255) NULL DEFAULT (NULL),
	[created_by] [int] NULL DEFAULT (NULL),
	[updated_by] [int] NULL DEFAULT (NULL),
	[created_at] [datetime2](0) NOT NULL,
	[updated_at] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_t_doctor_request_late_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[doctor_leave]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[doctor_leave](
	[id] [int] IDENTITY(7,1) NOT NULL,
	[doctor_id] [int] NOT NULL,
	[start_date] [date] NOT NULL,
	[end_date] [date] NOT NULL,
	[leave_type] [nvarchar](50) NULL DEFAULT (NULL),
	[cause] [nvarchar](255) NULL DEFAULT (NULL),
	[created_by] [int] NULL DEFAULT (NULL),
	[updated_by] [int] NULL DEFAULT (NULL),
	[created_at] [datetime2](0) NOT NULL,
	[updated_at] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_t_doctor_leave_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[doctor_leave_session]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[doctor_leave_session](
	[id] [int] IDENTITY(3,1) NOT NULL,
	[doctor_id] [int] NULL DEFAULT (NULL),
	[date] [date] NOT NULL,
	[session] [time](7) NULL DEFAULT (NULL),
	[cause] [nvarchar](255) NULL DEFAULT (NULL),
	[created_by] [int] NULL DEFAULT (NULL),
	[updated_by] [int] NULL DEFAULT (NULL),
	[created_at] [datetime2](0) NOT NULL,
	[updated_at] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_t_doctor_request_session_leave_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[doctor_schedule]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[doctor_schedule](
	[id] [int] IDENTITY(2,1) NOT NULL,
	[doctor_id] [tinyint] NOT NULL,
	[day_id] [tinyint] NOT NULL,
	[consultation_start] [time](7) NOT NULL,
	[consultation_end] [time](7) NOT NULL,
	[duration] [int] NOT NULL,
	[room_id] [int] NOT NULL,
	[created_at] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_at] [datetime] NULL CONSTRAINT [DF__master_do__updat__440B1D61]  DEFAULT (NULL),
	[updated_by] [int] NULL,
 CONSTRAINT [PK_master_doctor_schedule_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[doctor_specialization]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[doctor_specialization](
	[id] [tinyint] IDENTITY(1,1) NOT NULL,
	[holding_id] [int] NULL,
	[company_id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[master_account] [int] NULL,
	[detail_account] [int] NULL,
	[master_account_expense] [int] NULL,
	[detail_account_expense] [int] NULL,
	[master_account_payable] [int] NULL,
	[detail_account_payable] [int] NULL,
	[service_group] [varchar](50) NULL,
	[created_at] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_at] [datetime] NULL,
	[updated_by] [int] NULL,
 CONSTRAINT [PK__doctor_s__3213E83F1724452F] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gl_coa_detail]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gl_coa_detail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[holding_id] [int] NOT NULL,
	[company_id] [int] NOT NULL,
	[master_id] [int] NOT NULL,
	[code] [int] NOT NULL,
	[name] [varchar](255) NOT NULL,
	[group] [varchar](1) NOT NULL,
	[created_at] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_at] [datetime] NOT NULL,
	[updated_by] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gl_coa_master]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gl_coa_master](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [int] NOT NULL,
	[name] [varchar](255) NOT NULL,
	[group] [varchar](1) NOT NULL,
	[created_at] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_at] [datetime] NOT NULL,
	[updated_by] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[master_city]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[master_city](
	[id] [int] IDENTITY(9479,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[province_id] [int] NOT NULL,
	[created_at] [datetime] NOT NULL,
	[created_by] [int] NOT NULL CONSTRAINT [DF__master_ci__creat__3A81B327]  DEFAULT (NULL),
	[updated_at] [datetime] NULL CONSTRAINT [DF__master_ci__updat__3B75D760]  DEFAULT (NULL),
	[updated_by] [int] NULL,
 CONSTRAINT [PK_master_city_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[master_country]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[master_country](
	[id] [int] IDENTITY(247,1) NOT NULL,
	[name] [nvarchar](225) NOT NULL,
	[created_by] [int] NULL CONSTRAINT [DF__master_co__creat__44CA3770]  DEFAULT (NULL),
	[created_at] [datetime2](0) NULL,
	[updated_at] [datetime2](0) NULL CONSTRAINT [DF__master_co__updat__45BE5BA9]  DEFAULT (NULL),
 CONSTRAINT [PK_master_country_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[master_day]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[master_day](
	[id] [tinyint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](20) NOT NULL,
	[created_at] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_at] [datetime] NULL,
	[updated_by] [int] NULL,
 CONSTRAINT [PK__master_d__3213E83FCB3097B1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[master_identity_type]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[master_identity_type](
	[id] [tinyint] IDENTITY(6,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[length] [int] NULL,
	[created_at] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_at] [datetime2](0) NULL CONSTRAINT [DF__master_id__updat__46B27FE2]  DEFAULT (NULL),
	[updated_by] [int] NULL,
 CONSTRAINT [PK_master_id_type_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[master_job_type]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[master_job_type](
	[id] [int] IDENTITY(20,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[created_by] [int] NOT NULL,
	[created_at] [datetime2](0) NOT NULL,
	[updated_at] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_master_job_type_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[master_menu]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[master_menu](
	[id] [int] IDENTITY(3,1) NOT NULL,
	[sort] [int] NOT NULL,
	[parent_id] [int] NOT NULL,
	[name] [varchar](30) NOT NULL,
	[roles] [varchar](30) NOT NULL,
	[url] [varchar](20) NOT NULL,
	[active_classes] [varchar](20) NOT NULL,
	[icon] [varchar](50) NULL DEFAULT (NULL),
	[created_by] [int] NULL DEFAULT (NULL),
	[updated_by] [int] NULL DEFAULT (NULL),
	[created_at] [datetime2](0) NOT NULL,
	[updated_at] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_master_menu_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[master_province]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[master_province](
	[id] [int] IDENTITY(115,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[created_at] [datetime] NOT NULL,
	[created_by] [int] NOT NULL CONSTRAINT [DF__master_pr__creat__619B8048]  DEFAULT (NULL),
	[updated_at] [datetime] NULL CONSTRAINT [DF__master_pr__updat__628FA481]  DEFAULT (NULL),
	[updated_by] [int] NULL,
 CONSTRAINT [PK_master_province_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[master_religion]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[master_religion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](30) NOT NULL,
	[created_at] [datetime] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_at] [datetime] NULL,
	[updated_by] [int] NULL,
 CONSTRAINT [PK_master_religion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[master_room]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[master_room](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[room] [varchar](30) NOT NULL,
	[created_by] [int] NOT NULL,
	[created_at] [datetime2](0) NOT NULL,
	[updated_at] [datetime2](0) NULL,
 CONSTRAINT [PK_master_room_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[master_visit_code]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[master_visit_code](
	[id] [int] IDENTITY(7,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[description] [varchar](50) NULL DEFAULT (NULL),
	[created_by] [int] NOT NULL,
	[created_at] [datetime2](0) NOT NULL,
	[updated_at] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_master_visit_code_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[migrations]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[migrations](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[migration] [nvarchar](255) NOT NULL,
	[batch] [int] NOT NULL,
 CONSTRAINT [PK_migrations_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[patient]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[patient](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[hospital_id] [int] NULL,
	[holding_id] [int] NULL,
	[company_id] [int] NULL,
	[designation] [varchar](10) NULL,
	[first_name] [varchar](50) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[sex] [varchar](1) NOT NULL,
	[birthdate] [date] NOT NULL,
	[birthplace] [varchar](100) NOT NULL,
	[profile_image_url] [varchar](255) NULL,
	[initial_doctor_id] [int] NULL,
	[email] [varchar](100) NULL,
	[height] [real] NULL,
	[weight] [real] NULL,
	[blood_type] [bit] NULL,
	[date_of_marriage] [date] NULL,
	[address] [varchar](255) NULL,
	[city_id] [int] NULL,
	[home_phone_number] [varchar](20) NULL,
	[office_phone_number] [varchar](20) NULL,
	[mobile_phone_number] [varchar](20) NULL,
	[nationality] [varchar](20) NULL,
	[identity_type_id] [tinyint] NULL,
	[identity_number] [varchar](50) NULL,
	[religion_id] [int] NULL,
	[created_at] [datetime] NULL,
	[created_by] [int] NULL,
	[updated_at] [datetime] NULL,
	[updated_by] [int] NULL,
 CONSTRAINT [PK__patient__3213E83F8F1A8298] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[patient_emergency]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[patient_emergency](
	[id] [int] IDENTITY(4,1) NOT NULL,
	[patient_id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[relationship] [varchar](50) NOT NULL,
	[address] [varchar](255) NOT NULL,
	[city_id] [int] NOT NULL,
	[phone_number] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[created_at] [datetime] NOT NULL,
	[created_by] [int] NOT NULL CONSTRAINT [DF__master_pa__creat__59063A47]  DEFAULT (NULL),
	[updated_at] [datetime] NULL CONSTRAINT [DF__master_pa__updat__59FA5E80]  DEFAULT (NULL),
	[updated_by] [int] NULL,
 CONSTRAINT [PK_master_patient_emergency_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[patient_old]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[patient_old](
	[id] [bigint] IDENTITY(9,1) NOT NULL,
	[company_id] [int] NULL CONSTRAINT [DF__master_pa__compa__49C3F6B7]  DEFAULT (NULL),
	[first_name] [nvarchar](30) NOT NULL CONSTRAINT [DF__master_pa__first__4AB81AF0]  DEFAULT (N''),
	[last_name] [nvarchar](50) NOT NULL,
	[init_doctor] [bigint] NOT NULL CONSTRAINT [DF__master_pa__init___4BAC3F29]  DEFAULT ((1)),
	[id_number] [nvarchar](30) NOT NULL,
	[id_type] [nvarchar](255) NOT NULL CONSTRAINT [DF__master_pa__id_ty__4CA06362]  DEFAULT (N''),
	[birth_place] [nvarchar](20) NOT NULL CONSTRAINT [DF__master_pa__birth__4D94879B]  DEFAULT (N''),
	[birth_date] [date] NULL CONSTRAINT [DF__master_pa__birth__4E88ABD4]  DEFAULT (NULL),
	[sex] [nvarchar](255) NOT NULL CONSTRAINT [DF__master_pati__sex__4F7CD00D]  DEFAULT (N''),
	[body_height] [bigint] NOT NULL,
	[blood] [nvarchar](1) NOT NULL,
	[marital_status] [nvarchar](255) NOT NULL CONSTRAINT [DF__master_pa__marit__5070F446]  DEFAULT (N''),
	[married_date] [date] NULL CONSTRAINT [DF__master_pa__marri__5165187F]  DEFAULT (NULL),
	[religion] [nvarchar](255) NOT NULL CONSTRAINT [DF__master_pa__relig__52593CB8]  DEFAULT (N''),
	[nationality] [nvarchar](20) NOT NULL,
	[address] [nvarchar](256) NOT NULL CONSTRAINT [DF__master_pa__addre__534D60F1]  DEFAULT (N''),
	[city] [nvarchar](256) NOT NULL,
	[postal_code] [bigint] NOT NULL,
	[phone] [nvarchar](20) NOT NULL,
	[mobile] [nvarchar](20) NOT NULL,
	[mobile_2] [nvarchar](20) NULL CONSTRAINT [DF__master_pa__mobil__5441852A]  DEFAULT (NULL),
	[office_name] [nvarchar](20) NULL CONSTRAINT [DF__master_pa__offic__5535A963]  DEFAULT (NULL),
	[office_phone] [nvarchar](15) NOT NULL,
	[job_type] [nvarchar](30) NOT NULL,
	[email] [nvarchar](256) NOT NULL,
	[foto] [nvarchar](50) NULL CONSTRAINT [DF__master_pat__foto__5629CD9C]  DEFAULT (NULL),
	[active] [int] NOT NULL,
	[created_by] [int] NOT NULL,
	[created_at] [datetime] NOT NULL,
	[updated_at] [datetime] NULL CONSTRAINT [DF__master_pa__updat__571DF1D5]  DEFAULT (NULL),
 CONSTRAINT [PK_master_patient_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[patient_relation]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[patient_relation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[no_mr_induk] [int] NOT NULL,
	[no_mr_pasangan] [int] NOT NULL,
	[status] [int] NOT NULL,
	[created_by] [int] NOT NULL,
	[created_at] [datetime2](0) NOT NULL,
	[updated_at] [datetime2](0) NULL,
 CONSTRAINT [PK_master_patient_relation_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[patient_temporary]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[patient_temporary](
	[id] [int] IDENTITY(10,1) NOT NULL,
	[company_id] [int] NOT NULL,
	[first_name] [varchar](50) NOT NULL,
	[last_name] [varchar](50) NOT NULL,
	[sex] [varchar](1) NOT NULL,
	[initial_doctor_id] [int] NOT NULL,
	[birth_date] [date] NOT NULL,
	[mobile_phone_number] [varchar](20) NOT NULL,
	[identity_number] [varchar](50) NOT NULL,
	[identity_type_id] [tinyint] NOT NULL,
	[status] [int] NOT NULL,
	[created_by] [int] NOT NULL,
	[created_at] [datetime] NOT NULL,
	[updated_at] [datetime] NULL,
 CONSTRAINT [PK_master_patient_temp_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[reservation]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[reservation](
	[id] [int] IDENTITY(25,1) NOT NULL,
	[reservation_date] [date] NOT NULL,
	[session] [time](7) NOT NULL,
	[morula_id] [int] NULL DEFAULT (NULL),
	[temp_id] [int] NULL DEFAULT (NULL),
	[visit_code] [int] NOT NULL,
	[doctor_code] [varchar](5) NULL DEFAULT (NULL),
	[description] [varchar](max) NULL,
	[attendance] [int] NOT NULL,
	[created_by] [int] NULL DEFAULT (NULL),
	[created_at] [datetime2](0) NOT NULL,
	[updated_at] [datetime2](0) NULL DEFAULT (NULL),
 CONSTRAINT [PK_t_reservation_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[roles]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[roles](
	[id] [int] IDENTITY(9,1) NOT NULL,
	[name] [varchar](30) NOT NULL,
	[url] [varchar](10) NOT NULL,
	[description] [varchar](50) NULL CONSTRAINT [DF__roles__descripti__66603565]  DEFAULT (NULL),
	[created_by] [int] NOT NULL,
	[updated_by] [int] NULL CONSTRAINT [DF__roles__updated_b__6754599E]  DEFAULT (NULL),
	[created_at] [datetime] NOT NULL,
	[updated_at] [datetime] NULL,
 CONSTRAINT [PK_roles_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user_role]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_role](
	[id] [int] IDENTITY(2,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[role_id] [int] NOT NULL,
	[created_by] [int] NOT NULL,
	[updated_by] [int] NOT NULL,
	[created_at] [datetime2](0) NOT NULL,
	[updated_at] [datetime2](0) NOT NULL,
 CONSTRAINT [PK_user_role_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[users]    Script Date: 31/10/2017 10.58.38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[id] [bigint] IDENTITY(17,1) NOT NULL,
	[username] [nvarchar](50) NULL DEFAULT (NULL),
	[name] [nvarchar](50) NOT NULL,
	[email] [nvarchar](30) NOT NULL,
	[password] [nvarchar](100) NOT NULL,
	[phone] [nvarchar](20) NOT NULL,
	[description] [nvarchar](30) NULL DEFAULT (NULL),
	[roles] [nvarchar](20) NULL DEFAULT (NULL),
	[remember_token] [nvarchar](100) NULL DEFAULT (NULL),
	[api_token] [nvarchar](100) NULL DEFAULT (NULL),
	[photo] [nvarchar](50) NULL DEFAULT (NULL),
	[created_at] [datetime] NULL DEFAULT (NULL),
	[updated_at] [datetime] NULL DEFAULT (NULL),
 CONSTRAINT [PK_users_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[doctor] ON 

INSERT [dbo].[doctor] ([id], [holding_id], [company_id], [code], [name], [status_id], [specialization_id], [employee_id], [share_fee], [remark], [profile_image_url], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1, NULL, 1400, N'IND', N'dr. Indra N.C Anwar, Sp.OG', 1, 1, NULL, NULL, NULL, N'1508119802_dr_indra.jpg', CAST(N'2017-08-14 07:36:10.000' AS DateTime), 1, CAST(N'2017-10-16 10:12:02.000' AS DateTime), 1)
INSERT [dbo].[doctor] ([id], [holding_id], [company_id], [code], [name], [status_id], [specialization_id], [employee_id], [share_fee], [remark], [profile_image_url], [created_at], [created_by], [updated_at], [updated_by]) VALUES (2, NULL, 1400, N'IVN', N'dr. Ivan Rizal Sini, Sp.OG', 1, 1, NULL, NULL, NULL, N'1508119788_dr_ivan.jpg', CAST(N'2017-08-14 10:53:58.000' AS DateTime), 1, CAST(N'2017-10-16 10:11:54.000' AS DateTime), 1)
INSERT [dbo].[doctor] ([id], [holding_id], [company_id], [code], [name], [status_id], [specialization_id], [employee_id], [share_fee], [remark], [profile_image_url], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3, NULL, 1400, N'IRS', N'dr. Irham Suheimi, Sp.OG', 1, 1, NULL, NULL, NULL, N'1508119778_dr_irham.jpg', CAST(N'2017-08-14 10:53:58.000' AS DateTime), 1, CAST(N'2017-10-16 10:11:46.000' AS DateTime), 1)
INSERT [dbo].[doctor] ([id], [holding_id], [company_id], [code], [name], [status_id], [specialization_id], [employee_id], [share_fee], [remark], [profile_image_url], [created_at], [created_by], [updated_at], [updated_by]) VALUES (19, NULL, 1400, NULL, N'dr. Anggia M Lubis, SpOG', 1, 1, NULL, NULL, NULL, N'1508119764_dr_Anggia.jpg', CAST(N'2017-10-16 09:09:24.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[doctor] ([id], [holding_id], [company_id], [code], [name], [status_id], [specialization_id], [employee_id], [share_fee], [remark], [profile_image_url], [created_at], [created_by], [updated_at], [updated_by]) VALUES (20, NULL, 1400, NULL, N'dr. Caroline Hutomo, SpOG', 1, 1, NULL, NULL, NULL, N'1508120360_dr_caroline.jpg', CAST(N'2017-10-16 09:17:49.000' AS DateTime), 1, CAST(N'2017-10-16 09:19:20.000' AS DateTime), 1)
INSERT [dbo].[doctor] ([id], [holding_id], [company_id], [code], [name], [status_id], [specialization_id], [employee_id], [share_fee], [remark], [profile_image_url], [created_at], [created_by], [updated_at], [updated_by]) VALUES (21, NULL, 1400, NULL, N'dr. Aryando Pradana, SpOG', 1, 1, NULL, NULL, NULL, N'1508120353_dr_ariando.jpg', CAST(N'2017-10-16 09:19:13.000' AS DateTime), 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[doctor] OFF
SET IDENTITY_INSERT [dbo].[doctor_late] ON 

INSERT [dbo].[doctor_late] ([id], [doctor_id], [date], [session], [start_time], [cause], [created_by], [updated_by], [created_at], [updated_at]) VALUES (1, 2, CAST(N'2017-10-18' AS Date), CAST(N'14:54:00' AS Time), CAST(N'15:54:00' AS Time), NULL, 1, NULL, CAST(N'2017-10-17 14:55:48.0000000' AS DateTime2), CAST(N'2017-10-17 14:55:48.0000000' AS DateTime2))
INSERT [dbo].[doctor_late] ([id], [doctor_id], [date], [session], [start_time], [cause], [created_by], [updated_by], [created_at], [updated_at]) VALUES (2, 19, CAST(N'2017-10-12' AS Date), CAST(N'17:47:00' AS Time), CAST(N'04:25:00' AS Time), N'asd', 1, NULL, CAST(N'2017-10-17 16:47:55.0000000' AS DateTime2), CAST(N'2017-10-17 16:47:55.0000000' AS DateTime2))
INSERT [dbo].[doctor_late] ([id], [doctor_id], [date], [session], [start_time], [cause], [created_by], [updated_by], [created_at], [updated_at]) VALUES (3, 20, CAST(N'2017-10-20' AS Date), CAST(N'13:21:00' AS Time), CAST(N'14:21:00' AS Time), N'cause 1', 1, NULL, CAST(N'2017-10-19 13:21:54.0000000' AS DateTime2), CAST(N'2017-10-19 13:21:54.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[doctor_late] OFF
SET IDENTITY_INSERT [dbo].[doctor_leave] ON 

INSERT [dbo].[doctor_leave] ([id], [doctor_id], [start_date], [end_date], [leave_type], [cause], [created_by], [updated_by], [created_at], [updated_at]) VALUES (1, 1, CAST(N'2017-10-05' AS Date), CAST(N'2017-10-28' AS Date), NULL, N'z', 1, NULL, CAST(N'2017-10-17 16:35:49.0000000' AS DateTime2), CAST(N'2017-10-17 16:35:49.0000000' AS DateTime2))
INSERT [dbo].[doctor_leave] ([id], [doctor_id], [start_date], [end_date], [leave_type], [cause], [created_by], [updated_by], [created_at], [updated_at]) VALUES (2, 1, CAST(N'2017-10-04' AS Date), CAST(N'2017-10-28' AS Date), NULL, N'x', 1, NULL, CAST(N'2017-10-17 16:37:12.0000000' AS DateTime2), CAST(N'2017-10-17 16:37:12.0000000' AS DateTime2))
INSERT [dbo].[doctor_leave] ([id], [doctor_id], [start_date], [end_date], [leave_type], [cause], [created_by], [updated_by], [created_at], [updated_at]) VALUES (3, 1, CAST(N'2017-10-18' AS Date), CAST(N'2017-10-26' AS Date), NULL, N'sda', 1, NULL, CAST(N'2017-10-17 16:40:34.0000000' AS DateTime2), CAST(N'2017-10-17 16:40:34.0000000' AS DateTime2))
INSERT [dbo].[doctor_leave] ([id], [doctor_id], [start_date], [end_date], [leave_type], [cause], [created_by], [updated_by], [created_at], [updated_at]) VALUES (4, 3, CAST(N'2017-10-11' AS Date), CAST(N'2017-10-19' AS Date), N'Unpaid Leave', N'dsadz', 1, NULL, CAST(N'2017-10-17 16:45:53.0000000' AS DateTime2), CAST(N'2017-10-17 16:45:53.0000000' AS DateTime2))
INSERT [dbo].[doctor_leave] ([id], [doctor_id], [start_date], [end_date], [leave_type], [cause], [created_by], [updated_by], [created_at], [updated_at]) VALUES (5, 20, CAST(N'2017-10-18' AS Date), CAST(N'2017-10-19' AS Date), N'Unpaid Leave', N'dasdasdsadasd', 1, NULL, CAST(N'2017-10-18 10:29:56.0000000' AS DateTime2), CAST(N'2017-10-18 10:29:56.0000000' AS DateTime2))
INSERT [dbo].[doctor_leave] ([id], [doctor_id], [start_date], [end_date], [leave_type], [cause], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, 3, CAST(N'2017-10-20' AS Date), CAST(N'2017-10-21' AS Date), N'Unpaid Leave', N'duka euy', 1, NULL, CAST(N'2017-10-19 13:20:00.0000000' AS DateTime2), CAST(N'2017-10-19 13:20:00.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[doctor_leave] OFF
SET IDENTITY_INSERT [dbo].[doctor_leave_session] ON 

INSERT [dbo].[doctor_leave_session] ([id], [doctor_id], [date], [session], [cause], [created_by], [updated_by], [created_at], [updated_at]) VALUES (1, 1, CAST(N'2017-10-18' AS Date), CAST(N'14:11:00' AS Time), N'dd', 1, NULL, CAST(N'2017-10-17 14:11:42.0000000' AS DateTime2), CAST(N'2017-10-17 14:11:42.0000000' AS DateTime2))
INSERT [dbo].[doctor_leave_session] ([id], [doctor_id], [date], [session], [cause], [created_by], [updated_by], [created_at], [updated_at]) VALUES (2, 19, CAST(N'2017-10-20' AS Date), CAST(N'04:20:00' AS Time), N'cause 1', 1, NULL, CAST(N'2017-10-19 13:21:10.0000000' AS DateTime2), CAST(N'2017-10-19 13:21:10.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[doctor_leave_session] OFF
SET IDENTITY_INSERT [dbo].[doctor_schedule] ON 

INSERT [dbo].[doctor_schedule] ([id], [doctor_id], [day_id], [consultation_start], [consultation_end], [duration], [room_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1, 2, 1, CAST(N'09:00:00' AS Time), CAST(N'15:00:00' AS Time), 30, 1, CAST(N'2017-09-19 14:40:33.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[doctor_schedule] ([id], [doctor_id], [day_id], [consultation_start], [consultation_end], [duration], [room_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (2, 1, 4, CAST(N'10:00:00' AS Time), CAST(N'12:00:00' AS Time), 15, 1, CAST(N'2017-10-30 22:02:05.000' AS DateTime), 13, CAST(N'2017-10-30 22:02:05.000' AS DateTime), NULL)
INSERT [dbo].[doctor_schedule] ([id], [doctor_id], [day_id], [consultation_start], [consultation_end], [duration], [room_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3, 2, 2, CAST(N'08:00:00' AS Time), CAST(N'11:00:00' AS Time), 25, 1, CAST(N'2017-10-30 22:06:09.000' AS DateTime), 13, CAST(N'2017-10-30 22:06:09.000' AS DateTime), NULL)
INSERT [dbo].[doctor_schedule] ([id], [doctor_id], [day_id], [consultation_start], [consultation_end], [duration], [room_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (4, 2, 5, CAST(N'10:00:00' AS Time), CAST(N'11:00:00' AS Time), 15, 1, CAST(N'2017-10-30 22:10:37.000' AS DateTime), 13, CAST(N'2017-10-30 22:10:37.000' AS DateTime), NULL)
INSERT [dbo].[doctor_schedule] ([id], [doctor_id], [day_id], [consultation_start], [consultation_end], [duration], [room_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5, 3, 3, CAST(N'10:00:00' AS Time), CAST(N'11:00:00' AS Time), 15, 1, CAST(N'2017-10-31 10:34:01.000' AS DateTime), 13, CAST(N'2017-10-31 10:34:01.000' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[doctor_schedule] OFF
SET IDENTITY_INSERT [dbo].[doctor_specialization] ON 

INSERT [dbo].[doctor_specialization] ([id], [holding_id], [company_id], [name], [master_account], [detail_account], [master_account_expense], [detail_account_expense], [master_account_payable], [detail_account_payable], [service_group], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1, NULL, 1400, N'Obsgyn', NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2017-10-26 00:00:00.000' AS DateTime), 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[doctor_specialization] OFF
SET IDENTITY_INSERT [dbo].[master_city] ON 

INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1101, N'Kabupaten Simeulue', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:21.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1102, N'Kabupaten Aceh Singkil', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:39.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1103, N'Kabupaten Aceh Selatan', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:40.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1104, N'Kabupaten Aceh Tenggara', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:40.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1105, N'Kabupaten Aceh Timur', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:40.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1106, N'Kabupaten Aceh Tengah', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:40.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1107, N'Kabupaten Aceh Barat', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:41.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1108, N'Kabupaten Aceh Besar', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:41.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1109, N'Kabupaten Pidie', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:41.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1110, N'Kabupaten Bireuen', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:41.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1111, N'Kabupaten Aceh Utara', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:41.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1112, N'Kabupaten Aceh Barat Daya', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:42.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1113, N'Kabupaten Gayo Lues', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:42.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1114, N'Kabupaten Aceh Tamiang', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:42.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1115, N'Kabupaten Nagan Raya', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:42.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1116, N'Kabupaten Aceh Jaya', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:42.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1117, N'Kabupaten Bener Meriah', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:43.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1118, N'Kabupaten Pidie Jaya', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:43.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1171, N'Kota Banda Aceh', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:43.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1172, N'Kota Sabang', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:43.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1173, N'Kota Langsa', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:43.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1174, N'Kota Lhokseumawe', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:44.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1175, N'Kota Subulussalam', 11, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:44.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1201, N'Kabupaten Nias', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:44.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1202, N'Kabupaten Mandailing Natal', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:44.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1203, N'Kabupaten Tapanuli Selatan', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:45.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1204, N'Kabupaten Tapanuli Tengah', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:45.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1205, N'Kabupaten Tapanuli Utara', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:45.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1206, N'Kabupaten Toba Samosir', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:45.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1207, N'Kabupaten Labuhan Batu', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:45.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1208, N'Kabupaten Asahan', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:46.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1209, N'Kabupaten Simalungun', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:46.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1210, N'Kabupaten Dairi', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:46.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1211, N'Kabupaten Karo', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:46.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1212, N'Kabupaten Deli Serdang', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:46.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1213, N'Kabupaten Langkat', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:47.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1214, N'Kabupaten Nias Selatan', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:47.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1215, N'Kabupaten Humbang Hasundutan', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:47.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1216, N'Kabupaten Pakpak Bharat', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:47.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1217, N'Kabupaten Samosir', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:47.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1218, N'Kabupaten Serdang Bedagai', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:48.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1219, N'Kabupaten Batu Bara', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:48.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1220, N'Kabupaten Padang Lawas Utara', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:48.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1221, N'Kabupaten Padang Lawas', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:48.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1222, N'Kabupaten Labuhan Batu Selatan', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:49.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1223, N'Kabupaten Labuhan Batu Utara', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:49.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1224, N'Kabupaten Nias Utara', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:49.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1225, N'Kabupaten Nias Barat', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:49.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1271, N'Kota Sibolga', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:49.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1272, N'Kota Tanjung Balai', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:50.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1273, N'Kota Pematang Siantar', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:50.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1274, N'Kota Tebing Tinggi', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:50.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1275, N'Kota Medan', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:50.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1276, N'Kota Binjai', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:50.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1277, N'Kota Padangsidimpuan', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:51.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1278, N'Kota Gunungsitoli', 12, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:51.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1301, N'Kabupaten Kepulauan Mentawai', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:51.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1302, N'Kabupaten Pesisir Selatan', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:51.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1303, N'Kabupaten Solok', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:51.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1304, N'Kabupaten Sijunjung', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:52.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1305, N'Kabupaten Tanah Datar', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:52.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1306, N'Kabupaten Padang Pariaman', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:52.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1307, N'Kabupaten Agam', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:52.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1308, N'Kabupaten Lima Puluh Kota', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:53.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1309, N'Kabupaten Pasaman', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:53.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1310, N'Kabupaten Solok Selatan', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:53.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1311, N'Kabupaten Dharmasraya', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:53.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1312, N'Kabupaten Pasaman Barat', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:53.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1371, N'Kota Padang', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:54.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1372, N'Kota Solok', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:54.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1373, N'Kota Sawah Lunto', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:54.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1374, N'Kota Padang Panjang', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:54.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1375, N'Kota Bukittinggi', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:55.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1376, N'Kota Payakumbuh', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:55.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1377, N'Kota Pariaman', 13, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:55.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1401, N'Kabupaten Kuantan Singingi', 14, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:55.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1402, N'Kabupaten Indragiri Hulu', 14, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:56.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1403, N'Kabupaten Indragiri Hilir', 14, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:56.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1404, N'Kabupaten Pelalawan', 14, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:56.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1405, N'Kabupaten Siak', 14, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:56.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1406, N'Kabupaten Kampar', 14, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:56.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1407, N'Kabupaten Rokan Hulu', 14, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:57.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1408, N'Kabupaten Bengkalis', 14, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:57.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1409, N'Kabupaten Rokan Hilir', 14, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:57.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1410, N'Kabupaten Kepulauan Meranti', 14, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:57.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1471, N'Kota Pekanbaru', 14, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:58.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1473, N'Kota Dumai', 14, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:58.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1501, N'Kabupaten Kerinci', 15, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:58.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1502, N'Kabupaten Merangin', 15, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:58.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1503, N'Kabupaten Sarolangun', 15, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:59.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1504, N'Kabupaten Batang Hari', 15, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:59.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1505, N'Kabupaten Muaro Jambi', 15, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:59.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1506, N'Kabupaten Tanjung Jabung Timur', 15, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:09:59.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1507, N'Kabupaten Tanjung Jabung Barat', 15, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:00.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1508, N'Kabupaten Tebo', 15, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:00.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1509, N'Kabupaten Bungo', 15, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:00.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1571, N'Kota Jambi', 15, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:00.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1572, N'Kota Sungai Penuh', 15, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:00.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1601, N'Kabupaten Ogan Komering Ulu', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:01.000' AS DateTime), NULL)
GO
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1602, N'Kabupaten Ogan Komering Ilir', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:01.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1603, N'Kabupaten Muara Enim', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:01.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1604, N'Kabupaten Lahat', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:01.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1605, N'Kabupaten Musi Rawas', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:02.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1606, N'Kabupaten Musi Banyuasin', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:02.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1607, N'Kabupaten Banyu Asin', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:02.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1608, N'Kabupaten Ogan Komering Ulu Selatan', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:02.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1609, N'Kabupaten Ogan Komering Ulu Timur', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:03.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1610, N'Kabupaten Ogan Ilir', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:03.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1611, N'Kabupaten Empat Lawang', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:03.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1612, N'Kabupaten Penukal Abab Lematang Ilir', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:03.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1613, N'Kabupaten Musi Rawas Utara', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:03.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1671, N'Kota Palembang', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:04.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1672, N'Kota Prabumulih', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:04.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1673, N'Kota Pagar Alam', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:04.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1674, N'Kota Lubuklinggau', 16, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:05.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1701, N'Kabupaten Bengkulu Selatan', 17, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:05.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1702, N'Kabupaten Rejang Lebong', 17, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:05.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1703, N'Kabupaten Bengkulu Utara', 17, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:05.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1704, N'Kabupaten Kaur', 17, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:05.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1705, N'Kabupaten Seluma', 17, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:06.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1706, N'Kabupaten Mukomuko', 17, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:06.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1707, N'Kabupaten Lebong', 17, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:06.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1708, N'Kabupaten Kepahiang', 17, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:07.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1709, N'Kabupaten Bengkulu Tengah', 17, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:07.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1771, N'Kota Bengkulu', 17, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:07.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1801, N'Kabupaten Lampung Barat', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:07.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1802, N'Kabupaten Tanggamus', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:07.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1803, N'Kabupaten Lampung Selatan', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:08.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1804, N'Kabupaten Lampung Timur', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:08.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1805, N'Kabupaten Lampung Tengah', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:08.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1806, N'Kabupaten Lampung Utara', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:08.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1807, N'Kabupaten Way Kanan', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:09.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1808, N'Kabupaten Tulangbawang', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:09.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1809, N'Kabupaten Pesawaran', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:09.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1810, N'Kabupaten Pringsewu', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:10.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1811, N'Kabupaten Mesuji', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:10.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1812, N'Kabupaten Tulang Bawang Barat', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:10.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1813, N'Kabupaten Pesisir Barat', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:10.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1871, N'Kota Bandar Lampung', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:11.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1872, N'Kota Metro', 18, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:11.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1901, N'Kabupaten Bangka', 19, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:11.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1902, N'Kabupaten Belitung', 19, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:11.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1903, N'Kabupaten Bangka Barat', 19, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:12.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1904, N'Kabupaten Bangka Tengah', 19, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:12.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1905, N'Kabupaten Bangka Selatan', 19, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:12.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1906, N'Kabupaten Belitung Timur', 19, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:13.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1971, N'Kota Pangkal Pinang', 19, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:13.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (2101, N'Kabupaten Karimun', 21, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:14.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (2102, N'Kabupaten Bintan', 21, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:14.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (2103, N'Kabupaten Natuna', 21, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:14.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (2104, N'Kabupaten Lingga', 21, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:15.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (2105, N'Kabupaten Kepulauan Anambas', 21, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:15.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (2171, N'Kota Batam', 21, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:15.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (2172, N'Kota Tanjung Pinang', 21, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:16.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3101, N'Kabupaten Kepulauan Seribu', 31, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:16.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3171, N'Kota Jakarta Selatan', 31, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:16.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3172, N'Kota Jakarta Timur', 31, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:16.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3173, N'Kota Jakarta Pusat', 31, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:16.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3174, N'Kota Jakarta Barat', 31, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:17.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3175, N'Kota Jakarta Utara', 31, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:17.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3201, N'Kabupaten Bogor', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:17.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3202, N'Kabupaten Sukabumi', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:17.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3203, N'Kabupaten Cianjur', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:18.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3204, N'Kabupaten Bandung', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:18.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3205, N'Kabupaten Garut', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:18.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3206, N'Kabupaten Tasikmalaya', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:19.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3207, N'Kabupaten Ciamis', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:19.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3208, N'Kabupaten Kuningan', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:19.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3209, N'Kabupaten Cirebon', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:20.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3210, N'Kabupaten Majalengka', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:20.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3211, N'Kabupaten Sumedang', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:20.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3212, N'Kabupaten Indramayu', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:21.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3213, N'Kabupaten Subang', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:21.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3214, N'Kabupaten Purwakarta', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:22.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3215, N'Kabupaten Karawang', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:23.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3216, N'Kabupaten Bekasi', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:23.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3217, N'Kabupaten Bandung Barat', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:24.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3218, N'Kabupaten Pangandaran', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:24.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3271, N'Kota Bogor', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:24.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3272, N'Kota Sukabumi', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:25.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3273, N'Kota Bandung', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:25.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3274, N'Kota Cirebon', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:26.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3275, N'Kota Bekasi', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:26.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3276, N'Kota Depok', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:26.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3277, N'Kota Cimahi', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:26.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3278, N'Kota Tasikmalaya', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:27.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3279, N'Kota Banjar', 32, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:27.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3301, N'Kabupaten Cilacap', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:27.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3302, N'Kabupaten Banyumas', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:27.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3303, N'Kabupaten Purbalingga', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:28.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3304, N'Kabupaten Banjarnegara', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:28.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3305, N'Kabupaten Kebumen', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:28.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3306, N'Kabupaten Purworejo', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:28.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3307, N'Kabupaten Wonosobo', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:29.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3308, N'Kabupaten Magelang', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:29.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3309, N'Kabupaten Boyolali', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:30.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3310, N'Kabupaten Klaten', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:30.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3311, N'Kabupaten Sukoharjo', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:31.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3312, N'Kabupaten Wonogiri', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:31.000' AS DateTime), NULL)
GO
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3313, N'Kabupaten Karanganyar', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:32.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3314, N'Kabupaten Sragen', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:32.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3315, N'Kabupaten Grobogan', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:33.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3316, N'Kabupaten Blora', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:33.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3317, N'Kabupaten Rembang', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:36.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3318, N'Kabupaten Pati', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:36.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3319, N'Kabupaten Kudus', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:37.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3320, N'Kabupaten Jepara', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:37.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3321, N'Kabupaten Demak', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:37.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3322, N'Kabupaten Semarang', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:38.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3323, N'Kabupaten Temanggung', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:38.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3324, N'Kabupaten Kendal', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:38.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3325, N'Kabupaten Batang', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:39.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3326, N'Kabupaten Pekalongan', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:40.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3327, N'Kabupaten Pemalang', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:41.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3328, N'Kabupaten Tegal', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:42.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3329, N'Kabupaten Brebes', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:43.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3371, N'Kota Magelang', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:43.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3372, N'Kota Surakarta', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:44.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3373, N'Kota Salatiga', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:44.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3374, N'Kota Semarang', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:44.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3375, N'Kota Pekalongan', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:44.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3376, N'Kota Tegal', 33, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:45.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3401, N'Kabupaten Kulon Progo', 34, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:46.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3402, N'Kabupaten Bantul', 34, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:47.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3403, N'Kabupaten Gunung Kidul', 34, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:47.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3404, N'Kabupaten Sleman', 34, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:47.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3471, N'Kota Yogyakarta', 34, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:47.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3501, N'Kabupaten Pacitan', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:48.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3502, N'Kabupaten Ponorogo', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:48.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3503, N'Kabupaten Trenggalek', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:48.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3504, N'Kabupaten Tulungagung', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:48.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3505, N'Kabupaten Blitar', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:48.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3506, N'Kabupaten Kediri', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:49.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3507, N'Kabupaten Malang', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:49.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3508, N'Kabupaten Lumajang', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:49.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3509, N'Kabupaten Jember', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:49.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3510, N'Kabupaten Banyuwangi', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:49.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3511, N'Kabupaten Bondowoso', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:50.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3512, N'Kabupaten Situbondo', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:50.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3513, N'Kabupaten Probolinggo', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:50.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3514, N'Kabupaten Pasuruan', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:50.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3515, N'Kabupaten Sidoarjo', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:51.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3516, N'Kabupaten Mojokerto', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:51.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3517, N'Kabupaten Jombang', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:51.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3518, N'Kabupaten Nganjuk', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:51.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3519, N'Kabupaten Madiun', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:52.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3520, N'Kabupaten Magetan', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:52.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3521, N'Kabupaten Ngawi', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:52.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3522, N'Kabupaten Bojonegoro', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:53.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3523, N'Kabupaten Tuban', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:53.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3524, N'Kabupaten Lamongan', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:53.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3525, N'Kabupaten Gresik', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:53.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3526, N'Kabupaten Bangkalan', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:54.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3527, N'Kabupaten Sampang', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:54.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3528, N'Kabupaten Pamekasan', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:54.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3529, N'Kabupaten Sumenep', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:54.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3571, N'Kota Kediri', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:55.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3572, N'Kota Blitar', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:55.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3573, N'Kota Malang', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:55.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3574, N'Kota Probolinggo', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:56.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3575, N'Kota Pasuruan', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:56.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3576, N'Kota Mojokerto', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:56.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3577, N'Kota Madiun', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:57.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3578, N'Kota Surabaya', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:57.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3579, N'Kota Batu', 35, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:57.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3601, N'Kabupaten Pandeglang', 36, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:58.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3602, N'Kabupaten Lebak', 36, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:58.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3603, N'Kabupaten Tangerang', 36, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:59.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3604, N'Kabupaten Serang', 36, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:10:59.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3671, N'Kota Tangerang', 36, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:00.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3672, N'Kota Cilegon', 36, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:00.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3673, N'Kota Serang', 36, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:00.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3674, N'Kota Tangerang Selatan', 36, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:01.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5101, N'Kabupaten Jembrana', 51, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:01.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5102, N'Kabupaten Tabanan', 51, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:01.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5103, N'Kabupaten Badung', 51, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:02.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5104, N'Kabupaten Gianyar', 51, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:02.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5105, N'Kabupaten Klungkung', 51, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:02.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5106, N'Kabupaten Bangli', 51, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:02.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5107, N'Kabupaten Karang Asem', 51, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:02.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5108, N'Kabupaten Buleleng', 51, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:03.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5171, N'Kota Denpasar', 51, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:03.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5201, N'Kabupaten Lombok Barat', 52, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:03.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5202, N'Kabupaten Lombok Tengah', 52, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:03.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5203, N'Kabupaten Lombok Timur', 52, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:04.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5204, N'Kabupaten Sumbawa', 52, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:04.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5205, N'Kabupaten Dompu', 52, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:04.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5206, N'Kabupaten Bima', 52, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:04.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5207, N'Kabupaten Sumbawa Barat', 52, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:05.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5208, N'Kabupaten Lombok Utara', 52, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:05.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5271, N'Kota Mataram', 52, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:05.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5272, N'Kota Bima', 52, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:06.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5301, N'Kabupaten Sumba Barat', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:06.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5302, N'Kabupaten Sumba Timur', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:06.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5303, N'Kabupaten Kupang', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:06.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5304, N'Kabupaten Timor Tengah Selatan', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:07.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5305, N'Kabupaten Timor Tengah Utara', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:07.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5306, N'Kabupaten Belu', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:07.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5307, N'Kabupaten Alor', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:07.000' AS DateTime), NULL)
GO
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5308, N'Kabupaten Lembata', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:08.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5309, N'Kabupaten Flores Timur', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:08.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5310, N'Kabupaten Sikka', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:08.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5311, N'Kabupaten Ende', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:08.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5312, N'Kabupaten Ngada', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:09.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5313, N'Kabupaten Manggarai', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:09.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5314, N'Kabupaten Rote Ndao', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:09.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5315, N'Kabupaten Manggarai Barat', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:09.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5316, N'Kabupaten Sumba Tengah', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:10.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5317, N'Kabupaten Sumba Barat Daya', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:10.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5318, N'Kabupaten Nagekeo', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:10.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5319, N'Kabupaten Manggarai Timur', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:11.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5320, N'Kabupaten Sabu Raijua', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:11.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5321, N'Kabupaten Malaka', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:11.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5371, N'Kota Kupang', 53, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:11.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6101, N'Kabupaten Sambas', 61, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:12.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6102, N'Kabupaten Bengkayang', 61, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:12.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6103, N'Kabupaten Landak', 61, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:12.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6104, N'Kabupaten Mempawah', 61, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:12.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6105, N'Kabupaten Sanggau', 61, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:12.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6106, N'Kabupaten Ketapang', 61, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:13.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6107, N'Kabupaten Sintang', 61, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:13.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6108, N'Kabupaten Kapuas Hulu', 61, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:13.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6109, N'Kabupaten Sekadau', 61, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:13.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6110, N'Kabupaten Melawi', 61, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:14.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6111, N'Kabupaten Kayong Utara', 61, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:14.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6112, N'Kabupaten Kubu Raya', 61, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:15.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6171, N'Kota Pontianak', 61, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:15.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6172, N'Kota Singkawang', 61, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:15.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6201, N'Kabupaten Kotawaringin Barat', 62, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:15.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6202, N'Kabupaten Kotawaringin Timur', 62, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:15.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6203, N'Kabupaten Kapuas', 62, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:16.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6204, N'Kabupaten Barito Selatan', 62, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:16.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6205, N'Kabupaten Barito Utara', 62, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:16.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6206, N'Kabupaten Sukamara', 62, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:16.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6207, N'Kabupaten Lamandau', 62, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:17.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6208, N'Kabupaten Seruyan', 62, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:17.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6209, N'Kabupaten Katingan', 62, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:17.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6210, N'Kabupaten Pulang Pisau', 62, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:17.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6211, N'Kabupaten Gunung Mas', 62, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:18.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6212, N'Kabupaten Barito Timur', 62, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:18.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6213, N'Kabupaten Murung Raya', 62, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:18.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6271, N'Kota Palangka Raya', 62, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:18.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6301, N'Kabupaten Tanah Laut', 63, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:19.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6302, N'Kabupaten Kota Baru', 63, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:19.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6303, N'Kabupaten Banjar', 63, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:19.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6304, N'Kabupaten Barito Kuala', 63, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:19.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6305, N'Kabupaten Tapin', 63, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:20.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6306, N'Kabupaten Hulu Sungai Selatan', 63, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:20.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6307, N'Kabupaten Hulu Sungai Tengah', 63, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:20.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6308, N'Kabupaten Hulu Sungai Utara', 63, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:21.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6309, N'Kabupaten Tabalong', 63, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:21.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6310, N'Kabupaten Tanah Bumbu', 63, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:21.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6311, N'Kabupaten Balangan', 63, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:21.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6371, N'Kota Banjarmasin', 63, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:22.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6372, N'Kota Banjar Baru', 63, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:22.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6401, N'Kabupaten Paser', 64, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:22.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6402, N'Kabupaten Kutai Barat', 64, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:22.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6403, N'Kabupaten Kutai Kartanegara', 64, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:23.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6404, N'Kabupaten Kutai Timur', 64, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:23.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6405, N'Kabupaten Berau', 64, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:23.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6409, N'Kabupaten Penajam Paser Utara', 64, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:23.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6411, N'Kabupaten Mahakam Hulu', 64, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:24.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6471, N'Kota Balikpapan', 64, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:24.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6472, N'Kota Samarinda', 64, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:24.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6474, N'Kota Bontang', 64, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:24.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6501, N'Kabupaten Malinau', 65, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:25.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6502, N'Kabupaten Bulungan', 65, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:25.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6503, N'Kabupaten Tana Tidung', 65, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:25.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6504, N'Kabupaten Nunukan', 65, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:26.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6571, N'Kota Tarakan', 65, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:26.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7101, N'Kabupaten Bolaang Mongondow', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:26.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7102, N'Kabupaten Minahasa', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:27.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7103, N'Kabupaten Kepulauan Sangihe', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:27.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7104, N'Kabupaten Kepulauan Talaud', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:27.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7105, N'Kabupaten Minahasa Selatan', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:27.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7106, N'Kabupaten Minahasa Utara', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:27.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7107, N'Kabupaten Bolaang Mongondow Utara', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:28.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7108, N'Kabupaten Siau Tagulandang Biaro', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:28.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7109, N'Kabupaten Minahasa Tenggara', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:28.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7110, N'Kabupaten Bolaang Mongondow Selatan', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:28.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7111, N'Kabupaten Bolaang Mongondow Timur', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:29.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7171, N'Kota Manado', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:29.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7172, N'Kota Bitung', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:29.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7173, N'Kota Tomohon', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:29.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7174, N'Kota Kotamobagu', 71, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:30.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7201, N'Kabupaten Banggai Kepulauan', 72, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:30.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7202, N'Kabupaten Banggai', 72, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:30.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7203, N'Kabupaten Morowali', 72, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:31.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7204, N'Kabupaten Poso', 72, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:31.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7205, N'Kabupaten Donggala', 72, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:32.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7206, N'Kabupaten Toli-toli', 72, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:32.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7207, N'Kabupaten Buol', 72, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:32.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7208, N'Kabupaten Parigi Moutong', 72, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:32.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7209, N'Kabupaten Tojo Una-una', 72, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:33.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7210, N'Kabupaten Sigi', 72, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:33.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7211, N'Kabupaten Banggai Laut', 72, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:33.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7212, N'Kabupaten Morowali Utara', 72, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:33.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7271, N'Kota Palu', 72, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:33.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7301, N'Kabupaten Kepulauan Selayar', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:34.000' AS DateTime), NULL)
GO
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7302, N'Kabupaten Bulukumba', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:34.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7303, N'Kabupaten Bantaeng', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:34.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7304, N'Kabupaten Jeneponto', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:34.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7305, N'Kabupaten Takalar', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:34.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7306, N'Kabupaten Gowa', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:35.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7307, N'Kabupaten Sinjai', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:35.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7308, N'Kabupaten Maros', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:35.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7309, N'Kabupaten Pangkajene Dan Kepulauan', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:35.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7310, N'Kabupaten Barru', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:36.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7311, N'Kabupaten Bone', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:36.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7312, N'Kabupaten Soppeng', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:36.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7313, N'Kabupaten Wajo', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:36.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7314, N'Kabupaten Sidenreng Rappang', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:36.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7315, N'Kabupaten Pinrang', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:37.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7316, N'Kabupaten Enrekang', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:37.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7317, N'Kabupaten Luwu', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:37.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7318, N'Kabupaten Tana Toraja', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:38.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7322, N'Kabupaten Luwu Utara', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:38.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7325, N'Kabupaten Luwu Timur', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:38.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7326, N'Kabupaten Toraja Utara', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:38.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7371, N'Kota Makassar', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:39.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7372, N'Kota Parepare', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:39.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7373, N'Kota Palopo', 73, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:39.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7401, N'Kabupaten Buton', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:39.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7402, N'Kabupaten Muna', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:39.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7403, N'Kabupaten Konawe', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:40.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7404, N'Kabupaten Kolaka', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:40.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7405, N'Kabupaten Konawe Selatan', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:40.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7406, N'Kabupaten Bombana', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:41.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7407, N'Kabupaten Wakatobi', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:41.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7408, N'Kabupaten Kolaka Utara', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:41.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7409, N'Kabupaten Buton Utara', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:41.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7410, N'Kabupaten Konawe Utara', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:41.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7411, N'Kabupaten Kolaka Timur', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:42.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7412, N'Kabupaten Konawe Kepulauan', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:42.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7413, N'Kabupaten Muna Barat', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:42.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7414, N'Kabupaten Buton Tengah', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:42.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7415, N'Kabupaten Buton Selatan', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:43.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7471, N'Kota Kendari', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:43.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7472, N'Kota Baubau', 74, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:43.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7501, N'Kabupaten Boalemo', 75, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:44.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7502, N'Kabupaten Gorontalo', 75, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:44.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7503, N'Kabupaten Pohuwato', 75, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:45.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7504, N'Kabupaten Bone Bolango', 75, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:45.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7505, N'Kabupaten Gorontalo Utara', 75, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:45.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7571, N'Kota Gorontalo', 75, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:45.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7601, N'Kabupaten Majene', 76, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:46.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7602, N'Kabupaten Polewali Mandar', 76, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:46.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7603, N'Kabupaten Mamasa', 76, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:46.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7604, N'Kabupaten Mamuju', 76, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:47.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7605, N'Kabupaten Mamuju Utara', 76, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:47.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7606, N'Kabupaten Mamuju Tengah', 76, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:47.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8101, N'Kabupaten Maluku Tenggara Barat', 81, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:47.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8102, N'Kabupaten Maluku Tenggara', 81, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:48.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8103, N'Kabupaten Maluku Tengah', 81, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:48.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8104, N'Kabupaten Buru', 81, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:48.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8105, N'Kabupaten Kepulauan Aru', 81, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:49.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8106, N'Kabupaten Seram Bagian Barat', 81, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:49.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8107, N'Kabupaten Seram Bagian Timur', 81, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:49.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8108, N'Kabupaten Maluku Barat Daya', 81, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:49.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8109, N'Kabupaten Buru Selatan', 81, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:50.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8171, N'Kota Ambon', 81, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:50.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8172, N'Kota Tual', 81, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:50.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8201, N'Kabupaten Halmahera Barat', 82, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:50.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8202, N'Kabupaten Halmahera Tengah', 82, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:51.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8203, N'Kabupaten Kepulauan Sula', 82, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:51.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8204, N'Kabupaten Halmahera Selatan', 82, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:51.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8205, N'Kabupaten Halmahera Utara', 82, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:51.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8206, N'Kabupaten Halmahera Timur', 82, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:52.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8207, N'Kabupaten Pulau Morotai', 82, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:52.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8208, N'Kabupaten Pulau Taliabu', 82, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:52.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8271, N'Kota Ternate', 82, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:52.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (8272, N'Kota Tidore Kepulauan', 82, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:53.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9101, N'Kabupaten Fakfak', 91, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:53.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9102, N'Kabupaten Kaimana', 91, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:53.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9103, N'Kabupaten Teluk Wondama', 91, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:53.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9104, N'Kabupaten Teluk Bintuni', 91, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:54.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9105, N'Kabupaten Manokwari', 91, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:54.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9106, N'Kabupaten Sorong Selatan', 91, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:54.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9107, N'Kabupaten Sorong', 91, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:54.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9108, N'Kabupaten Raja Ampat', 91, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:55.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9109, N'Kabupaten Tambrauw', 91, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:55.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9110, N'Kabupaten Maybrat', 91, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:55.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9111, N'Kabupaten Manokwari Selatan', 91, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:55.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9112, N'Kabupaten Pegunungan Arfak', 91, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:56.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9171, N'Kota Sorong', 91, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:56.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9401, N'Kabupaten Merauke', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:56.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9402, N'Kabupaten Jayawijaya', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:57.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9403, N'Kabupaten Jayapura', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:57.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9404, N'Kabupaten Nabire', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:57.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9408, N'Kabupaten Kepulauan Yapen', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:57.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9409, N'Kabupaten Biak Numfor', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:58.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9410, N'Kabupaten Paniai', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:58.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9411, N'Kabupaten Puncak Jaya', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:58.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9412, N'Kabupaten Mimika', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:58.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9413, N'Kabupaten Boven Digoel', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:59.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9414, N'Kabupaten Mappi', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:59.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9415, N'Kabupaten Asmat', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:59.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9416, N'Kabupaten Yahukimo', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:11:59.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9417, N'Kabupaten Pegunungan Bintang', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:00.000' AS DateTime), NULL)
GO
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9418, N'Kabupaten Tolikara', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:00.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9419, N'Kabupaten Sarmi', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:00.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9420, N'Kabupaten Keerom', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:00.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9426, N'Kabupaten Waropen', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:01.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9427, N'Kabupaten Supiori', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:01.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9428, N'Kabupaten Mamberamo Raya', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:01.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9429, N'Kabupaten Nduga', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:01.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9430, N'Kabupaten Lanny Jaya', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:02.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9431, N'Kabupaten Mamberamo Tengah', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:02.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9432, N'Kabupaten Yalimo', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:02.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9433, N'Kabupaten Puncak', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:03.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9434, N'Kabupaten Dogiyai', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:03.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9435, N'Kabupaten Intan Jaya', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:03.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9436, N'Kabupaten Deiyai', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:03.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9471, N'Kota Jayapura', 94, CAST(N'2017-10-11 16:19:19.000' AS DateTime), 1, CAST(N'2017-10-11 16:12:04.000' AS DateTime), NULL)
INSERT [dbo].[master_city] ([id], [name], [province_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (9478, N'city 3a', 15, CAST(N'2017-10-13 16:14:38.000' AS DateTime), 1, CAST(N'2017-10-13 16:18:48.000' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[master_city] OFF
SET IDENTITY_INSERT [dbo].[master_country] ON 

INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (1, N'Afghanistan', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (2, N'Albania', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (3, N'Algeria', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (4, N'American Samoa', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (5, N'Andorra', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (6, N'Angola', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (7, N'Anguilla', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (8, N'Antarctica', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (9, N'Antigua And Barbuda', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (10, N'Argentina', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (11, N'Armenia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (12, N'Aruba', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (13, N'Australia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (14, N'Austria', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (15, N'Azerbaijan', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (16, N'Bahamas The', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (17, N'Bahrain', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (18, N'Bangladesh', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (19, N'Barbados', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (20, N'Belarus', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (21, N'Belgium', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (22, N'Belize', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (23, N'Benin', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (24, N'Bermuda', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (25, N'Bhutan', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (26, N'Bolivia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (27, N'Bosnia and Herzegovina', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (28, N'Botswana', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (29, N'Bouvet Island', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (30, N'Brazil', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (31, N'British Indian Ocean Territory', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (32, N'Brunei', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (33, N'Bulgaria', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (34, N'Burkina Faso', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (35, N'Burundi', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (36, N'Cambodia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (37, N'Cameroon', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (38, N'Canada', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (39, N'Cape Verde', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (40, N'Cayman Islands', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (41, N'Central African Republic', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (42, N'Chad', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (43, N'Chile', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (44, N'China', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (45, N'Christmas Island', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (46, N'Cocos (Keeling) Islands', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (47, N'Colombia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (48, N'Comoros', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (49, N'Republic Of The Congo', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (50, N'Democratic Republic Of The Congo', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (51, N'Cook Islands', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (52, N'Costa Rica', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (53, N'Cote D''Ivoire (Ivory Coast)', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (54, N'Croatia (Hrvatska)', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (55, N'Cuba', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (56, N'Cyprus', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (57, N'Czech Republic', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (58, N'Denmark', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (59, N'Djibouti', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (60, N'Dominica', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (61, N'Dominican Republic', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (62, N'East Timor', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (63, N'Ecuador', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (64, N'Egypt', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (65, N'El Salvador', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (66, N'Equatorial Guinea', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (67, N'Eritrea', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (68, N'Estonia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (69, N'Ethiopia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (70, N'External Territories of Australia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (71, N'Falkland Islands', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (72, N'Faroe Islands', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (73, N'Fiji Islands', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (74, N'Finland', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (75, N'France', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (76, N'French Guiana', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (77, N'French Polynesia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (78, N'French Southern Territories', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (79, N'Gabon', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (80, N'Gambia The', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (81, N'Georgia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (82, N'Germany', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (83, N'Ghana', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (84, N'Gibraltar', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (85, N'Greece', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (86, N'Greenland', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (87, N'Grenada', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (88, N'Guadeloupe', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (89, N'Guam', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (90, N'Guatemala', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (91, N'Guernsey and Alderney', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (92, N'Guinea', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (93, N'Guinea-Bissau', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (94, N'Guyana', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (95, N'Haiti', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (96, N'Heard and McDonald Islands', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (97, N'Honduras', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (98, N'Hong Kong S.A.R.', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (99, N'Hungary', NULL, NULL, NULL)
GO
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (100, N'Iceland', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (101, N'India', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (102, N'Indonesia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (103, N'Iran', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (104, N'Iraq', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (105, N'Ireland', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (106, N'Israel', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (107, N'Italy', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (108, N'Jamaica', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (109, N'Japan', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (110, N'Jersey', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (111, N'Jordan', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (112, N'Kazakhstan', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (113, N'Kenya', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (114, N'Kiribati', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (115, N'Korea North', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (116, N'Korea South', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (117, N'Kuwait', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (118, N'Kyrgyzstan', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (119, N'Laos', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (120, N'Latvia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (121, N'Lebanon', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (122, N'Lesotho', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (123, N'Liberia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (124, N'Libya', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (125, N'Liechtenstein', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (126, N'Lithuania', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (127, N'Luxembourg', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (128, N'Macau S.A.R.', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (129, N'Macedonia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (130, N'Madagascar', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (131, N'Malawi', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (132, N'Malaysia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (133, N'Maldives', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (134, N'Mali', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (135, N'Malta', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (136, N'Man (Isle of)', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (137, N'Marshall Islands', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (138, N'Martinique', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (139, N'Mauritania', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (140, N'Mauritius', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (141, N'Mayotte', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (142, N'Mexico', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (143, N'Micronesia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (144, N'Moldova', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (145, N'Monaco', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (146, N'Mongolia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (147, N'Montserrat', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (148, N'Morocco', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (149, N'Mozambique', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (150, N'Myanmar', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (151, N'Namibia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (152, N'Nauru', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (153, N'Nepal', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (154, N'Netherlands Antilles', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (155, N'Netherlands The', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (156, N'New Caledonia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (157, N'New Zealand', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (158, N'Nicaragua', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (159, N'Niger', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (160, N'Nigeria', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (161, N'Niue', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (162, N'Norfolk Island', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (163, N'Northern Mariana Islands', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (164, N'Norway', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (165, N'Oman', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (166, N'Pakistan', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (167, N'Palau', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (168, N'Palestinian Territory Occupied', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (169, N'Panama', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (170, N'Papua new Guinea', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (171, N'Paraguay', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (172, N'Peru', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (173, N'Philippines', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (174, N'Pitcairn Island', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (175, N'Poland', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (176, N'Portugal', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (177, N'Puerto Rico', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (178, N'Qatar', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (179, N'Reunion', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (180, N'Romania', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (181, N'Russia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (182, N'Rwanda', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (183, N'Saint Helena', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (184, N'Saint Kitts And Nevis', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (185, N'Saint Lucia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (186, N'Saint Pierre and Miquelon', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (187, N'Saint Vincent And The Grenadines', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (188, N'Samoa', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (189, N'San Marino', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (190, N'Sao Tome and Principe', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (191, N'Saudi Arabia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (192, N'Senegal', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (193, N'Serbia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (194, N'Seychelles', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (195, N'Sierra Leone', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (196, N'Singapore', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (197, N'Slovakia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (198, N'Slovenia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (199, N'Smaller Territories of the UK', NULL, NULL, NULL)
GO
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (200, N'Solomon Islands', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (201, N'Somalia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (202, N'South Africa', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (203, N'South Georgia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (204, N'South Sudan', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (205, N'Spain', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (206, N'Sri Lanka', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (207, N'Sudan', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (208, N'Suriname', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (209, N'Svalbard And Jan Mayen Islands', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (210, N'Swaziland', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (211, N'Sweden', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (212, N'Switzerland', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (213, N'Syria', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (214, N'Taiwan', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (215, N'Tajikistan', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (216, N'Tanzania', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (217, N'Thailand', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (218, N'Togo', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (219, N'Tokelau', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (220, N'Tonga', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (221, N'Trinidad And Tobago', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (222, N'Tunisia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (223, N'Turkey', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (224, N'Turkmenistan', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (225, N'Turks And Caicos Islands', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (226, N'Tuvalu', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (227, N'Uganda', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (228, N'Ukraine', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (229, N'United Arab Emirates', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (230, N'United Kingdom', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (231, N'United States', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (232, N'United States Minor Outlying Islands', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (233, N'Uruguay', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (234, N'Uzbekistan', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (235, N'Vanuatu', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (236, N'Vatican City State (Holy See)', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (237, N'Venezuela', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (238, N'Vietnam', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (239, N'Virgin Islands (British)', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (240, N'Virgin Islands (US)', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (241, N'Wallis And Futuna Islands', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (242, N'Western Sahara', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (243, N'Yemen', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (244, N'Yugoslavia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (245, N'Zambia', NULL, NULL, NULL)
INSERT [dbo].[master_country] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (246, N'Zimbabwe', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[master_country] OFF
SET IDENTITY_INSERT [dbo].[master_day] ON 

INSERT [dbo].[master_day] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1, N'Monday', CAST(N'2017-10-26 12:43:00.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[master_day] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (2, N'Tuesday', CAST(N'2017-10-26 12:43:00.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[master_day] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3, N'Wednesday', CAST(N'2017-10-26 12:43:00.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[master_day] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (4, N'Thursday', CAST(N'2017-10-26 12:43:00.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[master_day] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5, N'Friday', CAST(N'2017-10-26 12:43:00.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[master_day] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6, N'Saturday', CAST(N'2017-10-26 12:43:00.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[master_day] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (7, N'Sunday', CAST(N'2017-10-26 12:43:00.000' AS DateTime), 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[master_day] OFF
SET IDENTITY_INSERT [dbo].[master_identity_type] ON 

INSERT [dbo].[master_identity_type] ([id], [name], [length], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1, N'KTP', NULL, CAST(N'2017-10-19 16:05:01.000' AS DateTime), 1, CAST(N'2017-10-19 16:05:01.0000000' AS DateTime2), NULL)
INSERT [dbo].[master_identity_type] ([id], [name], [length], [created_at], [created_by], [updated_at], [updated_by]) VALUES (2, N'SIM', NULL, CAST(N'2017-10-19 16:05:32.000' AS DateTime), 1, CAST(N'2017-10-19 16:05:32.0000000' AS DateTime2), NULL)
INSERT [dbo].[master_identity_type] ([id], [name], [length], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3, N'Passport', NULL, CAST(N'2017-10-19 16:07:34.000' AS DateTime), 1, CAST(N'2017-10-19 16:07:34.0000000' AS DateTime2), NULL)
INSERT [dbo].[master_identity_type] ([id], [name], [length], [created_at], [created_by], [updated_at], [updated_by]) VALUES (4, N'KITAS', NULL, CAST(N'2017-10-19 16:14:16.000' AS DateTime), 1, CAST(N'2017-10-19 16:14:16.0000000' AS DateTime2), NULL)
INSERT [dbo].[master_identity_type] ([id], [name], [length], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5, N'Others', NULL, CAST(N'2017-10-19 16:14:24.000' AS DateTime), 1, CAST(N'2017-10-19 16:14:40.0000000' AS DateTime2), NULL)
SET IDENTITY_INSERT [dbo].[master_identity_type] OFF
SET IDENTITY_INSERT [dbo].[master_job_type] ON 

INSERT [dbo].[master_job_type] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (18, N'Athlete', 1, CAST(N'2017-10-19 15:46:39.0000000' AS DateTime2), CAST(N'2017-10-19 15:47:52.0000000' AS DateTime2))
INSERT [dbo].[master_job_type] ([id], [name], [created_by], [created_at], [updated_at]) VALUES (19, N'Lawyer', 1, CAST(N'2017-10-22 11:45:07.0000000' AS DateTime2), CAST(N'2017-10-22 11:45:15.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[master_job_type] OFF
SET IDENTITY_INSERT [dbo].[master_menu] ON 

INSERT [dbo].[master_menu] ([id], [sort], [parent_id], [name], [roles], [url], [active_classes], [icon], [created_by], [updated_by], [created_at], [updated_at]) VALUES (1, 1, 0, N'User', N'1', N'user', N'user', N'UserIconLightBrown.png', 1, NULL, CAST(N'2017-10-11 11:12:23.0000000' AS DateTime2), NULL)
INSERT [dbo].[master_menu] ([id], [sort], [parent_id], [name], [roles], [url], [active_classes], [icon], [created_by], [updated_by], [created_at], [updated_at]) VALUES (2, 2, 0, N'Role', N'1', N'role', N'role', N'RoleIconLightBrown.png', 1, NULL, CAST(N'2017-10-11 11:13:28.0000000' AS DateTime2), NULL)
SET IDENTITY_INSERT [dbo].[master_menu] OFF
SET IDENTITY_INSERT [dbo].[master_province] ON 

INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (11, N'Aceh', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (12, N'Sumatera Utara', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (13, N'Sumatera Barat', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:12.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (14, N'Riau', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:13.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (15, N'Jambi', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:13.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (16, N'Sumatera Selatan', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:13.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (17, N'Bengkulu', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:13.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (18, N'Lampung', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:14.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (19, N'Kepulauan Bangka Belitung', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:14.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (21, N'Kepulauan Riau', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:14.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (31, N'DKI Jakarta', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:15.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (32, N'Jawa Barat', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:15.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (33, N'Jawa Tengah', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:16.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (34, N'DI Yogyakarta', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:16.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (35, N'Jawa Timur', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:16.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (36, N'Banten', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:16.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (51, N'Bali', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:17.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (52, N'Nusa Tenggara Barat', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:17.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (53, N'Nusa Tenggara Timur', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:17.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (61, N'Kalimantan Barat', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:18.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (62, N'Kalimantan Tengah', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:18.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (63, N'Kalimantan Selatan', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:18.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (64, N'Kalimantan Timur', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:19.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (65, N'Kalimantan Utara', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:19.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (71, N'Sulawesi Utara', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:19.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (72, N'Sulawesi Tengah', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:19.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (73, N'Sulawesi Selatan', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:19.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (74, N'Sulawesi Tenggara', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:20.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (75, N'Gorontalo', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:20.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (76, N'Sulawesi Barat', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:20.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (81, N'Maluku', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:21.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (82, N'Maluku Utara', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:21.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (91, N'Papua Barat', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:21.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (94, N'Papua', CAST(N'2017-10-11 16:18:45.000' AS DateTime), 1, CAST(N'2017-10-11 16:13:22.000' AS DateTime), NULL)
INSERT [dbo].[master_province] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (114, N'Province 1a', CAST(N'2017-10-13 16:19:04.000' AS DateTime), 1, CAST(N'2017-10-13 16:19:11.000' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[master_province] OFF
SET IDENTITY_INSERT [dbo].[master_religion] ON 

INSERT [dbo].[master_religion] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1, N'Islam', CAST(N'2017-10-26 00:00:00.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[master_religion] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (2, N'Kristen Katholik', CAST(N'2017-10-26 00:00:00.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[master_religion] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3, N'Kristen Protestan', CAST(N'2017-10-26 00:00:00.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[master_religion] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (4, N'Hindu', CAST(N'2017-10-26 00:00:00.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[master_religion] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (5, N'Budha', CAST(N'2017-10-26 00:00:00.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[master_religion] ([id], [name], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6, N'Kong Hu Cu', CAST(N'2017-10-26 00:00:00.000' AS DateTime), 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[master_religion] OFF
SET IDENTITY_INSERT [dbo].[master_visit_code] ON 

INSERT [dbo].[master_visit_code] ([id], [name], [description], [created_by], [created_at], [updated_at]) VALUES (1, N'Programs', NULL, 1, CAST(N'2017-09-12 15:54:24.0000000' AS DateTime2), NULL)
INSERT [dbo].[master_visit_code] ([id], [name], [description], [created_by], [created_at], [updated_at]) VALUES (2, N'Pregnancy', NULL, 1, CAST(N'2017-09-12 15:54:24.0000000' AS DateTime2), NULL)
INSERT [dbo].[master_visit_code] ([id], [name], [description], [created_by], [created_at], [updated_at]) VALUES (3, N'IVF', NULL, 1, CAST(N'2017-09-12 15:54:24.0000000' AS DateTime2), CAST(N'2017-09-12 15:54:24.0000000' AS DateTime2))
INSERT [dbo].[master_visit_code] ([id], [name], [description], [created_by], [created_at], [updated_at]) VALUES (4, N'IUI', NULL, 1, CAST(N'2017-09-12 16:03:41.0000000' AS DateTime2), CAST(N'2017-09-12 16:03:41.0000000' AS DateTime2))
INSERT [dbo].[master_visit_code] ([id], [name], [description], [created_by], [created_at], [updated_at]) VALUES (5, N'TC', NULL, 1, CAST(N'2017-09-12 16:03:54.0000000' AS DateTime2), CAST(N'2017-09-12 16:03:54.0000000' AS DateTime2))
INSERT [dbo].[master_visit_code] ([id], [name], [description], [created_by], [created_at], [updated_at]) VALUES (6, N'OI', NULL, 1, CAST(N'2017-09-12 16:04:09.0000000' AS DateTime2), CAST(N'2017-09-12 16:04:09.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[master_visit_code] OFF
SET IDENTITY_INSERT [dbo].[patient] ON 

INSERT [dbo].[patient] ([id], [hospital_id], [holding_id], [company_id], [designation], [first_name], [last_name], [sex], [birthdate], [birthplace], [profile_image_url], [initial_doctor_id], [email], [height], [weight], [blood_type], [date_of_marriage], [address], [city_id], [home_phone_number], [office_phone_number], [mobile_phone_number], [nationality], [identity_type_id], [identity_number], [religion_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (4, NULL, NULL, 1400, NULL, N'perdana', N'imer', N'L', CAST(N'1988-01-01' AS Date), N'lampung', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, 1101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[patient] ([id], [hospital_id], [holding_id], [company_id], [designation], [first_name], [last_name], [sex], [birthdate], [birthplace], [profile_image_url], [initial_doctor_id], [email], [height], [weight], [blood_type], [date_of_marriage], [address], [city_id], [home_phone_number], [office_phone_number], [mobile_phone_number], [nationality], [identity_type_id], [identity_number], [religion_id], [created_at], [created_by], [updated_at], [updated_by]) VALUES (6, NULL, NULL, 1400, NULL, N'imam', N'nura', N'L', CAST(N'1990-01-01' AS Date), N'bekasi', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, 1112, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[patient] OFF
SET IDENTITY_INSERT [dbo].[patient_emergency] ON 

INSERT [dbo].[patient_emergency] ([id], [patient_id], [name], [relationship], [address], [city_id], [phone_number], [email], [created_at], [created_by], [updated_at], [updated_by]) VALUES (1, 5, N'Hendy', N'Family', N'dsadsa', 13, N'089898900', N'prdn@gmail.com', CAST(N'2017-10-06 00:00:00.000' AS DateTime), 7, NULL, NULL)
INSERT [dbo].[patient_emergency] ([id], [patient_id], [name], [relationship], [address], [city_id], [phone_number], [email], [created_at], [created_by], [updated_at], [updated_by]) VALUES (2, 1, N'inimim', N'ddd', N'ddd', 13, N'123', N'dsad@gmail.com', CAST(N'2017-10-10 11:26:41.000' AS DateTime), 13, CAST(N'2017-10-20 08:48:18.000' AS DateTime), NULL)
INSERT [dbo].[patient_emergency] ([id], [patient_id], [name], [relationship], [address], [city_id], [phone_number], [email], [created_at], [created_by], [updated_at], [updated_by]) VALUES (3, 2, N'dfsads', N'dsadsa', N'dsadsa', 13, N'2132132', N'dsadsa@gmail.com', CAST(N'2017-10-10 11:32:26.000' AS DateTime), 13, CAST(N'2017-10-10 11:32:26.000' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[patient_emergency] OFF
SET IDENTITY_INSERT [dbo].[patient_old] ON 

INSERT [dbo].[patient_old] ([id], [company_id], [first_name], [last_name], [init_doctor], [id_number], [id_type], [birth_place], [birth_date], [sex], [body_height], [blood], [marital_status], [married_date], [religion], [nationality], [address], [city], [postal_code], [phone], [mobile], [mobile_2], [office_name], [office_phone], [job_type], [email], [foto], [active], [created_by], [created_at], [updated_at]) VALUES (1, 1400, N'Perdana', N'Imer', 1, N'123123119', N'KTP', N'Jakarta', CAST(N'1990-01-31' AS Date), N'Male', 172, N'O', N'Married', CAST(N'2017-10-18' AS Date), N'Islam', N'Indonesian', N'Jakarta', N'Jakarta Selatan', 123123, N'1231231', N'321321', N'532432', N'Juke', N'21432432', N'dsadsad', N'prdn@prdn.com', N'1507624439_4.jpg', 1, 13, CAST(N'2017-08-14 07:47:16.000' AS DateTime), CAST(N'2017-10-20 08:48:18.000' AS DateTime))
INSERT [dbo].[patient_old] ([id], [company_id], [first_name], [last_name], [init_doctor], [id_number], [id_type], [birth_place], [birth_date], [sex], [body_height], [blood], [marital_status], [married_date], [religion], [nationality], [address], [city], [postal_code], [phone], [mobile], [mobile_2], [office_name], [office_phone], [job_type], [email], [foto], [active], [created_by], [created_at], [updated_at]) VALUES (2, 1400, N'Rona', N'Setyana', 1, N'1231231192', N'KTP', N'Bogor', CAST(N'1990-01-31' AS Date), N'Male', 0, N'o', N'Married', CAST(N'2017-10-21' AS Date), N'Islam', N'Indonesian', N'asdqweqwe', N'Jakarta Selatan', 0, N'12312', N'3213', N'213213', N'Juke', N'0', N'dsadsadsa', N'rona@rona.com', N'1507624709_6.jpg', 1, 13, CAST(N'2017-08-14 14:23:35.000' AS DateTime), CAST(N'2017-10-10 15:38:29.000' AS DateTime))
INSERT [dbo].[patient_old] ([id], [company_id], [first_name], [last_name], [init_doctor], [id_number], [id_type], [birth_place], [birth_date], [sex], [body_height], [blood], [marital_status], [married_date], [religion], [nationality], [address], [city], [postal_code], [phone], [mobile], [mobile_2], [office_name], [office_phone], [job_type], [email], [foto], [active], [created_by], [created_at], [updated_at]) VALUES (5, 1400, N'Dodi', N'Abc', 1, N'1231231', N'KTP', N'Jakarta', CAST(N'1980-11-12' AS Date), N'Male', 156, N'O', N'Married', NULL, N'Islam', N'Indonesian', N'Jakarta', N'Jakarta Barat', 123231, N'123123123', N'', N'', N'Juke', N'11111222', N'', N'Dodi@gmail.com', NULL, 1, 1, CAST(N'2017-09-11 16:32:29.000' AS DateTime), CAST(N'2017-09-11 16:32:29.000' AS DateTime))
INSERT [dbo].[patient_old] ([id], [company_id], [first_name], [last_name], [init_doctor], [id_number], [id_type], [birth_place], [birth_date], [sex], [body_height], [blood], [marital_status], [married_date], [religion], [nationality], [address], [city], [postal_code], [phone], [mobile], [mobile_2], [office_name], [office_phone], [job_type], [email], [foto], [active], [created_by], [created_at], [updated_at]) VALUES (6, 1400, N'Irvan', N'Jaya', 1, N'12312312', N'KTP', N'Jakarta', CAST(N'1980-11-13' AS Date), N'Male', 156, N'O', N'Married', NULL, N'Islam', N'Indonesian', N'Jakarta', N'Jakarta Utara', 123231, N'123123123', N'', N'', N'Juke', N'11111222', N'', N'Irvan@gmail.com', NULL, 1, 2, CAST(N'2017-09-11 16:37:02.000' AS DateTime), CAST(N'2017-09-11 16:37:02.000' AS DateTime))
INSERT [dbo].[patient_old] ([id], [company_id], [first_name], [last_name], [init_doctor], [id_number], [id_type], [birth_place], [birth_date], [sex], [body_height], [blood], [marital_status], [married_date], [religion], [nationality], [address], [city], [postal_code], [phone], [mobile], [mobile_2], [office_name], [office_phone], [job_type], [email], [foto], [active], [created_by], [created_at], [updated_at]) VALUES (7, 1400, N'Adrian', N'Riza', 1, N'123123123', N'KTP', N'Jakarta', CAST(N'1980-11-14' AS Date), N'Male', 156, N'O', N'Married', NULL, N'Islam', N'Indonesian', N'Jakarta', N'Jakarta Selatan', 123231, N'123123123', N'', N'', N'Juke', N'11111222', N'', N'adrian@gmail.com', NULL, 1, 2, CAST(N'2017-09-11 16:42:20.000' AS DateTime), CAST(N'2017-09-11 16:42:20.000' AS DateTime))
INSERT [dbo].[patient_old] ([id], [company_id], [first_name], [last_name], [init_doctor], [id_number], [id_type], [birth_place], [birth_date], [sex], [body_height], [blood], [marital_status], [married_date], [religion], [nationality], [address], [city], [postal_code], [phone], [mobile], [mobile_2], [office_name], [office_phone], [job_type], [email], [foto], [active], [created_by], [created_at], [updated_at]) VALUES (8, 1400, N'Lani', N'Amalia', 1, N'1231231232', N'KTP', N'Jakarta', CAST(N'1980-11-06' AS Date), N'Female', 156, N'O', N'Married', NULL, N'Islam', N'Indonesian', N'Jakarta', N'Jakarta Selatan', 123231, N'123123123', N'', N'', N'Juke', N'11111222', N'', N'Lani@gmail.com', NULL, 1, 2, CAST(N'2017-09-12 09:39:46.000' AS DateTime), CAST(N'2017-09-12 09:39:46.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[patient_old] OFF
SET IDENTITY_INSERT [dbo].[reservation] ON 

INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (1, CAST(N'2017-09-12' AS Date), CAST(N'08:00:00' AS Time), NULL, 3, 1, N'IVN', N'', 0, 1, CAST(N'2017-09-11 14:21:18.0000000' AS DateTime2), CAST(N'2017-09-11 14:21:18.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (2, CAST(N'2017-09-13' AS Date), CAST(N'08:00:00' AS Time), 1, 0, 1, N'IVN', N'', 0, 1, CAST(N'2017-09-11 14:37:33.0000000' AS DateTime2), CAST(N'2017-09-11 14:37:33.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (3, CAST(N'2017-09-12' AS Date), CAST(N'09:30:00' AS Time), 6, NULL, 1, N'IVN', N'', 0, 1, CAST(N'2017-09-11 14:38:04.0000000' AS DateTime2), CAST(N'2017-09-11 14:38:04.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (4, CAST(N'2017-09-12' AS Date), CAST(N'11:00:00' AS Time), 5, NULL, 1, N'IVN', N'', 0, 1, CAST(N'2017-09-11 14:38:55.0000000' AS DateTime2), CAST(N'2017-09-11 14:38:55.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (5, CAST(N'2017-09-13' AS Date), CAST(N'08:00:00' AS Time), 1, 0, 1, N'IVN', N'', 0, 1, CAST(N'2017-09-11 15:12:56.0000000' AS DateTime2), CAST(N'2017-09-11 15:12:56.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (6, CAST(N'2017-09-14' AS Date), CAST(N'08:00:00' AS Time), 1, 0, 1, N'IVN', N'', 0, 1, CAST(N'2017-09-11 15:18:29.0000000' AS DateTime2), CAST(N'2017-09-11 15:18:29.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (7, CAST(N'2017-09-15' AS Date), CAST(N'08:00:00' AS Time), 1, 0, 1, N'IVN', N'', 0, 1, CAST(N'2017-09-11 15:24:48.0000000' AS DateTime2), CAST(N'2017-09-11 15:24:48.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (8, CAST(N'2017-09-16' AS Date), CAST(N'08:00:00' AS Time), 1, 0, 1, N'IVN', N'', 0, 1, CAST(N'2017-09-11 16:11:32.0000000' AS DateTime2), CAST(N'2017-09-11 16:11:32.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (9, CAST(N'2017-09-17' AS Date), CAST(N'08:00:00' AS Time), 1, 0, 1, N'IVN', N'', 0, 2, CAST(N'2017-09-11 16:46:24.0000000' AS DateTime2), CAST(N'2017-09-11 16:46:24.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (10, CAST(N'2017-09-18' AS Date), CAST(N'08:00:00' AS Time), 1, 0, 1, N'IVN', N'', 0, 2, CAST(N'2017-09-12 09:39:26.0000000' AS DateTime2), CAST(N'2017-09-12 09:39:26.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (19, CAST(N'2017-09-12' AS Date), CAST(N'10:30:00' AS Time), 1, NULL, 1, N'IVN', NULL, 0, 2, CAST(N'2017-09-18 14:32:44.0000000' AS DateTime2), CAST(N'2017-09-18 14:32:44.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (20, CAST(N'2017-09-12' AS Date), CAST(N'11:30:00' AS Time), NULL, 4, 1, N'IVN', NULL, 0, 2, CAST(N'2017-09-18 14:55:27.0000000' AS DateTime2), CAST(N'2017-09-18 14:55:27.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (21, CAST(N'2017-09-12' AS Date), CAST(N'12:00:00' AS Time), 1, NULL, 1, N'IVN', NULL, 0, 2, CAST(N'2017-09-18 14:57:54.0000000' AS DateTime2), CAST(N'2017-09-18 14:57:54.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (22, CAST(N'2017-09-12' AS Date), CAST(N'10:00:00' AS Time), NULL, 5, 1, N'IVN', NULL, 0, 2, CAST(N'2017-09-18 14:59:57.0000000' AS DateTime2), CAST(N'2017-09-18 14:59:57.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (23, CAST(N'2017-09-12' AS Date), CAST(N'09:00:00' AS Time), NULL, 6, 1, NULL, N'dasdsadsa', 0, 2, CAST(N'2017-09-18 15:59:15.0000000' AS DateTime2), CAST(N'2017-09-18 15:59:15.0000000' AS DateTime2))
INSERT [dbo].[reservation] ([id], [reservation_date], [session], [morula_id], [temp_id], [visit_code], [doctor_code], [description], [attendance], [created_by], [created_at], [updated_at]) VALUES (24, CAST(N'2017-09-12' AS Date), CAST(N'12:30:00' AS Time), NULL, 7, 1, NULL, N'asdsadsa', 0, 2, CAST(N'2017-09-18 16:16:43.0000000' AS DateTime2), CAST(N'2017-09-18 16:16:43.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[reservation] OFF
SET IDENTITY_INSERT [dbo].[roles] ON 

INSERT [dbo].[roles] ([id], [name], [url], [description], [created_by], [updated_by], [created_at], [updated_at]) VALUES (1, N'Super Admin', N'admin', N'super admin', 1, 1, CAST(N'2017-09-19 14:35:58.000' AS DateTime), CAST(N'2017-09-26 15:37:49.000' AS DateTime))
INSERT [dbo].[roles] ([id], [name], [url], [description], [created_by], [updated_by], [created_at], [updated_at]) VALUES (2, N'Admin API', N'api', N'admin for access api', 1, 1, CAST(N'2017-09-19 14:37:25.000' AS DateTime), CAST(N'2017-09-19 14:37:25.000' AS DateTime))
INSERT [dbo].[roles] ([id], [name], [url], [description], [created_by], [updated_by], [created_at], [updated_at]) VALUES (3, N'Admin', N'admin', N'admin standard access app', 1, 1, CAST(N'2017-09-19 14:37:25.000' AS DateTime), CAST(N'2017-09-19 14:37:25.000' AS DateTime))
INSERT [dbo].[roles] ([id], [name], [url], [description], [created_by], [updated_by], [created_at], [updated_at]) VALUES (4, N'Admission', N'admission', N'admission for registration patient', 1, 1, CAST(N'2017-09-19 14:37:25.000' AS DateTime), CAST(N'2017-09-19 14:37:25.000' AS DateTime))
INSERT [dbo].[roles] ([id], [name], [url], [description], [created_by], [updated_by], [created_at], [updated_at]) VALUES (5, N'Nurse', N'nurse', N'nurse / doctor assistance', 1, NULL, CAST(N'2017-09-26 15:28:00.000' AS DateTime), CAST(N'2017-09-26 15:38:08.000' AS DateTime))
INSERT [dbo].[roles] ([id], [name], [url], [description], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Cashier', N'cashier', N'All floor', 1, NULL, CAST(N'2017-09-26 15:29:01.000' AS DateTime), CAST(N'2017-09-26 15:38:49.000' AS DateTime))
INSERT [dbo].[roles] ([id], [name], [url], [description], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Doctor', N'doctor', NULL, 1, NULL, CAST(N'2017-09-26 15:29:19.000' AS DateTime), CAST(N'2017-09-26 15:29:19.000' AS DateTime))
INSERT [dbo].[roles] ([id], [name], [url], [description], [created_by], [updated_by], [created_at], [updated_at]) VALUES (8, N'Laboratory', N'lab', NULL, 1, NULL, CAST(N'2017-09-26 15:29:39.000' AS DateTime), CAST(N'2017-09-26 15:29:39.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[roles] OFF
SET IDENTITY_INSERT [dbo].[user_role] ON 

INSERT [dbo].[user_role] ([id], [user_id], [role_id], [created_by], [updated_by], [created_at], [updated_at]) VALUES (1, 1, 3, 1, 1, CAST(N'2017-09-29 13:24:07.0000000' AS DateTime2), CAST(N'2017-09-29 13:24:07.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[user_role] OFF
SET IDENTITY_INSERT [dbo].[users] ON 

INSERT [dbo].[users] ([id], [username], [name], [email], [password], [phone], [description], [roles], [remember_token], [api_token], [photo], [created_at], [updated_at]) VALUES (1, N'admin', N'Administrator', N'admin@morulaivf.co.id', N'$2y$10$fCJ1wGnVi27aoaK.wFTCUe5f5lz6QJ5dUOauTqR1xdAmQ2P3LNFfu', N'11111', N'Admin User', N'1', N'00aJmyHgSljB4zy6Q9G4ALPPGBmzb4m8Lc8BojbdEvCGiae86vG7OVA768Ea', N'', N'1507880374_4.jpg', CAST(N'2017-09-06 03:32:19.000' AS DateTime), CAST(N'2017-10-13 07:39:34.000' AS DateTime))
INSERT [dbo].[users] ([id], [username], [name], [email], [password], [phone], [description], [roles], [remember_token], [api_token], [photo], [created_at], [updated_at]) VALUES (2, N'prdn', N'Imer Perdana', N'imer@jukesolutions.com', N'$2y$10$2oG9pxhgDXt2Xjm36y.OHe.ECzdJ1AfOHoD1R1ho2rC/tScDJV2hW', N'081297622704', N'Software Engineer', N'8,7,6,5,4,3,2,1', NULL, N'uEzFSH1CTVPj0mf2VLRmmtGVvNWUQwBR0u5QykJzFskllHEsEr7Z7dU806YcvyD23k2DgH5WLTYyU86TkepxeQRtY5gSINSuTuXX', NULL, CAST(N'2017-09-11 08:56:37.000' AS DateTime), CAST(N'2017-10-12 08:32:07.000' AS DateTime))
INSERT [dbo].[users] ([id], [username], [name], [email], [password], [phone], [description], [roles], [remember_token], [api_token], [photo], [created_at], [updated_at]) VALUES (12, N'dodistyo', N'Dodi', N'dodi@jukesolutions.com', N'$2y$10$Qu8Hn.BwqwpoUi8BKLDKteaTe9dY804WTn99BZrU7HJTi4VAh3Gb.', N'123123123', N'Programmer', N'4,3,1', NULL, NULL, NULL, CAST(N'2017-09-26 07:10:10.000' AS DateTime), CAST(N'2017-10-10 03:56:32.000' AS DateTime))
INSERT [dbo].[users] ([id], [username], [name], [email], [password], [phone], [description], [roles], [remember_token], [api_token], [photo], [created_at], [updated_at]) VALUES (13, N'admission', N'Admission', N'admission@morulaivf.co.id', N'$2y$10$NqMEdzuGPmf7IOzxX66/HOFL1wHxc8Y0niufov4FyuK6eFr6vsQHq', N'123123123', N'Admission User', N'4', N'JupwSkmFPH55CTDfcykd3WiuOYNhrIQRKKg1iMI2IvheSBHFGlA9CERNlZxP', NULL, N'1507880432_6.jpg', CAST(N'2017-09-28 09:06:23.000' AS DateTime), CAST(N'2017-10-13 07:40:32.000' AS DateTime))
INSERT [dbo].[users] ([id], [username], [name], [email], [password], [phone], [description], [roles], [remember_token], [api_token], [photo], [created_at], [updated_at]) VALUES (14, N'nurse', N'Nurse User', N'nurse@morulaivf.co.id', N'$2y$10$8SjjqNoAf10F12xHb9XsVOhkARkese0wVF9cxyTy8tpY.C57CdQyS', N'988988', NULL, N'5', NULL, NULL, NULL, CAST(N'2017-10-12 08:33:26.000' AS DateTime), CAST(N'2017-10-12 08:33:26.000' AS DateTime))
INSERT [dbo].[users] ([id], [username], [name], [email], [password], [phone], [description], [roles], [remember_token], [api_token], [photo], [created_at], [updated_at]) VALUES (15, N'doctor', N'Doctor User', N'dsad@dsads.com', N'$2y$10$49X6xAs0YZD.XQ2Ny3o5J.gksIqvXrFlolkMVJIcN8PY7dPctxGpq', N'21233333', NULL, N'7', NULL, NULL, NULL, CAST(N'2017-10-12 08:56:23.000' AS DateTime), CAST(N'2017-10-12 08:56:23.000' AS DateTime))
INSERT [dbo].[users] ([id], [username], [name], [email], [password], [phone], [description], [roles], [remember_token], [api_token], [photo], [created_at], [updated_at]) VALUES (16, N'jodig', N'jodi gunawan', N'jodi@gmail.com', N'$2y$10$cccfv/YYyZ5by2UollvyY.ccdyFe.0E5S4jj0QKMHwjXlmYI9X1J2', N'03123213', N'adsd
asdsad
asdd', N'4', NULL, NULL, NULL, CAST(N'2017-10-13 04:29:30.000' AS DateTime), CAST(N'2017-10-13 04:29:30.000' AS DateTime))
INSERT [dbo].[users] ([id], [username], [name], [email], [password], [phone], [description], [roles], [remember_token], [api_token], [photo], [created_at], [updated_at]) VALUES (17, N'admins', N'dsa', N'dsad@kklkcom.com', N'$2y$10$EcCpqPLQTx6tOclbbCEmc.IaHOaL35AojJq/oa9kBX87Idq/aOFR.', N'009008', N'test', N'3', NULL, NULL, NULL, CAST(N'2017-10-19 16:51:21.000' AS DateTime), CAST(N'2017-10-19 16:56:21.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[users] OFF
/****** Object:  Index [clinic_registration_id]    Script Date: 31/10/2017 10.58.38 ******/
CREATE UNIQUE NONCLUSTERED INDEX [clinic_registration_id] ON [dbo].[clinic_registration]
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [master_city$u_propinsi_id_nama]    Script Date: 31/10/2017 10.58.38 ******/
ALTER TABLE [dbo].[master_city] ADD  CONSTRAINT [master_city$u_propinsi_id_nama] UNIQUE NONCLUSTERED 
(
	[province_id] ASC,
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [master_country$name]    Script Date: 31/10/2017 10.58.38 ******/
ALTER TABLE [dbo].[master_country] ADD  CONSTRAINT [master_country$name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [master_id_type$name]    Script Date: 31/10/2017 10.58.38 ******/
ALTER TABLE [dbo].[master_identity_type] ADD  CONSTRAINT [master_id_type$name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [master_job_type$name]    Script Date: 31/10/2017 10.58.38 ******/
ALTER TABLE [dbo].[master_job_type] ADD  CONSTRAINT [master_job_type$name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [master_province$nama]    Script Date: 31/10/2017 10.58.38 ******/
ALTER TABLE [dbo].[master_province] ADD  CONSTRAINT [master_province$nama] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [patient_id]    Script Date: 31/10/2017 10.58.38 ******/
CREATE UNIQUE NONCLUSTERED INDEX [patient_id] ON [dbo].[patient]
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [master_patient$id_number]    Script Date: 31/10/2017 10.58.38 ******/
ALTER TABLE [dbo].[patient_old] ADD  CONSTRAINT [master_patient$id_number] UNIQUE NONCLUSTERED 
(
	[id_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [patients_init_dokter_foreign]    Script Date: 31/10/2017 10.58.38 ******/
CREATE NONCLUSTERED INDEX [patients_init_dokter_foreign] ON [dbo].[patient_old]
(
	[init_doctor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [patients_kewarganegaraan_foreign]    Script Date: 31/10/2017 10.58.38 ******/
CREATE NONCLUSTERED INDEX [patients_kewarganegaraan_foreign] ON [dbo].[patient_old]
(
	[nationality] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [patients_perusahaan_foreign]    Script Date: 31/10/2017 10.58.38 ******/
CREATE NONCLUSTERED INDEX [patients_perusahaan_foreign] ON [dbo].[patient_old]
(
	[office_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [no_mr_induk]    Script Date: 31/10/2017 10.58.38 ******/
CREATE NONCLUSTERED INDEX [no_mr_induk] ON [dbo].[patient_relation]
(
	[no_mr_induk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [no_mr_pasangan]    Script Date: 31/10/2017 10.58.38 ******/
CREATE NONCLUSTERED INDEX [no_mr_pasangan] ON [dbo].[patient_relation]
(
	[no_mr_pasangan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [master_patient_temp$id_number]    Script Date: 31/10/2017 10.58.38 ******/
ALTER TABLE [dbo].[patient_temporary] ADD  CONSTRAINT [master_patient_temp$id_number] UNIQUE NONCLUSTERED 
(
	[identity_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [patients_init_dokter_foreign]    Script Date: 31/10/2017 10.58.38 ******/
CREATE NONCLUSTERED INDEX [patients_init_dokter_foreign] ON [dbo].[patient_temporary]
(
	[initial_doctor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [users$email]    Script Date: 31/10/2017 10.58.38 ******/
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [users$email] UNIQUE NONCLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [users$username]    Script Date: 31/10/2017 10.58.38 ******/
ALTER TABLE [dbo].[users] ADD  CONSTRAINT [users$username] UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[clinic_registration] ADD  CONSTRAINT [DF_clinic_registration_patient_is_attend]  DEFAULT ((0)) FOR [patient_is_attend]
GO
ALTER TABLE [dbo].[master_room] ADD  DEFAULT (NULL) FOR [updated_at]
GO
ALTER TABLE [dbo].[patient_relation] ADD  DEFAULT (NULL) FOR [updated_at]
GO
ALTER TABLE [dbo].[patient_temporary] ADD  CONSTRAINT [DF__master_pa__compa__5BE2A6F2]  DEFAULT (NULL) FOR [company_id]
GO
ALTER TABLE [dbo].[patient_temporary] ADD  CONSTRAINT [DF__master_pa__init___5EBF139D]  DEFAULT ((1)) FOR [initial_doctor_id]
GO
ALTER TABLE [dbo].[patient_temporary] ADD  CONSTRAINT [DF__master_pa__updat__60A75C0F]  DEFAULT (NULL) FOR [updated_at]
GO
ALTER TABLE [dbo].[clinic_family_link]  WITH CHECK ADD  CONSTRAINT [FKclinic_fam131626] FOREIGN KEY([partner_id])
REFERENCES [dbo].[patient] ([id])
GO
ALTER TABLE [dbo].[clinic_family_link] CHECK CONSTRAINT [FKclinic_fam131626]
GO
ALTER TABLE [dbo].[clinic_family_link]  WITH CHECK ADD  CONSTRAINT [FKclinic_fam594120] FOREIGN KEY([patient_id])
REFERENCES [dbo].[patient] ([id])
GO
ALTER TABLE [dbo].[clinic_family_link] CHECK CONSTRAINT [FKclinic_fam594120]
GO
ALTER TABLE [dbo].[clinic_registration]  WITH CHECK ADD  CONSTRAINT [FKclinic_reg267427] FOREIGN KEY([doctor_id])
REFERENCES [dbo].[doctor] ([id])
GO
ALTER TABLE [dbo].[clinic_registration] CHECK CONSTRAINT [FKclinic_reg267427]
GO
ALTER TABLE [dbo].[clinic_registration]  WITH CHECK ADD  CONSTRAINT [FKclinic_reg384024] FOREIGN KEY([patient_id])
REFERENCES [dbo].[patient] ([id])
GO
ALTER TABLE [dbo].[clinic_registration] CHECK CONSTRAINT [FKclinic_reg384024]
GO
ALTER TABLE [dbo].[clinic_registration]  WITH CHECK ADD  CONSTRAINT [FKclinic_reg78435] FOREIGN KEY([replacement_doctor_id])
REFERENCES [dbo].[doctor] ([id])
GO
ALTER TABLE [dbo].[clinic_registration] CHECK CONSTRAINT [FKclinic_reg78435]
GO
ALTER TABLE [dbo].[clinic_registration_info]  WITH CHECK ADD  CONSTRAINT [FKclinic_reg190948] FOREIGN KEY([doctor_id])
REFERENCES [dbo].[doctor] ([id])
GO
ALTER TABLE [dbo].[clinic_registration_info] CHECK CONSTRAINT [FKclinic_reg190948]
GO
ALTER TABLE [dbo].[doctor_schedule]  WITH CHECK ADD  CONSTRAINT [FKdoctor_sch614551] FOREIGN KEY([day_id])
REFERENCES [dbo].[master_day] ([id])
GO
ALTER TABLE [dbo].[doctor_schedule] CHECK CONSTRAINT [FKdoctor_sch614551]
GO
ALTER TABLE [dbo].[doctor_schedule]  WITH CHECK ADD  CONSTRAINT [FKdoctor_sch92314] FOREIGN KEY([doctor_id])
REFERENCES [dbo].[doctor] ([id])
GO
ALTER TABLE [dbo].[doctor_schedule] CHECK CONSTRAINT [FKdoctor_sch92314]
GO
ALTER TABLE [dbo].[doctor_specialization]  WITH CHECK ADD  CONSTRAINT [FKmaster_spe131508] FOREIGN KEY([master_account_expense])
REFERENCES [dbo].[gl_coa_master] ([id])
GO
ALTER TABLE [dbo].[doctor_specialization] CHECK CONSTRAINT [FKmaster_spe131508]
GO
ALTER TABLE [dbo].[doctor_specialization]  WITH CHECK ADD  CONSTRAINT [FKmaster_spe243373] FOREIGN KEY([detail_account_expense])
REFERENCES [dbo].[gl_coa_detail] ([id])
GO
ALTER TABLE [dbo].[doctor_specialization] CHECK CONSTRAINT [FKmaster_spe243373]
GO
ALTER TABLE [dbo].[doctor_specialization]  WITH CHECK ADD  CONSTRAINT [FKmaster_spe411493] FOREIGN KEY([detail_account_payable])
REFERENCES [dbo].[gl_coa_detail] ([id])
GO
ALTER TABLE [dbo].[doctor_specialization] CHECK CONSTRAINT [FKmaster_spe411493]
GO
ALTER TABLE [dbo].[doctor_specialization]  WITH CHECK ADD  CONSTRAINT [FKmaster_spe448232] FOREIGN KEY([master_account_payable])
REFERENCES [dbo].[gl_coa_master] ([id])
GO
ALTER TABLE [dbo].[doctor_specialization] CHECK CONSTRAINT [FKmaster_spe448232]
GO
ALTER TABLE [dbo].[doctor_specialization]  WITH CHECK ADD  CONSTRAINT [FKmaster_spe551982] FOREIGN KEY([master_account])
REFERENCES [dbo].[gl_coa_master] ([id])
GO
ALTER TABLE [dbo].[doctor_specialization] CHECK CONSTRAINT [FKmaster_spe551982]
GO
ALTER TABLE [dbo].[master_city]  WITH CHECK ADD  CONSTRAINT [FKmaster_cit778400] FOREIGN KEY([province_id])
REFERENCES [dbo].[master_province] ([id])
GO
ALTER TABLE [dbo].[master_city] CHECK CONSTRAINT [FKmaster_cit778400]
GO
ALTER TABLE [dbo].[patient]  WITH CHECK ADD  CONSTRAINT [FKpatient480025] FOREIGN KEY([identity_type_id])
REFERENCES [dbo].[master_identity_type] ([id])
GO
ALTER TABLE [dbo].[patient] CHECK CONSTRAINT [FKpatient480025]
GO
ALTER TABLE [dbo].[patient]  WITH CHECK ADD  CONSTRAINT [FKpatient656117] FOREIGN KEY([city_id])
REFERENCES [dbo].[master_city] ([id])
GO
ALTER TABLE [dbo].[patient] CHECK CONSTRAINT [FKpatient656117]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.master_doctor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'doctor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'morula.t_doctor_request_late' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'doctor_late'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'morula.t_doctor_leave' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'doctor_leave'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'morula.t_doctor_request_session_leave' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'doctor_leave_session'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.master_doctor_schedule' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'doctor_schedule'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.master_city' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'master_city'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'morula.master_country' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'master_country'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'morula.master_id_type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'master_identity_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'morula.master_job_type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'master_job_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.master_menu' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'master_menu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.master_province' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'master_province'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.master_room' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'master_room'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.master_visit_code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'master_visit_code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.migrations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'migrations'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.master_patient_emergency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'patient_emergency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.master_patient' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'patient_old'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.master_patient_relation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'patient_relation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.master_patient_temp' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'patient_temporary'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.t_reservation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'reservation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.roles' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'roles'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.user_role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user_role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'moruladev.users' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users'
GO
USE [master]
GO
ALTER DATABASE [morula] SET  READ_WRITE 
GO
